# $Id: Makefile.kmk $
## @file
# Top-level makefile for the VirtualBox Guest Additions.
#

#
# Copyright (C) 2006-2007 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

SUB_DEPTH = ../../..
include	$(KBUILD_PATH)/subheader.kmk

#
# Globals
#
VBOX_PATH_ADDITIONS_SRC := $(PATH_SUB_CURRENT)

#
# Cross building of the additions is generally done by remote building
# by means of smbfs, cifs, VBOX_ONLY_ADDITIONS=1 and setting KBUILD_TARGET
# and KBUILD_TARGET_ARCH to the desired target and architecture.
#
# Limited support for cross building the windows additions using wine
# is provided. There are a couple of issues with the approach (lack of
# signing, no VC++ 8 support, ++) that makes it unsuitable for releases.
#
#
# Note! VBOX_WITH_ADDITIONS is checked for by our parent makefile.
#
# Note! VBOX_WITH_X11_ADDITIONS is set in Config.kmk
#
# Note! The additions build box will set the VBOX_WITH_ADDITIONS_ISO.win.x86
#       variables before invoking us from the root makefile.
#
#  ==>  All we have to worry about is what to do on the target we're on.
#
VBOX_WITH_ADDITIONS_ISO.$(KBUILD_TARGET).$(KBUILD_TARGET_ARCH) = 1

# Include sub-makefiles.
include $(PATH_SUB_CURRENT)/common/Makefile.kmk

ifdef VBOX_WITH_X11_ADDITIONS
 include $(PATH_SUB_CURRENT)/x11/Makefile.kmk
endif

ifeq ($(KBUILD_TARGET),freebsd)
 include $(PATH_SUB_CURRENT)/freebsd/Makefile.kmk
endif
ifeq ($(KBUILD_TARGET),linux)
 include $(PATH_SUB_CURRENT)/linux/Makefile.kmk
endif
#ifeq ($(KBUILD_TARGET),os2)
# include $(PATH_SUB_CURRENT)/os2/Makefile.kmk
#endif
ifeq ($(KBUILD_TARGET),solaris)
 include $(PATH_SUB_CURRENT)/solaris/Makefile.kmk
endif
ifeq ($(KBUILD_TARGET),win)
 include $(PATH_SUB_CURRENT)/WINNT/Makefile.kmk
endif

# The packing target rule, but only if we're on the local build box.
# (VBOX_WITHOUT_ADDITIONS_ISO is used by the additions build box, see the root makefile.)
ifndef VBOX_WITHOUT_ADDITIONS_ISO
 PACKING += $(PATH_BIN)/additions/VBoxGuestAdditions.iso
endif

include	$(KBUILD_PATH)/subfooter.kmk

#
# File per-OS/arch file specs for the additions iso (alphabetical order).
#
# We test for the VBOX_WITH_ADDITIONS_ISO.os.arch so that we don't have to
# do the $(if )'ing down where the GUESTADDITIONS_FILESPEC.os.arch down
# in the dependency list and mkisofs command.
#

ifdef VBOX_WITH_ADDITIONS_ISO.freebsd.amd64
 VBOX_PATH_ADDITIONS.freebsd.amd64 = $(PATH_OUT_BASE)/freebsd.amd64/$(KBUILD_TYPE)/bin/additions
 GUESTADDITIONS_FILESPEC.freebsd.amd64 = \
 	VBoxFreeBSDAdditions-amd64.tbz=$(VBOX_PATH_ADDITIONS.freebsd.amd64)/VBoxFreeBSDAdditions.tbz
endif
ifdef VBOX_WITH_ADDITIONS_ISO.freebsd.x86
 VBOX_PATH_ADDITIONS.freebsd.x86 = $(PATH_OUT_BASE)/freebsd.x86/$(KBUILD_TYPE)/bin/additions
 GUESTADDITIONS_FILESPEC.freebsd.x86 = \
 	VBoxFreeBSDAdditions-x86.tbz=$(VBOX_PATH_ADDITIONS.freebsd.x86)/VBoxFreeBSDAdditions.tbz
endif


ifdef VBOX_WITH_ADDITIONS_ISO.os2.x86
 VBOX_PATH_ADDITIONS.os2.x86 = $(PATH_OUT_BASE)/os2.x86/$(KBUILD_TYPE)/bin/additions
 GUESTADDITIONS_FILESPEC.os2.x86 = \
	32Bit/OS2/VBoxGuest.sys=$(VBOX_PATH_ADDITIONS.os2.x86)/VBoxGuest.sys \
	32Bit/OS2/VBoxService.exe=$(VBOX_PATH_ADDITIONS.os2.x86)/VBoxService.exe \
	32Bit/OS2/gengradd.dll=$(VBOX_PATH_ADDITIONS.os2.x86)/gengradd.dll \
	32Bit/OS2/libc063.dll=$(VBOX_PATH_ADDITIONS_SRC)/os2/Bin/libc063.dll \
	32Bit/OS2/readme.txt=$(VBOX_PATH_ADDITIONS_SRC)/os2/Bin/readme.txt \
	32Bit/OS2/vboxmouse.sys=$(VBOX_PATH_ADDITIONS.os2.x86)/vboxmouse.sys
else ifdef VBOX_WITH_OS2_ADDITIONS_BIN
 GUESTADDITIONS_FILESPEC.os2.x86 = \
	32Bit/OS2/VBoxGuest.sys=$(VBOX_PATH_ADDITIONS_SRC)/os2/Bin/VBoxGuest.sys \
	32Bit/OS2/VBoxService.exe=$(VBOX_PATH_ADDITIONS_SRC)/os2/Bin/VBoxService.exe \
	32Bit/OS2/gengradd.dll=$(VBOX_PATH_ADDITIONS_SRC)/os2/Bin/gengradd.dll \
	32Bit/OS2/libc063.dll=$(VBOX_PATH_ADDITIONS_SRC)/os2/Bin/libc063.dll \
	32Bit/OS2/readme.txt=$(VBOX_PATH_ADDITIONS_SRC)/os2/Bin/readme.txt \
	32Bit/OS2/vboxmouse.sys=$(VBOX_PATH_ADDITIONS_SRC)/os2/Bin/vboxmouse.sys
endif


ifdef VBOX_WITH_ADDITIONS_ISO.linux.amd64
 VBOX_PATH_ADDITIONS.linux.amd64 = $(PATH_OUT_BASE)/linux.amd64/$(KBUILD_TYPE)/bin/additions
 GUESTADDITIONS_FILESPEC.linux.amd64 = \
 	VBoxLinuxAdditions-amd64.run=$(VBOX_PATH_ADDITIONS.linux.amd64)/VBoxLinuxAdditions.run
endif
ifdef VBOX_WITH_ADDITIONS_ISO.linux.x86
 VBOX_PATH_ADDITIONS.linux.x86 = $(PATH_OUT_BASE)/linux.x86/$(KBUILD_TYPE)/bin/additions
## @todo 64-bit additions: rename this package, update docs (?) and tests (?). create wrapper? create gnome/kde autorun app (xplatform) ?
 GUESTADDITIONS_FILESPEC.linux.x86 = \
 	VBoxLinuxAdditions-x86.run=$(VBOX_PATH_ADDITIONS.linux.x86)/VBoxLinuxAdditions.run \
	autorun.sh=$(VBOX_PATH_ADDITIONS.linux.x86)/autorun.sh
endif

ifdef VBOX_WITH_ADDITIONS_ISO.solaris.amd64
 VBOX_PATH_ADDITIONS.solaris.amd64 = $(PATH_OUT_BASE)/solaris.amd64/$(KBUILD_TYPE)/bin/additions
 GUESTADDITIONS_FILESPEC.solaris.amd64 = \
 	VBoxSolarisAdditions-amd64.pkg=$(VBOX_PATH_ADDITIONS.solaris.amd64)/VBoxSolarisAdditions.pkg
endif
ifdef VBOX_WITH_ADDITIONS_ISO.solaris.x86
 VBOX_PATH_ADDITIONS.solaris.x86 = $(PATH_OUT_BASE)/solaris.x86/$(KBUILD_TYPE)/bin/additions
 GUESTADDITIONS_FILESPEC.solaris.x86 = \
 	VBoxSolarisAdditions-x86.pkg=$(VBOX_PATH_ADDITIONS.solaris.x86)/VBoxSolarisAdditions.pkg
endif
ifdef VBOX_WITH_COMBINED_SOLARIS_GUEST_PACKAGE
 # Build combined 32bit and 64bit solaris additions, not just a single arch.
 # This assumes that the 32bit build directory contains the combined additions
 # for 32bit and 64bit solaris. This just modifies variables set above.
 GUESTADDITIONS_FILESPEC.solaris.x86 = \
 	VBoxSolarisAdditions.pkg=$(VBOX_PATH_ADDITIONS.solaris.x86)/VBoxSolarisAdditions.pkg
 GUESTADDITIONS_FILESPEC.solaris.amd64 =
endif

ifdef VBOX_WITH_ADDITIONS_ISO.win.amd64
 VBOX_PATH_ADDITIONS.win.amd64 = $(PATH_OUT_BASE)/win.amd64/$(KBUILD_TYPE)/bin/additions
 GUESTADDITIONS_FILESPEC.win.amd64 = \
	VBoxWindowsAdditions-amd64.exe=$(VBOX_PATH_ADDITIONS.win.amd64)/VBoxWindowsAdditions-amd64.exe
endif

ifdef VBOX_WITH_ADDITIONS_ISO.win.x86
 VBOX_PATH_ADDITIONS.win.x86 = $(PATH_OUT_BASE)/win.x86/$(KBUILD_TYPE)/bin/additions
 GUESTADDITIONS_FILESPEC.win.x86 = \
	VBoxWindowsAdditions-x86.exe=$(VBOX_PATH_ADDITIONS.win.x86)/VBoxWindowsAdditions-x86.exe \
	VBoxWindowsAdditions.exe=$(VBOX_PATH_ADDITIONS.win.x86)/VBoxWindowsAdditions.exe \
	AUTORUN.INF=$(VBOX_PATH_ADDITIONS_SRC)/WINNT/Installer/ISO/AUTORUN.INF \
	32Bit/Readme.txt=$(VBOX_PATH_ADDITIONS_SRC)/WINNT/Installer/ISO/ReadmeDrivers.txt \
	64Bit/Readme.txt=$(VBOX_PATH_ADDITIONS_SRC)/WINNT/Installer/ISO/ReadmeDrivers.txt
endif # win.x86

#
# Build the Guest Additions ISO image.
#
ifndef VBOX_WITHOUT_ADDITIONS_ISO
$(VBOX_PATH_ADDITIONS_ISO)/VBoxGuestAdditions.iso: \
		$(filter-out %=deleteme=,\
			$(subst =,=deleteme= ,\
				$(GUESTADDITIONS_FILESPEC.win.x86) \
				$(GUESTADDITIONS_FILESPEC.win.amd64) \
				$(GUESTADDITIONS_FILESPEC.solaris.x86) \
				$(GUESTADDITIONS_FILESPEC.solaris.amd64) \
				$(GUESTADDITIONS_FILESPEC.os2.x86) \
				$(GUESTADDITIONS_FILESPEC.linux.x86) \
				$(GUESTADDITIONS_FILESPEC.linux.amd64) \
				$(GUESTADDITIONS_FILESPEC.freebsd.x86) \
				$(GUESTADDITIONS_FILESPEC.freebsd.amd64) \
			)\
		) \
		$(VBOX_SVN_REV_KMK) \
		$(VBOX_PATH_ADDITIONS_SRC)/Makefile.kmk
	$(call MSG_TOOL,mkisofs,,$@)
	$(QUIET)$(MKDIR) -p $(@D)
	@# use iso-level 3 which is the most ISO conforming level with least restrictions; iso-level 4 maps to iso-level 2
	@# with some extra restrictions removal (not conforming to ISO9660) which some platforms like Solaris 10 does not like.
	$(VBOX_MKISOFS) -rational-rock -joliet -iso-level 3 \
		-volid "VBOXADDITIONS_$(VBOX_VERSION_STRING)_$(VBOX_SVN_REV)" -l -graft-points -o $@ \
		$(GUESTADDITIONS_FILESPEC.win) \
		$(GUESTADDITIONS_FILESPEC.win.x86) \
		$(GUESTADDITIONS_FILESPEC.win.amd64) \
		$(GUESTADDITIONS_FILESPEC.solaris.x86) \
		$(GUESTADDITIONS_FILESPEC.solaris.amd64) \
		$(GUESTADDITIONS_FILESPEC.os2.x86) \
		$(GUESTADDITIONS_FILESPEC.linux.x86) \
		$(GUESTADDITIONS_FILESPEC.linux.amd64) \
		$(GUESTADDITIONS_FILESPEC.freebsd.x86) \
		$(GUESTADDITIONS_FILESPEC.freebsd.amd64)

# Alias for creating the iso.
.PHONY: additions-iso
additions-iso: $(VBOX_PATH_ADDITIONS_ISO)/VBoxGuestAdditions.iso

endif

