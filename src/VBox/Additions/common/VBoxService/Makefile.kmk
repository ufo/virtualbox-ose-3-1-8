# $Id: Makefile.kmk $
## @file
# Sub-Makefile for the Cross Platform Guest Addition Services.
#

#
# Copyright (C) 2007 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

SUB_DEPTH = ../../../../..
include	$(KBUILD_PATH)/subheader.kmk

#
# Target lists.
#
PROGRAMS += VBoxService
PROGRAMS.win.x86 += VBoxServiceNT

#
# VBoxService
#
VBoxService_TEMPLATE      = VBOXGUESTR3EXE
VBoxService_DEFS          = VBOXSERVICE_TIMESYNC
VBoxService_DEFS.win     += _WIN32_WINNT=0x0501
VBoxService_DEFS.os2      = VBOX_WITH_HGCM VBOXSERVICE_CLIPBOARD
ifdef VBOX_WITH_GUEST_PROPS
 VBoxService_DEFS        += VBOX_WITH_GUEST_PROPS VBOXSERVICE_VMINFO
 if1of ($(KBUILD_TARGET), win)
  VBoxService_DEFS       += VBOX_WITH_HGCM VBOXSERVICE_EXEC
 endif
endif
VBoxService_SOURCES       = \
	VBoxService.cpp \
	VBoxServiceTimeSync.cpp \
	VBoxServiceUtils.cpp
ifdef VBOX_WITH_GUEST_PROPS
 VBoxService_SOURCES.win  = \
 	VBoxServiceVMInfo-win.cpp
 VBoxService_SOURCES     += \
 	VBoxServiceVMInfo.cpp
 if1of ($(KBUILD_TARGET), win)
  VBoxService_SOURCES    += \
	VBoxServiceExec.cpp
 endif
endif
VBoxService_SOURCES.win  += \
	VBoxService-win.rc \
	VBoxService-win.cpp
VBoxService_SOURCES.os2   = \
	VBoxService-os2.def \
	VBoxServiceClipboard-os2.cpp
VBoxService_LIBS          = \
	$(VBOX_LIB_IPRT_GUEST_R3) \
	$(VBOX_LIB_VBGL_R3) \
	$(VBOX_LIB_IPRT_GUEST_R3)
ifdef VBOX_WITH_GUEST_PROPS
 VBoxService_LIBS.win    += \
	Secur32.lib \
	WtsApi32.lib \
	Psapi.lib
 VBoxService_LIBS.solaris += \
	nsl
endif

#
# VBoxServiceNT - NT4 version of VBoxService.
#
VBoxServiceNT_TEMPLATE    = VBOXGUESTR3EXE
VBoxServiceNT_EXTENDS     = VBoxService
VBoxServiceNT_DEFS.win    = _WIN32_WINNT=0x0400 TARGET_NT4

#VBoxServiceVMInfo.cpp uses VBOX_SVN_REV.
VBoxServiceVMInfo.cpp_DEFS = VBOX_SVN_REV=$(VBOX_SVN_REV)
VBoxServiceVMInfo.cpp_DEPS = $(VBOX_SVN_REV_KMK)

#
# The icon is configurable.
#
VBoxService-win.rc_INCS = $(PATH_VBoxService)
VBoxService-win.rc_DEPS = $(PATH_VBoxService)/VBoxService-win-icon.rc
VBoxService-win.rc_CLEAN = $(PATH_VBoxService)/VBoxService-win-icon.rc

# Icon include file.
$$(PATH_VBoxService)/VBoxService-win-icon.rc: $(VBOX_WINDOWS_ADDITIONS_ICON_FILE) $$(VBoxService_PATH)/Makefile.kmk | $$(dir $$@)
	$(RM) -f $@
	$(APPEND) $@ 'IDI_VIRTUALBOX ICON DISCARDABLE "$(subst /,\\,$(VBOX_WINDOWS_ADDITIONS_ICON_FILE))"'

include	$(KBUILD_PATH)/subfooter.kmk

