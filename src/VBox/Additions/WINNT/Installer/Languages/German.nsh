
LangString VBOX_TEST ${LANG_GERMAN}                     "Das ist eine Test-Nachricht von $(^Name)!"

LangString VBOX_NOADMIN ${LANG_GERMAN}                  "Sie ben�tigen Administrations-Rechte zum (De-)Installieren der $(^Name)!$\r$\nDas Setup wird nun beendet."

LangString VBOX_NOTICE_ARCH_X86 ${LANG_GERMAN}          "Diese Applikation l�uft nur auf 32-bit Windows-Systemen. Bitte installieren Sie die 64-bit Version der $(^Name)!"
LangString VBOX_NOTICE_ARCH_AMD64 ${LANG_GERMAN}        "Diese Applikation l�uft nur auf 64-bit Windows-Systemen. Bitte installieren Sie die 32-bit Version der $(^Name)!"
LangString VBOX_NT4_NO_SP6 ${LANG_GERMAN}               "Es ist kein Service Pack 6 f�r NT 4.0 installiert.$\r$\nEs wird empfohlen das Service-Pack vor dieser Installation zu installieren. Trotzdem jetzt ohne Service-Pack installieren?"

LangString VBOX_PLATFORM_UNSUPPORTED ${LANG_GERMAN}     "Diese Plattform wird noch nicht durch diese Guest Additions unterst�tzt!"

LangString VBOX_INNOTEK_FOUND ${LANG_GERMAN}            "Eine veraltete Version der Guest Additions ist auf diesem System bereits installiert. Diese muss erst deinstalliert werden bevor aktuelle Guest Additions installiert werden k�nnen.$\r$\n$\r$\nJetzt die alten Guest Additions deinstallieren?"
LangString VBOX_INNOTEK_ABORTED ${LANG_GERMAN}          "Die Installation der Guest Additions kann nicht fortgesetzt werden.$\r$\nBitte deinstallieren Sie erst die alten Guest Additions!"
LangString VBOX_INNOTEK_REBOOT ${LANG_GERMAN}           "Es wird dringend empfohlen das System neu zu starten bevor die neuen Guest Additions installiert werden.$\r$\nBitte starten Sie die Installation nach dem Neustart erneut.$\r$\n$\r$\nJetzt neu starten?"

LangString VBOX_COMPONENT_MAIN ${LANG_GERMAN}                       "VirtualBox Guest Additions"
LangString VBOX_COMPONENT_MAIN_DESC ${LANG_GERMAN}                  "Hauptkomponenten der VirtualBox Guest Additions"
LangString VBOX_COMPONENT_AUTOLOGON ${LANG_GERMAN}                  "Unterst�tzung f�r automatisches Anmelden"
LangString VBOX_COMPONENT_AUTOLOGON_DESC ${LANG_GERMAN}             "Erm�glicht automatisches Anmelden von Benutzern"
LangString VBOX_COMPONENT_AUTOLOGON_WARN_3RDPARTY ${LANG_GERMAN}    "Es ist bereits eine Komponente f�r das automatische Anmelden installiert.$\r$\nFalls Sie diese Komponente nun mit der von VirtualBox ersetzen, k�nnte das System instabil werden.$\r$\nDennoch installieren?"
LangString VBOX_COMPONENT_D3D  ${LANG_GERMAN}                       "Direct3D-Unterst�tzung (Experimentell)"
LangString VBOX_COMPONENT_D3D_DESC  ${LANG_GERMAN}                  "Erm�glicht Direct3D-Unterst�tzung f�r G�ste (Experimentell)"
LangString VBOX_COMPONENT_D3D_NO_SM ${LANG_GERMAN}                  "Windows befindet sich aktuell nicht im abgesicherten Modus.$\r$\nDaher kann die D3D-Unterst�tzung nicht installiert werden."

LangString VBOX_WFP_WARN_REPLACE ${LANG_GERMAN}         "Das Setup hat gerade Systemdateien ersetzt um die ${PRODUCT_NAME} korrekt installieren zu k�nnen.$\r$\nFalls nun ein Warn-Dialog des Windows-Dateischutzes erscheint, diesen bitte abbrechen und die Dateien nicht wiederherstellen lassen!"
LangString VBOX_REBOOT_REQUIRED ${LANG_GERMAN}          "Um alle �nderungen durchf�hren zu k�nnen, muss das System neu gestartet werden. Jetzt neu starten?"

LangString VBOX_EXTRACTION_COMPLETE ${LANG_GERMAN}      "$(^Name): Die Dateien wurden erfolgreich nach $\"$INSTDIR$\" entpackt!"

LangString VBOX_ERROR_INST_FAILED ${LANG_GERMAN}        "Es trat ein Fehler w�hrend der Installation auf!$\r$\nBitte werfen Sie einen Blick in die Log-Datei unter '$INSTDIR\install_ui.log' f�r mehr Informationen."

LangString VBOX_UNINST_CONFIRM ${LANG_GERMAN}           "Wollen Sie wirklich die $(^Name) deinstallieren?"
LangString VBOX_UNINST_SUCCESS ${LANG_GERMAN}           "$(^Name) wurden erfolgreich deinstalliert."
