# $Id: Makefile.kmk $
## @file
# Top-level makefile for the VMM.
#

#
# Copyright (C) 2006-2010 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

SUB_DEPTH = ../../..
include	$(KBUILD_PATH)/subheader.kmk

# Include sub-makefiles.
include $(PATH_SUB_CURRENT)/testcase/Makefile.kmk


#
# VMMR3.dll
#
LIBRARIES += VMMR3

VMMR3_TEMPLATE  = VBOXR3

VMMR3_DEFS      = IN_VMM_R3 IN_DIS IN_GMM_R3 IN_DBG
## @todo eliminate IN_GMM_R3
ifdef VBOX_WITH_PREALLOC_RAM_BY_DEFAULT
 VMMR3_DEFS    += VBOX_WITH_PREALLOC_RAM_BY_DEFAULT
endif
ifdef VBOX_WITH_R0_LOGGING
 VMMR3_DEFS    += VBOX_WITH_R0_LOGGING
endif
ifdef VBOX_WITH_MULTI_CORE
 VMMR3_DEFS    += VBOX_WITH_MULTI_CORE
endif
ifdef VBOX_WITH_USB
 VMMR3_DEFS    += VBOX_WITH_USB
endif
ifdef VBOX_WITH_VMI
 VMMR3_DEFS    += \
 	VBOX_WITH_VMI
endif
ifdef VBOX_WITH_PDM_ASYNC_COMPLETION
 VMMR3_DEFS    += VBOX_WITH_PDM_ASYNC_COMPLETION
endif
ifdef VBOX_WITH_LIVE_MIGRATION
 VMMR3_DEFS    += VBOX_WITH_LIVE_MIGRATION
endif
ifdef VBOX_WITH_RAW_MODE
 VMMR3_DEFS    += VBOX_WITH_RAW_MODE
endif
VMMR3_DEFS.darwin = VMM_R0_SWITCH_STACK
VMMR3_DEFS.darwin.x86 = \
	VBOX_WITH_2X_4GB_ADDR_SPACE   VBOX_WITH_2X_4GB_ADDR_SPACE_IN_R3 \
	VBOX_WITH_HYBRID_32BIT_KERNEL VBOX_WITH_HYBRID_32BIT_KERNEL_IN_R3

VMMR3_INCS     = \
	. \
	PATM
ifdef VBOX_WITH_VMI
 VMMR3_INCS   += \
 	PARAV
endif

VMMR3_SOURCES   = \
	CFGM.cpp \
	CPUM.cpp \
	DBGF.cpp \
	DBGFAddr.cpp \
	DBGFAddrSpace.cpp \
	DBGFBp.cpp \
	DBGFCpu.cpp \
	DBGFDisas.cpp \
	DBGFInfo.cpp \
	DBGFLog.cpp \
	DBGFMem.cpp \
	DBGFOS.cpp \
	DBGFStack.cpp \
	DBGFSym.cpp \
	EM.cpp \
	EMRaw.cpp \
	EMHwaccm.cpp \
	IOM.cpp \
	GMM.cpp \
	MM.cpp \
	MMHeap.cpp \
	MMHyper.cpp \
	MMPagePool.cpp \
	MMUkHeap.cpp \
	PDM.cpp \
	PDMDevice.cpp \
	PDMDevHlp.cpp \
	PDMDevMiscHlp.cpp \
	PDMDriver.cpp \
	PDMLdr.cpp \
	PDMCritSect.cpp \
	PDMQueue.cpp \
	PDMThread.cpp \
	PGM.cpp \
	PGMDbg.cpp \
	PGMHandler.cpp \
	PGMMap.cpp \
	PGMPhys.cpp \
	PGMPool.cpp \
	PGMSavedState.cpp \
	SELM.cpp \
	SSM.cpp \
	STAM.cpp \
	TM.cpp \
	TRPM.cpp \
	VM.cpp \
	VMEmt.cpp \
	VMReq.cpp \
	VMM.cpp \
	VMMGuruMeditation.cpp \
	VMMSwitcher.cpp \
	VMMTests.cpp \
	HWACCM.cpp \
	VMMAll/CPUMAllRegs.cpp \
	VMMAll/CPUMStack.cpp \
	VMMAll/DBGFAll.cpp \
	VMMAll/HWACCMAll.cpp \
	VMMAll/IOMAll.cpp \
	VMMAll/IOMAllMMIO.cpp \
	VMMAll/MMAll.cpp \
	VMMAll/MMAllHyper.cpp \
	VMMAll/MMAllPagePool.cpp \
	VMMAll/PDMAll.cpp \
	VMMAll/PDMAllCritSect.cpp \
	VMMAll/PDMAllQueue.cpp \
	VMMAll/PGMAll.cpp \
	VMMAll/PGMAllHandler.cpp \
	VMMAll/PGMAllMap.cpp \
	VMMAll/PGMAllPhys.cpp \
	VMMAll/PGMAllPool.cpp \
	VMMAll/REMAll.cpp \
	VMMAll/SELMAll.cpp \
	VMMAll/EMAll.cpp \
	VMMAll/EMAllA.asm \
	VMMAll/TMAll.cpp \
	VMMAll/TMAllCpu.cpp \
	VMMAll/TMAllReal.cpp \
	VMMAll/TMAllVirtual.cpp \
	VMMAll/TRPMAll.cpp \
	VMMAll/VMAll.cpp \
	VMMAll/VMMAll.cpp \
	VMMAll/VMMAllA.asm \
	PATM/CSAM.cpp \
	PATM/VMMAll/CSAMAll.cpp \
	PATM/PATM.cpp \
	PATM/PATMPatch.cpp \
	PATM/PATMGuest.cpp \
	PATM/PATMA.asm \
	PATM/PATMSSM.cpp \
	PATM/VMMAll/PATMAll.cpp
ifdef VBOX_WITH_VMI
 VMMR3_SOURCES += \
 	PARAV/PARAV.cpp \
 	PARAV/PARAVAll.cpp
endif
ifdef VBOX_WITH_USB
 VMMR3_SOURCES += PDMUsb.cpp
endif
ifdef VBOX_WITH_PDM_ASYNC_COMPLETION
 VMMR3_SOURCES += \
 	PDMAsyncCompletion.cpp \
 	PDMAsyncCompletionFile.cpp \
 	PDMAsyncCompletionFileFailsafe.cpp \
 	PDMAsyncCompletionFileNormal.cpp \
 	PDMAsyncCompletionFileCache.cpp
endif

ifdef VBOX_WITH_RAW_MODE
 VMMR3_SOURCES.x86 += \
 	VMMSwitcher/32BitTo32Bit.asm \
 	VMMSwitcher/32BitToPAE.asm \
 	VMMSwitcher/32BitToAMD64.asm \
 	VMMSwitcher/PAETo32Bit.asm \
 	VMMSwitcher/PAEToAMD64.asm \
 	VMMSwitcher/PAEToPAE.asm
 VMMR3_SOURCES.amd64 = \
 	VMMSwitcher/AMD64To32Bit.asm \
 	VMMSwitcher/AMD64ToPAE.asm
 VMMR3_SOURCES.darwin.x86 += \
 	VMMSwitcher/AMD64ToPAE.asm
endif


# SSM wish to know the build type, host os and arch.
SSM.cpp_DEFS +=	\
	KBUILD_TYPE=\"$(KBUILD_TYPE)\" \
	KBUILD_TARGET=\"$(KBUILD_TARGET)\" \
	KBUILD_TARGET_ARCH=\"$(KBUILD_TARGET_ARCH)\"

#
# The VMM DLL.
#
DLLS += VBoxVMM
VBoxVMM_TEMPLATE = VBOXR3
VBoxVMM_DEFS = $(VMMR3_DEFS)
VBoxVMM_DEFS.$(KBUILD_TARGET) = $(VMMR3_DEFS.$(KBUILD_TARGET))
VBoxVMM_SOURCES = VBoxVMMDeps.cpp
VBoxVMM_SONAME.linux = VBoxVMM.so

VBoxVMM_LIBS = \
	$(PATH_LIB)/VMMR3$(VBOX_SUFF_LIB) \
	$(PATH_LIB)/DisasmR3$(VBOX_SUFF_LIB)
ifdef VBOX_WITH_DEBUGGER
 VBoxVMM_LIBS += \
 	$(PATH_LIB)/Debugger$(VBOX_SUFF_LIB)
endif
VBoxVMM_LIBS += \
	$(LIB_REM) \
	$(LIB_RUNTIME)

VBoxVMM_LIBS.win = $(PATH_TOOL_$(VBOX_VCC_TOOL)_LIB)/delayimp.lib
VBoxVMM_LDFLAGS.win = /DELAYLOAD:dbghelp.dll
VBoxVMM_LDFLAGS.linux = -Wl,--no-undefined
VBoxVMM_LDFLAGS.darwin = -install_name $(VBOX_DYLD_EXECUTABLE_PATH)/VBoxVMM.dylib
VBoxVMM_LDFLAGS.solaris = -mimpure-text
#ifdef VBOX_WITH_PDM_ASYNC_COMPLETION
# ifeq ($(KBUILD_HOST), linux)
#VBoxVMM_LIBS += aio
# endif
#endif



#
# SSMStandalone.lib/a for linking with VBoxSVC and other executables.
#
LIBRARIES += SSMStandalone
SSMStandalone_TEMPLATE  = VBOXR3EXE
SSMStandalone_DEFS      = IN_VMM_R3 IN_VMM_STATIC SSM_STANDALONE
SSMStandalone_SOURCES   = SSM.cpp


if defined(VBOX_WITH_RAW_MODE) && $(intersects $(VBOX_LDR_FMT32), pe lx)

 #
 # VMMGCBuiltin.lib
 #
 LIBRARIES += VMMGCBuiltin
 VMMGCBuiltin_TEMPLATE = VBOXGC
 ifeq ($(VBOX_LDR_FMT32),pe)
  VMMGCBuiltin_SOURCES = VMMGC/VMMGCBuiltin.def
 endif
 ifeq ($(VBOX_LDR_FMT32),lx)
  VMMGCBuiltin_SOURCES = $(PATH_VMMGCBuiltin)/VMMGCBuiltin.def
  $$(PATH_VMMGCBuiltin)/VMMGCBuiltin.def: $(PATH_SUB_CURRENT)/VMMGC/VMMGCBuiltin.def | $$(dir $$@)
	$(SED) -e '/not-os2/d' -e 's/^[ \t][ \t]*\([a-zA-Z]\)/    _\1/' -e 's/[ \t]DATA[ \t]*/ /' --output $@ $<
 endif


 #
 # VMMGCImp.lib
 #
 LIBRARIES += VMMGCImp
 VMMGCImp_TEMPLATE = VBOXGC
 VMMGCImp_SOURCES = $(PATH_VMMGCImp)/VMMGC.def
 VMMGCImp_CLEAN   = $(PATH_VMMGCImp)/VMMGC.def

 $$(PATH_VMMGCImp)/VMMGC.def: $(PATH_SUB_CURRENT)/VMMGC/VMMGC.def | $$(dir $$@)
 ifeq ($(VBOX_LDR_FMT32),lx)
	$(SED) \
		-e '/not-os2/d' \
		-e 's/^[ \t][ \t]*\([a-zA-Z]\)/    _\1/' \
		-e 's/[ \t]DATA[ \t]*/ /' \
		--output $@ \
		$<
	$(APPEND) "$@" ""
	$(APPEND) "$@" "    ___ehInit"
 else
	$(SED) \
		-e '/not-win/d' \
		-e '/not-$(KBUILD_TARGET_ARCH)/d' \
		--output $@ $<
 endif

endif # RC && (pe || lx)


if1of ($(VBOX_LDR_FMT), pe lx)
 #
 # VMMR0Imp.lib
 #
 LIBRARIES += VMMR0Imp
 VMMR0Imp_TEMPLATE = VBOXR0
 VMMR0Imp_SOURCES  = $(PATH_VMMR0Imp)/VMMR0.def
 VMMR0Imp_CLEAN    = $(PATH_VMMR0Imp)/VMMR0.def

 $$(PATH_VMMR0Imp)/VMMR0.def: $(PATH_SUB_CURRENT)/VMMR0/VMMR0.def | $$(dir $$@)
 ifeq ($(VBOX_LDR_FMT),lx)
	$(SED) \
		-e '/not-os2/d' \
		-e '/not-amd64/d' \
		-e 's/^[ \t][ \t]*\([a-zA-Z]\)/    _\1/' \
		-e 's/[ \t]DATA[ \t]*/ /'  \
		--output $@ $<
	$(APPEND) "$@" ""
	$(APPEND) "$@" "    ___ehInit"
 else
	$(SED) \
		-e '/not-win/d' \
		-e '/not-$(KBUILD_TARGET_ARCH)/d' \
		--output $@ $<
 endif
endif # R0: pe + lx


ifdef VBOX_WITH_RAW_MODE
 #
 # VMMGC.gc
 #
 SYSMODS += VMMGC
 VMMGC_TEMPLATE  = VBOXGC
 VMMGC_SYSSUFF   = .gc

 VMMGC_DEFS      = IN_VMM_RC IN_RT_GC IN_RT_RC IN_DIS DIS_CORE_ONLY VBOX_WITH_RAW_MODE
 ifdef VBOX_WITH_R0_LOGGING
  VMMGC_DEFS    += VBOX_WITH_R0_LOGGING
 endif
 ifdef VBOX_WITH_MULTI_CORE
  VMMGC_DEFS    += VBOX_WITH_MULTI_CORE
 endif
 ifdef VBOX_WITH_VMI
  VMMGC_DEFS    += VBOX_WITH_VMI
 endif
 ifeq ($(KBUILD_TARGET_ARCH),x86)
  VMMGC_DEFS.darwin = VMM_R0_SWITCH_STACK
  VMMGC_DEFS.darwin = \
  	VBOX_WITH_2X_4GB_ADDR_SPACE   VBOX_WITH_2X_4GB_ADDR_SPACE_IN_RC \
  	VBOX_WITH_HYBRID_32BIT_KERNEL VBOX_WITH_HYBRID_32BIT_KERNEL_IN_RC
 endif

 VMMGC_INCS     := \
 	. \
 	VMMGC \
 	PATM

 VMMGC_LIBS      = \
 	$(PATH_LIB)/DisasmGC$(VBOX_SUFF_LIB) \
 	$(PATH_LIB)/RuntimeGC$(VBOX_SUFF_LIB)
 ifneq ($(filter pe lx,$(VBOX_LDR_FMT32)),)
  VMMGC_LIBS    += \
 	$(PATH_LIB)/VMMGCBuiltin$(VBOX_SUFF_LIB)
 endif

 ifeq ($(VBOX_LDR_FMT32),pe)
  VMMGC_LDFLAGS  = -Entry:VMMGCEntry
 endif

 VMMGC_SOURCES   = \
 	VMMGC/VMMGC0.asm \
 	VMMGC/VMMGCDeps.cpp \
 	VMMGC/CPUMGC.cpp \
 	VMMGC/CPUMGCA.asm \
 	VMMGC/EMGCA.asm \
 	VMMGC/IOMGC.cpp \
 	VMMGC/MMRamGC.cpp \
 	VMMGC/MMRamGCA.asm \
 	VMMGC/PDMGCDevice.cpp \
 	VMMGC/PGMGC.cpp \
 	VMMGC/SELMGC.cpp \
 	VMMGC/TRPMGC.cpp \
 	VMMGC/TRPMGCHandlers.cpp \
 	VMMGC/TRPMGCHandlersA.asm \
 	VMMGC/VMMGC.cpp \
 	VMMGC/VMMGCA.asm \
 	VMMGC/HWACCMGCA.asm \
 	VMMRZ/DBGFRZ.cpp \
 	VMMRZ/VMMRZ.cpp \
 	VMMAll/CPUMAllRegs.cpp \
 	VMMAll/CPUMAllA.asm \
 	VMMAll/DBGFAll.cpp \
 	VMMAll/IOMAll.cpp \
 	VMMAll/IOMAllMMIO.cpp \
 	VMMAll/EMAll.cpp \
 	VMMAll/EMAllA.asm \
 	VMMAll/MMAll.cpp \
 	VMMAll/MMAllHyper.cpp \
 	VMMAll/PDMAll.cpp \
 	VMMAll/PDMAllCritSect.cpp \
 	VMMAll/PDMAllQueue.cpp \
 	VMMAll/PGMAll.cpp \
 	VMMAll/PGMAllHandler.cpp \
 	VMMAll/PGMAllMap.cpp \
 	VMMAll/PGMAllPhys.cpp \
 	VMMAll/PGMAllPool.cpp \
 	VMMAll/REMAll.cpp \
 	VMMAll/SELMAll.cpp \
 	VMMAll/TMAll.cpp \
 	VMMAll/TMAllCpu.cpp \
 	VMMAll/TMAllReal.cpp \
 	VMMAll/TMAllVirtual.cpp \
 	VMMAll/TRPMAll.cpp \
 	VMMAll/VMAll.cpp \
 	VMMAll/VMMAll.cpp \
 	VMMAll/VMMAllA.asm \
 	PATM/VMMGC/CSAMGC.cpp \
 	PATM/VMMAll/CSAMAll.cpp \
 	PATM/VMMGC/PATMGC.cpp \
 	PATM/VMMAll/PATMAll.cpp
 ifdef VBOX_WITH_VMI
  VMMGC_SOURCES += \
  	PARAV/PARAV.cpp \
  	PARAV/PARAVAll.cpp
 endif
 ifeq ($(VBOX_LDR_FMT32),pe)
  VMMGC_SOURCES += VMMGC/VMMGC.def
 endif
 ifeq ($(VBOX_LDR_FMT32),lx)
  VMMGC_SOURCES += $(PATH_VMMGCImp)/VMMGC.def
 endif


 # the very last one.
 VMMGC_SOURCES += VMMGC/VMMGC99.asm

 VMMGC/VMMGCDeps.cpp_CXXFLAGS.win = -Oi- -TC ## @todo rename VMMGCDeps.cpp to .c
endif # VBOX_WITH_RAW_MODE


#
# VMMR0.r0
#
SYSMODS += VMMR0
VMMR0_TEMPLATE  = VBOXR0
VMMR0_SYSSUFF   = .r0

VMMR0_DEFS      = IN_VMM_R0 IN_RT_R0 IN_DIS DIS_CORE_ONLY IN_GVMM_R0 IN_GMM_R0 IN_INTNET_R0 RTASSERT_HAVE_SHOULD_PANIC
## @todo eliminate IN_GVMM_R0 IN_GMM_R0
ifdef VBOX_WITH_R0_LOGGING
 VMMR0_DEFS    += VBOX_WITH_R0_LOGGING
endif
ifdef VBOX_WITH_VMMR0_DISABLE_PREEMPTION
 VMMR0_DEFS    += VBOX_WITH_VMMR0_DISABLE_PREEMPTION
endif
ifdef VBOX_WITH_MULTI_CORE
 VMMR0_DEFS    += VBOX_WITH_MULTI_CORE
endif
ifdef VBOX_WITH_RAW_MODE
 VMMR0_DEFS    += VBOX_WITH_RAW_MODE
endif
VMMR0_DEFS.darwin = VMM_R0_SWITCH_STACK
VMMR0_DEFS.darwin.x86 = \
	VBOX_WITH_2X_4GB_ADDR_SPACE   VBOX_WITH_2X_4GB_ADDR_SPACE_IN_R0 \
	VBOX_WITH_HYBRID_32BIT_KERNEL VBOX_WITH_HYBRID_32BIT_KERNEL_IN_R0
VMMR0_DEFS.win.amd64  = VBOX_WITH_KERNEL_USING_XMM

ifeq ($(VBOX_LDR_FMT),pe)
 VMMR0_LDFLAGS  = -Entry:VMMR0EntryEx
endif
ifeq ($(VBOX_LDR_FMT),elf)
 VMMR0_LDFLAGS  = -e VMMR0EntryEx
endif
VMMR0_INCS      = \
	. \
	PATM

VMMR0_SOURCES   = \
	VMMR0/CPUMR0.cpp \
	VMMR0/CPUMR0A.asm \
	VMMR0/GMMR0.cpp \
	VMMR0/GVMMR0.cpp \
	VMMR0/HWACCMR0.cpp \
	VMMR0/HWACCMR0A.asm \
	VMMR0/HWSVMR0.cpp \
	VMMR0/HWVMXR0.cpp \
	VMMR0/PDMR0Device.cpp \
	VMMR0/PGMR0.cpp \
	VMMR0/TRPMR0.cpp \
	VMMR0/TRPMR0A.asm \
	VMMR0/VMMR0.cpp \
	VMMRZ/DBGFRZ.cpp \
	VMMRZ/VMMRZ.cpp \
	VMMAll/CPUMAllA.asm \
	VMMAll/CPUMAllRegs.cpp \
	VMMAll/CPUMStack.cpp \
	VMMAll/DBGFAll.cpp \
	VMMAll/EMAll.cpp \
	VMMAll/EMAllA.asm \
	VMMAll/HWACCMAll.cpp \
	VMMAll/IOMAll.cpp \
	VMMAll/IOMAllMMIO.cpp \
	VMMAll/MMAll.cpp \
	VMMAll/MMAllHyper.cpp \
	VMMAll/MMAllPagePool.cpp \
	VMMAll/PDMAll.cpp \
	VMMAll/PDMAllCritSect.cpp \
	VMMAll/PDMAllQueue.cpp \
	VMMAll/PGMAll.cpp \
	VMMAll/PGMAllHandler.cpp \
	VMMAll/PGMAllMap.cpp \
	VMMAll/PGMAllPhys.cpp \
	VMMAll/PGMAllPool.cpp \
	VMMAll/REMAll.cpp \
	VMMAll/SELMAll.cpp \
	VMMAll/TMAll.cpp \
	VMMAll/TMAllCpu.cpp \
	VMMAll/TMAllReal.cpp \
	VMMAll/TMAllVirtual.cpp \
	VMMAll/TRPMAll.cpp \
	VMMAll/VMAll.cpp \
	VMMAll/VMMAll.cpp \
	VMMAll/VMMAllA.asm
ifeq ($(VBOX_LDR_FMT),pe)
 VMMR0_SOURCES += VMMR0/VMMR0.def
endif
ifeq ($(VBOX_LDR_FMT),lx)
 VMMR0_SOURCES += $(PATH_VMMR0Imp)/VMMR0.def
endif
VMMR0_SOURCES.amd64 = \
	VMMR0/VMMR0JmpA-amd64.asm
VMMR0_SOURCES.x86 = \
	VMMR0/VMMR0JmpA-x86.asm
VMMR0_SOURCES.darwin.x86 = \
	VMMR0/PGMR0DynMap.cpp

# disable annoying warnings about array subscript above array bounds in aPages[]
PGMPool.cpp_CXXFLAGS = $(if-expr $(KBUILD_TARGET) == "win",,$(VBOX_GCC_Wno-array_bounds))
VMMAll/PGMAllPool.cpp_CXXFLAGS = $(if-expr $(KBUILD_TARGET) == "win",,$(VBOX_GCC_Wno-array_bounds))


VMMR0_LIBS = \
	$(PATH_LIB)/ServicesR0$(VBOX_SUFF_LIB) \
	$(PATH_LIB)/RuntimeR0$(VBOX_SUFF_LIB) \
	$(PATH_LIB)/DisasmR0$(VBOX_SUFF_LIB)
ifneq ($(filter pe lx,$(VBOX_LDR_FMT)),)
 VMMR0_LIBS += \
 	$(PATH_LIB)/SUPR0$(VBOX_SUFF_LIB)
endif

#
# For vmmGetSvnRev.
#
VMMAll/VMMAll.cpp_DEFS = VBOX_SVN_REV=$(VBOX_SVN_REV)

include	$(KBUILD_PATH)/subfooter.kmk


# Alias the PGM templates to the object in which they are defined.
PGMInternal.o \
PGMBth.o   PGMGst.o   PGMShw.o \
PGMBth.obj PGMGst.obj PGMShw.obj: PGM.o
PGMAllBth.o   PGMAllGst.o   PGMAllShw.o \
PGMAllBth.obj PGMAllGst.obj PGMAllShw.obj: PGMAll.o
PGMGCBth.o   PGMGCGst.o   PGMGCShw.o \
PGMGCBth.obj PGMGCGst.obj PGMGCShw.obj: PGMGC.o
PGMPhysRWTmpl.o PGMPhysRWTmpl.obj: PGMPhys.o

