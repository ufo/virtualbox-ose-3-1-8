#
# VBox GUI: additional Qt project file (for Qt Designer).
#
# NOTE: This file is intended to be opened by Qt Designer
#       as a project file (to work with .ui files)
#

#
# Copyright (C) 2006-2007 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

TEMPLATE	= app
LANGUAGE	= C++

FORMS = \
    src/VBoxCloseVMDlg.ui \
    src/VBoxVMInformationDlg.ui \
    src/VBoxMediaManagerDlg.ui \
    src/VBoxSnapshotDetailsDlg.ui \
    src/VBoxSnapshotsWgt.ui \
    src/VBoxRegistrationDlg.ui \
    src/VBoxTakeSnapshotDlg.ui \
    src/VBoxUpdateDlg.ui \
    src/VBoxVMLogViewer.ui \
    src/settings/VBoxSettingsDialog.ui \
    src/settings/vm/VBoxVMSettingsGeneral.ui \
    src/settings/vm/VBoxVMSettingsSystem.ui \
    src/settings/vm/VBoxVMSettingsDisplay.ui \
    src/settings/vm/VBoxVMSettingsHD.ui \
    src/settings/vm/VBoxVMSettingsAudio.ui \
    src/settings/vm/VBoxVMSettingsNetwork.ui \
    src/settings/vm/VBoxVMSettingsSerial.ui \
    src/settings/vm/VBoxVMSettingsParallel.ui \
    src/settings/vm/VBoxVMSettingsUSB.ui \
    src/settings/vm/VBoxVMSettingsUSBFilterDetails.ui \
    src/settings/vm/VBoxVMSettingsSF.ui \
    src/settings/vm/VBoxVMSettingsSFDetails.ui \
    src/settings/global/VBoxGLSettingsGeneral.ui \
    src/settings/global/VBoxGLSettingsInput.ui \
    src/settings/global/VBoxGLSettingsUpdate.ui \
    src/settings/global/VBoxGLSettingsLanguage.ui \
    src/settings/global/VBoxGLSettingsNetwork.ui \
    src/settings/global/VBoxGLSettingsNetworkDetails.ui \
    src/wizards/newvm/VBoxNewVMWzd.ui \
    src/wizards/newhd/VBoxNewHDWzd.ui \
    src/wizards/firstrun/VBoxVMFirstRunWzd.ui \
    src/wizards/exportappliance/VBoxExportApplianceWzd.ui \
    src/wizards/importappliance/VBoxImportApplianceWzd.ui \
    src/widgets/VBoxApplianceEditorWgt.ui

TRANSLATIONS = \
	nls/VirtualBox_en.ts \
	nls/VirtualBox_el.ts \
	nls/VirtualBox_tr.ts \
	nls/VirtualBox_id.ts \
	nls/VirtualBox_ca.ts \
	nls/VirtualBox_ca_VA.ts \
	nls/VirtualBox_sk.ts \
	nls/VirtualBox_eu.ts \
	nls/VirtualBox_nl.ts \
	nls/VirtualBox_cs.ts \
	nls/VirtualBox_hu.ts \
	nls/VirtualBox_fi.ts \
	nls/VirtualBox_sr.ts \
	nls/VirtualBox_ko.ts \
	nls/VirtualBox_sv.ts \
	nls/VirtualBox_pt.ts \
	nls/VirtualBox_pt_BR.ts \
	nls/VirtualBox_ja.ts \
	nls/VirtualBox_pl.ts \
	nls/VirtualBox_ar.ts \
	nls/VirtualBox_de.ts \
	nls/VirtualBox_es.ts \
	nls/VirtualBox_gl_ES.ts \
	nls/VirtualBox_fr.ts \
	nls/VirtualBox_it.ts \
	nls/VirtualBox_ro.ts \
	nls/VirtualBox_ru.ts \
	nls/VirtualBox_zh_CN.ts \
	nls/VirtualBox_zh_TW.ts \
	nls/VirtualBox_km_KH.ts \
	nls/VirtualBox_bg.ts \
	nls/VirtualBox_uk.ts \
	nls/VirtualBox_da.ts

