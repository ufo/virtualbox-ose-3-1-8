<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>VBoxGlobal</name>
    <message numerus="yes">
        <location filename="../src/globals/VBoxGlobal.h" line="362"/>
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n year</numerusform>
            <numerusform>%n years</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/globals/VBoxGlobal.h" line="367"/>
        <source>%n month(s)</source>
        <translation>
            <numerusform>%n month</numerusform>
            <numerusform>%n months</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/globals/VBoxGlobal.h" line="372"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n day</numerusform>
            <numerusform>%n days</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/globals/VBoxGlobal.h" line="377"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n hour</numerusform>
            <numerusform>%n hours</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/globals/VBoxGlobal.h" line="387"/>
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n second</numerusform>
            <numerusform>%n seconds</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../src/globals/VBoxGlobal.h" line="382"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minute</numerusform>
            <numerusform>%n minutes</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>VBoxTakeSnapshotDlg</name>
    <message numerus="yes">
        <location filename="../src/VBoxTakeSnapshotDlg.cpp" line="60"/>
        <source>Warning: You are taking a snapshot of a running machine which has %n immutable image(s) attached to it. As long as you are working from this snapshot the immutable image(s) will not be reset to avoid loss of data.</source>
        <translation>
            <numerusform>Warning: You are taking a snapshot of a running machine which has %n immutable image attached to it. As long as you are working from this snapshot the immutable image will not be reset to avoid loss of data.</numerusform>
            <numerusform>Warning: You are taking a snapshot of a running machine which has %n immutable images attached to it. As long as you are working from this snapshot the immutable images will not be reset to avoid loss of data.</numerusform>
        </translation>
    </message>
</context>
</TS>
