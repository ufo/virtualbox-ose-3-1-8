<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>@@@</name>
    <message>
        <source>English</source>
        <comment>Native language name</comment>
        <translation>Deutsch</translation>
    </message>
    <message>
        <source>--</source>
        <comment>Native language country name (empty if this language is for all countries)</comment>
        <translation></translation>
    </message>
    <message>
        <source>English</source>
        <comment>Language name, in English</comment>
        <translation>German</translation>
    </message>
    <message>
        <source>--</source>
        <comment>Language country name, in English (empty if native country name is empty)</comment>
        <translation></translation>
    </message>
    <message>
        <source>Sun Microsystems, Inc.</source>
        <comment>Comma-separated list of translators</comment>
        <translation type="obsolete">Sun Microsystems, Inc.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Executable &lt;b&gt;%1&lt;/b&gt; requires Qt %2.x, found Qt %3.</source>
        <translation>Programm &lt;b&gt;%1&lt;/b&gt; benötigt Qt Version %2.x, aber Version %3 gefunden.</translation>
    </message>
    <message>
        <source>Incompatible Qt Library Error</source>
        <translation>Inkompatible Qt-Bibliothek</translation>
    </message>
    <message>
        <source>VirtualBox - Error In %1</source>
        <translation>VirtualBox - Fehler in %1</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;b&gt;%1 (rc=%2)&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;%1 (rc=%2)&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;</translation>
    </message>
    <message>
        <source>Please try reinstalling VirtualBox.</source>
        <translation>Eine Neuinstallation behebt möglicherweise das Problem.</translation>
    </message>
    <message>
        <source>The VirtualBox Linux kernel driver (vboxdrv) is either not loaded or there is a permission problem with /dev/vboxdrv. Please reinstall the kernel module by executing&lt;br/&gt;&lt;br/&gt;  &lt;font color=blue&gt;&apos;/etc/init.d/vboxdrv setup&apos;&lt;/font&gt;&lt;br/&gt;&lt;br/&gt;as root. Users of Ubuntu, Fedora or Mandriva should install the DKMS package first. This package keeps track of Linux kernel changes and recompiles the vboxdrv kernel module if necessary.</source>
        <translation>Der VirtualBox Kerntreiber für Linux (vboxdrv) ist entweder nicht geladen oder auf das Gerät /dev/vboxdrv konnte nicht zugegriffen werden. Richten Sie das Kernmodul neu ein, indem Sie&lt;br/&gt;&lt;br/&gt;&lt;font color=blue&gt;&apos;/etc/init.d/vboxdrv setup&apos;&lt;/font&gt;&lt;br/&gt;&lt;br/&gt;mit Root-Rechten ausführen. Falls Sie eine Linux-Distribution von Ubuntu, Fedora oder Mandriva verweden, sollten Sie das Paket DKMS zuerst installieren. Dieses compiliert die Kernmodule nach einem Kernupgrade automatisch neu.</translation>
    </message>
    <message>
        <source>Make sure the kernel module has been loaded successfully.</source>
        <translation>Stellen Sie sicher, dass das Kernmodul erfolgreich geladen wurde.</translation>
    </message>
    <message>
        <source>VirtualBox - Runtime Error</source>
        <translation>VirtualBox - Fehler in der Laufzeitumgebung</translation>
    </message>
    <message>
        <source>&lt;b&gt;Cannot access the kernel driver!&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;</source>
        <translation>&lt;b&gt;Der Kerntreiber kann nicht angesprochen werden!&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;</translation>
    </message>
    <message>
        <source>Unknown error %2 during initialization of the Runtime</source>
        <translation>Unbekannter Fehler %2 während der Initialisierung der Laufzeitumgebung</translation>
    </message>
    <message>
        <source>Kernel driver not accessible</source>
        <translation>Auf den Kerntreiber kann nicht zugegriffen werden</translation>
    </message>
    <message>
        <source>The VirtualBox kernel modules do not match this version of VirtualBox. The installation of VirtualBox was apparently not successful. Please try completely uninstalling and reinstalling VirtualBox.</source>
        <translation>Die VirtualBox-Kernmodule passen nicht zu dieser Version von VirtualBox. Es wird empfohlen, VirtualBox komplett neu zu installieren.</translation>
    </message>
    <message>
        <source>The VirtualBox kernel modules do not match this version of VirtualBox. The installation of VirtualBox was apparently not successful. Executing&lt;br/&gt;&lt;br/&gt;  &lt;font color=blue&gt;&apos;/etc/init.d/vboxdrv setup&apos;&lt;/font&gt;&lt;br/&gt;&lt;br/&gt;may correct this. Make sure that you do not mix the OSE version and the PUEL version of VirtualBox.</source>
        <translation>Die VirtualBox-Kernmodule passen nicht zu dieser Version von VirtualBox. Die Installation von VirtualBox war möglicherweise nicht vollständig. Durch Ausführen von &lt;br/&gt;&lt;br/&gt;&lt;font color=blue&gt;&apos;/etc/init.d/vboxdrv setup&apos;&lt;/font&gt;&lt;br/&gt;&lt;br/&gt; sollte dieses Problem behoben werden. Bitte stellen Sie sicher, dass Sie die OSE-Version von VirtualBox nicht mit der PUEL-Version mischen.</translation>
    </message>
    <message>
        <source>This error means that the kernel driver was either not able to allocate enough memory or that some mapping operation failed.</source>
        <translation>Diese Fehlermeldung bedeutet, dass der Kerntreiber entweder nicht ausreichend Speicher anfordern konnte oder dass eine Mapping-Operation fehlgeschlagen ist.</translation>
    </message>
</context>
<context>
    <name>QIArrowSplitter</name>
    <message>
        <source>&amp;Back</source>
        <translation>&amp;Zurück</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation>&amp;Vorwärts</translation>
    </message>
</context>
<context>
    <name>QIFileDialog</name>
    <message>
        <source>Select a directory</source>
        <translation>Wählen Sie ein Verzeichnis</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>Wählen Sie eine Datei</translation>
    </message>
</context>
<context>
    <name>QIHotKeyEdit</name>
    <message>
        <source>Left </source>
        <translation>Links</translation>
    </message>
    <message>
        <source>Right </source>
        <translation>Rechts</translation>
    </message>
    <message>
        <source>Left Shift</source>
        <translation>Umsch Links</translation>
    </message>
    <message>
        <source>Right Shift</source>
        <translation>Umsch Rechts</translation>
    </message>
    <message>
        <source>Left Ctrl</source>
        <translation>Strg Links</translation>
    </message>
    <message>
        <source>Right Ctrl</source>
        <translation>Strg Rechts</translation>
    </message>
    <message>
        <source>Left Alt</source>
        <translation>Alt Links</translation>
    </message>
    <message>
        <source>Right Alt</source>
        <translation>Alt Rechts</translation>
    </message>
    <message>
        <source>Left WinKey</source>
        <translation>Win Links</translation>
    </message>
    <message>
        <source>Right WinKey</source>
        <translation>Win Rechts</translation>
    </message>
    <message>
        <source>Menu key</source>
        <translation>Menü</translation>
    </message>
    <message>
        <source>Alt Gr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <source>Caps Lock</source>
        <translation>Umsch Lock</translation>
    </message>
    <message>
        <source>Scroll Lock</source>
        <translation>Rollen Lock</translation>
    </message>
    <message>
        <source>&lt;key_%1&gt;</source>
        <translation>&lt;Taste_%1&gt;</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation>Druck</translation>
    </message>
    <message>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <source>F7</source>
        <translation>F7</translation>
    </message>
    <message>
        <source>F8</source>
        <translation>F8</translation>
    </message>
    <message>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <source>F12</source>
        <translation>F12</translation>
    </message>
    <message>
        <source>F13</source>
        <translation>F13</translation>
    </message>
    <message>
        <source>F14</source>
        <translation>F14</translation>
    </message>
    <message>
        <source>F15</source>
        <translation>F15</translation>
    </message>
    <message>
        <source>F16</source>
        <translation>F16</translation>
    </message>
    <message>
        <source>F17</source>
        <translation>F17</translation>
    </message>
    <message>
        <source>F18</source>
        <translation>F18</translation>
    </message>
    <message>
        <source>F19</source>
        <translation>F19</translation>
    </message>
    <message>
        <source>F20</source>
        <translation>F20</translation>
    </message>
    <message>
        <source>F21</source>
        <translation>F21</translation>
    </message>
    <message>
        <source>F22</source>
        <translation>F22</translation>
    </message>
    <message>
        <source>F23</source>
        <translation>F23</translation>
    </message>
    <message>
        <source>F24</source>
        <translation>F24</translation>
    </message>
    <message>
        <source>Num Lock</source>
        <translation>Num</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Vorw</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Rückw</translation>
    </message>
</context>
<context>
    <name>QIHttp</name>
    <message>
        <source>Connection timed out</source>
        <translation>Die Verbindung konnte innerhalb der vorgegebenen Zeit nicht aufgebaut werden</translation>
    </message>
    <message>
        <source>Could not locate the file on the server (response: %1)</source>
        <translation>Die Datei konnte auf dem Server nicht gefunden werden (Antwort: %1)</translation>
    </message>
</context>
<context>
    <name>QILabel</name>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
</context>
<context>
    <name>QIMessageBox</name>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignorieren</translation>
    </message>
    <message>
        <source>&amp;Details</source>
        <translation>&amp;Details</translation>
    </message>
    <message>
        <source>&amp;Details (%1 of %2)</source>
        <translation>&amp;Details (%1 von %2)</translation>
    </message>
</context>
<context>
    <name>QIWidgetValidator</name>
    <message>
        <source>not complete</source>
        <comment>value state</comment>
        <translation>nicht komplett</translation>
    </message>
    <message>
        <source>invalid</source>
        <comment>value state</comment>
        <translation>unzulässig</translation>
    </message>
    <message>
        <source>&lt;qt&gt;The value of the &lt;b&gt;%1&lt;/b&gt; field on the &lt;b&gt;%2&lt;/b&gt; page is %3.&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;Der Wert des Feldes &lt;b&gt;%1&lt;/b&gt; auf der Seite &lt;b&gt;%2&lt;/b&gt; beträgt %3.&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&lt;qt&gt;One of the values on the &lt;b&gt;%1&lt;/b&gt; page is %2.&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;Einer der Werte der Seite &lt;b&gt;%1&lt;/b&gt; ist %2.&lt;/qt&gt;</translation>
    </message>
</context>
<context>
    <name>VBoxAboutDlg</name>
    <message>
        <source>VirtualBox - About</source>
        <translation>VirtualBox - Über</translation>
    </message>
    <message>
        <source>VirtualBox Graphical User Interface</source>
        <translation>VirtualBox graphische Benutzeroberfläche</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
</context>
<context>
    <name>VBoxAdditionsDownloader</name>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Downloading the VirtualBox Guest Additions CD image from &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;...&lt;/nobr&gt;</source>
        <translation>Lade das CDROM-Abbild mit den VirtualBox-Gasterweiterungen von &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;...&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Cancel the VirtualBox Guest Additions CD image download</source>
        <translation>Bricht das Herunterladen der VirtualBox-Gasterweiterungen ab</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to save the downloaded file as &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;.&lt;/nobr&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die heruntergeladene Datei konnte nicht als &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt; gespeichert werden.&lt;/nobr&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Select folder to save Guest Additions image to</source>
        <translation>Wählen Sie den Ordner aus, in dem die Gasterweiterungen gespeichert werden sollen</translation>
    </message>
</context>
<context>
    <name>VBoxApplianceEditorWgt</name>
    <message>
        <source>Virtual System %1</source>
        <translation>Virtuelles System %1</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <source>Product-URL</source>
        <translation>Produkt-URL</translation>
    </message>
    <message>
        <source>Vendor</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <source>Vendor-URL</source>
        <translation>Hersteller-URL</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <source>Guest OS Type</source>
        <translation>Gast-Betriebssystem</translation>
    </message>
    <message>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <source>RAM</source>
        <translation>RAM</translation>
    </message>
    <message>
        <source>Hard Disk Controller (IDE)</source>
        <translation>Festplatten-Controller IDE</translation>
    </message>
    <message>
        <source>Hard Disk Controller (SATA)</source>
        <translation>Festplatten-Controller SATA</translation>
    </message>
    <message>
        <source>Hard Disk Controller (SCSI)</source>
        <translation>Festplatten-Controller SCSI</translation>
    </message>
    <message>
        <source>DVD</source>
        <translation>DVD-Laufwerk</translation>
    </message>
    <message>
        <source>Floppy</source>
        <translation>Diskettenlaufwerk</translation>
    </message>
    <message>
        <source>Network Adapter</source>
        <translation>Netzwerk-Adapter</translation>
    </message>
    <message>
        <source>USB Controller</source>
        <translation>USB-Controller</translation>
    </message>
    <message>
        <source>Sound Card</source>
        <translation>Soundkarte</translation>
    </message>
    <message>
        <source>Virtual Disk Image</source>
        <translation>Virtuelles Plattenabbild</translation>
    </message>
    <message>
        <source>Unknown Hardware Item</source>
        <translation>Unbekannter Hardware</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>&lt;b&gt;Original Value:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Originalwert:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <source>Warnings:</source>
        <translation>Warnungen:</translation>
    </message>
</context>
<context>
    <name>VBoxCloseVMDlg</name>
    <message>
        <source>Close Virtual Machine</source>
        <translation>Beenden der virtuellen Maschine</translation>
    </message>
    <message>
        <source>You want to:</source>
        <translation>Sie möchten:</translation>
    </message>
    <message>
        <source>&amp;Save the machine state</source>
        <translation>den Zustand der virtuellen Maschine &amp;speichern</translation>
    </message>
    <message>
        <source>&amp;Power off the machine</source>
        <translation>die virtuelle Maschine &amp;ausschalten</translation>
    </message>
    <message>
        <source>S&amp;end the shutdown signal</source>
        <translation>die virtuelle Maschine mittels ACPI-Event &amp;herunterfahren</translation>
    </message>
    <message>
        <source>&lt;p&gt;When checked, the machine will be returned to the state stored in the current snapshot after it is turned off. This is useful if you are sure that you want to discard the results of your last sessions and start again at that snapshot.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Ist dieses Kästchen aktiviert, dann wird sofort nach dem Ausschalten der Maschinenzustand aus dem aktuellen Sicherungspunkt wieder hergestellt. Dies ist dann empfehlenswert, wenn Sie die Arbeit der letzten Sitzung verwerfen und zum aktuellen Sicherungspunkt zurückkehren wollen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Saves the current execution state of the virtual machine to the physical hard disk of the host PC.&lt;/p&gt;&lt;p&gt;Next time this machine is started, it will be restored from the saved state and continue execution from the same place you saved it at, which will let you continue your work immediately.&lt;/p&gt;&lt;p&gt;Note that saving the machine state may take a long time, depending on the guest operating system type and the amount of memory you assigned to the virtual machine.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sichert den aktuellen Ausführungszustand der virtuellen Maschine auf die physische Festplatte des Host-PCs.&lt;/p&gt;&lt;p&gt;Beim nächsten Start wird sie aus diesem Zustand wieder hergestellt und an der selben Stelle fortgesetzt, an der Sie den Zustand gesichert haben.&lt;/p&gt;&lt;p&gt;Bitte beachten Sie, dass das Sichern des Zustandes eine längere Zeit in Anspruch nehmen kann. Die benötigte Zeit ist abhängig vom Gastsystem und von der Größe dem Gastsystem zugewiesenen Speichers.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Sends the ACPI Power Button press event to the virtual machine.&lt;/p&gt;&lt;p&gt;Normally, the guest operating system running inside the virtual machine will detect this event and perform a clean shutdown procedure. This is a recommended way to turn off the virtual machine because all applications running inside it will get a chance to save their data and state.&lt;/p&gt;&lt;p&gt;If the machine doesn&apos;t respond to this action then the guest operating system may be misconfigured or doesn&apos;t understand ACPI Power Button events at all. In this case you should select the &lt;b&gt;Power off the machine&lt;/b&gt; action to stop virtual machine execution.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sendet das ACPI-Ereignis &apos;Netztaste gedrückt&apos; an die virtuelle Maschine.&lt;/p&gt;&lt;p&gt;Übliche Gastsysteme werden dieses Ereignis empfangen und die Maschine in Folge dessen sauber herunterfahren. Dies ist das empfohlene Vorgehen, um die Maschine auszuschalten, weil alle Anwendungen innerhalb der Maschine Gelegenheit zum Sichern Ihrer Daten bekommen.&lt;/p&gt;&lt;p&gt;Falls der Gast nicht auf dieses Ereignis reagiert, ist dieser entweder falsch konfiguriert oder versteht dieses Ereignis generell nicht. In diesem Fall sollten Sie &lt;b&gt;die virtuelle Maschine ausschalten&lt;/b&gt; wählen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Turns off the virtual machine.&lt;/p&gt;&lt;p&gt;Note that this action will stop machine execution immediately so that the guest operating system running inside it will not be able to perform a clean shutdown procedure which may result in &lt;i&gt;data loss&lt;/i&gt; inside the virtual machine. Selecting this action is recommended only if the virtual machine does not respond to the &lt;b&gt;Send the shutdown signal&lt;/b&gt; action.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Schaltet die virtuelle Maschine aus.&lt;/p&gt;&lt;p&gt;Bitte beachten Sie, dass dadurch die Ausführung der Maschine sofort unterbrochen wird und das Gastsystem somit keine Gelegenheit hat, sich sauber zu beenden. Dadurch kann es zu &lt;i&gt;Datenverlust&lt;/i&gt; innerhalb der virtuellen Maschine kommen. Diese Aktion sollte nur dann durchgeführt werden, wenn die virtuelle Maschine nicht auf &lt;b&gt;Sende Signal zum Herunterfahren&lt;/b&gt; reagiert.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Restore the machine state stored in the current snapshot</source>
        <translation>Der Ausführungszustand wird auf den aktuellen Sicherungspunkt zurückgesetzt.</translation>
    </message>
    <message>
        <source>&amp;Restore current snapshot &apos;%1&apos;</source>
        <translation>&amp;Zurückkehren auf Sicherungspunkt &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>VBoxConsoleWnd</name>
    <message>
        <source>VirtualBox OSE</source>
        <translation>VirtualBox OSE</translation>
    </message>
    <message>
        <source>&amp;Fullscreen Mode</source>
        <translation>&amp;Vollbildmodus</translation>
    </message>
    <message>
        <source>Switch to fullscreen mode</source>
        <translation>Schaltet in den Vollbildmodus</translation>
    </message>
    <message>
        <source>Mouse Integration</source>
        <comment>enable/disable...</comment>
        <translation>Mauszeiger-Integration</translation>
    </message>
    <message>
        <source>Auto-resize Guest Display</source>
        <comment>enable/disable...</comment>
        <translation>Größe des Gastes automatisch anpassen</translation>
    </message>
    <message>
        <source>Auto-resize &amp;Guest Display</source>
        <translation>Größe des &amp;Gastes automatisch anpassen</translation>
    </message>
    <message>
        <source>Automatically resize the guest display when the window is resized (requires Guest Additions)</source>
        <translation>Passt die Größe der Gastanzeige automatisch an, wenn sich die Fenstergröße ändert (erfordert Gasterweiterungen)</translation>
    </message>
    <message>
        <source>&amp;Adjust Window Size</source>
        <translation>Fenstergröße &amp;anpassen</translation>
    </message>
    <message>
        <source>Adjust window size and position to best fit the guest display</source>
        <translation>Passt Fenstergröße und -position an die Auflösung des Gastes an</translation>
    </message>
    <message>
        <source>Send the Ctrl-Alt-Del sequence to the virtual machine</source>
        <translation>Sendet die Sequenz Strg-Alt-Entf (Affengriff) an die virtuelle Maschine</translation>
    </message>
    <message>
        <source>&amp;Insert Ctrl-Alt-Backspace</source>
        <translation>&amp;Sende Strg-Alt-Rücktaste</translation>
    </message>
    <message>
        <source>Send the Ctrl-Alt-Backspace sequence to the virtual machine</source>
        <translation>Sendet die Sequenz Strg-Alt-Rücktaste an die virtuelle Maschine</translation>
    </message>
    <message>
        <source>&amp;Reset</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <source>Reset the virtual machine</source>
        <translation>Zurücksetzen der virtuellen Maschine</translation>
    </message>
    <message>
        <source>ACPI S&amp;hutdown</source>
        <translation>Ausschalten per &amp;ACPI</translation>
    </message>
    <message>
        <source>Send the ACPI Power Button press event to the virtual machine</source>
        <translation>Sendet das ACPI-Ereignis &quot;Einschaltknopf gedrückt&quot; an die virtuelle Maschine</translation>
    </message>
    <message>
        <source>&amp;Close...</source>
        <translation>&amp;Schließen...</translation>
    </message>
    <message>
        <source>Close the virtual machine</source>
        <translation>Schließt die virtuelle Maschine</translation>
    </message>
    <message>
        <source>Take &amp;Snapshot...</source>
        <translation>&amp;Sicherungspunkt erstellen...</translation>
    </message>
    <message>
        <source>Take a snapshot of the virtual machine</source>
        <translation>Erstellt einen Sicherungspunkt der virtuellen Maschine</translation>
    </message>
    <message>
        <source>Enable or disable remote desktop (RDP) connections to this machine</source>
        <translation>Ein- oder Ausschalten der Fernsteuerung (remote desktop protocol, RDP) für diese virtuelle Maschine</translation>
    </message>
    <message>
        <source>&amp;Shared Folders...</source>
        <translation>&amp;Gemeinsame Ordner...</translation>
    </message>
    <message>
        <source>Create or modify shared folders</source>
        <translation>Öffnet den Dialog für gemeinsame Ordner</translation>
    </message>
    <message>
        <source>&amp;Install Guest Additions...</source>
        <translation>&amp;Gasterweiterungen installieren...</translation>
    </message>
    <message>
        <source>Mount the Guest Additions installation image</source>
        <translation>Medium mit Gasterweiterungen einbinden</translation>
    </message>
    <message>
        <source>&amp;USB Devices</source>
        <translation>&amp;USB-Geräte</translation>
    </message>
    <message>
        <source>&amp;Devices</source>
        <translation>&amp;Geräte</translation>
    </message>
    <message>
        <source>De&amp;bug</source>
        <translation>De&amp;bug</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <source>&lt;hr&gt;The VRDP Server is listening on port %1</source>
        <translation>&lt;hr&gt;VRDP-Server lauscht an Port %1</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <source>Suspend the execution of the virtual machine</source>
        <translation>Suspendiert die Ausführung der virtuellen Maschine</translation>
    </message>
    <message>
        <source>R&amp;esume</source>
        <translation>&amp;Fortfahren</translation>
    </message>
    <message>
        <source>Resume the execution of the virtual machine</source>
        <translation>Fährt mit der Ausführung der virtuellen Maschine fort</translation>
    </message>
    <message>
        <source>Disable &amp;Mouse Integration</source>
        <translation>&amp;Mauszeiger-Integration deaktivieren</translation>
    </message>
    <message>
        <source>Temporarily disable host mouse pointer integration</source>
        <translation>Integration des Mauszeigers vorübergehend deaktivieren</translation>
    </message>
    <message>
        <source>Enable &amp;Mouse Integration</source>
        <translation>&amp;Mauszeiger-Integration aktivieren</translation>
    </message>
    <message>
        <source>Enable temporarily disabled host mouse pointer integration</source>
        <translation>Aktiviert vorübergehend die deaktivierte Integration des Mauszeigers</translation>
    </message>
    <message>
        <source>Snapshot %1</source>
        <translation>Sicherungspunkt %1</translation>
    </message>
    <message>
        <source>&amp;Insert Ctrl-Alt-Del</source>
        <translation>&amp;Sende Strg-Alt-Entf</translation>
    </message>
    <message>
        <source>&amp;Machine</source>
        <translation>&amp;Maschine</translation>
    </message>
    <message>
        <source>Seam&amp;less Mode</source>
        <translation>Naht&amp;loser Modus</translation>
    </message>
    <message>
        <source>Switch to seamless desktop integration mode</source>
        <translation>Schaltet in den nahtlosen Fenstermodus (Fenster des Gastes werden nahtlos in den Hostdesktop integriert)</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No hard disks attached&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>HDD tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;Keine Festplatten angeschlossen&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;Adapter %1 (%2)&lt;/b&gt;: cable %3&lt;/nobr&gt;</source>
        <comment>Network adapters tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;Adapter %1 (%2)&lt;/b&gt;: Kabel %3&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>connected</source>
        <comment>Network adapters tooltip</comment>
        <translation>verbunden</translation>
    </message>
    <message>
        <source>disconnected</source>
        <comment>Network adapters tooltip</comment>
        <translation>getrennt</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;All network adapters are disabled&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>Network adapters tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;Alle Netzwerkadapter sind deaktiviert&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No USB devices attached&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;Keine USB-Geräte angeschlossen&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;USB Controller is disabled&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;USB-Controller ist deaktiviert&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No shared folders&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>Shared folders tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;Keine gemeinsamen Ordner&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Session I&amp;nformation Dialog</source>
        <translation>Session-I&amp;nformationen</translation>
    </message>
    <message>
        <source>Show Session Information Dialog</source>
        <translation>Zeigt einen Dialog mit Session-Informationen</translation>
    </message>
    <message>
        <source>&amp;Statistics...</source>
        <comment>debug action</comment>
        <translation>&amp;Statistiken...</translation>
    </message>
    <message>
        <source>&amp;Command Line...</source>
        <comment>debug action</comment>
        <translation>&amp;Kommandozeile...</translation>
    </message>
    <message>
        <source>Indicates whether the guest display auto-resize function is On (&lt;img src=:/auto_resize_on_16px.png/&gt;) or Off (&lt;img src=:/auto_resize_off_16px.png/&gt;). Note that this function requires Guest Additions to be installed in the guest OS.</source>
        <translation>Zeigt, ob die automatische Größenanpassung des Fenster aktiviert ist (&lt;img src=:/auto_resize_on_16px.png/&gt;) oder nicht (&lt;img src=:/auto_resize_off_16px.png/&gt;). Beachten Sie, dass für diese Funktion Gasterweiterungen im Gast-BS installiert sein müssen.</translation>
    </message>
    <message>
        <source>Indicates whether the host mouse pointer is captured by the guest OS:&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_disabled_16px.png/&gt;&amp;nbsp;&amp;nbsp;pointer is not captured&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_16px.png/&gt;&amp;nbsp;&amp;nbsp;pointer is captured&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_seamless_16px.png/&gt;&amp;nbsp;&amp;nbsp;mouse integration (MI) is On&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_can_seamless_16px.png/&gt;&amp;nbsp;&amp;nbsp;MI is Off, pointer is captured&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_can_seamless_uncaptured_16px.png/&gt;&amp;nbsp;&amp;nbsp;MI is Off, pointer is not captured&lt;/nobr&gt;&lt;br&gt;Note that the mouse integration feature requires Guest Additions to be installed in the guest OS.</source>
        <translation>Zeigt, ob der Mauszeiger im Gast-BS gefangen ist:&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_disabled_16px.png/&gt;&amp;nbsp;&amp;nbsp;Zeiger ist nicht gefangen&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_16px.png/&gt;&amp;nbsp;&amp;nbsp;Zeiger ist gefangen&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_seamless_16px.png/&gt;&amp;nbsp;&amp;nbsp;Mauszeiger-Integration (MI) ist an&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_can_seamless_16px.png/&gt;&amp;nbsp;&amp;nbsp;MI ist aus, Zeiger ist gefangen&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_can_seamless_uncaptured_16px.png/&gt;&amp;nbsp;&amp;nbsp;MI ist aus, Zeiger ist nicht gefangen&lt;/nobr&gt;&lt;br&gt;Beachten Sie, dass für die Mauszeiger-Integration die Gasterweiterungen im Gast installiert sein müssen.</translation>
    </message>
    <message>
        <source>Indicates whether the keyboard is captured by the guest OS (&lt;img src=:/hostkey_captured_16px.png/&gt;) or not (&lt;img src=:/hostkey_16px.png/&gt;).</source>
        <translation>Zeigt, ob die Tastatur vom Gast gefangen ist (&lt;img src=:/hostkey_captured_16px.png/&gt;) oder nicht (&lt;img src=:/hostkey_16px.png/&gt;).</translation>
    </message>
    <message>
        <source>Indicates whether the Remote Display (VRDP Server) is enabled (&lt;img src=:/vrdp_16px.png/&gt;) or not (&lt;img src=:/vrdp_disabled_16px.png/&gt;).</source>
        <translation>Zeigt an, ob die Fernsteuerung (VRDP-Server) aktiviert ist (&lt;img src=:/vrdp_16px.png/&gt;) oder nicht (&lt;img src=:/vrdp_disabled_16px.png/&gt;).</translation>
    </message>
    <message>
        <source>&amp;Logging...</source>
        <comment>debug action</comment>
        <translation>&amp;Logging...</translation>
    </message>
    <message>
        <source>Shows the currently assigned Host key.&lt;br&gt;This key, when pressed alone, toggles the keyboard and mouse capture state. It can also be used in combination with other keys to quickly perform actions from the main menu.</source>
        <translation>Zeigt die momentan zugeordnete Host-Taste.&lt;br&gt;Wird diese Taste allein gedrückt, schaltet sie den Fangmodus für Tastatur und Maus um. Die Taste führt in Kombination mit anderen Tasten bestimmte Aktionen aus dem Hauptmenü aus.</translation>
    </message>
    <message>
        <source>Sun VirtualBox</source>
        <translation>Sun VirtualBox</translation>
    </message>
    <message>
        <source>Indicates the status of the hardware virtualization features used by this virtual machine:&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%1:&lt;/b&gt;&amp;nbsp;%2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%3:&lt;/b&gt;&amp;nbsp;%4&lt;/nobr&gt;</source>
        <comment>Virtualization Stuff LED</comment>
        <translation>Zeigt an, ob diese virtuelle Maschine Hardware-Virtualisierung benutzt:&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%1:&lt;/b&gt;&amp;nbsp;%2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%3:&lt;/b&gt;&amp;nbsp;%4&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%1:&lt;/b&gt;&amp;nbsp;%2&lt;/nobr&gt;</source>
        <comment>Virtualization Stuff LED</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%1:&lt;/b&gt;&amp;nbsp;%2&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source> EXPERIMENTAL build %1r%2 - %3</source>
        <translation> Experimentelle Version %1r%2 - %3</translation>
    </message>
    <message>
        <source>&amp;CD/DVD Devices</source>
        <translation>&amp;CD/DVD-Laufwerke</translation>
    </message>
    <message>
        <source>&amp;Floppy Devices</source>
        <translation>&amp;Diskettenlaufwerke</translation>
    </message>
    <message>
        <source>&amp;Network Adapters...</source>
        <translation>&amp;Netzwerkadapter...</translation>
    </message>
    <message>
        <source>Change the settings of network adapters</source>
        <translation>Ändert die Einstellungen der Netzwerkadapter</translation>
    </message>
    <message>
        <source>&amp;Remote Display</source>
        <translation>&amp;Fernsteuerung</translation>
    </message>
    <message>
        <source>Remote Desktop (RDP) Server</source>
        <comment>enable/disable...</comment>
        <translation>Server für Fernsteuerung (RDP</translation>
    </message>
    <message>
        <source>More CD/DVD Images...</source>
        <translation>Mehr CD/DVD-Abbilder...</translation>
    </message>
    <message>
        <source>Unmount CD/DVD Device</source>
        <translation>CD/DVD-Medium auswerfen</translation>
    </message>
    <message>
        <source>More Floppy Images...</source>
        <translation>Mehr Diskettenabbilder...</translation>
    </message>
    <message>
        <source>Unmount Floppy Device</source>
        <translation>Diskette auswerfen</translation>
    </message>
    <message>
        <source>No CD/DVD Devices Attached</source>
        <translation>Kein CD/DVD-Gerät angeschlossen</translation>
    </message>
    <message>
        <source>No Floppy Devices Attached</source>
        <translation>Kein Diskettenlaufwerk angeschlossen</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the virtual hard disks:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>HDD tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Zeigt die Aktivität der virtuellen Festplatten:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the CD/DVD devices:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>CD/DVD tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Zeigt die Aktivität der CD/DVD-ROM-Laufwerke:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No CD/DVD devices attached&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>CD/DVD tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;Keine CD/DVD-Laufwerke angeschlossen&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the floppy devices:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>FD tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Zeigt die Aktivität der Diskettenlaufwerke:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No floppy devices attached&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>FD tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;Keine Diskettenlaufwerke angeschlossen&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the network interfaces:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>Network adapters tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Zeigt die Aktivität der Netzwerkadapter:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the attached USB devices:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Zeigt die Aktivität der angeschlossenen USB-Geräte:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the machine&apos;s shared folders:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>Shared folders tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Zeigt die Aktivität der gemeinsamen Ordner:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>VBoxDownloaderWgt</name>
    <message>
        <source>The download process has been cancelled by the user.</source>
        <translation>Das Herunterladen wurde vom Nutzer unterbrochen.</translation>
    </message>
</context>
<context>
    <name>VBoxEmptyFileSelector</name>
    <message>
        <source>&amp;Choose...</source>
        <translation>&amp;Auswählen...</translation>
    </message>
</context>
<context>
    <name>VBoxExportApplianceWzd</name>
    <message>
        <source>Select a file to export into</source>
        <translation>Wählt eine Datei aus, in die exportiert werden soll</translation>
    </message>
    <message>
        <source>Open Virtualization Format (%1)</source>
        <translation></translation>
    </message>
    <message>
        <source>Appliance</source>
        <translation></translation>
    </message>
    <message>
        <source>Exporting Appliance ...</source>
        <translation>Exportiere Appliance ...</translation>
    </message>
    <message>
        <source>Appliance Export Wizard</source>
        <translation>Appliance Exportassistent</translation>
    </message>
    <message>
        <source>Welcome to the Appliance Export Wizard!</source>
        <translation>Appliance Exportassistent</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Zurück</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>&amp;Weiter &gt;</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Appliance Export Settings</source>
        <translation>Appliance Exporteinstellungen</translation>
    </message>
    <message>
        <source>&amp;Export &gt;</source>
        <translation>&amp;Exportieren</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This wizard will guide you through the process of exporting an appliance. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Use the &lt;span style=&quot; font-weight:600;&quot;&gt;Next&lt;/span&gt; button to go the next page of the wizard and the &lt;span style=&quot; font-weight:600;&quot;&gt;Back&lt;/span&gt; button to return to the previous page.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Please select the virtual machines that you wish to the appliance. You can select more than one. Please note that these machines have to be turned off before they can be exported.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Dieser Assistent hilft Ihnen beim Export einer Appliance. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wählen Sie &lt;span style=&quot; font-weight:600;&quot;&gt;Weiter&lt;/span&gt;, um auf die nächste Seite zu gelangen und &lt;span style=&quot; font-weight:600;&quot;&gt;Zurück&lt;/span&gt;, um auf die vorherige Seite zurückzukehren.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Bitte wählen Sie die virtuellen Maschinen aus, die in die Appliance exportiert werden sollen. Sie können auch mehrere VMs auswählen. Bitte beachten Sie, dass diese Maschinen ausgeschaltet sein müssen, damit sie exportiert werden können.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Here you can change additional configuration values of the selected virtual machines. You can modify most of the properties shown by double-clicking on the items.</source>
        <translation>Hier können Sie weitere Einstellungen der ausgewählten virtuellen Maschinen ändern. Sie können die meisten der gezeigten Eigenschaften durch Doppelklick auf den Eintrag ändern.</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Standardeinstellungen</translation>
    </message>
    <message>
        <source>Write in legacy OVF 0.9 format for compatibility with other virtualization products.</source>
        <translation>Erstelle das ältere OVF-Format 0.9 für die Kompatibilität mit anderen Virtualisierungsprodukten.</translation>
    </message>
    <message>
        <source>&amp;Write legacy OVF 0.9</source>
        <translation>Erzeuge &amp;Legacy OVF 0.9</translation>
    </message>
    <message>
        <source>Please choose a filename to export the OVF to.</source>
        <translation>Bitte wählen Sie eine Datei für den OVF-Export.</translation>
    </message>
    <message>
        <source>Please complete the additional fields like the username, password and the bucket, and provide a filename for the OVF target.</source>
        <translation>Bitte fügen Sie die Felder für Nutzername, Passwort und Bucket aus und geben Sie einen Dateinamen für den OVF-Export an.</translation>
    </message>
    <message>
        <source>Please complete the additional fields like the username, password, hostname and the bucket, and provide a filename for the OVF target.</source>
        <translation>Bitte fügen Sie die Felder für Nutzername, Passwort, Hostname und Bucket aus und geben Sie einen Dateinamen für den OVF-Export an.</translation>
    </message>
    <message>
        <source>Checking files ...</source>
        <translation>Überprüfe Dateien ...</translation>
    </message>
    <message>
        <source>Removing files ...</source>
        <translation>Lösche Dateien ...</translation>
    </message>
    <message>
        <source>Please specify the target for the OVF export. You can choose between a local file system export, uploading the OVF to the Sun Cloud service or an S3 storage server.</source>
        <translation>Bitte geben Sie eine Datei für den OVF-Export an. Sie können zwischen Abspeichern auf einem lokalen Dateisystem, dem Upload des OVF in den Sun Cloud-Service oder in einen S3-Storage-Server wählen.</translation>
    </message>
    <message>
        <source>&amp;Local Filesystem </source>
        <translation>&amp;Lokales Dateisystem</translation>
    </message>
    <message>
        <source>Sun &amp;Cloud</source>
        <translation>Sun &amp;Cloud</translation>
    </message>
    <message>
        <source>&amp;Simple Storage System (S3)</source>
        <translation>&amp;Simple Storage System (S3)</translation>
    </message>
    <message>
        <source>&amp;Username:</source>
        <translation>&amp;Nutzername:</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation>&amp;Passwort:</translation>
    </message>
    <message>
        <source>&amp;File:</source>
        <translation>&amp;Datei:</translation>
    </message>
    <message>
        <source>&amp;Bucket:</source>
        <translation>&amp;Bucket:</translation>
    </message>
    <message>
        <source>&amp;Hostname:</source>
        <translation>&amp;Hostname:</translation>
    </message>
</context>
<context>
    <name>VBoxFilePathSelectorWidget</name>
    <message>
        <source>&lt;reset to default&gt;</source>
        <translation>&lt;Voreinstellung&gt;</translation>
    </message>
    <message>
        <source>The actual default path value will be displayed after accepting the changes and opening this dialog again.</source>
        <translation>Der Standardpfad wird nach Bestätigen der Änderungen und erneutes Öffnen dieses Dialogs angezeigt.</translation>
    </message>
    <message>
        <source>&lt;not selected&gt;</source>
        <translation>&lt;nicht ausgewählt&gt;</translation>
    </message>
    <message>
        <source>Please use the &lt;b&gt;Other...&lt;/b&gt; item from the drop-down list to select a path.</source>
        <translation>Benutzen Sie &lt;b&gt;Ändern...&lt;/b&gt; zur Auswahl des gewünschten Pfades.</translation>
    </message>
    <message>
        <source>Other...</source>
        <translation>Ändern...</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <source>Opens a dialog to select a different folder.</source>
        <translation>Öffnet einen Dialog zur Auswahl eines anderen Verzeichnisses.</translation>
    </message>
    <message>
        <source>Resets the folder path to the default value.</source>
        <translation>Setzt den Verzeichnispfad auf die Voreinstellung zurück.</translation>
    </message>
    <message>
        <source>Opens a dialog to select a different file.</source>
        <translation>Öffnet einen Dialog um eine andere Datei auszuwählen.</translation>
    </message>
    <message>
        <source>Resets the file path to the default value.</source>
        <translation>Setzt den Pfad auf die Voreinstellung zurück.</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <source>Please type the folder path here.</source>
        <translation>Bitte geben Sie hier das gewünschte Verzeichnis ein.</translation>
    </message>
    <message>
        <source>Please type the file path here.</source>
        <translation>Bitte geben Sie hier den gewünschten Pfad ein.</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsDlg</name>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Input</source>
        <translation>Eingabe</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Update</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <source>VirtualBox - %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Netzwerk</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsGeneral</name>
    <message>
        <source>Displays the path to the default virtual machine folder. This folder is used, if not explicitly specified otherwise, when creating new virtual machines.</source>
        <translation>Zeigt den voreingestellten Pfad für virtuelle Maschinen. Neue virtuelle Maschinen werden standardmäßig in diesem Ordner erzeugt und vorhandene werden hier zuerst gesucht.</translation>
    </message>
    <message>
        <source>Displays the path to the library that provides authentication for Remote Display (VRDP) clients.</source>
        <translation>Zeigt den Pfad der Authentisierungsbibliothek für ferngesteuerte Clients (VRDP).</translation>
    </message>
    <message>
        <source>Default &amp;Hard Disk Folder:</source>
        <translation>Voreingestellter Pfad für &amp;Festplatten:</translation>
    </message>
    <message>
        <source>Default &amp;Machine Folder:</source>
        <translation>Voreingestellter Pfad für &amp;VMs:</translation>
    </message>
    <message>
        <source>V&amp;RDP Authentication Library:</source>
        <translation>V&amp;RDP-Authentisierungsbibliothek:</translation>
    </message>
    <message>
        <source>Displays the path to the default hard disk folder. This folder is used, if not explicitly specified otherwise, when adding existing or creating new virtual hard disks.</source>
        <translation>Zeigt den voreingestellten Pfad für Festplattenabbilder. Neue virtuelle Medien werden standardmäßig in diesem Ordner erzeugt und vorhandene werden hier zuerst gesucht.</translation>
    </message>
    <message>
        <source>When checked, the application will provide an icon with the context menu in the system tray.</source>
        <translation>Aktiviert das Systray-Icon mit einem Kontextmenü.</translation>
    </message>
    <message>
        <source>&amp;Show System Tray Icon</source>
        <translation>&amp;Zeige Systray-Icon</translation>
    </message>
    <message>
        <source>When checked, the Dock Icon will reflect the VM window content in realtime.</source>
        <translation>Aktualisiert das Dock-Icon in Echtzeit.</translation>
    </message>
    <message>
        <source>&amp;Dock Icon Realtime Preview</source>
        <translation>Echtzeit-Vorschau im &amp;Dock-Icon</translation>
    </message>
    <message>
        <source>&amp;Auto show Dock and Menubar in fullscreen</source>
        <translation>&amp;Automatische Anzeige von Dock und Menübar im Vollbildmodus</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsInput</name>
    <message>
        <source>Host &amp;Key:</source>
        <translation>&amp;Host-Taste:</translation>
    </message>
    <message>
        <source>Displays the key used as a Host Key in the VM window. Activate the entry field and press a new Host Key. Note that alphanumeric, cursor movement and editing keys cannot be used.</source>
        <translation>Zeigt die Taste, die als Host-Taste in für VM-Fenster verwendet wird. Aktivieren Sie dieses Feld und betätigen Sie eine neue Host-Taste. Als Host-Taste eignen sich üblicherweise nur Strg, Umsch, Alt usw.</translation>
    </message>
    <message>
        <source>When checked, the keyboard is automatically captured every time the VM window is activated. When the keyboard is captured, all keystrokes (including system ones like Alt-Tab) are directed to the VM.</source>
        <translation>Ist diese Funktion eingeschaltet, wird die Tastatur jedes Mal automatisch gefangen, wenn das VM-Fenster aktiviert wird. In diesem Fall werden alle Tastendrücke (einschließlich Alt-Tab) in die VM umgeleitet.</translation>
    </message>
    <message>
        <source>&amp;Auto Capture Keyboard</source>
        <translation>&amp;Auto-Fangmodus für Tastatur</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsLanguage</name>
    <message>
        <source> (built-in)</source>
        <comment>Language</comment>
        <translation> (eingebaut)</translation>
    </message>
    <message>
        <source>&lt;unavailable&gt;</source>
        <comment>Language</comment>
        <translation>&lt;nicht verfügbar&gt;</translation>
    </message>
    <message>
        <source>&lt;unknown&gt;</source>
        <comment>Author(s)</comment>
        <translation>&lt;unbekannt&gt;</translation>
    </message>
    <message>
        <source>Default</source>
        <comment>Language</comment>
        <translation>Voreingestellt</translation>
    </message>
    <message>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <source>&amp;Interface Language:</source>
        <translation>&amp;Sprache der Nutzeroberfläche:</translation>
    </message>
    <message>
        <source>Lists all available user interface languages. The effective language is written in &lt;b&gt;bold&lt;/b&gt;. Select &lt;i&gt;Default&lt;/i&gt; to reset to the system default language.</source>
        <translation>Zeigt alle verfügbaren Sprachen für die Nutzeroberfläche. Die aktuelle Sprache wird &lt;b&gt;fett&lt;/b&gt; hervorgehoben. Wählen Sie &lt;i&gt;Voreingestellt&lt;/i&gt; um die voreingestellte Sprache Ihres Systems einzustellen.</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Id</source>
        <translation></translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <source>Author(s):</source>
        <translation>Autor(en):</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsNetwork</name>
    <message>
        <source>%1 network</source>
        <comment>&lt;adapter name&gt; network</comment>
        <translation>%1-Netzwerk</translation>
    </message>
    <message>
        <source>host IPv4 address of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>Die Host-IPv4-Adresse von &lt;b&gt;%1&lt;/b&gt; ist ungültig</translation>
    </message>
    <message>
        <source>host IPv4 network mask of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>Die Host-IPv4-Netmaske von &lt;b&gt;%1&lt;/b&gt; ist ungültig</translation>
    </message>
    <message>
        <source>host IPv6 address of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>Die Host-IPv6-Adresse von &lt;b&gt;%1&lt;/b&gt; ist ungültig</translation>
    </message>
    <message>
        <source>DHCP server address of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>Die DHCP-Server-Adresse von &lt;b&gt;%1&lt;/b&gt; ist ungültig</translation>
    </message>
    <message>
        <source>DHCP server network mask of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>Die DHCP-Server-Netzmaske von &lt;b&gt;%1&lt;/b&gt; ist ungültig</translation>
    </message>
    <message>
        <source>DHCP lower address bound of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>Die kleinste IP-Adresse für den DHCP-Server von &lt;b&gt;%1&lt;/b&gt; ist ungültig</translation>
    </message>
    <message>
        <source>DHCP upper address bound of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>Die größte IP-Adresse für den DHCP-Server von &lt;b&gt;%1&lt;/b&gt; ist ungültig</translation>
    </message>
    <message>
        <source>Adapter</source>
        <translation>Adapter</translation>
    </message>
    <message>
        <source>Automatically configured</source>
        <comment>interface</comment>
        <translation>automatisch konfiguriert</translation>
    </message>
    <message>
        <source>Manually configured</source>
        <comment>interface</comment>
        <translation>manuell konfiguriert</translation>
    </message>
    <message>
        <source>IPv4 Address</source>
        <translation>IPv4-Adresse</translation>
    </message>
    <message>
        <source>Not set</source>
        <comment>address</comment>
        <translation>nicht gesetzt</translation>
    </message>
    <message>
        <source>IPv4 Network Mask</source>
        <translation>IPv4 Netzmaske</translation>
    </message>
    <message>
        <source>Not set</source>
        <comment>mask</comment>
        <translation>nicht gesetzt</translation>
    </message>
    <message>
        <source>IPv6 Address</source>
        <translation>IPv6-Adresse</translation>
    </message>
    <message>
        <source>IPv6 Network Mask Length</source>
        <translation>IPv6 Netzmasken-Länge</translation>
    </message>
    <message>
        <source>Not set</source>
        <comment>length</comment>
        <translation>nicht gesetzt</translation>
    </message>
    <message>
        <source>DHCP Server</source>
        <translation>DHCP-Server</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>server</comment>
        <translation>aktiviert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>server</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <source>Network Mask</source>
        <translation>Netzmaske</translation>
    </message>
    <message>
        <source>Lower Bound</source>
        <translation>Kleinste Adresse</translation>
    </message>
    <message>
        <source>Not set</source>
        <comment>bound</comment>
        <translation>nicht gesetzt</translation>
    </message>
    <message>
        <source>Upper Bound</source>
        <translation>Größte Adresse</translation>
    </message>
    <message>
        <source>&amp;Add host-only network</source>
        <translation>Host-only Netzwerk &amp;hinzufügen</translation>
    </message>
    <message>
        <source>&amp;Remove host-only network</source>
        <translation>Host-only Netzwerk &amp;entfernen</translation>
    </message>
    <message>
        <source>&amp;Edit host-only network</source>
        <translation>Host-only Netzwerk &amp;ändern</translation>
    </message>
    <message>
        <source>Performing</source>
        <comment>creating/removing host-only network</comment>
        <translation>Ausführung</translation>
    </message>
    <message>
        <source>&amp;Host-only Networks:</source>
        <translation>&amp;Host-only Netzwerke:</translation>
    </message>
    <message>
        <source>Lists all available host-only networks.</source>
        <translation>Zeigt die verfügbaren host-only Netzwerke.</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsNetworkDetails</name>
    <message>
        <source>Host-only Network Details</source>
        <translation>Einstellungen für Host-only Netzwerk</translation>
    </message>
    <message>
        <source>&amp;Adapter</source>
        <translation>&amp;Adapter</translation>
    </message>
    <message>
        <source>Manual &amp;Configuration</source>
        <translation>Manuelle &amp;Konfiguration</translation>
    </message>
    <message>
        <source>Use manual configuration for this host-only network adapter.</source>
        <translation>Erlaubt die manuelle Konfiguration dieses Host-only Netzwerkadapters.</translation>
    </message>
    <message>
        <source>&amp;IPv4 Address:</source>
        <translation>&amp;IPv4-Adresse:</translation>
    </message>
    <message>
        <source>Displays the host IPv4 address for this adapter.</source>
        <translation>Zeigt die IPv4-Adresse für diesen Adapter.</translation>
    </message>
    <message>
        <source>IPv4 Network &amp;Mask:</source>
        <translation>IPv4-Netz&amp;maske:</translation>
    </message>
    <message>
        <source>Displays the host IPv4 network mask for this adapter.</source>
        <translation>Zeigt die IPv4-Netzmaske für diesen Adapter.</translation>
    </message>
    <message>
        <source>I&amp;Pv6 Address:</source>
        <translation>I&amp;Pv6-Adresse:</translation>
    </message>
    <message>
        <source>Displays the host IPv6 address for this adapter if IPv6 is supported.</source>
        <translation>Zeigt die IPv6-Adresse für diesen Adapter falls IPv6 unterstützt wird.</translation>
    </message>
    <message>
        <source>IPv6 Network Mask &amp;Length:</source>
        <translation>IPv6 Netzmasken-&amp;Länge:</translation>
    </message>
    <message>
        <source>Displays the host IPv6 network mask prefix length for this adapter if IPv6 is supported.</source>
        <translation>Zeigt die IPv6 Netzmasken-Prefixlänge für diesen Adapter falls IPv6 unterstützt wird.</translation>
    </message>
    <message>
        <source>&amp;DHCP Server</source>
        <translation>&amp;DHCP-Server</translation>
    </message>
    <message>
        <source>&amp;Enable Server</source>
        <translation>Server &amp;aktivieren</translation>
    </message>
    <message>
        <source>Indicates whether the DHCP Server is enabled on machine startup or not.</source>
        <translation>Zeigt an, ob der DHCP-Server beim Start der Maschine aktiviert wird oder nicht.</translation>
    </message>
    <message>
        <source>Server Add&amp;ress:</source>
        <translation>Server-A&amp;dresse:</translation>
    </message>
    <message>
        <source>Displays the address of the DHCP server servicing the network associated with this host-only adapter.</source>
        <translation>Zeigt die Adresse des DHCP-Servers der die IP-Adressen für das Netzwerk an diesem Hostadapter verwaltet.</translation>
    </message>
    <message>
        <source>Server &amp;Mask:</source>
        <translation>Server-&amp;Maske:</translation>
    </message>
    <message>
        <source>Displays the network mask of the DHCP server servicing the network associated with this host-only adapter.</source>
        <translation>Zeigt die Netzmaske des DHCP-Servers, der die IP-Adressen an diesem Host-only Netzwerkadapter verwaltet.</translation>
    </message>
    <message>
        <source>&amp;Lower Address Bound:</source>
        <translation>&amp;Kleinste Adresse:</translation>
    </message>
    <message>
        <source>Displays the lower address bound offered by the DHCP server servicing the network associated with this host-only adapter.</source>
        <translation>Zeigt die kleinste IP-Adresse, die der DHCP-Server für das Netzwerk an diesem Host-only Netzwerkadapter verwaltet.</translation>
    </message>
    <message>
        <source>&amp;Upper Address Bound:</source>
        <translation>&amp;Größte Adresse:</translation>
    </message>
    <message>
        <source>Displays the upper address bound offered by the DHCP server servicing the network associated with this host-only adapter.</source>
        <translation>Zeigt die größte IP-Adresse, die der DHCP-Server für das Netzwerk an diesem Host-only Netzwerkadapter verwaltet.</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsUpdate</name>
    <message>
        <source>When checked, the application will periodically connect to the VirtualBox website and check whether a new VirtualBox version is available.</source>
        <translation>Durch Aktivierung erlauben Sie eine periodische Abfrage der VirtualBox-Webseite nach einer neuen Version von VirtualBox.</translation>
    </message>
    <message>
        <source>&amp;Check for updates</source>
        <translation>Nach &amp;Updates suchen</translation>
    </message>
    <message>
        <source>&amp;Once per:</source>
        <translation>&amp;Einmal pro:</translation>
    </message>
    <message>
        <source>Specifies how often the new version check should be performed. Note that if you want to completely disable this check, just clear the above check box.</source>
        <translation>Gibt an, wie oft die Versionsüberprüfung ausgeführt werden soll. Um den Test zu deaktivieren brauchen Sie nur die obige Checkbox deaktivieren.</translation>
    </message>
    <message>
        <source>Next Check:</source>
        <translation>Nächste Überprüfung:</translation>
    </message>
    <message>
        <source>Check for:</source>
        <translation>Überprüfen auf:</translation>
    </message>
    <message>
        <source>&lt;p&gt;Choose this if you only wish to be notified about stable updates to VirtualBox.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie diesen Punkt, wenn Sie über neue stabile Versionen von VirtualBox benachrichtigt werden wollen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Stable release versions</source>
        <translation>&amp;Stabile Versionen</translation>
    </message>
    <message>
        <source>&lt;p&gt;Choose this if you wish to be notified about all new VirtualBox releases.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie diese Möglichkeit, um über alle neuen Versionen von VirtualBox benachrichtigt zu werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;All new releases</source>
        <translation>&amp;Alle neuen Versionen</translation>
    </message>
    <message>
        <source>&lt;p&gt;Choose this to be notified about all new VirtualBox releases and pre-release versions of VirtualBox.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie diese Möglichkeit, um über alle neuen Versionen von VirtualBox inklusive Vorab-Versionen benachrichtigt zu werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>All new releases and &amp;pre-releases</source>
        <translation>Alle Versionen inkl. &amp;Vorab-Versionen</translation>
    </message>
</context>
<context>
    <name>VBoxGlobal</name>
    <message>
        <source>Unknown device %1:%2</source>
        <comment>USB device details</comment>
        <translation>Unbekanntes Gerät %1:%2</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Vendor ID: %1&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Product ID: %2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Revision: %3&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;nobr&gt;Hersteller ID: %1&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Produkt ID: %2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Revision: %3&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;Serial No. %1&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;Seriennr. %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;State: %1&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;Zustand: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Name</source>
        <comment>details report</comment>
        <translation>Name</translation>
    </message>
    <message>
        <source>OS Type</source>
        <comment>details report</comment>
        <translation>Gastbetriebssystem</translation>
    </message>
    <message>
        <source>Base Memory</source>
        <comment>details report</comment>
        <translation>Hauptspeicher</translation>
    </message>
    <message>
        <source>General</source>
        <comment>details report</comment>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Video Memory</source>
        <comment>details report</comment>
        <translation>Grafikspeicher</translation>
    </message>
    <message>
        <source>Boot Order</source>
        <comment>details report</comment>
        <translation>Bootreihenfolge</translation>
    </message>
    <message>
        <source>ACPI</source>
        <comment>details report</comment>
        <translation>ACPI</translation>
    </message>
    <message>
        <source>IO APIC</source>
        <comment>details report</comment>
        <translation></translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (ACPI)</comment>
        <translation>aktiviert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (ACPI)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (IO APIC)</comment>
        <translation>aktiviert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (IO APIC)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Host Driver</source>
        <comment>details report (audio)</comment>
        <translation>Host-Treiber</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (audio)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Audio</source>
        <comment>details report</comment>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Bridged adapter, %1</source>
        <comment>details report (network)</comment>
        <translation>Netzwerkbrücke, %1</translation>
    </message>
    <message>
        <source>Host-only adapter, &apos;%1&apos;</source>
        <comment>details report (network)</comment>
        <translation>Host-only Adapter, &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Adapter %1</source>
        <comment>details report (network)</comment>
        <translation>Adapter %1</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (network)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Network</source>
        <comment>details report</comment>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <source>Device Filters</source>
        <comment>details report (USB)</comment>
        <translation>Gerätefilter</translation>
    </message>
    <message>
        <source>%1 (%2 active)</source>
        <comment>details report (USB)</comment>
        <translation>%1 (%2 aktiv)</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (USB)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Powered Off</source>
        <comment>MachineState</comment>
        <translation>ausgeschaltet</translation>
    </message>
    <message>
        <source>Saved</source>
        <comment>MachineState</comment>
        <translation>gesichert</translation>
    </message>
    <message>
        <source>Aborted</source>
        <comment>MachineState</comment>
        <translation>abgebrochen</translation>
    </message>
    <message>
        <source>Running</source>
        <comment>MachineState</comment>
        <translation>wird ausgeführt</translation>
    </message>
    <message>
        <source>Paused</source>
        <comment>MachineState</comment>
        <translation>angehalten</translation>
    </message>
    <message>
        <source>Starting</source>
        <comment>MachineState</comment>
        <translation>wird gestartet</translation>
    </message>
    <message>
        <source>Stopping</source>
        <comment>MachineState</comment>
        <translation>wird anhalten</translation>
    </message>
    <message>
        <source>Saving</source>
        <comment>MachineState</comment>
        <translation>wird gesichert</translation>
    </message>
    <message>
        <source>Restoring</source>
        <comment>MachineState</comment>
        <translation>wird wiederhergestellt</translation>
    </message>
    <message>
        <source>Closed</source>
        <comment>SessionState</comment>
        <translation>Geschlossen</translation>
    </message>
    <message>
        <source>Open</source>
        <comment>SessionState</comment>
        <translation>Geöffnet</translation>
    </message>
    <message>
        <source>Spawning</source>
        <comment>SessionState</comment>
        <translation>Erzeugen</translation>
    </message>
    <message>
        <source>Closing</source>
        <comment>SessionState</comment>
        <translation>Schließe</translation>
    </message>
    <message>
        <source>None</source>
        <comment>DeviceType</comment>
        <translation>Kein</translation>
    </message>
    <message>
        <source>Floppy</source>
        <comment>DeviceType</comment>
        <translation>Diskette</translation>
    </message>
    <message>
        <source>CD/DVD-ROM</source>
        <comment>DeviceType</comment>
        <translation>CD/DVD-ROM</translation>
    </message>
    <message>
        <source>Hard Disk</source>
        <comment>DeviceType</comment>
        <translation>Platte</translation>
    </message>
    <message>
        <source>Network</source>
        <comment>DeviceType</comment>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <source>Normal</source>
        <comment>DiskType</comment>
        <translation>Normal</translation>
    </message>
    <message>
        <source>Immutable</source>
        <comment>DiskType</comment>
        <translation>Nicht veränderlich</translation>
    </message>
    <message>
        <source>Writethrough</source>
        <comment>DiskType</comment>
        <translation>Durchschreibend</translation>
    </message>
    <message>
        <source>Null</source>
        <comment>VRDPAuthType</comment>
        <translation></translation>
    </message>
    <message>
        <source>External</source>
        <comment>VRDPAuthType</comment>
        <translation>Extern</translation>
    </message>
    <message>
        <source>Guest</source>
        <comment>VRDPAuthType</comment>
        <translation>Gast</translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>USBFilterActionType</comment>
        <translation>Ignorieren</translation>
    </message>
    <message>
        <source>Hold</source>
        <comment>USBFilterActionType</comment>
        <translation>Halten</translation>
    </message>
    <message>
        <source>Null Audio Driver</source>
        <comment>AudioDriverType</comment>
        <translation>Null Audiotreiber</translation>
    </message>
    <message>
        <source>Windows Multimedia</source>
        <comment>AudioDriverType</comment>
        <translation>Windows Multimedia</translation>
    </message>
    <message>
        <source>OSS Audio Driver</source>
        <comment>AudioDriverType</comment>
        <translation>OSS-Audio-Treiber</translation>
    </message>
    <message>
        <source>ALSA Audio Driver</source>
        <comment>AudioDriverType</comment>
        <translation>ALSA-Audio-Treiber</translation>
    </message>
    <message>
        <source>Windows DirectSound</source>
        <comment>AudioDriverType</comment>
        <translation></translation>
    </message>
    <message>
        <source>CoreAudio</source>
        <comment>AudioDriverType</comment>
        <translation></translation>
    </message>
    <message>
        <source>Not attached</source>
        <comment>NetworkAttachmentType</comment>
        <translation>nicht angeschlossen</translation>
    </message>
    <message>
        <source>NAT</source>
        <comment>NetworkAttachmentType</comment>
        <translation></translation>
    </message>
    <message>
        <source>Bridged Adapter</source>
        <comment>NetworkAttachmentType</comment>
        <translation>Netzwerkbrücke</translation>
    </message>
    <message>
        <source>Internal Network</source>
        <comment>NetworkAttachmentType</comment>
        <translation>Internes Netzwerk</translation>
    </message>
    <message>
        <source>Host-only Adapter</source>
        <comment>NetworkAttachmentType</comment>
        <translation>Host-only Adapter</translation>
    </message>
    <message>
        <source>Not supported</source>
        <comment>USBDeviceState</comment>
        <translation>nicht unterstützt</translation>
    </message>
    <message>
        <source>Unavailable</source>
        <comment>USBDeviceState</comment>
        <translation>nicht verfügbar</translation>
    </message>
    <message>
        <source>Busy</source>
        <comment>USBDeviceState</comment>
        <translation>beschäftigt</translation>
    </message>
    <message>
        <source>Available</source>
        <comment>USBDeviceState</comment>
        <translation>verfügbar</translation>
    </message>
    <message>
        <source>Held</source>
        <comment>USBDeviceState</comment>
        <translation>gehalten</translation>
    </message>
    <message>
        <source>Captured</source>
        <comment>USBDeviceState</comment>
        <translation>gefangen</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>ClipboardType</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Host To Guest</source>
        <comment>ClipboardType</comment>
        <translation>Host zu Gast</translation>
    </message>
    <message>
        <source>Guest To Host</source>
        <comment>ClipboardType</comment>
        <translation>Gast zu Host</translation>
    </message>
    <message>
        <source>Bidirectional</source>
        <comment>ClipboardType</comment>
        <translation>Bidirektional</translation>
    </message>
    <message>
        <source>Port %1</source>
        <comment>details report (serial ports)</comment>
        <translation>Port %1</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (serial ports)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Serial Ports</source>
        <comment>details report</comment>
        <translation>Serielle Schnittstellen</translation>
    </message>
    <message>
        <source>USB</source>
        <comment>details report</comment>
        <translation>USB</translation>
    </message>
    <message>
        <source>Shared Folders</source>
        <comment>details report (shared folders)</comment>
        <translation>Gemeinsame Ordner</translation>
    </message>
    <message>
        <source>None</source>
        <comment>details report (shared folders)</comment>
        <translation>keine</translation>
    </message>
    <message>
        <source>Shared Folders</source>
        <comment>details report</comment>
        <translation>Gemeinsame Ordner</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <comment>PortMode</comment>
        <translation>nicht verbunden</translation>
    </message>
    <message>
        <source>Host Pipe</source>
        <comment>PortMode</comment>
        <translation>Host-Pipe</translation>
    </message>
    <message>
        <source>Host Device</source>
        <comment>PortMode</comment>
        <translation>Host-Schnittstelle</translation>
    </message>
    <message>
        <source>User-defined</source>
        <comment>serial port</comment>
        <translation>benutzerdefiniert</translation>
    </message>
    <message>
        <source>VT-x/AMD-V</source>
        <comment>details report</comment>
        <translation>VT-x/AMD-V</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (VT-x/AMD-V)</comment>
        <translation>aktiviert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (VT-x/AMD-V)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Controller</source>
        <comment>details report (audio)</comment>
        <translation>Controller</translation>
    </message>
    <message>
        <source>Port %1</source>
        <comment>details report (parallel ports)</comment>
        <translation>Port %1</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (parallel ports)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Parallel Ports</source>
        <comment>details report</comment>
        <translation>Parallel-Ports</translation>
    </message>
    <message>
        <source>PulseAudio</source>
        <comment>AudioDriverType</comment>
        <translation>PulseAudio</translation>
    </message>
    <message>
        <source>ICH AC97</source>
        <comment>AudioControllerType</comment>
        <translation></translation>
    </message>
    <message>
        <source>SoundBlaster 16</source>
        <comment>AudioControllerType</comment>
        <translation></translation>
    </message>
    <message>
        <source>PAE/NX</source>
        <comment>details report</comment>
        <translation></translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (PAE/NX)</comment>
        <translation>aktiviert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (PAE/NX)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>USB</source>
        <comment>DeviceType</comment>
        <translation>USB</translation>
    </message>
    <message>
        <source>Shared Folder</source>
        <comment>DeviceType</comment>
        <translation>Gemeinsame Ordner</translation>
    </message>
    <message>
        <source>IDE</source>
        <comment>StorageBus</comment>
        <translation></translation>
    </message>
    <message>
        <source>SATA</source>
        <comment>StorageBus</comment>
        <translation></translation>
    </message>
    <message>
        <source>Primary</source>
        <comment>StorageBusChannel</comment>
        <translation>Primärer</translation>
    </message>
    <message>
        <source>Secondary</source>
        <comment>StorageBusChannel</comment>
        <translation>Sekundärer</translation>
    </message>
    <message>
        <source>Master</source>
        <comment>StorageBusDevice</comment>
        <translation>Master</translation>
    </message>
    <message>
        <source>Slave</source>
        <comment>StorageBusDevice</comment>
        <translation>Slave</translation>
    </message>
    <message>
        <source>Port %1</source>
        <comment>StorageBusChannel</comment>
        <translation>Port %1</translation>
    </message>
    <message>
        <source>Solaris Audio</source>
        <comment>AudioDriverType</comment>
        <translation></translation>
    </message>
    <message>
        <source>PCnet-PCI II (Am79C970A)</source>
        <comment>NetworkAdapterType</comment>
        <translation></translation>
    </message>
    <message>
        <source>PCnet-FAST III (Am79C973)</source>
        <comment>NetworkAdapterType</comment>
        <translation></translation>
    </message>
    <message>
        <source>Intel PRO/1000 MT Desktop (82540EM)</source>
        <comment>NetworkAdapterType</comment>
        <translation></translation>
    </message>
    <message>
        <source>Intel PRO/1000 T Server (82543GC)</source>
        <comment>NetworkAdapterType</comment>
        <translation></translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Vendor ID: %1&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;Hersteller-ID: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Product ID: %2&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;Produkt-ID: %2&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Revision: %3&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;Revision: %3&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Product: %4&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;Produkt: %4&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Manufacturer: %5&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;Hersteller: %5&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Serial No.: %1&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;Seriennr: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Port: %1&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;Port: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;State: %1&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;Zustand: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Adapter %1</source>
        <comment>network</comment>
        <translation>Adapter %1</translation>
    </message>
    <message>
        <source>Checking...</source>
        <comment>medium</comment>
        <translation>Überprüfen...</translation>
    </message>
    <message>
        <source>Inaccessible</source>
        <comment>medium</comment>
        <translation>Nicht zugreifbar</translation>
    </message>
    <message>
        <source>3D Acceleration</source>
        <comment>details report</comment>
        <translation>3D-Beschleunigung</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (3D Acceleration)</comment>
        <translation>aktiviert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (3D Acceleration)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Setting Up</source>
        <comment>MachineState</comment>
        <translation>wird geändert</translation>
    </message>
    <message>
        <source>Differencing</source>
        <comment>DiskType</comment>
        <translation>Differentiell</translation>
    </message>
    <message>
        <source>Nested Paging</source>
        <comment>details report</comment>
        <translation>Nested Paging</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (Nested Paging)</comment>
        <translation>aktiviert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (Nested Paging)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Internal network, &apos;%1&apos;</source>
        <comment>details report (network)</comment>
        <translation>Internes Netzwerk, &apos;%1&apos;</translation>
    </message>
    <message>
        <source>SCSI</source>
        <comment>StorageBus</comment>
        <translation>SCSI</translation>
    </message>
    <message>
        <source>PIIX3</source>
        <comment>StorageControllerType</comment>
        <translation>PIIX3</translation>
    </message>
    <message>
        <source>PIIX4</source>
        <comment>StorageControllerType</comment>
        <translation>PIIX4</translation>
    </message>
    <message>
        <source>ICH6</source>
        <comment>StorageControllerType</comment>
        <translation>ICH6</translation>
    </message>
    <message>
        <source>AHCI</source>
        <comment>StorageControllerType</comment>
        <translation>AHCI</translation>
    </message>
    <message>
        <source>Lsilogic</source>
        <comment>StorageControllerType</comment>
        <translation></translation>
    </message>
    <message>
        <source>BusLogic</source>
        <comment>StorageControllerType</comment>
        <translation></translation>
    </message>
    <message>
        <source>Intel PRO/1000 MT Server (82545EM)</source>
        <comment>NetworkAdapterType</comment>
        <translation></translation>
    </message>
    <message>
        <source>&lt;nobr&gt;%1 MB&lt;/nobr&gt;</source>
        <comment>details report</comment>
        <translation>&lt;nobr&gt;%1 MB&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Processor(s)</source>
        <comment>details report</comment>
        <translation>Prozessor(en)</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;%1&lt;/nobr&gt;</source>
        <comment>details report</comment>
        <translation>&lt;nobr&gt;%1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>System</source>
        <comment>details report</comment>
        <translation>System</translation>
    </message>
    <message>
        <source>Remote Display Server Port</source>
        <comment>details report (VRDP Server)</comment>
        <translation>Port für Fernsteuerung</translation>
    </message>
    <message>
        <source>Remote Display Server</source>
        <comment>details report (VRDP Server)</comment>
        <translation>Fernsteuerung</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (VRDP Server)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Display</source>
        <comment>details report</comment>
        <translation>Anzeige</translation>
    </message>
    <message>
        <source>Raw File</source>
        <comment>PortMode</comment>
        <translation>Datei</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (2D Video Acceleration)</comment>
        <translation>aktiviert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (2D Video Acceleration)</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>2D Video Acceleration</source>
        <comment>details report</comment>
        <translation>2D-Video-Beschleunigung</translation>
    </message>
    <message>
        <source>Not Attached</source>
        <comment>details report (Storage)</comment>
        <translation>nicht angeschlossen</translation>
    </message>
    <message>
        <source>Storage</source>
        <comment>details report</comment>
        <translation>Massenspeicher</translation>
    </message>
    <message>
        <source>Teleported</source>
        <comment>MachineState</comment>
        <translation>teleportiert</translation>
    </message>
    <message>
        <source>Guru Meditation</source>
        <comment>MachineState</comment>
        <translation>Guru Meditation</translation>
    </message>
    <message>
        <source>Teleporting</source>
        <comment>MachineState</comment>
        <translation>wird teleportiert</translation>
    </message>
    <message>
        <source>Taking Live Snapshot</source>
        <comment>MachineState</comment>
        <translation>Live Snapshot</translation>
    </message>
    <message>
        <source>Teleporting Paused VM</source>
        <comment>MachineState</comment>
        <translation>angehalten teleportieren</translation>
    </message>
    <message>
        <source>Restoring Snapshot</source>
        <comment>MachineState</comment>
        <translation>wiederherstellen</translation>
    </message>
    <message>
        <source>Deleting Snapshot</source>
        <comment>MachineState</comment>
        <translation>Sicherungspunkt löschen</translation>
    </message>
    <message>
        <source>Floppy</source>
        <comment>StorageBus</comment>
        <translation>Diskette</translation>
    </message>
    <message>
        <source>Device %1</source>
        <comment>StorageBusDevice</comment>
        <translation>Gerät %1</translation>
    </message>
    <message>
        <source>IDE Primary Master</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>Primärer Master</translation>
    </message>
    <message>
        <source>IDE Primary Slave</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>Primärer Slave</translation>
    </message>
    <message>
        <source>IDE Secondary Master</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>Sekundärer Master</translation>
    </message>
    <message>
        <source>IDE Secondary Slave</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>Sekundärer Slave</translation>
    </message>
    <message>
        <source>SATA Port %1</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>SATA-Port %1</translation>
    </message>
    <message>
        <source>SCSI Port %1</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>SCSI-Port %1</translation>
    </message>
    <message>
        <source>Floppy Device %1</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>Diskettenlaufwerk %1</translation>
    </message>
    <message>
        <source>Paravirtualized Network (virtio-net)</source>
        <comment>NetworkAdapterType</comment>
        <translation>Paravirtualisiertes Netzwerk (virtio-net)</translation>
    </message>
    <message>
        <source>I82078</source>
        <comment>StorageControllerType</comment>
        <translation></translation>
    </message>
    <message>
        <source>Empty</source>
        <comment>medium</comment>
        <translation>leer</translation>
    </message>
    <message>
        <source>Host Drive &apos;%1&apos;</source>
        <comment>medium</comment>
        <translation>Hostlaufwerk &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Host Drive %1 (%2)</source>
        <comment>medium</comment>
        <translation>Hostlaufwerk %1 (%2)</translation>
    </message>
    <message>
        <source>&lt;p style=white-space:pre&gt;Type (Format):  %1 (%2)&lt;/p&gt;</source>
        <comment>medium</comment>
        <translation>&lt;p style=white-space:pre&gt;Type (Format):  %1 (%2)&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Attached to:  %1&lt;/p&gt;</source>
        <comment>image</comment>
        <translation>&lt;p&gt;Angeschlossen an: %1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;i&gt;Not Attached&lt;/i&gt;</source>
        <comment>image</comment>
        <translation>&lt;i&gt;Nicht angeschlossen&lt;/i&gt;</translation>
    </message>
    <message>
        <source>&lt;i&gt;Checking accessibility...&lt;/i&gt;</source>
        <comment>medium</comment>
        <translation>&lt;i&gt;Überprüfe Zugriffsrecht...&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Failed to check media accessibility.</source>
        <comment>medium</comment>
        <translation>Die Zugriffsrechte des Mediums konnten nicht ermittelt werden.</translation>
    </message>
    <message>
        <source>&lt;b&gt;No medium selected&lt;/b&gt;</source>
        <comment>medium</comment>
        <translation>&lt;b&gt;Kein Medium ausgewählt&lt;/b&gt;</translation>
    </message>
    <message>
        <source>You can also change this while the machine is running.</source>
        <translation>Dieses Medium kann auch während der Ausführung gewechselt werden.</translation>
    </message>
    <message>
        <source>&lt;b&gt;No media available&lt;/b&gt;</source>
        <comment>medium</comment>
        <translation>&lt;b&gt;Kein Medium verfügbar&lt;/b&gt;</translation>
    </message>
    <message>
        <source>You can create media images using the virtual media manager.</source>
        <translation>Sie können Medienabbilder im Manager für virtuelle Medien erzeugen.</translation>
    </message>
    <message>
        <source>Attaching this hard disk will be performed indirectly using a newly created differencing hard disk.</source>
        <comment>medium</comment>
        <translation>Diese Festplatte wird indirekt mittels einer neu erzeugten Differenzdatei angeschlossen.</translation>
    </message>
    <message>
        <source>Some of the media in this hard disk chain are inaccessible. Please use the Virtual Media Manager in &lt;b&gt;Show Differencing Hard Disks&lt;/b&gt; mode to inspect these media.</source>
        <comment>medium</comment>
        <translation>Einige der zu dieser virtuellen Festplatte gehörigen Dateien sind nicht zugreifbar. Bitte verwenden Sie den Manager für virtuelle Medien im Modus &lt;b&gt;Zeige Differenz-Abbilder&lt;/b&gt; um diese Dateien anzuzeigen.</translation>
    </message>
    <message>
        <source>This base hard disk is indirectly attached using the following differencing hard disk:</source>
        <comment>medium</comment>
        <translation>Diese Basisfestplatte ist indirekt über die folgenden Differenzfestplatten eingebunden:</translation>
    </message>
    <message numerus="yes">
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n Jahr</numerusform>
            <numerusform>%n Jahre</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n month(s)</source>
        <translation>
            <numerusform>%n Monat</numerusform>
            <numerusform>%n Monate</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n Tag</numerusform>
            <numerusform>%n Tage</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n Stunde</numerusform>
            <numerusform>%n Stunden</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n Minute</numerusform>
            <numerusform>%n Minuten</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n Sekunde</numerusform>
            <numerusform>%n Sekunden</numerusform>
        </translation>
    </message>
    <message>
        <source>(CD/DVD)</source>
        <translation>(CD/DVD)</translation>
    </message>
</context>
<context>
    <name>VBoxGlobalSettings</name>
    <message>
        <source>&apos;%1 (0x%2)&apos; is an invalid host key code.</source>
        <translation>&apos;%1 (0x%2)&apos; ist kein gültiger Tastencode.</translation>
    </message>
    <message>
        <source>The value &apos;%1&apos; of the key &apos;%2&apos; doesn&apos;t match the regexp constraint &apos;%3&apos;.</source>
        <translation>Der Wert &apos;%1&apos; des Schlüssels &apos;%2&apos; passt nicht auf den regulären Ausdruck &apos;%3&apos;.</translation>
    </message>
    <message>
        <source>Cannot delete the key &apos;%1&apos;.</source>
        <translation>Der Schlüssel &apos;%1&apos; konnte nicht gelöscht werden.</translation>
    </message>
</context>
<context>
    <name>VBoxHelpButton</name>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
</context>
<context>
    <name>VBoxImportApplianceWgt</name>
    <message>
        <source>Importing Appliance ...</source>
        <translation>Appliance importieren ...</translation>
    </message>
    <message>
        <source>Reading Appliance ...</source>
        <translation>Appliance lesen ...</translation>
    </message>
</context>
<context>
    <name>VBoxImportApplianceWzd</name>
    <message>
        <source>Select an appliance to import</source>
        <translation>Auswahl einer Appliance für den Import</translation>
    </message>
    <message>
        <source>Open Virtualization Format (%1)</source>
        <translation>Open Virtualization Format (%1)</translation>
    </message>
    <message>
        <source>Appliance Import Wizard</source>
        <translation>Appliance importieren</translation>
    </message>
    <message>
        <source>Welcome to the Appliance Import Wizard!</source>
        <translation>Appliance importieren</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This wizard will guide you through importing an appliance. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Use the &lt;span style=&quot; font-weight:600;&quot;&gt;Next&lt;/span&gt; button to go the next page of the wizard and the &lt;span style=&quot; font-weight:600;&quot;&gt;Back&lt;/span&gt; button to return to the previous page.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;VirtualBox currently supports importing appliances saved in the Open Virtualization Format (OVF). To continue, select the file to import below:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Dieser Assistent begleitet Sie beim Import einer Appliance. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wählen Sie &lt;span style=&quot; font-weight:600;&quot;&gt;Weiter&lt;/span&gt;, um auf die nächste Seite zu gelangen und &lt;span style=&quot; font-weight:600;&quot;&gt;Zurück&lt;/span&gt;, um auf die vorherige Seite zurückzukehren.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Für den Import einer Appliance müssen Sie zuerst eine Datei mit der Beschreibung dieser Appliance auswählen. VirtualBox unterstützt im Moment das Open Virtualization Format (OVF). Wählen Sie die zu importierende Datei aus:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Zurück</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>&amp;Weiter &gt;</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Appliance Import Settings</source>
        <translation>Appliance Import-Einstellungen</translation>
    </message>
    <message>
        <source>These are the virtual machines contained in the appliance and the suggested settings of the imported VirtualBox machines. You can change many of the properties shown by double-clicking on the items and disable others using the check boxes below.</source>
        <translation>Dies sind die in der Appliance beschriebenen virtuellen Maschinen mit den entsprechenden Abbildungen für den Import in VirtualBox. Sie können Änderungen an vielen dieser Einstellungen mittels Doppelklick bzw. duch Auswahl der entsprechenden Checkbox ändern.</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Standardeinstellungen</translation>
    </message>
    <message>
        <source>&amp;Import &gt;</source>
        <translation>&amp;Importieren &gt;</translation>
    </message>
</context>
<context>
    <name>VBoxImportLicenseViewer</name>
    <message>
        <source>&lt;b&gt;The virtual system &quot;%1&quot; requires that you agree to the terms and conditions of the software license agreement shown below.&lt;/b&gt;&lt;br /&gt;&lt;br /&gt;Click &lt;b&gt;Agree&lt;/b&gt; to continue or click &lt;b&gt;Disagree&lt;/b&gt; to cancel the import.</source>
        <translation>&lt;b&gt;Das virtuelle System &quot;%1&quot; verlangt, dass Sie die Bedingungen der unten gezeigten Software-Lizenz anerkennen.&lt;b&gt;&lt;br/&gt;&lt;br/&gt;Wählen Sie &lt;b&gt;Zustimmen&lt;/b&gt;, um fortzufahren oder &lt;b&gt;Ablehnen&lt;/b&gt;, um den Import abzubrechen.</translation>
    </message>
    <message>
        <source>Software License Agreement</source>
        <translation>Software-Lizenz</translation>
    </message>
    <message>
        <source>&amp;Disagree</source>
        <translation>&amp;Ablehnen</translation>
    </message>
    <message>
        <source>&amp;Agree</source>
        <translation>&amp;Zustimmen</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation>&amp;Drucken...</translation>
    </message>
    <message>
        <source>&amp;Save...</source>
        <translation>&amp;Abspeichern...</translation>
    </message>
    <message>
        <source>Text (*.txt)</source>
        <translation>Text (*.txt)</translation>
    </message>
    <message>
        <source>Save license to file...</source>
        <translation>Lizenztext speichern unter ...</translation>
    </message>
</context>
<context>
    <name>VBoxLicenseViewer</name>
    <message>
        <source>VirtualBox License</source>
        <translation></translation>
    </message>
    <message>
        <source>I &amp;Agree</source>
        <translation>&amp;Zustimmen</translation>
    </message>
    <message>
        <source>I &amp;Disagree</source>
        <translation>&amp;Ablehnen</translation>
    </message>
</context>
<context>
    <name>VBoxLineTextEdit</name>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Ändern</translation>
    </message>
</context>
<context>
    <name>VBoxLogSearchPanel</name>
    <message>
        <source>Close the search panel</source>
        <translation>Schließt das Suchfeld</translation>
    </message>
    <message>
        <source>Find </source>
        <translation>Suche</translation>
    </message>
    <message>
        <source>Enter a search string here</source>
        <translation>Geben Sie den Suchtext hier ein</translation>
    </message>
    <message>
        <source>&amp;Previous</source>
        <translation>&amp;Rückwärts</translation>
    </message>
    <message>
        <source>Search for the previous occurrence of the string</source>
        <translation>Durchsucht den Text rückwärts</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation>&amp;Vorwärts</translation>
    </message>
    <message>
        <source>Search for the next occurrence of the string</source>
        <translation>Durchsucht den Text vorwärts</translation>
    </message>
    <message>
        <source>C&amp;ase Sensitive</source>
        <translation>Unterscheidet &amp;Groß- und Kleinschreibung</translation>
    </message>
    <message>
        <source>Perform case sensitive search (when checked)</source>
        <translation>Groß- und Kleinschreibung berücksichtigen (wenn ausgewählt) oder ignorieren</translation>
    </message>
    <message>
        <source>String not found</source>
        <translation>Suchtext nicht gefunden</translation>
    </message>
</context>
<context>
    <name>VBoxMediaManagerDlg</name>
    <message>
        <source>&amp;Actions</source>
        <translation>&amp;Aktionen</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation>&amp;Neu...</translation>
    </message>
    <message>
        <source>&amp;Add...</source>
        <translation>&amp;Hinzufügen...</translation>
    </message>
    <message>
        <source>R&amp;emove</source>
        <translation>&amp;Entfernen</translation>
    </message>
    <message>
        <source>Re&amp;lease</source>
        <translation>&amp;Freigeben</translation>
    </message>
    <message>
        <source>Re&amp;fresh</source>
        <translation>&amp;Aktualisieren</translation>
    </message>
    <message>
        <source>Create a new virtual hard disk</source>
        <translation>Erstellt eine neue virtuelle Festplatte</translation>
    </message>
    <message>
        <source>Add an existing medium</source>
        <translation>Vorhandenes Medium hinzufügen</translation>
    </message>
    <message>
        <source>Remove the selected medium</source>
        <translation>Ausgewähltes Medium entfernen</translation>
    </message>
    <message>
        <source>Release the selected medium by detaching it from the machines</source>
        <translation>Hebt die Verbindung des ausgewählten Medium an alle virtuellen Maschinen auf</translation>
    </message>
    <message>
        <source>Refresh the media list</source>
        <translation>Aktualisiert die Medienliste</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Ort</translation>
    </message>
    <message>
        <source>Type (Format)</source>
        <translation>Type (Format)</translation>
    </message>
    <message>
        <source>Attached to</source>
        <translation>angeschlossen an</translation>
    </message>
    <message>
        <source>Checking accessibility</source>
        <translation>Überprüfe Zugriffsrecht</translation>
    </message>
    <message>
        <source>&amp;Select</source>
        <translation>&amp;Auswählen</translation>
    </message>
    <message>
        <source>All hard disk images (%1)</source>
        <translation>Alle Plattenabbilder (%1)</translation>
    </message>
    <message>
        <source>All files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <source>Select a hard disk image file</source>
        <translation>Wählen Sie ein Plattenabbild aus</translation>
    </message>
    <message>
        <source>CD/DVD-ROM images (*.iso);;All files (*)</source>
        <translation>CD/DVD-ROM-Abbilder (*.iso);;Alle Dateien (*)</translation>
    </message>
    <message>
        <source>Select a CD/DVD-ROM disk image file</source>
        <translation>Wählen Sie ein CD/DVD-ROM-Abbild aus</translation>
    </message>
    <message>
        <source>Floppy images (*.img);;All files (*)</source>
        <translation>Diskettenabbilder (*.img);;Alle Dateien (*)</translation>
    </message>
    <message>
        <source>Select a floppy disk image file</source>
        <translation>Wählen Sie ein Diskettenabbild aus</translation>
    </message>
    <message>
        <source>&lt;i&gt;Not&amp;nbsp;Attached&lt;/i&gt;</source>
        <translation>&lt;i&gt;nicht&amp;nbsp;angeschlossen&lt;/i&gt;</translation>
    </message>
    <message>
        <source>--</source>
        <comment>no info</comment>
        <translation></translation>
    </message>
    <message>
        <source>Virtual Media Manager</source>
        <translation>Manager für virtuelle Medien</translation>
    </message>
    <message>
        <source>Hard &amp;Disks</source>
        <translation>&amp;Festplatten</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Virtual Size</source>
        <translation>Endgröße</translation>
    </message>
    <message>
        <source>Actual Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <source>&amp;CD/DVD Images</source>
        <translation>&amp;CD/DVD-Abbilder</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <source>&amp;Floppy Images</source>
        <translation>&amp;Diskettenabbilder</translation>
    </message>
    <message>
        <source>Attached to</source>
        <comment>VMM: Virtual Disk</comment>
        <translation>angeschlossen an</translation>
    </message>
    <message>
        <source>Attached to</source>
        <comment>VMM: CD/DVD Image</comment>
        <translation>angeschlossen an</translation>
    </message>
    <message>
        <source>Attached to</source>
        <comment>VMM: Floppy Image</comment>
        <translation>angeschlossen an</translation>
    </message>
</context>
<context>
    <name>VBoxMiniToolBar</name>
    <message>
        <source>Always show the toolbar</source>
        <translation>Toolbar immer zeigen</translation>
    </message>
    <message>
        <source>Exit Full Screen or Seamless Mode</source>
        <translation>Vollbild-/Seamless-Modus verlassen</translation>
    </message>
    <message>
        <source>Close VM</source>
        <translation>VM beenden</translation>
    </message>
</context>
<context>
    <name>VBoxNetworkDialog</name>
    <message>
        <source>Network Adapters</source>
        <translation>Netzwerkadapter</translation>
    </message>
</context>
<context>
    <name>VBoxNewHDWzd</name>
    <message>
        <source>Create New Virtual Disk</source>
        <translation>Neue virtuelle Festplatte erstellen</translation>
    </message>
    <message>
        <source>Welcome to the Create New Virtual Disk Wizard!</source>
        <translation>Erstellen einer virtuellen Festplatte</translation>
    </message>
    <message>
        <source>Virtual Disk Location and Size</source>
        <translation>Lage und Größe der virtuellen Festplatte</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;%1 Bytes&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;%1 Byte&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Hard disk images (*.vdi)</source>
        <translation>Plattenabbilder (*.vdi)</translation>
    </message>
    <message>
        <source>Select a file for the new hard disk image file</source>
        <translation>Wählen Sie eine Datei für ein neues Plattenabbild aus</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Zurück</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>&amp;Weiter &gt;</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation>&amp;Fertig</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>summary</comment>
        <translation>Typ</translation>
    </message>
    <message>
        <source>Location</source>
        <comment>summary</comment>
        <translation>Ort</translation>
    </message>
    <message>
        <source>Size</source>
        <comment>summary</comment>
        <translation>Größe</translation>
    </message>
    <message>
        <source>Bytes</source>
        <comment>summary</comment>
        <translation>Byte</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>&lt;p&gt;This wizard will help you to create a new virtual hard disk for your virtual machine.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Next&lt;/b&gt; button to go to the next page of the wizard and the &lt;b&gt;Back&lt;/b&gt; button to return to the previous page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Dieser Assistent wird Ihnen helfen, eine neue virtuelle Festplatte für Ihre virtuelle Maschine zu erstellen.&lt;/p&gt;&lt;p&gt;Wählen Sie &lt;b&gt;Weiter&lt;/b&gt;, um auf die nächste Seite des Assistenten zu wechseln bzw. &lt;b&gt;Zurück&lt;/b&gt;, um auf die vorherige Seite zurückzukehren.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Hard Disk Storage Type</source>
        <translation>Typ der Festplatte</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the type of virtual hard disk you want to create.&lt;/p&gt;&lt;p&gt;A &lt;b&gt;dynamically expanding storage&lt;/b&gt; initially occupies a very small amount of space on your physical hard disk. It will grow dynamically (up to the size specified) as the Guest OS claims disk space.&lt;/p&gt;&lt;p&gt;A &lt;b&gt;fixed-size storage&lt;/b&gt; does not grow. It is stored in a file of approximately the same size as the size of the virtual hard disk. The creation of a fixed-size storage may take a long time depending on the storage size and the write performance of your harddisk.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie den Typ der virtuellen Festplatte.&lt;/p&gt;&lt;p&gt;Ein &lt;b&gt;dynamisch expandierendes Medium&lt;/b&gt; belegt bei der Erzeugung nur sehr wenig Platz auf der physischen Festplatte. Es wächst in dem Maße dynamisch (bis zur vorher festgelegten Größe), wie das Gastsystem Blöcke auf der virtuellen Platte beschreibt.&lt;/p&gt;&lt;p&gt;Ein &lt;b&gt;Medium fester Größe&lt;/b&gt; wächst nicht zur Laufzeit, sondern wird sofort mit der endgültigen Größe erzeugt. Das Erstellen eines Mediums fester Größe kann in Abhängigkeit von der Größe und der Schreibrate der Festplatte sehr lange (Minuten) dauern.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Storage Type</source>
        <translation>Datenspeichertyp</translation>
    </message>
    <message>
        <source>&amp;Dynamically expanding storage</source>
        <translation>&amp;Dynamisch wachsendes Medium</translation>
    </message>
    <message>
        <source>&amp;Fixed-size storage</source>
        <translation>Medium &amp;fester Größe</translation>
    </message>
    <message>
        <source>&lt;p&gt;Press the &lt;b&gt;Select&lt;/b&gt; button to select the location of a file to store the hard disk data or type a file name in the entry field.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Klicken Sie auf &lt;b&gt;Auswählen&lt;/b&gt;, um den Speicherort der Daten auf der Festplatte auszuwählen oder tippen Sie den Namen in das Eingabefeld.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Location</source>
        <translation>&amp;Ort</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the size of the virtual hard disk in megabytes. This size will be reported to the Guest OS as the maximum size of this hard disk.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie die Größe der virtuellen Festplatte in Megabyte. Diese Größe wird dem Gastsystem als Größe der virtuellen Festplatte übermittelt.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation>&amp;Größe</translation>
    </message>
    <message>
        <source>You are going to create a new virtual hard disk with the following parameters:</source>
        <translation>Sie möchten eine neue virtuelle Festplatte mit den folgenden Parametern erstellen:</translation>
    </message>
    <message>
        <source>If the above settings are correct, press the &lt;b&gt;Finish&lt;/b&gt; button. Once you press it, a new hard disk will be created.</source>
        <translation>Klicken Sie auf &lt;b&gt;Fertig&lt;/b&gt;, wenn alle oben angegebenen Einstellungen richtig sind. Damit wird eine neue virtuelle Festplatte erstellt.</translation>
    </message>
</context>
<context>
    <name>VBoxNewVMWzd</name>
    <message>
        <source>Create New Virtual Machine</source>
        <translation>Neue virtuelle Maschine erstellen</translation>
    </message>
    <message>
        <source>Welcome to the New Virtual Machine Wizard!</source>
        <translation>Erstellen einer Virtuellen Maschine</translation>
    </message>
    <message>
        <source>N&amp;ame</source>
        <translation>N&amp;ame</translation>
    </message>
    <message>
        <source>OS &amp;Type</source>
        <translation>&amp;Typ des Gastbetriebssystems</translation>
    </message>
    <message>
        <source>VM Name and OS Type</source>
        <translation>VM-Name und BS-Typ</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the amount of base memory (RAM) in megabytes to be allocated to the virtual machine.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie die Größe des Hauptspeichers (RAM) in Megabyte, die für die virtuelle Maschine verwendet werden soll.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Base &amp;Memory Size</source>
        <translation>Größe &amp;Hauptspeicher</translation>
    </message>
    <message>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <source>Memory</source>
        <translation>Speicher</translation>
    </message>
    <message>
        <source>Virtual Hard Disk</source>
        <translation>Virtuelle Festplatte</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <source>The recommended base memory size is &lt;b&gt;%1&lt;/b&gt; MB.</source>
        <translation>Die empfohlene Hauptspeichergröße beträgt &lt;b&gt;%1&lt;/b&gt; MB.</translation>
    </message>
    <message>
        <source>The recommended size of the boot hard disk is &lt;b&gt;%1&lt;/b&gt; MB.</source>
        <translation>Die empfohlene Größe der Bootplatte beträgt &lt;b&gt;%1&lt;/b&gt; MB.</translation>
    </message>
    <message>
        <source>&lt;p&gt;This wizard will guide you through the steps that are necessary to create a new virtual machine for VirtualBox.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Next&lt;/b&gt; button to go the next page of the wizard and the &lt;b&gt;Back&lt;/b&gt; button to return to the previous page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Dieser Dialog wird Sie durch die für die Erstellung einer virtuellen Maschine notwendigen Schritte führen.&lt;/p&gt;&lt;p&gt;Klicken Sie auf &lt;b&gt;Weiter&lt;/b&gt;, um auf die nächste Seite des Assistenten zu gelangen bzw. auf &lt;b&gt;Zurück&lt;/b&gt;, um auf die vorherige Seite zurückzukehren.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Zurück</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>&amp;Weiter &gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Enter a name for the new virtual machine and select the type of the guest operating system you plan to install onto the virtual machine.&lt;/p&gt;&lt;p&gt;The name of the virtual machine usually indicates its software and hardware configuration. It will be used by all VirtualBox components to identify your virtual machine.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Geben Sie einen Namen für die neue virtuelle Maschine ein und wählen Sie den Typ des Gast-Betriebssystems, das Sie installieren wollen.&lt;/p&gt;&lt;p&gt;Der Name der virtuellen Maschine gibt üblicherweise einen Anhaltspunkt über die Software und die Konfiguration der virtuellen Hardware. Er wird von allen VirtualBox-Produkten benutzt, um die VM eindeutig zu identifizieren.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;You are going to create a new virtual machine with the following parameters:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sie möchten eine neue virtuelle Festplatte mit den folgenden Parametern erstellen:&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;If the above is correct press the &lt;b&gt;Finish&lt;/b&gt; button. Once you press it, a new virtual machine will be created. &lt;/p&gt;&lt;p&gt;Note that you can alter these and all other setting of the created virtual machine at any time using the &lt;b&gt;Settings&lt;/b&gt; dialog accessible through the menu of the main window.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Klicken Sie auf &lt;b&gt;Fertig&lt;/b&gt;, wenn alle oben angegebenen Einstellungen richtig sind. Damit wird eine neue virtuelle Maschine erzeugt.&lt;/p&gt;&lt;/p&gt;Sie können diese und andere Einstellungen der virtuellen Maschine jederzeit im Dialog &lt;b&gt;Einstellungen&lt;/b&gt; ausgehend vom Hauptfenster ändern.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation>&amp;Fertig</translation>
    </message>
    <message>
        <source>MB</source>
        <comment>megabytes</comment>
        <translation></translation>
    </message>
    <message>
        <source>Name</source>
        <comment>summary</comment>
        <translation>Name</translation>
    </message>
    <message>
        <source>OS Type</source>
        <comment>summary</comment>
        <translation>BS-Typ</translation>
    </message>
    <message>
        <source>Base Memory</source>
        <comment>summary</comment>
        <translation>Hauptspeicher</translation>
    </message>
    <message>
        <source>Boot Hard Disk</source>
        <comment>summary</comment>
        <translation>Bootfestplatte</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select a hard disk image to be used as the boot hard disk of the virtual machine. You can either create a new hard disk using the &lt;b&gt;New&lt;/b&gt; button or select an existing hard disk image from the drop-down list or by pressing the &lt;b&gt;Existing&lt;/b&gt; button (to invoke the Virtual Media Manager dialog).&lt;/p&gt;&lt;p&gt;If you need a more complicated hard disk setup, you can also skip this step and attach hard disks later using the VM Settings dialog.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie ein Abbild einer Festplatte, die als Bootplatte der virtuellen Maschine dienen soll. Sie können entweder eine neue Festplatte durch Klicken auf &lt;b&gt;Neu&lt;/b&gt; erstellen oder ein existierendes Abbild durch Klicken auf &lt;b&gt;Existierend&lt;/b&gt; auswählen (durch Aufruf des Managers virtueller Platten).&lt;/p&gt;&lt;p&gt;Falls die virtuelle Platte zusätzliche Parameter benötigt, kann dieser Schritt auch übersprungen werden und ein Abbild später über den VM-Einstellungs-Dialog angeschlossen werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Boot Hard &amp;Disk (Primary Master)</source>
        <translation>B&amp;oot Festplatte (Primärer Master)</translation>
    </message>
    <message>
        <source>&amp;Create new hard disk</source>
        <translation>Festplatte &amp;erzeugen</translation>
    </message>
    <message>
        <source>&amp;Use existing hard disk</source>
        <translation>Festplatte &amp;benutzen</translation>
    </message>
</context>
<context>
    <name>VBoxOSTypeSelectorWidget</name>
    <message>
        <source>Operating &amp;System:</source>
        <translation>&amp;Betriebssystem:</translation>
    </message>
    <message>
        <source>Displays the operating system family that you plan to install into this virtual machine.</source>
        <translation>Zeigt die Familie des Betriebssystems, das Sie in dieser virtuellen Maschine installieren wollen.</translation>
    </message>
    <message>
        <source>Displays the operating system type that you plan to install into this virtual machine (called a guest operating system).</source>
        <translation>Zeigt den Typ des Betriebssystems, das Sie in der virtuellen Maschine installieren wollen (auch als Gast bezeichnet).</translation>
    </message>
    <message>
        <source>&amp;Version:</source>
        <translation>&amp;Version:</translation>
    </message>
</context>
<context>
    <name>VBoxProblemReporter</name>
    <message>
        <source>VirtualBox - Information</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - Information</translation>
    </message>
    <message>
        <source>VirtualBox - Question</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - Frage</translation>
    </message>
    <message>
        <source>VirtualBox - Warning</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - Warnung</translation>
    </message>
    <message>
        <source>VirtualBox - Error</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - Fehler</translation>
    </message>
    <message>
        <source>VirtualBox - Critical Error</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - Kritischer Fehler</translation>
    </message>
    <message>
        <source>Do not show this message again</source>
        <comment>msg box flag</comment>
        <translation>Diese Meldung später nicht mehr zeigen</translation>
    </message>
    <message>
        <source>The following files already exist:&lt;br /&gt;&lt;br /&gt;%1&lt;br /&gt;&lt;br /&gt;Are you sure you want to replace them? Replacing them will overwrite their contents.</source>
        <translation>Die folgenden Dateien sind bereits vorhanden: &lt;br/&gt;&lt;br/&gt;%1&lt;br/&gt;&lt;br/&gt;Sind Sie sicher, dass Sie diese überschreiben wollen?</translation>
    </message>
    <message>
        <source>You are running a prerelease version of VirtualBox. This version is not suitable for production use.</source>
        <translation>Sie führen eine Vorabversion von VirtualBox aus. Diese Version ist nicht für den produktiven Einsatz geeignet.</translation>
    </message>
    <message>
        <source>Failed to open &lt;tt&gt;%1&lt;/tt&gt;. Make sure your desktop environment can properly handle URLs of this type.</source>
        <translation>&lt;tt&gt;%1&lt;/tt&gt; konnte nicht geöffnet werden. Stellen Sie sicher, dass Ihre Benutzeroberfläche URLs anzeigen kann.</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to initialize COM or to find the VirtualBox COM server. Most likely, the VirtualBox server is not running or failed to start.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die COM-Schnittstelle konnte nicht initialisiert werden oder der COM-Server wurde nicht gefunden. Möglicherweise ist der VirtualBox-Server nicht gestartet.&lt;/p&gt;&lt;p&gt;Die Anwendung wird nun geschlossen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to create the VirtualBox COM object.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Das COM-Objekt für VirtualBox konnte nicht erzeugt werden.&lt;/p&gt;&lt;p&gt;Die Anwendung wird nun beendet.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to set global VirtualBox properties.</source>
        <translation>Die globalen Eigenschaften konnten nicht definiert werden.</translation>
    </message>
    <message>
        <source>Failed to access the USB subsystem.</source>
        <translation>Kein Zugriff auf das USB-Subsystem.</translation>
    </message>
    <message>
        <source>Failed to create a new virtual machine.</source>
        <translation>Eine neue virtuelle Maschine konnte nicht erstellt werden.</translation>
    </message>
    <message>
        <source>Failed to create a new virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die neue virtuelle Maschine &lt;b&gt;%1&lt;/b&gt; konnte nicht erstellt werden.</translation>
    </message>
    <message>
        <source>Failed to apply the settings to the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die Einstellungen für die virtuelle Maschine &lt;b&gt;%1&lt;/b&gt; konnten nicht übernommen werden.</translation>
    </message>
    <message>
        <source>Failed to start the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die virtuelle Maschine &lt;b&gt;%1&lt;/b&gt; konnte nicht gestartet werden.</translation>
    </message>
    <message>
        <source>Failed to pause the execution of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die virtuelle Maschine &lt;b&gt;%1&lt;/b&gt; konnte nicht angehalten werden.</translation>
    </message>
    <message>
        <source>Failed to resume the execution of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die Ausführung der virtuellen Maschine &lt;b&gt;%1&lt;/b&gt; konnte nicht fortgesetzt werden.</translation>
    </message>
    <message>
        <source>Failed to save the state of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Der Zustand der virtuellen Maschine &lt;b&gt;%1&lt;/b&gt; konnte nicht gesichert werden.</translation>
    </message>
    <message>
        <source>Failed to create a snapshot of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Es konnte kein Sicherungspunkt der virtuellen Maschine &lt;b&gt;%1&lt;/b&gt; erstellt werden.</translation>
    </message>
    <message>
        <source>Failed to stop the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die virtuelle Maschine &lt;b&gt;%1&lt;/b&gt; konnte nicht angehalten werden.</translation>
    </message>
    <message>
        <source>Failed to remove the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die virtuelle Maschine &lt;b&gt;%1&lt;/b&gt; konnte nicht gelöscht werden.</translation>
    </message>
    <message>
        <source>Failed to discard the saved state of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Der gesicherte Zustand der virtuellen Maschine &lt;b&gt;%1&lt;/b&gt; konnte nicht freigegeben werden.</translation>
    </message>
    <message>
        <source>&lt;p&gt;VT-x/AMD-V hardware acceleration has been enabled, but is not operational. Your 64-bit guest will fail to detect a 64-bit CPU and will not be able to boot.&lt;/p&gt;&lt;p&gt;Please ensure that you have enabled VT-x/AMD-V properly in the BIOS of your host computer.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die VT-x/AMD-V-Hardware-Virtualisierung wurde aktiviert, ist aber nicht funktionsbereit. Ihr 64-Bit-Gast wird keine 64-Bit-CPU erkennen und daher höchstwahrscheinlich nicht booten.&lt;/p&gt;&lt;p&gt;Bitte stellen Sie sicher, dass VT-x/AMD-V ordnungsgemäß im BIOS Ihres Computers aktiviert wurde. Installieren Sie ggf. ein BIOS-Update.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>There is no virtual machine named &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Es gibt keine virtuelle Maschine mit dem Namen &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to permanently delete the virtual machine &lt;b&gt;%1&lt;/b&gt;?&lt;/p&gt;&lt;p&gt;This operation cannot be undone.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie die virtuelle Maschine &lt;b&gt;%1&lt;/b&gt; wirklich dauerhaft löschen?&lt;/p&gt;&lt;p&gt;Dieser Vorgang kann nicht rückgängig gemacht werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to unregister the inaccessible virtual machine &lt;b&gt;%1&lt;/b&gt;?&lt;/p&gt;&lt;p&gt;You will not be able to register it again from GUI.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie die Bindung der inaktiven virtuellen Maschine &lt;b&gt;%1&lt;/b&gt; wirklich aufheben?&lt;/p&gt;&lt;p&gt;Sie kann mit dieser GUI nicht wieder zugewiesen werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to discard the saved state of the virtual machine &lt;b&gt;%1&lt;/b&gt;?&lt;/p&gt;&lt;p&gt;This operation is equivalent to resetting or powering off the machine without doing a proper shutdown of the guest OS.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie den gesicherten Zustand der virtuellen Maschine &lt;b&gt;%1&lt;/b&gt; wirklich freigeben?&lt;/p&gt;&lt;p&gt;Diese Operation ist gleichbedeutend mit dem Ausschalten der VM ohne sauberes Herunterfahren durch das Gast-Betriebssystem.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;There are hard disks attached to ports of the additional controller. If you disable the additional controller, all these hard disks will be automatically detached.&lt;/p&gt;&lt;p&gt;Are you sure you want to disable the additional controller?&lt;/p&gt;</source>
        <translation>&lt;p&gt;An den zusätzlichen Controller sind Festplatten angeschlossen. Durch Deaktivieren dieses Controllers werden diese Festplatten automatisch aus der Konfiguration entfernt.&lt;/p&gt;&lt;p&gt;Sind Sie sicher, dass Sie diesen Controller deaktivieren möchten?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;There are hard disks attached to ports of the additional controller. If you change the additional controller, all these hard disks will be automatically detached.&lt;/p&gt;&lt;p&gt;Are you sure you want to change the additional controller?&lt;/p&gt;</source>
        <translation>&lt;p&gt;An den zusätzlichen Controller sind Festplatten angeschlossen. Durch Ändern dieses Controllers werden diese Festplatten automatisch aus der Konfiguration entfernt.&lt;/p&gt;&lt;p&gt;Sind Sie sicher, dass Sie diesen Controller deaktivieren möchten?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to create a new session.</source>
        <translation>Eine neue Sitzung konnte nicht angelegt werden.</translation>
    </message>
    <message>
        <source>Failed to open a session for the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Für die virtuelle Maschine &lt;b&gt;%1&lt;/b&gt; konnte keine neue Sitzung eröffnet werden.</translation>
    </message>
    <message>
        <source>&lt;p&gt;Deleting this host-only network will remove the host-only interface this network is based on. Do you want to remove the (host-only network) interface &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;?&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Note:&lt;/b&gt; this interface may be in use by one or more virtual network adapters belonging to one of your VMs. After it is removed, these adapters will no longer be usable until you correct their settings by either choosing a different interface name or a different adapter attachment type.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie wirklich dieses Host-only Netzinterface &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; löschen?&lt;/p&gt;&lt;p&gt;&lt;b&gt;Beachten Sie:&lt;/b&gt; Dieses Interface wird möglicherweise von mehreren virtuellen Netzadaptern benutzt. Nach dem Löschen sind diese Adapter nicht mehr benutzbar bis Sie die Einstellungen dieser virtuellen Maschinen angepasst haben.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to remove the host network interface &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Das Hostinterface &lt;b&gt;%1&lt;/b&gt; konnte nicht gelöscht werden.</translation>
    </message>
    <message>
        <source>Failed to attach the USB device &lt;b&gt;%1&lt;/b&gt; to the virtual machine &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>Das USB-Gerät &lt;b&gt;%1&lt;/b&gt; konnte nicht an die virtuelle Maschine &lt;b&gt;%2&lt;/b&gt; gebunden werden.</translation>
    </message>
    <message>
        <source>Failed to detach the USB device &lt;b&gt;%1&lt;/b&gt; from the virtual machine &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>Die Bindung des USB-Gerätes &lt;b&gt;%1&lt;/b&gt; an die virtuelle Maschine &lt;b&gt;%2&lt;/b&gt; konnte nicht aufgehoben werden.</translation>
    </message>
    <message>
        <source>Failed to create the shared folder &lt;b&gt;%1&lt;/b&gt; (pointing to &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) for the virtual machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>Der gemeinsame Ordner &lt;b&gt;%1&lt;/b&gt; (mit Verweis auf &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) für die virtuelle Maschine &lt;b&gt;%3&lt;/b&gt; konnte nicht erstellt werden.</translation>
    </message>
    <message>
        <source>Failed to remove the shared folder &lt;b&gt;%1&lt;/b&gt; (pointing to &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) from the virtual machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>Der gemeinsame Ordner &lt;b&gt;%1&lt;/b&gt; (Verweis auf &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) konnte nicht aus der virtuellen Maschine &lt;b&gt;%3&lt;/b&gt; entfernt werden.</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Virtual Machine reports that the guest OS does not support &lt;b&gt;mouse pointer integration&lt;/b&gt; in the current video mode. You need to capture the mouse (by clicking over the VM display or pressing the host key) in order to use the mouse inside the guest OS.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Der VM-Monitor zeigt an, dass das Gastbetriebssystem keine &lt;b&gt;Mauszeiger-Integration&lt;/b&gt; für den aktuellen Videomodus unterstützt. Um die Maus im Gastsystem zu nutzen, muss diese durch Mausklick im VM-Fenster gefangen werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Virtual Machine is currently in the &lt;b&gt;Paused&lt;/b&gt; state and not able to see any keyboard or mouse input. If you want to continue to work inside the VM, you need to resume it by selecting the corresponding action from the menu bar.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die virtuelle Maschine ist momentan &lt;b&gt;angehalten&lt;/b&gt; und nimmt deshalb keine Maus- und Tastatureingaben entgegen. Um mit der Arbeit der virtuellen Maschine fortzufahren, müssen Sie die virtuelle Maschine weiterlaufen lassen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Cannot run VirtualBox in &lt;i&gt;VM Selector&lt;/i&gt; mode due to local restrictions.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;VirtualBox kann aufgrund von lokalen Beschränkungen nicht im &lt;i&gt;VM Selektor-Modus&lt;/i&gt; ausgeführt werden.&lt;/p&gt;&lt;p&gt;Die Anwendung wird nun beendet.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to create appliance.</source>
        <translation>Appliance konnte nicht erstellt werden.</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Fatal Error&lt;/nobr&gt;</source>
        <comment>runtime error info</comment>
        <translation>&lt;nobr&gt;Schwerwiegender Fehler&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Non-Fatal Error&lt;/nobr&gt;</source>
        <comment>runtime error info</comment>
        <translation>&lt;nobr&gt;Normaler Fehler&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Warning&lt;/nobr&gt;</source>
        <comment>runtime error info</comment>
        <translation>&lt;nobr&gt;Warnung&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Error ID: &lt;/nobr&gt;</source>
        <comment>runtime error info</comment>
        <translation>&lt;nobr&gt;Fehler ID: &lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Severity: </source>
        <comment>runtime error info</comment>
        <translation>Dringlichkeit:</translation>
    </message>
    <message>
        <source>&lt;p&gt;A fatal error has occurred during virtual machine execution! The virtual machine will be powered off. Please copy the following error message using the clipboard to help diagnose the problem:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Während der Ausführung der virtuellen Maschine ist ein schwerwiegender Fehler aufgetreten! Die VM wird abgeschaltet. Es wird empfohlen, die folgende Fehlermeldung für eine spätere Untersuchung in die Zwischenablage zu kopieren:&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;An error has occurred during virtual machine execution! The error details are shown below. You may try to correct the error and resume the virtual machine execution.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Während der Ausführung der virtuellen Maschine ist ein Fehler aufgetreten. Einzelheiten werden unten gezeigt. Sie können versuchen, den angezeigten Fehler zu beheben und mit der Ausführung fortzufahren.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The virtual machine execution may run into an error condition as described below. We suggest that you take an appropriate action to avert the error.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die Ausführung der virtuellen Maschine kann zu dem unten beschriebenen Fehler führen. Sie können diese Meldung ignorieren, sollten aber angemessen reagieren, um diesen Fehler zu vermeiden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Result&amp;nbsp;Code: </source>
        <comment>error info</comment>
        <translation>Fehlercode:</translation>
    </message>
    <message>
        <source>Component: </source>
        <comment>error info</comment>
        <translation>Komponente:</translation>
    </message>
    <message>
        <source>Interface: </source>
        <comment>error info</comment>
        <translation>Interface:</translation>
    </message>
    <message>
        <source>Callee: </source>
        <comment>error info</comment>
        <translation>Callee:</translation>
    </message>
    <message>
        <source>Callee&amp;nbsp;RC: </source>
        <comment>error info</comment>
        <translation>Callee&amp;nbsp;RC:</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not find a language file for the language &lt;b&gt;%1&lt;/b&gt; in the directory &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;The language will be temporarily reset to the system default language. Please go to the &lt;b&gt;Preferences&lt;/b&gt; dialog which you can open from the &lt;b&gt;File&lt;/b&gt; menu of the main VirtualBox window, and select one of the existing languages on the &lt;b&gt;Language&lt;/b&gt; page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Für die Sprache &lt;b&gt;%1&lt;/b&gt; konnte im Verzeichnis &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; keine Sprachdatei gefunden werden.&lt;/p&gt;&lt;p&gt;Die Sprache wird vorübergehend auf die voreingestellte Systemsprache zurückgesetzt. Bitte benutzen Sie die &lt;b&gt;Globalen Einstellungen&lt;/b&gt;, um eine andere Sprache auszuwählen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not load the language file &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt;. &lt;p&gt;The language will be temporarily reset to English (built-in). Please go to the &lt;b&gt;Preferences&lt;/b&gt; dialog which you can open from the &lt;b&gt;File&lt;/b&gt; menu of the main VirtualBox window, and select one of the existing languages on the &lt;b&gt;Language&lt;/b&gt; page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die Sprachdatei &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; konnte nicht geöffnet werden.&lt;p&gt;Die Sprache wird vorübergehend auf Englisch zurückgesetzt. Bitte wählen Sie in den &lt;b&gt;Globalen Einstellungen&lt;/b&gt; eine neue Sprache aus.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The VirtualBox Guest Additions installed in the Guest OS are too old: the installed version is %1, the expected version is %2. Some features that require Guest Additions (mouse integration, guest display auto-resize) will most likely stop working properly.&lt;/p&gt;&lt;p&gt;Please update the Guest Additions to the current version by choosing &lt;b&gt;Install Guest Additions&lt;/b&gt; from the &lt;b&gt;Devices&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die im Gast installierten VirtualBox Gasterweiterungen sind zu alt: Version %1 ist installiert, erwartet wird Version %2. Einige Features, die Gasterweiterungen benötigen (Mauszeiger-Integration, automatische Anpassung der Gastanzeige) funktionieren höchstwahrscheinlich nicht.&lt;/p&gt;&lt;p&gt;Bitte aktualisieren Sie die Gasterweiterungen durch &lt;b&gt;Gasterweiterungen installieren&lt;/b&gt; aus dem Menü &lt;b&gt;Geräte&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The VirtualBox Guest Additions installed in the Guest OS are outdated: the installed version is %1, the expected version is %2. Some features that require Guest Additions (mouse integration, guest display auto-resize) may not work as expected.&lt;/p&gt;&lt;p&gt;It is recommended to update the Guest Additions to the current version  by choosing &lt;b&gt;Install Guest Additions&lt;/b&gt; from the &lt;b&gt;Devices&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die im Gast installierten VirtualBox Gasterweiterungen sind überholt: Version %1 ist installiert, erwartet wird Version %2. Einige Features, die Gasterweiterungen benötigen (Mauszeiger-Integration, automatische Anpassung der Gastanzeige) funktionieren möglicherweise nicht wie erwartet.&lt;/p&gt;&lt;p&gt;Bitte aktualisieren Sie die Gasterweiterungen durch &lt;b&gt;Gasterweiterungen installieren&lt;/b&gt; aus dem Menü &lt;b&gt;Geräte&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The VirtualBox Guest Additions installed in the Guest OS are too recent for this version of VirtualBox: the installed version is %1, the expected version is %2.&lt;/p&gt;&lt;p&gt;Using a newer version of Additions with an older version of VirtualBox is not supported. Please install the current version of the Guest Additions by choosing &lt;b&gt;Install Guest Additions&lt;/b&gt; from the &lt;b&gt;Devices&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die im Gast installierten VirtualBox Gasterweiterungen sind zu neu für diese Version von VirtualBox: Version %1 ist installiert, erwartet wird Version %2.&lt;/p&gt;&lt;p&gt;Die Nutzung einer neueren Version der Gasterweiterungen mit einer älteren Version von VirtualBox wird nicht unterstützt. Bitte aktualisieren Sie die Gasterweiterungen durch &lt;b&gt;Gasterweiterungen installieren&lt;/b&gt; aus dem Menü &lt;b&gt;Geräte&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to change the snapshot folder path of the virtual machine &lt;b&gt;%1&lt;b&gt; to &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>Der Ordner für Sicherungspunkte der virtuellen Maschine &lt;b&gt;%1&lt;/b&gt; konnte nicht nach &lt;nobr&gt;%2&lt;/b&gt;&lt;/nobr&gt; geändert werden.</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to remove the shared folder &lt;b&gt;%1&lt;/b&gt; (pointing to &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) from the virtual machine &lt;b&gt;%3&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Please close all programs in the guest OS that may be using this shared folder and try again.&lt;/p&gt;</source>
        <translation>Der gemeinsame Ordner &lt;b&gt;%1&lt;/b&gt; (mit Verweis auf &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) konnte nicht aus der virtuellen Maschine &lt;b&gt;%3&lt;/b&gt; entfernt werden.&lt;/p&gt;&lt;p&gt;Bitte schließen Sie alle Programme im Gast, die diesen gemeinsamen Ordner benutzen, und versuchen Sie die Aktion erneut.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not find the VirtualBox Guest Additions CD image file &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; or &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;.&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;Do you wish to download this CD image from the Internet?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Ein CDROM-Abbild mit den VirtualBox-Gasterweiterungen konnte weder unter &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; noch unter &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; gefunden werden.&lt;/p&gt;&lt;p&gt;Möchten Sie diese Datei nun aus dem Internet herunterladen?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to download the VirtualBox Guest Additions CD image from &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;.&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;%3&lt;/p&gt;</source>
        <translation>&lt;p&gt;Das CDROM-Abbild mit den VirtualBox Gasterweiterungen konnte nicht von &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; heruntergeladen werden.&lt;/p&gt;&lt;p&gt;%3&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to download the VirtualBox Guest Additions CD image from &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; (size %3 bytes)?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sind Sie sicher, dass Sie ein CDROM-Abbild mit den VirtualBox Gasterweiterungen von &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; (Größe %3 Byte) herunterladen wollen?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The VirtualBox Guest Additions CD image has been successfully downloaded from &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; and saved locally as &lt;nobr&gt;&lt;b&gt;%3&lt;/b&gt;.&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;Do you wish to register this CD image and mount it on the virtual CD/DVD drive?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Das CDROM-Abbild mit den VirtualBox Gasterweiterungen wurde erfolgreich von &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; heruntergeladen und lokal als &lt;nobr&gt;&lt;b&gt;%3&lt;/b&gt;&lt;/nobr&gt; gespeichert.&lt;/p&gt;&lt;p&gt;Möchten Sie das CDROM-Abbild registrieren und als virtuelle CD/DVD einbinden?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The virtual machine window is optimized to work in &lt;b&gt;%1&amp;nbsp;bit&lt;/b&gt; color mode but the virtual display is currently set to &lt;b&gt;%2&amp;nbsp;bit&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Please open the display properties dialog of the guest OS and select a &lt;b&gt;%3&amp;nbsp;bit&lt;/b&gt; color mode, if it is available, for best possible performance of the virtual video subsystem.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Note&lt;/b&gt;. Some operating systems, like OS/2, may actually work in 32&amp;nbsp;bit mode but report it as 24&amp;nbsp;bit (16 million colors). You may try to select a different color mode to see if this message disappears or you can simply disable the message now if you are sure the required color mode (%4&amp;nbsp;bit) is not available in the guest OS.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Das Fenster der virtuellen Maschine ist optimiert für eine Anzeige im &lt;b&gt;%1-Bit&lt;/b&gt;-Farbmodus, der aktuelle Modus des virtuellen Bildschirms ist aber auf &lt;b&gt;%2-&amp;nbsp;Bit&lt;/b&gt; gesetzt.&lt;/p&gt;&lt;p&gt;Bitte öffnen Sie die Einstellungen für die Anzeige im Gast und wählen Sie den &lt;b&gt;%3-Bit&lt;/b&gt;-Farbmodus, falls verfügbar, um die beste Performance der Anzeige zu erreichen.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Beachten Sie&lt;/b&gt;: Manche Betriebssysteme (z.B. OS/2) arbeiten tatsächlich im 32-Bit-Farbmodus, zeigen aber einen 24-Bit-Farbmodus (16 Millionen Farben) an. Sie können versuchen, einen anderen Farbmodus einzustellen, um zu testen, ob diese Meldung verschwindet. Oder Sie können diese Meldung nun deaktivieren falls Sie sicher sind, dass der erforderliche Farbmodus (%4&amp;nbsp;Bit) im Gast-Betriebssystem nicht verfügbar ist.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;You didn&apos;t attach a hard disk to the new virtual machine. The machine will not be able to boot unless you attach a hard disk with a guest operating system or some other bootable media to it later using the machine settings dialog or the First Run Wizard.&lt;/p&gt;&lt;p&gt;Do you wish to continue?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sie haben der neuen virtuellen Maschine keine Festplatte zugewiesen. Die Maschine kann nicht booten bis Sie der VM eine Festplatte mit einem Betriebssystem oder ein anderes bootbares Medium zuweisen. Sie können dafür die VM-Einstellungen oder den Startassistenten verwenden.&lt;/p&gt;&lt;p&gt;Möchten Sie fortfahren?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to find license files in &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>Es konnte keine Lizenzdatei in &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; gefunden werden.</translation>
    </message>
    <message>
        <source>Failed to open the license file &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;. Check file permissions.</source>
        <translation>Die Lizenzdatei &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; konnte nicht geöffnet werden. Bitte überprüfen Sie die Zugriffsrechte.</translation>
    </message>
    <message>
        <source>Failed to send the ACPI Power Button press event to the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Das ACPI-Ereignis &lt;i&gt;Netztaste gedrückt&lt;/i&gt; konnte nicht an die virtuelle Maschine &lt;b&gt;%1&lt;/b&gt; übermittelt werden.</translation>
    </message>
    <message>
        <source>&lt;p&gt;Congratulations! You have been successfully registered as a user of VirtualBox.&lt;/p&gt;&lt;p&gt;Thank you for finding time to fill out the registration form!&lt;/p&gt;</source>
        <translation>&lt;p&gt;Herzlichen Glückwunsch! Sie haben sich erfolgreich als Nutzer von VirtualBox registriert.&lt;/p&gt;&lt;p&gt;Vielen Dank, dass Sie sich die Zeit zum Ausfüllen des Formulars genommen haben!&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Delete</source>
        <comment>machine</comment>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>Unregister</source>
        <comment>machine</comment>
        <translation>Freigeben</translation>
    </message>
    <message>
        <source>Discard</source>
        <comment>saved state</comment>
        <translation>Verwerfen</translation>
    </message>
    <message>
        <source>Download</source>
        <comment>additions</comment>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <source>Mount</source>
        <comment>additions</comment>
        <translation>Einbinden</translation>
    </message>
    <message>
        <source>&lt;p&gt;The host key is currently defined as &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <comment>additional message box paragraph</comment>
        <translation>&lt;p&gt;Die Host-Taste ist momentan auf &lt;b&gt;%1&lt;/b&gt; eingestellt.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Capture</source>
        <comment>do input capture</comment>
        <translation>Fangen</translation>
    </message>
    <message>
        <source>Check</source>
        <comment>inaccessible media message box</comment>
        <translation>Überprüfen</translation>
    </message>
    <message>
        <source>Switch</source>
        <comment>fullscreen</comment>
        <translation>Einschalten</translation>
    </message>
    <message>
        <source>Switch</source>
        <comment>seamless</comment>
        <translation>Einschalten</translation>
    </message>
    <message>
        <source>&lt;p&gt;Do you really want to reset the virtual machine?&lt;/p&gt;&lt;p&gt;This will cause any unsaved data in applications running inside it to be lost.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie die virtuelle Maschine wirklich zurücksetzen?&lt;/p&gt;&lt;p&gt;Dabei gehen ungesicherte Daten aller noch offenen Anwendungen des Gastes verloren.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Reset</source>
        <comment>machine</comment>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <source>Continue</source>
        <comment>no hard disk attached</comment>
        <translation>Fortfahren</translation>
    </message>
    <message>
        <source>Go Back</source>
        <comment>no hard disk attached</comment>
        <translation>Zurück</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to save the global VirtualBox settings to &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die globalen Einstellungen für VirtualBox konnten nicht als &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; abgespeichert werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to load the global GUI configuration from &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die globale GUI-Konfiguration konnte von &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; nicht geladen werden.&lt;/p&gt;&lt;p&gt;Die Anwendung wird nun beendet.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to save the global GUI configuration to &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die globale GUI-Konfiguration konnte nicht in der Datei &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; gespeichert werden.&lt;/p&gt;&lt;p&gt;Die Anwendung wird nun beendet.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to save the settings of the virtual machine &lt;b&gt;%1&lt;/b&gt; to &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt;.</source>
        <translation>Die Einstellungen der virtuellen Maschine &lt;b&gt;%1&lt;/b&gt; konnten nicht nach &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; abgespeichert werden.</translation>
    </message>
    <message>
        <source>Failed to load the settings of the virtual machine &lt;b&gt;%1&lt;/b&gt; from &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt;.</source>
        <translation>Die Einstellungen der virtuellen Maschine &lt;b&gt;%1&lt;/b&gt; konnten nicht von der Datei &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; geladen werden.</translation>
    </message>
    <message>
        <source>Disable</source>
        <comment>hard disk</comment>
        <translation>Deaktivieren</translation>
    </message>
    <message>
        <source>Failed to copy file &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; to &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; (%3).</source>
        <translation>Die Datei &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; konnte nicht nach &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; kopiert werden (%3).</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not enter seamless mode due to insufficient guest video memory.&lt;/p&gt;&lt;p&gt;You should configure the virtual machine to have at least &lt;b&gt;%1&lt;/b&gt; of video memory.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Der nahtlose Modus konnte nicht aktiviert werden, weil dem Gast zu wenig Video-RAM zugewiesen wurde.&lt;/p&gt;&lt;p&gt;Sie sollten der VM mindestens &lt;b&gt;%1&lt;/b&gt; Video-RAM zur Verfügung stellen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not switch the guest display to fullscreen mode due to insufficient guest video memory.&lt;/p&gt;&lt;p&gt;You should configure the virtual machine to have at least &lt;b&gt;%1&lt;/b&gt; of video memory.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;Ignore&lt;/b&gt; to switch to fullscreen mode anyway or press &lt;b&gt;Cancel&lt;/b&gt; to cancel the operation.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Der Vollbildmodus konnte nicht aktiviert werden, weil dem Gast zu wenig Video-RAM zugewiesen wurde.&lt;/p&gt;&lt;p&gt;Sie sollten die VM so konfigurieren, dass ihr zumindest &lt;b&gt;%1&lt;/b&gt; Video-RAM zur Verfügung steht.&lt;/p&gt;&lt;p&gt;Wählen Sie &lt;b&gt;Ignorieren&lt;/b&gt; um dennoch in den Vollbildmodus zu wechseln oder betätigen Sie &lt;b&gt;Abbrechen&lt;/b&gt; um dies nicht zu tun.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>You are already running the most recent version of VirtualBox.</source>
        <translation>Sie haben bereits die neueste Version von VirtualBox installiert.</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have &lt;b&gt;clicked the mouse&lt;/b&gt; inside the Virtual Machine display or pressed the &lt;b&gt;host key&lt;/b&gt;. This will cause the Virtual Machine to &lt;b&gt;capture&lt;/b&gt; the host mouse pointer (only if the mouse pointer integration is not currently supported by the guest OS) and the keyboard, which will make them unavailable to other applications running on your host machine.&lt;/p&gt;&lt;p&gt;You can press the &lt;b&gt;host key&lt;/b&gt; at any time to &lt;b&gt;uncapture&lt;/b&gt; the keyboard and mouse (if it is captured) and return them to normal operation. The currently assigned host key is shown on the status bar at the bottom of the Virtual Machine window, next to the&amp;nbsp;&lt;img src=:/hostkey_16px.png/&gt;&amp;nbsp;icon. This icon, together with the mouse icon placed nearby, indicate the current keyboard and mouse capture state.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sie haben in das VM-Fenster &lt;b&gt;geklickt&lt;/b&gt; oder die &lt;b&gt;Host-Taste&lt;/b&gt; betätigt. Maus sowie Tastatur werden daher &lt;b&gt;gefangen&lt;/b&gt; (die Maus nur, falls Mauszeiger-Integration vom aktuellen Gast-System nicht unterstützt wird). Dadurch sind diese für andere Anwendungen neben VirtualBox nicht verfügbar.&lt;/p&gt;&lt;p&gt;Durch Betätigen der Host-Taste kann dieser Modus jederzeit beendet werden. Die momentan zugeordnete Host-Taste wird in der Statusleiste am unteren Rand des VM-Fensters gezeigt (Symbol&amp;nbsp;&lt;img src=:/hostkey_16px.png/&gt;&amp;nbsp;). Zusammen mit dem daneben liegenden Maus-Icon zeigen diese den aktuellen Tastatur- und Maus-Fangmodus.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have the &lt;b&gt;Auto capture keyboard&lt;/b&gt; option turned on. This will cause the Virtual Machine to automatically &lt;b&gt;capture&lt;/b&gt; the keyboard every time the VM window is activated and make it unavailable to other applications running on your host machine: when the keyboard is captured, all keystrokes (including system ones like Alt-Tab) will be directed to the VM.&lt;/p&gt;&lt;p&gt;You can press the &lt;b&gt;host key&lt;/b&gt; at any time to &lt;b&gt;uncapture&lt;/b&gt; the keyboard and mouse (if it is captured) and return them to normal operation. The currently assigned host key is shown on the status bar at the bottom of the Virtual Machine window, next to the&amp;nbsp;&lt;img src=:/hostkey_16px.png/&gt;&amp;nbsp;icon. This icon, together with the mouse icon placed nearby, indicate the current keyboard and mouse capture state.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sie haben den Modus &lt;b&gt;Tastatur fangen&lt;/b&gt; eingeschaltet. Wird das VM-Fenster aktiviert, wird die Tastatur automatisch &lt;b&gt;gefangen&lt;/b&gt; und damit für andere Anwendungen temporär nicht mehr verfügbar: Alle Tasteneingaben (inklusive Alt-Tab) werden in die VM umgeleitet.&lt;/p&gt;&lt;/p&gt;Sie können die &lt;b&gt;Host-Taste&lt;/b&gt; jederzeit betätigen, um diesen Modus für Tastatur (und Maus, falls ebenfalls gefangen) zu beenden. Die momentan zugeordnete Host-Taste wird in der Statusleiste am unteren Ende des VM-Fensters neben dem&amp;nbsp;&lt;img src=:/hostkey_16px.png/&gt;&amp;nbsp;Icon gezeigt. Zusammen mit dem Icon für die Maus daneben zeigt es den aktuellen Status für Maus und Tastatur.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Virtual Machine reports that the guest OS supports &lt;b&gt;mouse pointer integration&lt;/b&gt;. This means that you do not need to &lt;i&gt;capture&lt;/i&gt; the mouse pointer to be able to use it in your guest OS -- all mouse actions you perform when the mouse pointer is over the Virtual Machine&apos;s display are directly sent to the guest OS. If the mouse is currently captured, it will be automatically uncaptured.&lt;/p&gt;&lt;p&gt;The mouse icon on the status bar will look like&amp;nbsp;&lt;img src=:/mouse_seamless_16px.png/&gt;&amp;nbsp;to inform you that mouse pointer integration is supported by the guest OS and is currently turned on.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Note&lt;/b&gt;: Some applications may behave incorrectly in mouse pointer integration mode. You can always disable it for the current session (and enable it again) by selecting the corresponding action from the menu bar.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die virtuelle Maschine meldet Unterstützung der &lt;b&gt;Mauszeiger-Integration&lt;/b&gt; durch das Gastsystem. Dies bedeutet, dass die Maus nicht gefangen werden muss, um es im Gastsystem zu benutzen, sondern alle Mausaktionen über der VM-Anzeigebereich werden direkt an den Gast weitergeleitet. Der Fangmodus wird automatisch aufgehoben, falls die Maus momentan gefangen ist.&lt;/p&gt;&lt;p&gt;Das Mausicon in der Statuszeile wird so &amp;nbsp;&lt;img src=:/mouse_seamless_16px.png/&gt;&amp;nbsp;aussehen, um anzuzeigen, dass die Mauszeiger-Integration durch den Gast unterstützt und momentan aktiv ist.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Beachten Sie&lt;/b&gt;: Einige Anwendungen verhalten sich möglicherweise inkorrekt, wenn die Mauszeiger-Integration aktiv ist. Sie können diesen Modus durch Auswahl des entsprechenden Eintrages in der Menüzeile jederzeit deaktivieren.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The virtual machine window will be now switched to &lt;b&gt;fullscreen&lt;/b&gt; mode. You can go back to windowed mode at any time by pressing &lt;b&gt;%1&lt;/b&gt;. Note that the &lt;i&gt;Host&lt;/i&gt; key is currently defined as &lt;b&gt;%2&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Note that the main menu bar is hidden in fullscreen mode. You can access it by pressing &lt;b&gt;Host+Home&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die virtuelle Maschine wird nun in den &lt;b&gt;Vollbildmodus&lt;/b&gt; geschaltet. Sie können jederzeit durch Betätigen von &lt;b&gt;%1&lt;/b&gt; zum normalen Fenstermodus zurückkehren. Als &lt;i&gt;Host-Taste&lt;/i&gt; ist momentan &lt;b&gt;%2&lt;/b&gt; eingestellt.&lt;/p&gt;&lt;p&gt;Das Hauptmenü wird im Vollbildmodus nicht angezeigt. Auf dieses kann durch Betätigen von &lt;b&gt;Host+Home&lt;/b&gt; zugegriffen werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The virtual machine window will be now switched to &lt;b&gt;Seamless&lt;/b&gt; mode. You can go back to windowed mode at any time by pressing &lt;b&gt;%1&lt;/b&gt;. Note that the &lt;i&gt;Host&lt;/i&gt; key is currently defined as &lt;b&gt;%2&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Note that the main menu bar is hidden in seamless mode. You can access it by pressing &lt;b&gt;Host+Home&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die virtuelle Maschine wird nun in den &lt;b&gt;nahtlosen&lt;/b&gt; Anzeigemodus geschaltet. Durch Betätigen von &lt;b&gt;%1&lt;/b&gt; können Sie jederzeit in den normalen Fenstermodus zurückkehren. Die &lt;i&gt;Host-Taste&lt;/i&gt; ist momentan als &lt;b&gt;%2&lt;/b&gt; eingestellt.&lt;/p&gt;&lt;p&gt;Das Hauptmenü wird im nahtlosen Modus nicht angezeigt. Auf dieses kann durch Betätigung von &lt;b&gt;Host+Home&lt;/b&gt; zugegriffen werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Contents...</source>
        <translation>&amp;Inhalt...</translation>
    </message>
    <message>
        <source>Show the online help contents</source>
        <translation>Zeigt die Online-Hilfe</translation>
    </message>
    <message>
        <source>&amp;VirtualBox Web Site...</source>
        <translation>Webseite von &amp;VirtualBox...</translation>
    </message>
    <message>
        <source>Open the browser and go to the VirtualBox product web site</source>
        <translation>Öffnet den Browser mit der Produktwebseite von VirtualBox</translation>
    </message>
    <message>
        <source>&amp;Reset All Warnings</source>
        <translation>Alle Warnungen &amp;zurücksetzen</translation>
    </message>
    <message>
        <source>Go back to showing all suppressed warnings and messages</source>
        <translation>Alle unterdrückten Warnungen werden beim nächsten Mal erneut angezeigt</translation>
    </message>
    <message>
        <source>R&amp;egister VirtualBox...</source>
        <translation>VirtualBox r&amp;egistrieren...</translation>
    </message>
    <message>
        <source>Open VirtualBox registration form</source>
        <translation>Öffnet das Formular zur Registrierung von VirtualBox</translation>
    </message>
    <message>
        <source>C&amp;heck for Updates...</source>
        <translation>Überprüfung auf &amp;Update...</translation>
    </message>
    <message>
        <source>Check for a new VirtualBox version</source>
        <translation>Überprüft, ob eine neue Version von VirtualBox verfügbar ist</translation>
    </message>
    <message>
        <source>&amp;About VirtualBox...</source>
        <translation>&amp;Über VirtualBox...</translation>
    </message>
    <message>
        <source>Show a dialog with product information</source>
        <translation>Zeigt einen Dialog mit Produktinformationen</translation>
    </message>
    <message>
        <source>&lt;p&gt;A new version of VirtualBox has been released! Version &lt;b&gt;%1&lt;/b&gt; is available at &lt;a href=&quot;http://www.virtualbox.org/&quot;&gt;virtualbox.org&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;You can download this version using the link:&lt;/p&gt;&lt;p&gt;&lt;a href=%2&gt;%3&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Eine neue Version von VirtualBox ist verfügbar! Version &lt;b&gt;%1&lt;/b&gt; ist auf &lt;a href=&quot;http://www.virtualbox.org/&quot;&gt;virtualbox.org&lt;/a&gt; verfügbar.&lt;/p&gt;&lt;p&gt;Sie können diese Version von der folgenden Adresse herunterladen:&lt;/p&gt;&lt;p&gt;&lt;a href=%2&gt;%3&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to release the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;?&lt;/p&gt;&lt;p&gt;This will detach it from the following virtual machine(s): &lt;b&gt;%3&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; wirklich freigeben?&lt;/p&gt;&lt;p&gt;Dieses Medium ist momentan an folgende virtuelle Maschinen gebunden: &lt;b&gt;%3&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Release</source>
        <comment>detach medium</comment>
        <translation>Freigeben</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to remove the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; from the list of known media?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie die %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; wirklich von der Liste der bekannten Medien löschen?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Note that as this hard disk is inaccessible its storage unit cannot be deleted right now.</source>
        <translation>Beachten Sie, dass die Festplatte nicht zugreifbar ist. Daher kann diese momentan nicht gelöscht werden.</translation>
    </message>
    <message>
        <source>The next dialog will let you choose whether you also want to delete the storage unit of this hard disk or keep it for later usage.</source>
        <translation>Im folgenden Dialog haben Sie die Möglichkeit, das Speichermedium explizit zu löschen oder für eine spätere Verwendung zu bewahren.</translation>
    </message>
    <message>
        <source>&lt;p&gt;Note that the storage unit of this medium will not be deleted and that it will be possible to add it to the list later again.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Beachten Sie, dass der Datencontainer für dieses Medium nicht gelöscht wird und Sie daher das Medium später zu dieser Liste wieder hinzufügen können.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Remove</source>
        <comment>medium</comment>
        <translation>Entfernen</translation>
    </message>
    <message>
        <source>&lt;p&gt;The hard disk storage unit at location &lt;b&gt;%1&lt;/b&gt; already exists. You cannot create a new virtual hard disk that uses this location because it can be already used by another virtual hard disk.&lt;/p&gt;&lt;p&gt;Please specify a different location.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Der Datencontainer &lt;b&gt;%1&lt;/b&gt; ist bereits vorhanden. Sie können keine neue virtuelle Festplatte mit diesem Container erstellen, weil dieser bereits von einer anderen virtuellen Maschine benutzt werden kann.&lt;/p&gt;&lt;p&gt;Bitte wählen Sie einen anderen Namen bzw. einen anderen Ordner.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Do you want to delete the storage unit of the hard disk &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;?&lt;/p&gt;&lt;p&gt;If you select &lt;b&gt;Delete&lt;/b&gt; then the specified storage unit will be permanently deleted. This operation &lt;b&gt;cannot be undone&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;If you select &lt;b&gt;Keep&lt;/b&gt; then the hard disk will be only removed from the list of known hard disks, but the storage unit will be left untouched which makes it possible to add this hard disk to the list later again.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie den Datencontainer der virtuellen Festplatte &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; wirklich löschen?&lt;/p&gt;&lt;p&gt;Wählen Sie &lt;b&gt;Löschen&lt;/b&gt;, um den Container permanent zu löschen. Diese Operation &lt;b&gt;kann nicht&lt;/b&gt; rückgäng gemacht werden.&lt;/p&gt;&lt;p&gt;Wählen Sie &lt;b&gt;Behalten&lt;/b&gt; um die virtuelle Festplatte von der Liste der bekannten Medien zu löschen, den Datencontainer aber zu behalten. Dadurch können Sie die virtuelle Festplatte später wieder hinzufügen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Delete</source>
        <comment>hard disk storage</comment>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>Keep</source>
        <comment>hard disk storage</comment>
        <translation>Behalten</translation>
    </message>
    <message>
        <source>Failed to delete the storage unit of the hard disk &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Der Datencontainer der virtuellen Festplatte &lt;b&gt;%1&lt;/b&gt; konnte nicht gelöscht werden.</translation>
    </message>
    <message>
        <source>Failed to create the hard disk storage &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;.&lt;/nobr&gt;</source>
        <translation>Die virtuelle Festplatte &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; konnte nicht erzeugt werden.</translation>
    </message>
    <message>
        <source>Failed to open the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>Die %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <source>Failed to close the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>Die %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; konnte nicht geschlossen werden.</translation>
    </message>
    <message>
        <source>Failed to determine the accessibility state of the medium &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>Die Rechte für das Medium &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; konnten nicht ermittelt werden.</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to connect to the VirtualBox online registration service due to the following error:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die Verbindung zur VirtualBox-Online-Registrierung konnte aufgrund des folgenden Fehlers nicht aufgebaut werden:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Unable to obtain the new version information due to the following error:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Neue Versionsinformationen konnten aufgrund des folgenden Fehlers nicht ermittelt werden:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;One or more virtual hard disks, CD/DVD or floppy media are not currently accessible. As a result, you will not be able to operate virtual machines that use these media until they become accessible later.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;Check&lt;/b&gt; to open the Virtual Media Manager window and see what media are inaccessible, or press &lt;b&gt;Ignore&lt;/b&gt; to ignore this message.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Eine oder mehrere virtuelle Festplatten, CD/DVD-ROMs oder Diskettenmedien sind momentan nicht zugreifbar. Virtuelle Maschinen, die diese Medien benutzen, können so lange nicht benutzt werden, bis die Medien wieder zugreifbar werden.&lt;/p&gt;&lt;p&gt;Wählen Sie &lt;b&gt;Überprüfen&lt;/b&gt; um den Manager für virtuelle Medien zu öffnen oder wählen Sie &lt;b&gt;Ignorieren&lt;/b&gt;, um dieses Problem zu ignorieren.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;A critical error has occurred while running the virtual machine and the machine execution has been stopped.&lt;/p&gt;&lt;p&gt;For help, please see the Community section on &lt;a href=http://www.virtualbox.org&gt;http://www.virtualbox.org&lt;/a&gt; or your support contract. Please provide the contents of the log file &lt;tt&gt;VBox.log&lt;/tt&gt; and the image file &lt;tt&gt;VBox.png&lt;/tt&gt;, which you can find in the &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; directory, as well as a description of what you were doing when this error happened. Note that you can also access the above files by selecting &lt;b&gt;Show Log&lt;/b&gt; from the &lt;b&gt;Machine&lt;/b&gt; menu of the main VirtualBox window.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;OK&lt;/b&gt; if you want to power off the machine or press &lt;b&gt;Ignore&lt;/b&gt; if you want to leave it as is for debugging. Please note that debugging requires special knowledge and tools, so it is recommended to press &lt;b&gt;OK&lt;/b&gt; now.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Ein schwerwiegender Fehler ist aufgetreten, und die Ausführung der virtuellen Maschine wurde unterbrochen.&lt;/p&gt;&lt;p&gt;Zusätzliche Informationen zu diesem Fehler suchen Sie bitte in der Community-Sektion auf &lt;a href=http://www.virtualbox.org&gt;http://www.virtualbox.org&lt;/a&gt; bzw. handeln Sie gemäß Ihres Supportvertrages. Bitte geben Sie die Logdatei &lt;tt&gt;VBox.log&lt;/tt&gt;, den Screenshot &lt;tt&gt;VBox.png&lt;/tt&gt;, den Sie im Verzeichnis &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; finden können, sowie eine Beschreibung der Maßnahmen, die zu diesem Fehler führten, weiter. Sie können diese Dateien auch durch Auswahl von &lt;b&gt;Zeige Log...&lt;/b&gt; im Menü &lt;b&gt;Maschine&lt;/b&gt; des Hauptfensters finden.&lt;/p&gt;&lt;p&gt;Wählen Sie &lt;b&gt;OK&lt;/b&gt;, wenn Sie die virtuelle Maschine ausschalten wollen. Wählen Sie &lt;b&gt;Ignorieren&lt;/b&gt;, wenn Sie diese für Debugging offen lassen wollen. Zum Debuggen sind spezielle Kenntnisse und Tools notwendig, so dass die empfohlene Aktion hier &lt;b&gt;OK&lt;/b&gt; ist.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Could not access USB on the host system, because neither the USB file system (usbfs) nor the DBus and hal services are currently available. If you wish to use host USB devices inside guest systems, you must correct this and restart VirtualBox.</source>
        <translation>Der Zugriff auf USB-Geräte des Hosts ist nicht möglich, weil weder das USB-Dateisystem (usbfs) mit den entsprechenden Rechten gemountet ist, noch der DBus / Hal-Service verfügbar ist. Falls Sie USB-Geräte des Hosts innerhalb von Gästen nutzen wollen, müssen Sie dieses Problem beheben und VirtualBox neu starten.</translation>
    </message>
    <message>
        <source>You are trying to shut down the guest with the ACPI power button. This is currently not possible because the guest does not support software shutdown.</source>
        <translation>Sie versuchen den Gast mittels ACPI-Signal zu beenden. Dies ist nicht möglich, weil der Gast momentan das ACPI-Subsystem nicht benutzt.</translation>
    </message>
    <message>
        <source>Close VM</source>
        <translation>VM beenden</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>Fortfahren</translation>
    </message>
    <message>
        <source>Change</source>
        <comment>hard disk</comment>
        <translation>Ändern</translation>
    </message>
    <message>
        <source>Failed to create the host-only network interface.</source>
        <translation>Das Host-only-Interface konnte nicht erzeugt werden.</translation>
    </message>
    <message>
        <source>Failed to open appliance.</source>
        <translation>Die Appliance konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <source>Failed to open/interpret appliance &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die Appliance &lt;b&gt;%1&lt;/b&gt; konnte nicht geöffnet bzw. nicht interpretiert werden.</translation>
    </message>
    <message>
        <source>Failed to import appliance &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die Appliance &lt;b&gt;%1&lt;/b&gt; konnte nicht importiert werden.</translation>
    </message>
    <message>
        <source>Failed to create an appliance.</source>
        <translation>Die Appliance konnte nicht erzeugt werden.</translation>
    </message>
    <message>
        <source>Failed to prepare the export of the appliance &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Der Export der Appliance konnte nicht vorbereitet werden &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>Failed to export appliance &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>Die Appliance &lt;b&gt;%1&lt;/b&gt; konnte nicht exportiert werden.</translation>
    </message>
    <message>
        <source>Failed to remove the file &lt;b&gt;%1&lt;/b&gt;.&lt;br /&gt;&lt;br /&gt;Please try to remove the file yourself and try again.</source>
        <translation>Die Datei &lt;b&gt;%1&lt;/b&gt; konnte nicht gelöscht werden.&lt;br/&gt;&lt;br/&gt;Bitte versuchen Sie, die Datei manuell zu löschen und wiederholen Sie dann diese Aktion.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>&lt;p&gt;Your existing VirtualBox settings files will be automatically converted from the old format to a new format required by the new version of VirtualBox.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;OK&lt;/b&gt; to start VirtualBox now or press &lt;b&gt;Exit&lt;/b&gt; if you want to terminate the VirtualBox application without any further actions.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die vorhandenen VirtualBox-Einstellungen werden automatisch aus dem alten Format in ein neues Format für die aktuelle Version von VirtualBox konvertiert.&lt;/p&gt;&lt;p&gt;Wählen Sie &lt;b&gt;OK&lt;/b&gt;, um VirtualBox nun zu starten oder &lt;b&gt;Beenden&lt;/b&gt;, um VirtualBox ohne Speichern der Konvertierungen zu beenden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>A file named &lt;b&gt;%1&lt;/b&gt; already exists. Are you sure you want to replace it?&lt;br /&gt;&lt;br /&gt;Replacing it will overwrite its contents.</source>
        <translation>Eine Datei mit dem Namen &lt;b&gt;%1&lt;/b&gt; existiert bereits. Sind Sie sicher, dass Sie diese ersetzen möchten?&lt;br/&gt;&lt;br/&gt;Durch Ersetzen wird der alte Inhalt überschrieben.</translation>
    </message>
    <message>
        <source>&lt;p&gt;VT-x/AMD-V hardware acceleration has been enabled, but is not operational. Certain guests (e.g. OS/2 and QNX) require this feature.&lt;/p&gt;&lt;p&gt;Please ensure that you have enabled VT-x/AMD-V properly in the BIOS of your host computer.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die Einstellung VT-X/AMD-V wurde aktiviert, diese Funktion ist aber nicht verfügbar. Bestimmte Gäste (z.B. OS/2 und QNX) werden ohne diese Funktion nicht ordnungsgemäß ausgeführt.&lt;/p&gt;&lt;p&gt;Bitte stellen Sie sicher, dass VT-x/AMD-V ordnungsgemäß im BIOS ihres Computers aktiviert wurde.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Invalid e-mail address or password specified.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Ungültige E-Mail-Adresse oder ungültiges Passwort angegeben.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to register the VirtualBox product.&lt;/p&gt;&lt;p&gt;%1&lt;/p&gt;</source>
        <translation>&lt;p&gt;Fehler beim Registrieren von VirtualBox.&lt;/p&gt;&lt;p&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to check files.</source>
        <translation>Dateien konnten nicht überprüft werden.</translation>
    </message>
    <message>
        <source>Failed to remove file.</source>
        <translation>Dateien konnten nicht gelöscht werden.</translation>
    </message>
    <message>
        <source>You seem to have the USBFS filesystem mounted at /sys/bus/usb/drivers. We strongly recommend that you change this, as it is a severe mis-configuration of your system which could cause USB devices to fail in unexpected ways.</source>
        <translation>Sie haben anscheinend das USBFS-Dateisystem unter /sys/bus/usb/drivers eingebunden. Es wird dringend empfohlen, dies zu korrigieren, weil anderenfalls USB-Geräte nicht ordnungsgemäß funktionieren.</translation>
    </message>
    <message>
        <source>You are running an EXPERIMENTAL build of VirtualBox. This version is not suitable for production use.</source>
        <translation>Sie verwenden eine EXPERIMENTELLE Version von VirtualBox. Diese Version ist nicht für den produktiven Einsatz gedacht.</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to restore snapshot &lt;b&gt;%1&lt;/b&gt;? This will cause you to lose your current machine state, which cannot be recovered.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie wirklich zum Sicherungspunkt &lt;b&gt;%1&lt;/b&gt; zurückkehren? Dadurch wird der aktuelle Zustand der virtuellen Maschine unwiderruflich verworfen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>Zurückkehren</translation>
    </message>
    <message>
        <source>&lt;p&gt;Deleting the snapshot will cause the state information saved in it to be lost, and disk data spread over several image files that VirtualBox has created together with the snapshot will be merged into one file. This can be a lengthy process, and the information in the snapshot cannot be recovered.&lt;/p&gt;&lt;/p&gt;Are you sure you want to delete the selected snapshot &lt;b&gt;%1&lt;/b&gt;?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wenn ein Sicherungspunkt gelöscht wird, dann werden die Zustandsinformationen des Sicherungspunktes verworfen und Dateien, die Differenzinformationen enthalten, zusammengefasst. Dieser Vorgang kann einige Zeit in Anspruch nehmen, und die Information aus dem Sicherungspunkt kann danach nicht mehr restaueriert werden.&lt;/p&gt;&lt;p&gt;Möchten Sie den Sicherungspunkt &lt;b&gt;%1&lt;/b&gt; wirklich löschen?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>Failed to restore the snapshot &lt;b&gt;%1&lt;/b&gt; of the virtual machine &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>Fehler beim Zurückkehren auf den Sicherungspunkt &lt;b&gt;%1&lt;b&gt; der virtuellen Maschine &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>Failed to delete the snapshot &lt;b&gt;%1&lt;/b&gt; of the virtual machine &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>Fehler beim Löschen des Sicherungspunktes &lt;b&gt;%1&lt;/b&gt; der virtuellen Maschine &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>&lt;p&gt;There are no unused media available for the newly created attachment.&lt;/p&gt;&lt;p&gt;Press the &lt;b&gt;Create&lt;/b&gt; button to start the &lt;i&gt;New Virtual Disk&lt;/i&gt; wizard and create a new medium, or press the &lt;b&gt;Select&lt;/b&gt; if you wish to open the &lt;i&gt;Virtual Media Manager&lt;/i&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Es sind nicht verwendete Medien für den neu erzeugten Anschluss vorhanden.&lt;/p&gt;&lt;p&gt;Betätigen Sie &lt;b&gt;Erzeugen&lt;/b&gt;, um ein neues Medium zu erzeugen oder &lt;b&gt;Auswählen&lt;/b&gt;, um den &lt;i&gt;Manager für virtuelle Medien&lt;/i&gt; zu öffnen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Create</source>
        <comment>medium</comment>
        <translation>&amp;Erzeugen</translation>
    </message>
    <message>
        <source>&amp;Select</source>
        <comment>medium</comment>
        <translation>&amp;Auswählen</translation>
    </message>
    <message>
        <source>&lt;p&gt;There are no unused media available for the newly created attachment.&lt;/p&gt;&lt;p&gt;Press the &lt;b&gt;Select&lt;/b&gt; if you wish to open the &lt;i&gt;Virtual Media Manager&lt;/i&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Es sind keine nicht verwendeten Medien für den neuen Anschluss verfügbar.&lt;/p&gt;&lt;p&gt;Betätigen Sie &lt;b&gt;Auswählen&lt;/b&gt; zum Öffnen des &lt;i&gt;Managers für virtuelle Medien&lt;/i&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to attach the %1 to slot &lt;i&gt;%2&lt;/i&gt; of the machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>%1 konnte nicht an den Slot &lt;i&gt;%2&lt;/i&gt; der Maschine &lt;b&gt;%3&lt;/b&gt; angeschlossen werden.</translation>
    </message>
    <message>
        <source>Failed to detach the %1 from slot &lt;i&gt;%2&lt;/i&gt; of the machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>%1 konnte nicht vom Slot &lt;i&gt;%2&lt;/i&gt; der Maschine &lt;b&gt;%3&lt;/b&gt; getrennt werden.</translation>
    </message>
    <message>
        <source>Unable to mount the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; on the machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>%1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; konnte nicht an die Maschine &lt;b&gt;%3&lt;/b&gt; gebunden werden.</translation>
    </message>
    <message>
        <source> Would you like to force mounting of this medium?</source>
        <translation> Möchten Sie die Anbindung des Mediums erzwingen?</translation>
    </message>
    <message>
        <source>Unable to unmount the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; from the machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>%1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; der Maschine &lt;b&gt;%3&lt;/b&gt; konnte nicht ausgeworfen werden.</translation>
    </message>
    <message>
        <source> Would you like to force unmounting of this medium?</source>
        <translation> Möchten Sie den Auswurf dieses Mediums erzwingen?</translation>
    </message>
    <message>
        <source>Force Unmount</source>
        <translation>Auswurf erzwingen</translation>
    </message>
    <message>
        <source>Failed to eject the disk from the virtual drive. The drive may be locked by the guest operating system. Please check this and try again.</source>
        <translation>Das Medium konnte nicht ausgeworfen werden, da es möglicherweise vom Gast blockiert wird. Bitte überprüfen Sie dies und versuchen Sie es noch einmal.</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not insert the VirtualBox Guest Additions installer CD image into the virtual machine &lt;b&gt;%1&lt;/b&gt;, as the machine has no CD/DVD-ROM drives. Please add a drive using the storage page of the virtual machine settings dialog.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Das CD-Abbild mit den Gast-Erweiterungen konnte nicht an die Maschine &lt;b&gt;%1&lt;/b&gt; gebunden werden, da diese Maschine kein CD/DVD-Laufwerk besitzt. Bitte fügen Sie ein solches auf der Seite Massenspeicher der Einstellungen für die virtuelle Maschine hinzu.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <comment>warnAboutSettingsAutoConversion message box</comment>
        <translation>B&amp;eenden</translation>
    </message>
    <message>
        <source>&lt;p&gt;The following VirtualBox settings files will be automatically converted from the old format to a new format required by the new version of VirtualBox.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;OK&lt;/b&gt; to start VirtualBox now or press &lt;b&gt;Exit&lt;/b&gt; if you want to terminate the VirtualBox application without any further actions.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die folgenden Einstellungen von VirtualBox werden automatisch vom alten Format in ein neues Format konvertiert.&lt;/p&gt;&lt;p&gt;Betätigen Sie &lt;b&gt;OK&lt;/b&gt; um VirtualBox zu starten oder &lt;b&gt;Beenden&lt;/b&gt; falls Sie VirtualBox ohne weitere Änderungen abbrechen wollen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>hard disk</source>
        <comment>failed to mount ...</comment>
        <translation>Die Festplatte</translation>
    </message>
    <message>
        <source>CD/DVD</source>
        <comment>failed to mount ... host-drive</comment>
        <translation>Dass CD/DVD-Laufwerk</translation>
    </message>
    <message>
        <source>CD/DVD image</source>
        <comment>failed to mount ...</comment>
        <translation>Das CD/DVD-Abbild</translation>
    </message>
    <message>
        <source>floppy</source>
        <comment>failed to mount ... host-drive</comment>
        <translation>Das Diskettenlaufwerk</translation>
    </message>
    <message>
        <source>floppy image</source>
        <comment>failed to mount ...</comment>
        <translation>Das Diskettenabbild</translation>
    </message>
    <message>
        <source>hard disk</source>
        <comment>failed to attach ...</comment>
        <translation>Die Festplatte</translation>
    </message>
    <message>
        <source>CD/DVD device</source>
        <comment>failed to attach ...</comment>
        <translation>Das CD/DVD-Laufwerk</translation>
    </message>
    <message>
        <source>floppy device</source>
        <comment>failed to close ...</comment>
        <translation>Das Diskettenlaufwerk</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to delete the CD/DVD-ROM device?&lt;/p&gt;&lt;p&gt;You will not be able to mount any CDs or ISO images or install the Guest Additions without it!&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie wirklich das CD/DVD-ROM-Laufwerk löschen?&lt;/p&gt;&lt;p&gt;Ohne CD/DVD-ROM-Laufwerk können Sie keine CDs oder CD-Abbilder einbinden und die Gast-Erweiterungen nicht installieren!&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <comment>medium</comment>
        <translation>&amp;Entfernen</translation>
    </message>
</context>
<context>
    <name>VBoxProgressDialog</name>
    <message>
        <source>%1 days, %2 hours remaining</source>
        <translation type="obsolete">noch %1 Tag(e), %2 Stunde(n)</translation>
    </message>
    <message>
        <source>%1 days, %2 minutes remaining</source>
        <translation type="obsolete">noch %1 Tage und %2 Minute(n)</translation>
    </message>
    <message>
        <source>%1 days remaining</source>
        <translation type="obsolete">noch %1 Tage</translation>
    </message>
    <message>
        <source>1 day, %1 hours remaining</source>
        <translation type="obsolete">noch einen Tag und %1 Stunde(n)</translation>
    </message>
    <message>
        <source>1 day, %1 minutes remaining</source>
        <translation type="obsolete">noch einen Tag und %1 Minute(n)</translation>
    </message>
    <message>
        <source>1 day remaining</source>
        <translation type="obsolete">noch einen Tag</translation>
    </message>
    <message>
        <source>%1 hours, %2 minutes remaining</source>
        <translation type="obsolete">noch %1 Stunden und %2 Minute(n)</translation>
    </message>
    <message>
        <source>1 hour, %1 minutes remaining</source>
        <translation type="obsolete">noch eine Stunde und %1 Minute(n)</translation>
    </message>
    <message>
        <source>1 hour remaining</source>
        <translation type="obsolete">noch eine Stunde</translation>
    </message>
    <message>
        <source>%1 minutes remaining</source>
        <translation type="obsolete">noch %1 Minuten</translation>
    </message>
    <message>
        <source>1 minute, %2 seconds remaining</source>
        <translation type="obsolete">noch eine Minute und %2 Sekunden</translation>
    </message>
    <message>
        <source>1 minute remaining</source>
        <translation type="obsolete">noch eine Minute</translation>
    </message>
    <message>
        <source>%1 seconds remaining</source>
        <translation type="obsolete">noch %1 Sekunden</translation>
    </message>
    <message>
        <source>A few seconds remaining</source>
        <translation>noch einige Sekunden</translation>
    </message>
    <message>
        <source>Canceling...</source>
        <translation>Wird abgebrochen...</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <source>Cancel the current operation</source>
        <translation>Aktuelle Operation abbrechen</translation>
    </message>
    <message>
        <source>%1, %2 remaining</source>
        <comment>You may wish to translate this more like &quot;Time remaining: %1, %2&quot;</comment>
        <translation>noch %1, %2</translation>
    </message>
    <message>
        <source>%1 remaining</source>
        <comment>You may wish to translate this more like &quot;Time remaining: %1&quot;</comment>
        <translation>noch %1</translation>
    </message>
</context>
<context>
    <name>VBoxRegistrationDlg</name>
    <message>
        <source>VirtualBox Registration Dialog</source>
        <translation type="obsolete">VirtualBox registrieren</translation>
    </message>
    <message>
        <source>Enter your full name using Latin characters.</source>
        <translation type="obsolete">Geben Sie hier Ihren vollen Namen in lateinischen Buchstaben ein.</translation>
    </message>
    <message>
        <source>Enter your e-mail address. Please use a valid address here.</source>
        <translation type="obsolete">Tragen Sie hier eine gültige EMail-Adresse ein.</translation>
    </message>
    <message>
        <source>Welcome to the VirtualBox Registration Form!</source>
        <translation type="obsolete">VirtualBox registrieren</translation>
    </message>
    <message>
        <source>Could not perform connection handshake.</source>
        <translation type="obsolete">Der Server lieferte nicht die erwarteten Daten als Antwort.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>&lt;p&gt;Please fill out this registration form to let us know that you use VirtualBox and, optionally, to keep you informed about VirtualBox news and updates.&lt;/p&gt;&lt;p&gt;Please use Latin characters only to fill in  the fields below. Sun Microsystems will use this information only to gather product usage statistics and to send you VirtualBox newsletters. In particular, Sun Microsystems will never pass your data to third parties. Detailed information about how we use your personal data can be found in the &lt;b&gt;Privacy Policy&lt;/b&gt; section of the VirtualBox Manual or on the &lt;a href=http://www.virtualbox.org/wiki/PrivacyPolicy&gt;Privacy Policy&lt;/a&gt; page of the VirtualBox web-site.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Bitte füllen Sie dieses Formular aus, damit wir wissen, dass Sie VirtualBox benutzen. Außerdem möchten wir Sie gern über Updates sowie über Neuigkeiten rund um VirtualBox informieren.&lt;p&gt;&lt;p&gt;Bitte benutzen Sie möglichst lateinische Buchstaben. Sun Microsystems wird diese Daten nur benutzen, um Nutzerstatistiken von VirtualBox zu erstellen und um Ihnen optional Informationen zu VirtualBox zu senden. Insbesondere wird Sun Microsystems Ihre persönlichen Daten niemals an Dritte weitergeben. Detaillierte Informationen über die Nutzung Ihrer persönlichen Daten können Sie im Abschnitt &lt;b&gt;Privacy Policy&lt;/b&gt; des Handbuchs oder unter &lt;a href=http://www.virtualbox.org/wiki/PrivacyPolicy&gt;Privacy Policy&lt;/a&gt; auf der VirtualBox-Webseite nachlesen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>I &amp;already have a Sun Online account:</source>
        <translation type="obsolete">Ich &amp;besitze bereits einen Sun-Online-Account:</translation>
    </message>
    <message>
        <source>&amp;E-mail:</source>
        <translation type="obsolete">&amp;E-mail:</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation type="obsolete">&amp;Passwort:</translation>
    </message>
    <message>
        <source>I &amp;would like to create a new Sun Online account:</source>
        <translation type="obsolete">Ich &amp;möchte einen neuen Sun-Account für die Registrierung erzeugen:</translation>
    </message>
    <message>
        <source>&amp;First Name:</source>
        <translation type="obsolete">&amp;Vorname:</translation>
    </message>
    <message>
        <source>&amp;Last Name:</source>
        <translation type="obsolete">&amp;Nachname:</translation>
    </message>
    <message>
        <source>&amp;Company:</source>
        <translation type="obsolete">&amp;Firma:</translation>
    </message>
    <message>
        <source>Co&amp;untry:</source>
        <translation type="obsolete">&amp;Land:</translation>
    </message>
    <message>
        <source>E-&amp;mail:</source>
        <translation type="obsolete">E-&amp;mail:</translation>
    </message>
    <message>
        <source>P&amp;assword:</source>
        <translation type="obsolete">P&amp;asswort:</translation>
    </message>
    <message>
        <source>Co&amp;nfirm Password:</source>
        <translation type="obsolete">Pa&amp;sswort bestätigen:</translation>
    </message>
    <message>
        <source>&amp;Register</source>
        <translation type="obsolete">&amp;Registrieren</translation>
    </message>
    <message>
        <source>Select Country/Territory</source>
        <translation type="obsolete">Wählen Sie das Land/Gebiet</translation>
    </message>
</context>
<context>
    <name>VBoxSFDialog</name>
    <message>
        <source>Shared Folders</source>
        <translation>Gemeinsame Ordner</translation>
    </message>
</context>
<context>
    <name>VBoxScreenshotViewer</name>
    <message>
        <source>Screenshot of %1 (%2)</source>
        <translation>Screenshot von %1 (%2)</translation>
    </message>
    <message>
        <source>Click to view non-scaled screenshot.</source>
        <translation>Klicken für nicht skalierten Screenshot.</translation>
    </message>
    <message>
        <source>Click to view scaled screenshot.</source>
        <translation>Klicken für skalierten Screenshot.</translation>
    </message>
</context>
<context>
    <name>VBoxSelectorWnd</name>
    <message>
        <source>VirtualBox OSE</source>
        <translation>VirtualBox OSE</translation>
    </message>
    <message>
        <source>&amp;Details</source>
        <translation>&amp;Details</translation>
    </message>
    <message>
        <source>&amp;Preferences...</source>
        <comment>global settings</comment>
        <translation>&amp;Globale Einstellungen...</translation>
    </message>
    <message>
        <source>Display the global settings dialog</source>
        <translation>Zeigt den Dialog für globale Einstellungen</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>B&amp;eenden</translation>
    </message>
    <message>
        <source>Close application</source>
        <translation>Anwendung schließen</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation>&amp;Neu...</translation>
    </message>
    <message>
        <source>Create a new virtual machine</source>
        <translation>Neue virtuelle Maschine erzeugen</translation>
    </message>
    <message>
        <source>&amp;Settings...</source>
        <translation>Änd&amp;ern...</translation>
    </message>
    <message>
        <source>Configure the selected virtual machine</source>
        <translation>Einstellungen der ausgewählten virtuellen Maschine ändern</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Löschen</translation>
    </message>
    <message>
        <source>Delete the selected virtual machine</source>
        <translation>Löscht die ausgewählte virtuelle Maschine</translation>
    </message>
    <message>
        <source>D&amp;iscard</source>
        <translation>&amp;Verwerfen</translation>
    </message>
    <message>
        <source>Discard the saved state of the selected virtual machine</source>
        <translation>Verwirft den gesicherten Zustand der ausgewählten virtuellen Maschine</translation>
    </message>
    <message>
        <source>Refresh the accessibility state of the selected virtual machine</source>
        <translation>Aktualisiert den Zustand der ausgewählten virtuellen Maschine</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <source>&amp;Snapshots</source>
        <translation>&amp;Sicherungspunkte</translation>
    </message>
    <message>
        <source>D&amp;escription</source>
        <translation>&amp;Beschreibung</translation>
    </message>
    <message>
        <source>D&amp;escription *</source>
        <translation>&amp;Beschreibung *</translation>
    </message>
    <message>
        <source>S&amp;how</source>
        <translation>&amp;Zeigen</translation>
    </message>
    <message>
        <source>Switch to the window of the selected virtual machine</source>
        <translation>Wechselt zum Fenster der ausgewählten virtuellen Maschine</translation>
    </message>
    <message>
        <source>S&amp;tart</source>
        <translation>&amp;Starten</translation>
    </message>
    <message>
        <source>Start the selected virtual machine</source>
        <translation>Starten der virtuellen Maschine</translation>
    </message>
    <message>
        <source>&amp;Machine</source>
        <translation>&amp;Maschine</translation>
    </message>
    <message>
        <source>Show &amp;Log...</source>
        <translation>Zeige &amp;Log...</translation>
    </message>
    <message>
        <source>Show the log files of the selected virtual machine</source>
        <translation>Zeigt die Log-Dateien der ausgewählten virtuellen Maschine</translation>
    </message>
    <message>
        <source>R&amp;esume</source>
        <translation>&amp;Fortfahren</translation>
    </message>
    <message>
        <source>Resume the execution of the virtual machine</source>
        <translation>Fährt mit der Ausführung der virtuellen Maschine fort</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <source>Suspend the execution of the virtual machine</source>
        <translation>Suspendiert die Ausführung der virtuellen Maschine</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Welcome to VirtualBox!&lt;/h3&gt;&lt;p&gt;The left part of this window is  a list of all virtual machines on your computer. The list is empty now because you haven&apos;t created any virtual machines yet.&lt;img src=:/welcome.png align=right/&gt;&lt;/p&gt;&lt;p&gt;In order to create a new virtual machine, press the &lt;b&gt;New&lt;/b&gt; button in the main tool bar located at the top of the window.&lt;/p&gt;&lt;p&gt;You can press the &lt;b&gt;%1&lt;/b&gt; key to get instant help, or visit &lt;a href=http://www.virtualbox.org&gt;www.virtualbox.org&lt;/a&gt; for the latest information and news.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Willkommen zu VirtualBox!&lt;/h3&gt;&lt;p&gt;Der linke Teil dieses Fensters zeigt eine Liste aller virtuellen Maschinen auf Ihrem Computer. Diese Liste ist momentan leer, da Sie noch keine virtuelle Maschine erstellt haben.&lt;img src=:/welcome.png align=right/&gt;&lt;/p&gt;&lt;p&gt;Um eine virtuelle Maschine anzulegen, wählen Sie &lt;b&gt;Neu&lt;/b&gt; in der Symbolleiste am oberen Rand des Fensters.&lt;/p&gt;&lt;p&gt;Die Taste &lt;b&gt;%1&lt;/b&gt; öffnet das Hilfefenster. Für aktuelle Produktinformationen aus dem Internet öffnen Sie &lt;a href=http://www.virtualbox.org&gt;www.virtualbox.org&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Virtual Media Manager...</source>
        <translation>Manager für virtuelle &amp;Medien...</translation>
    </message>
    <message>
        <source>Display the Virtual Media Manager dialog</source>
        <translation>Zeigt den Manager für virtuelle Medien</translation>
    </message>
    <message>
        <source>Log</source>
        <comment>icon text</comment>
        <translation>Loggen</translation>
    </message>
    <message>
        <source>&amp;Import Appliance...</source>
        <translation>Appliance &amp;importieren ...</translation>
    </message>
    <message>
        <source>Import an appliance into VirtualBox</source>
        <translation>Importiert eine Appliance in VirtualBox</translation>
    </message>
    <message>
        <source>&amp;Export Appliance...</source>
        <translation>Appliance &amp;exportieren ...</translation>
    </message>
    <message>
        <source>Export one or more VirtualBox virtual machines as an appliance</source>
        <translation>Exportiert eine Appliance aus VirtualBox</translation>
    </message>
    <message>
        <source>Sun VirtualBox</source>
        <translation>Sun VirtualBox</translation>
    </message>
    <message>
        <source>Re&amp;fresh</source>
        <translation>&amp;Aktualisieren</translation>
    </message>
</context>
<context>
    <name>VBoxSettingsDialog</name>
    <message>
        <source>&lt;i&gt;Select a settings category from the list on the left-hand side and move the mouse over a settings item to get more information&lt;/i&gt;.</source>
        <translation>&lt;i&gt;Wählen Sie eine Kategorie aus der Liste auf der linken Seite und fahren Sie mit der Maus über eine Einstellung, um mehr Informationen zu erhalten&lt;/i&gt;.</translation>
    </message>
    <message>
        <source>Invalid settings detected</source>
        <translation>Ungültige Einstellungen erkannt</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <source>Non-optimal settings detected</source>
        <translation>Einige Einstellungen sind nicht optimal</translation>
    </message>
    <message>
        <source>On the &lt;b&gt;%1&lt;/b&gt; page, %2</source>
        <translation>Auf der Seite &lt;b&gt;%1&lt;/b&gt; %2</translation>
    </message>
</context>
<context>
    <name>VBoxSnapshotDetailsDlg</name>
    <message>
        <source>Details of %1 (%2)</source>
        <translation>Details von %1 (%2)</translation>
    </message>
    <message>
        <source>Click to enlarge the screenshot.</source>
        <translation>Klicken, um Screenshot zu vergrößern.</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>&amp;Name:</translation>
    </message>
    <message>
        <source>Taken:</source>
        <translation>Gespeichert:</translation>
    </message>
    <message>
        <source>&amp;Description:</source>
        <translation>&amp;Beschreibung:</translation>
    </message>
    <message>
        <source>D&amp;etails:</source>
        <translation>D&amp;etails:</translation>
    </message>
</context>
<context>
    <name>VBoxSnapshotsWgt</name>
    <message>
        <source>VBoxSnapshotsWgt</source>
        <translation></translation>
    </message>
    <message>
        <source>Current State (changed)</source>
        <comment>Current State (Modified)</comment>
        <translation>Aktueller Zustand (verändert)</translation>
    </message>
    <message>
        <source>Current State</source>
        <comment>Current State (Unmodified)</comment>
        <translation>Aktueller Zustand</translation>
    </message>
    <message>
        <source>The current state differs from the state stored in the current snapshot</source>
        <translation>Der aktuelle Zustand unterscheidet sich vom gespeicherten Zustand des aktuellen Sicherungspunktes</translation>
    </message>
    <message>
        <source>The current state is identical to the state stored in the current snapshot</source>
        <translation>Der aktuelle Zustand ist identisch mit dem gespeicherten Zustand des aktuellen Sicherungspunktes</translation>
    </message>
    <message>
        <source> (current, </source>
        <comment>Snapshot details</comment>
        <translation> (aktuell,</translation>
    </message>
    <message>
        <source>online)</source>
        <comment>Snapshot details</comment>
        <translation>online)</translation>
    </message>
    <message>
        <source>offline)</source>
        <comment>Snapshot details</comment>
        <translation>offline)</translation>
    </message>
    <message>
        <source>Taken at %1</source>
        <comment>Snapshot (time)</comment>
        <translation>Erstellt %1</translation>
    </message>
    <message>
        <source>Taken on %1</source>
        <comment>Snapshot (date + time)</comment>
        <translation>Erstellt am %1</translation>
    </message>
    <message>
        <source>%1 since %2</source>
        <comment>Current State (time or date + time)</comment>
        <translation>%1 seit %2</translation>
    </message>
    <message>
        <source>Snapshot %1</source>
        <translation>Sicherungspunkt %1</translation>
    </message>
    <message>
        <source>Take &amp;Snapshot</source>
        <translation>Sicherungspunkt &amp;erstellen</translation>
    </message>
    <message>
        <source>S&amp;how Details</source>
        <translation>Einzel&amp;heiten zeigen</translation>
    </message>
    <message>
        <source>Take a snapshot of the current virtual machine state</source>
        <translation>Erzeugt einen Sicherungspunkt der aktuellen virtuellen Maschine</translation>
    </message>
    <message>
        <source>Show the details of the selected snapshot</source>
        <translation>Zeigt Einzelheiten des ausgewählten Sicherungspunktes</translation>
    </message>
    <message>
        <source> (%1)</source>
        <translation> (%1)</translation>
    </message>
    <message numerus="yes">
        <source> (%n day(s) ago)</source>
        <translation type="obsolete">
            <numerusform> (vor %n Tag)</numerusform>
            <numerusform> (vor %n Tagen)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source> (%n hour(s) ago)</source>
        <translation type="obsolete">
            <numerusform> (vor %n Stunde)</numerusform>
            <numerusform> (vor %n Stunden)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source> (%n minute(s) ago)</source>
        <translation type="obsolete">
            <numerusform> (vor %n Minute)</numerusform>
            <numerusform> (vor %n Minuten)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source> (%n second(s) ago)</source>
        <translation type="obsolete">
            <numerusform> (vor %n Sekunde)</numerusform>
            <numerusform> (vor %n Sekunden)</numerusform>
        </translation>
    </message>
    <message>
        <source>&amp;Restore Snapshot</source>
        <translation>Sicherungspunkt &amp;wiederherstellen</translation>
    </message>
    <message>
        <source>&amp;Delete Snapshot</source>
        <translation>Sicherungspunkt &amp;löschen</translation>
    </message>
    <message>
        <source>Restore the selected snapshot of the virtual machine</source>
        <translation>Der ausgewählte Sicherungspunkt wird wieder hergestellt</translation>
    </message>
    <message>
        <source>Delete the selected snapshot of the virtual machine</source>
        <translation>Der ausgewählte Sicherungspunkt wird gelöscht</translation>
    </message>
    <message>
        <source> (%1 ago)</source>
        <translation> (vor %1)</translation>
    </message>
</context>
<context>
    <name>VBoxSwitchMenu</name>
    <message>
        <source>Disable</source>
        <translation>Deaktivieren</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Aktivieren</translation>
    </message>
</context>
<context>
    <name>VBoxTakeSnapshotDlg</name>
    <message>
        <source>Take Snapshot of Virtual Machine</source>
        <translation>Erzeugt einen Sicherungspunkt der virtuellen Maschine</translation>
    </message>
    <message>
        <source>Snapshot &amp;Name</source>
        <translation>&amp;Name des Sicherungspunktes</translation>
    </message>
    <message>
        <source>Snapshot &amp;Description</source>
        <translation>&amp;Beschreibung des Sicherungspunktes</translation>
    </message>
    <message numerus="yes">
        <source>Warning: You are taking a snapshot of a running machine which has %n immutable image(s) attached to it. As long as you are working from this snapshot the immutable image(s) will not be reset to avoid loss of data.</source>
        <translation>
            <numerusform>Warnung: Sie erstellen einen Snapshot von einer laufenden Machine, die %n unveränderliches Festplattenabbild angeschlossen hat. So lange Sie mit diesem Sicherungspunkt arbeiten, wird das unveränderliche Festplattenabbild nicht zurück gesetzt, um Datenverlust zu vermeiden.</numerusform>
            <numerusform>Warnung: Sie erstellen einen Snapshot von einer laufenden Machine, die %n unveränderliche Festplattenabbilder angeschlossen hat. So lange Sie mit diesen Sicherungspunkt arbeiten, werden die unveränderlichen Festplattenabbilder nicht zurück gesetzt, um Datenverlust zu vermeiden.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>VBoxTextEditor</name>
    <message>
        <source>Edit text</source>
        <translation>Text bearbeiten</translation>
    </message>
    <message>
        <source>&amp;Replace...</source>
        <translation>&amp;Ersetzen...</translation>
    </message>
    <message>
        <source>Replaces the current text with the content of a file.</source>
        <translation>Ersetzt den aktuellen Text mit dem Inhalt einer Datei.</translation>
    </message>
    <message>
        <source>Text (*.txt);;All (*.*)</source>
        <translation>Text (*.txt);;Alle (*.**)</translation>
    </message>
    <message>
        <source>Select a file to open...</source>
        <translation>Wählen Sie eine Datei zum Öffnen ...</translation>
    </message>
</context>
<context>
    <name>VBoxTrayIcon</name>
    <message>
        <source>Show Selector Window</source>
        <translation>Selektor-Fenster zeigen</translation>
    </message>
    <message>
        <source>Show the selector window assigned to this menu</source>
        <translation>Zeigt das diesem Menü zugeordnete Selektor-Fenster</translation>
    </message>
    <message>
        <source>Hide Tray Icon</source>
        <translation>Tray-Icon verstecken</translation>
    </message>
    <message>
        <source>Remove this icon from the system tray</source>
        <translation>Entfernt das Icon aus dem Systray</translation>
    </message>
    <message>
        <source>&amp;Other Machines...</source>
        <comment>tray menu</comment>
        <translation>&amp;Andere Maschinen...</translation>
    </message>
</context>
<context>
    <name>VBoxUSBMenu</name>
    <message>
        <source>&lt;no devices available&gt;</source>
        <comment>USB devices</comment>
        <translation>&lt;keine Geräte verfügbar&gt;</translation>
    </message>
    <message>
        <source>No supported devices connected to the host PC</source>
        <comment>USB device tooltip</comment>
        <translation>Keine unterstützten Geräte mit dem PC verbunden</translation>
    </message>
</context>
<context>
    <name>VBoxUpdateDlg</name>
    <message>
        <source>1 day</source>
        <translation>1 Tag</translation>
    </message>
    <message>
        <source>2 days</source>
        <translation>2 Tage</translation>
    </message>
    <message>
        <source>3 days</source>
        <translation>3 Tage</translation>
    </message>
    <message>
        <source>4 days</source>
        <translation>4 Tage</translation>
    </message>
    <message>
        <source>5 days</source>
        <translation>5 Tage</translation>
    </message>
    <message>
        <source>6 days</source>
        <translation>6 Tage</translation>
    </message>
    <message>
        <source>1 week</source>
        <translation>1 Woche</translation>
    </message>
    <message>
        <source>2 weeks</source>
        <translation>2 Wochen</translation>
    </message>
    <message>
        <source>3 weeks</source>
        <translation>3 Wochen</translation>
    </message>
    <message>
        <source>1 month</source>
        <translation>1 Monat</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Nie</translation>
    </message>
    <message>
        <source>Chec&amp;k</source>
        <translation>&amp;Überprüfen</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <source>VirtualBox Update Wizard</source>
        <translation>VirtualBox aktualisieren</translation>
    </message>
    <message>
        <source>Check for Updates</source>
        <translation>Auf neue Version überprüfen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <source>&lt;p&gt;A new version of VirtualBox has been released! Version &lt;b&gt;%1&lt;/b&gt; is available at &lt;a href=&quot;http://www.virtualbox.org/&quot;&gt;virtualbox.org&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;You can download this version using the link:&lt;/p&gt;&lt;p&gt;&lt;a href=%2&gt;%3&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Eine neue Version von VirtualBox ist verfügbar! Version &lt;b&gt;%1&lt;/b&gt; ist auf &lt;a href=&quot;http://www.virtualbox.org/&quot;&gt;virtualbox.org&lt;/a&gt; verfügbar.&lt;/p&gt;&lt;p&gt;Sie können diese Version von der folgenden Adresse herunterladen:&lt;/p&gt;&lt;p&gt;&lt;a href=%2&gt;%3&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Unable to obtain the new version information due to the following network error:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die Überprüfung auf eine neue Version konnte aufgrund des folgenden Netzwerkfehlers nicht durchgeführt werden:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>You are already running the most recent version of VirtualBox.</source>
        <translation>Sie haben bereits die neueste Version von VirtualBox installiert.</translation>
    </message>
    <message>
        <source>&lt;p&gt;This wizard will connect to the VirtualBox web-site and check if a newer version of VirtualBox is available.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Check&lt;/b&gt; button to check for a new version now or the &lt;b&gt;Cancel&lt;/b&gt; button if you do not want to perform this check.&lt;/p&gt;&lt;p&gt;You can run this wizard at any time by choosing &lt;b&gt;Check for Updates...&lt;/b&gt; from the &lt;b&gt;Help&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VBoxVMDescriptionPage</name>
    <message>
        <source>No description. Press the Edit button below to add it.</source>
        <translation>Noch keine Beschreibung. Betätigen Sie den Edit-Knopf um eine Beschreibung zu erstellen.</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editieren</translation>
    </message>
    <message>
        <source>Edit (Ctrl+E)</source>
        <translation>Editieren (Strg+E)</translation>
    </message>
</context>
<context>
    <name>VBoxVMDetailsView</name>
    <message>
        <source>The selected virtual machine is &lt;i&gt;inaccessible&lt;/i&gt;. Please inspect the error message shown below and press the &lt;b&gt;Refresh&lt;/b&gt; button if you want to repeat the accessibility check:</source>
        <translation>Die ausgewählte virtuelle Maschine ist &lt;i&gt;nicht zugreifbar&lt;/i&gt;. Bitte beachten Sie die Fehlermeldung, die unten gezeigt wird und betätigen Sie den &lt;b&gt;Refreshknopf&lt;/b&gt;, falls Sie den Test wiederholen wollen:</translation>
    </message>
</context>
<context>
    <name>VBoxVMFirstRunWzd</name>
    <message>
        <source>First Run Wizard</source>
        <translation>Startassistent</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have started a newly created virtual machine for the first time. This wizard will help you to perform the steps necessary for installing an operating system of your choice onto this virtual machine.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Next&lt;/b&gt; button to go to the next page of the wizard and the &lt;b&gt;Back&lt;/b&gt; button to return to the previous page. You can also press &lt;b&gt;Cancel&lt;/b&gt; if you want to cancel the execution of this wizard.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sie haben eine neu erzeugte virtuelle Maschine zum ersten Mal gestartet. Dieser Assistent wird Ihnen helfen, die für die Installation eines Betriebssystems Ihrer Wahl notwendigen Schritte auszuführen.&lt;/p&gt;&lt;p&gt;Betätigen Sie &lt;b&gt;Weiter&lt;/b&gt;, um auf die nächste Seite des Assistenten zu wechseln und &lt;b&gt;Zurück&lt;/b&gt; für die Rückkehr auf die vorherige Seite. Sie können ebenso &lt;b&gt;Abbrechen&lt;/b&gt; betätigen, um den Assistenten sofort abzubrechen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Welcome to the First Run Wizard!</source>
        <translation>Startassistent</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the type of media you would like to use for installation.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie den Typ des Mediums, das Sie für die Installation verwenden möchten.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Media Type</source>
        <translation>Medientyp</translation>
    </message>
    <message>
        <source>&amp;CD/DVD-ROM Device</source>
        <translation>&amp;CD/DVD-ROM-Laufwerk</translation>
    </message>
    <message>
        <source>&amp;Floppy Device</source>
        <translation>&amp;Diskettenlaufwerk</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the media which contains the setup program of the operating system you want to install. This media must be bootable, otherwise the setup program will not be able to start.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie das Medium mit dem Installationsprogramm des Betriebssystems, das Sie installieren wollen. Dieses Medium muss bootbar sein, anderenfalls kann das Installationsprogramm nicht starten.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Media Source</source>
        <translation>Medienquelle</translation>
    </message>
    <message>
        <source>Select Installation Media</source>
        <translation>Installationsmedium auswählen</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have selected the following media to boot from:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sie haben die folgenden Medien zum Booten ausgewählt:&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <source>CD/DVD-ROM Device</source>
        <translation>CD/DVD-ROM-Gerät</translation>
    </message>
    <message>
        <source>Floppy Device</source>
        <translation>Diskettenlaufwerk</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have started a newly created virtual machine for the first time. This wizard will help you to perform the steps necessary for booting an operating system of your choice on the virtual machine.&lt;/p&gt;&lt;p&gt;Note that you will not be able to install an operating system into this virtual machine right now because you did not attach any hard disk to it. If this is not what you want, you can cancel the execution of this wizard, select &lt;b&gt;Settings&lt;/b&gt; from the &lt;b&gt;Machine&lt;/b&gt; menu of the main VirtualBox window to access the settings dialog of this machine and change the hard disk configuration.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Next&lt;/b&gt; button to go to the next page of the wizard and the &lt;b&gt;Back&lt;/b&gt; button to return to the previous page. You can also press &lt;b&gt;Cancel&lt;/b&gt; if you want to cancel the execution of this wizard.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sie haben eine neu erzeugte virtuelle Maschine zum ersten Mal gestartet. Dieser Assistent wird Ihnen helfen, die für die Installation eines Betriebssystems Ihrer Wahl notwendigen Schritte auszuführen.&lt;/p&gt;&lt;p&gt;Beachten Sie, dass Sie kein Betriebssystem in dieser virtuellen Maschine installieren können, da Sie ihr keine Festplatte zugewiesen haben. Falls Sie eine Festplatte zuweisen wollen, beenden Sie diesen Assistenten, wählen Sie &lt;b&gt;Einstellungen&lt;/b&gt; aus dem Menü &lt;b&gt;Maschine&lt;/b&gt; des Hauptfensters um den Einstellungsdialog dieser VM zu aktivieren und ändern Sie die Konfiguration der Festplatten.&lt;/p&gt;&lt;p&gt;Betätigen Sie &lt;b&gt;Weiter&lt;/b&gt; um auf die nächste Seite des Assistenten zu wechseln und &lt;b&gt;Zurück&lt;/b&gt; für die Rückkehr auf die vorherige Seite. Sie können ebenso &lt;b&gt;Abbrechen&lt;/b&gt; betätigen, um den Assistenten sofort abzubrechen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the type of media you would like to use for booting an operating system.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie den Typ des Mediums, den Sie zum Booten des Betriebssystems benutzen wollen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the media that contains the operating system you want to work with. This media must be bootable, otherwise the operating system will not be able to start.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wählen Sie das Medium mit dem Betriebssystem, mit dem Sie arbeiten wollen. Dieses Medium muss bootbar sein, weil das System sonst nicht starten kann.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have selected the following media to boot an operating system from:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sie haben das folgende Medium zum Booten eines Betriebssystems ausgewählt:&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;If the above is correct, press the &lt;b&gt;Finish&lt;/b&gt; button. Once you press it, the selected media will be mounted on the virtual machine and the machine will start execution.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wenn die Auswahl stimmt, wählen Sie bitte &lt;b&gt;Fertigstellen&lt;/b&gt;. Danach wird das ausgewählte Medium der virtuellen Maschine zugewiesen und die Ausführung der Maschine beginnt.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Rückw</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>&amp;Weiter &gt;</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation>&amp;Fertig</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>summary</comment>
        <translation>Typ</translation>
    </message>
    <message>
        <source>Source</source>
        <comment>summary</comment>
        <translation>Quelle</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>&lt;p&gt;If the above is correct, press the &lt;b&gt;Finish&lt;/b&gt; button. Once you press it, the selected media will be temporarily mounted on the virtual machine and the machine will start execution.&lt;/p&gt;&lt;p&gt;Please note that when you close the virtual machine, the specified media will be automatically unmounted and the boot device will be set back to the first hard disk.&lt;/p&gt;&lt;p&gt;Depending on the type of the setup program, you may need to manually unmount (eject) the media after the setup program reboots the virtual machine, to prevent the installation process from starting again. You can do this by selecting the corresponding &lt;b&gt;Unmount...&lt;/b&gt; action in the &lt;b&gt;Devices&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Betätigen Sie &lt;b&gt;Fertig&lt;/b&gt; wenn diese Angaben richtig sind. Nachdem Sie dies getan haben, wird das ausgewählte Medium temporär der virtuellen Maschine zugewiesen und diese startet.&lt;/p&gt;￼&lt;p&gt;Bitte beachten Sie, dass die Zuweisung des Mediums automatisch nach dem Ausschalten der virtuellen Maschine aufgehoben und die Bootreihenfolge auf die erste Festplatte zurückgesetzt wird.&lt;/p&gt;￼&lt;p&gt;Abhängig von der Art des Installationsprogrammes kann es möglich sein, dass Sie das Medium manuell auswerfen müssen, wenn nach der Installation die virtuelle Maschine neu bootet, anderenfalls würde das Installationsprogramm erneut starten. Die Zuweisung kann durch den Punkt &lt;b&gt;Trennen...&lt;/b&gt; im Menü &lt;b&gt;Geräte&lt;/b&gt; menu aufgehoben werden.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>VBoxVMInformationDlg</name>
    <message>
        <source>%1 - Session Information</source>
        <translation>%1 - Session-Informationen</translation>
    </message>
    <message>
        <source>&amp;Details</source>
        <translation>&amp;Basis</translation>
    </message>
    <message>
        <source>&amp;Runtime</source>
        <translation>&amp;Laufzeit</translation>
    </message>
    <message>
        <source>DMA Transfers</source>
        <translation>DMA-Übertragungen</translation>
    </message>
    <message>
        <source>PIO Transfers</source>
        <translation>PIO-Übertragungen</translation>
    </message>
    <message>
        <source>Data Read</source>
        <translation>Daten gelesen</translation>
    </message>
    <message>
        <source>Data Written</source>
        <translation>Daten geschrieben</translation>
    </message>
    <message>
        <source>Data Transmitted</source>
        <translation>Daten gesendet</translation>
    </message>
    <message>
        <source>Data Received</source>
        <translation>Daten empfangen</translation>
    </message>
    <message>
        <source>Runtime Attributes</source>
        <translation>Laufzeit-Attribute</translation>
    </message>
    <message>
        <source>Screen Resolution</source>
        <translation>Auflösung</translation>
    </message>
    <message>
        <source>Version %1.%2</source>
        <comment>guest additions</comment>
        <translation>Version %1.%2</translation>
    </message>
    <message>
        <source>Not Detected</source>
        <comment>guest additions</comment>
        <translation>nicht erkannt</translation>
    </message>
    <message>
        <source>Not Detected</source>
        <comment>guest os type</comment>
        <translation>nicht erkannt</translation>
    </message>
    <message>
        <source>Guest Additions</source>
        <translation>Gast-Erweiterungen</translation>
    </message>
    <message>
        <source>Guest OS Type</source>
        <translation>Gast-Betriebssystem</translation>
    </message>
    <message>
        <source>No Network Adapters</source>
        <translation>Keine Netzwerkadapter</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>nested paging</comment>
        <translation>aktiviert</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>nested paging</comment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <source>Nested Paging</source>
        <translation>Nested Paging</translation>
    </message>
    <message>
        <source>VBoxVMInformationDlg</source>
        <translation></translation>
    </message>
    <message>
        <source>Not Available</source>
        <comment>details report (VRDP server port)</comment>
        <translation>nicht verfügbar</translation>
    </message>
    <message>
        <source>Storage Statistics</source>
        <translation>Massenspeicher</translation>
    </message>
    <message>
        <source>No Storage Devices</source>
        <translation>Keine Massenspeicher</translation>
    </message>
    <message>
        <source>Network Statistics</source>
        <translation>Netzwerkadapter</translation>
    </message>
</context>
<context>
    <name>VBoxVMListView</name>
    <message>
        <source>Inaccessible</source>
        <translation>Nicht zugreifbar</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;%1&lt;br&gt;&lt;/nobr&gt;&lt;nobr&gt;%2 since %3&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Session %4&lt;/nobr&gt;</source>
        <comment>VM tooltip (name, last state change, session state)</comment>
        <translation>&lt;nobr&gt;%1&lt;br&gt;&lt;/nobr&gt;&lt;nobr&gt;%2 seit %3&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Session %4&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;/nobr&gt;&lt;nobr&gt;Inaccessible since %2&lt;/nobr&gt;</source>
        <comment>Inaccessible VM tooltip (name, last state change)</comment>
        <translation>&lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;/nobr&gt;&lt;nobr&gt;Nicht zugreifbar seit %2&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>S&amp;how</source>
        <translation>&amp;Zeigen</translation>
    </message>
    <message>
        <source>Switch to the window of the selected virtual machine</source>
        <translation>Wechselt zum Fenster der ausgewählten virtuellen Maschine</translation>
    </message>
    <message>
        <source>S&amp;tart</source>
        <translation>&amp;Starten</translation>
    </message>
    <message>
        <source>Start the selected virtual machine</source>
        <translation>Starten der virtuellen Maschine</translation>
    </message>
    <message>
        <source>R&amp;esume</source>
        <translation>&amp;Fortfahren</translation>
    </message>
    <message>
        <source>Resume the execution of the virtual machine</source>
        <translation>Fährt mit der Ausführung der virtuellen Maschine fort</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <source>Suspend the execution of the virtual machine</source>
        <translation>Suspendiert die Ausführung der virtuellen Maschine</translation>
    </message>
</context>
<context>
    <name>VBoxVMLogViewer</name>
    <message>
        <source>Log Viewer</source>
        <translation>Log-Anzeige</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Sichern</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation>&amp;Aktualisieren</translation>
    </message>
    <message>
        <source>%1 - VirtualBox Log Viewer</source>
        <translation>%1 - VirtualBox Log-Anzeige</translation>
    </message>
    <message>
        <source>&lt;p&gt;No log files found. Press the &lt;b&gt;Refresh&lt;/b&gt; button to rescan the log folder &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Keine Log-Dateien gefunden. Klicken Sie auf &lt;b&gt;Aktualisieren&lt;/b&gt;, um erneut im Ordner &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; zu suchen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Save VirtualBox Log As</source>
        <translation>VirtualBox Log-Datei sichern als</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>&amp;Suche</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsAudio</name>
    <message>
        <source>When checked, a virtual PCI audio card will be plugged into the virtual machine and will communicate with the host audio system using the specified driver.</source>
        <translation>Der virtuelle PCI Audioadapter verwendet den angegebenen Treiber, um mit dem Audioadapter des Hosts zu kommunizieren.</translation>
    </message>
    <message>
        <source>Enable &amp;Audio</source>
        <translation>Audio &amp;aktivieren</translation>
    </message>
    <message>
        <source>Host Audio &amp;Driver:</source>
        <translation>Audio-&amp;Treiber des Hosts:</translation>
    </message>
    <message>
        <source>Controls the audio output driver. The &lt;b&gt;Null Audio Driver&lt;/b&gt; makes the guest see an audio card, however every access to it will be ignored.</source>
        <translation>Richtet den Treiber für Audioausgabe ein. Wird der &lt;b&gt;Null-Audiotreiber&lt;/b&gt; ausgewählt, erkennt der Gast eine Audio-Karte, Ein- und Ausgabe werden aber ignoriert.</translation>
    </message>
    <message>
        <source>Audio &amp;Controller:</source>
        <translation>Audio-&amp;Controller:</translation>
    </message>
    <message>
        <source>Selects the type of the virtual sound card. Depending on this value, VirtualBox will provide different audio hardware to the virtual machine.</source>
        <translation>Wählt den Typ der virtuellen Soundkarte. Ausgehend von dieser Einstellung emuliert VirtualBox unterschiedliche Audiokarten.</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsDisplay</name>
    <message>
        <source>you have assigned less than &lt;b&gt;%1&lt;/b&gt; of video memory which is the minimum amount required to switch the virtual machine to fullscreen or seamless mode.</source>
        <translation>haben Sie weniger als &lt;b&gt;%1&lt;/b&gt; Grafikspeicher reserviert. Um in den Vollbildmodus oder in den nahtlosen Modus schalten zu können, ist das zu wenig.</translation>
    </message>
    <message>
        <source>&lt;qt&gt;%1&amp;nbsp;MB&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;%1&amp;nbsp;MB&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&amp;Video</source>
        <translation>&amp;Anzeige</translation>
    </message>
    <message>
        <source>Video &amp;Memory:</source>
        <translation>&amp;Grafikspeicher:</translation>
    </message>
    <message>
        <source>Controls the amount of video memory provided to the virtual machine.</source>
        <translation>Legt die Größe des Grafikspeichers für die virtuelle Maschine fest.</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>Extended Features:</source>
        <translation>Erweiterte Einstellungen:</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will be given access to the 3D graphics capabilities available on the host.</source>
        <translation>Durch Aktivieren dieser Checkbox bekommt die virtuelle Maschine Zugriff auf 3D-Grafik-Beschleunigung auf dem Host.</translation>
    </message>
    <message>
        <source>Enable &amp;3D Acceleration</source>
        <translation>&amp;3D-Beschleunigung aktivieren</translation>
    </message>
    <message>
        <source>&amp;Remote Display</source>
        <translation>&amp;Fernsteuerung</translation>
    </message>
    <message>
        <source>When checked, the VM will act as a Remote Desktop Protocol (RDP) server, allowing remote clients to connect and operate the VM (when it is running) using a standard RDP client.</source>
        <translation>Aktiviert die Fernsteuerung (RDP = Remote Desktop Protocol) über die sich entfernte RDP-Clients mit der VM verbinden und diese steuern können.</translation>
    </message>
    <message>
        <source>&amp;Enable Server</source>
        <translation>Server &amp;aktivieren</translation>
    </message>
    <message>
        <source>Server &amp;Port:</source>
        <translation>&amp;Serverport:</translation>
    </message>
    <message>
        <source>Authentication &amp;Method:</source>
        <translation>Authentisierungs&amp;methode:</translation>
    </message>
    <message>
        <source>Defines the VRDP authentication method.</source>
        <translation>Festlegen der VRDP Authentisierungsmethode.</translation>
    </message>
    <message>
        <source>Authentication &amp;Timeout:</source>
        <translation>&amp;Zeitüberschreitung für Authentisierung:</translation>
    </message>
    <message>
        <source>Specifies the timeout for guest authentication, in milliseconds.</source>
        <translation>Legt die maximale Zeit für die Anmeldung des Gastes in Millisekunden fest.</translation>
    </message>
    <message>
        <source>you have assigned less than &lt;b&gt;%1&lt;/b&gt; of video memory which is the minimum amount required for HD Video to be played efficiently.</source>
        <translation>haben Sie weniger als &lt;b&gt;%1&lt;/b&gt; Grafikspeicher allokiert. Dies ist der Minimalwert, um HD-Videos effizient abspielen zu können.</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will be given access to the Video Acceleration capabilities available on the host.</source>
        <translation>Erlaubt dem Gast Zugriff auf Funktionen zur Video-Beschleunigung des Host-Grafikadapters.</translation>
    </message>
    <message>
        <source>Enable &amp;2D Video Acceleration</source>
        <translation>&amp;2D-Video-Beschleunigung aktivieren</translation>
    </message>
    <message>
        <source>The VRDP Server port number. You may specify &lt;tt&gt;0&lt;/tt&gt; (zero), to select port 3389, the standard port for RDP.</source>
        <translation>Die Portnummer des RDP-Servers. Der Wert &lt;tt&gt;0&lt;/tt&gt; (Null) wählt den RDP-Standardport 3389.</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsDlg</name>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Storage</source>
        <translation>Massenspeicher</translation>
    </message>
    <message>
        <source>Hard Disks</source>
        <translation>Festplatten</translation>
    </message>
    <message>
        <source>CD/DVD-ROM</source>
        <translation>CD/DVD-ROM</translation>
    </message>
    <message>
        <source>Floppy</source>
        <translation>Diskette</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <source>Ports</source>
        <translation></translation>
    </message>
    <message>
        <source>Serial Ports</source>
        <translation>Serielle Schnittstellen</translation>
    </message>
    <message>
        <source>Parallel Ports</source>
        <translation>Parallele Schnittstellen</translation>
    </message>
    <message>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <source>Shared Folders</source>
        <translation>Gemeinsame Ordner</translation>
    </message>
    <message>
        <source>%1 - %2</source>
        <translation></translation>
    </message>
    <message>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <source>Display</source>
        <translation>Anzeige</translation>
    </message>
    <message>
        <source>you have selected a 64-bit guest OS type for this VM. As such guests require hardware virtualization (VT-x/AMD-V), this feature will be enabled automatically.</source>
        <translation>haben Sie ein 64-Bit-Gastbestriebssystem ausgewählt. Da solche Gäste Hardware-Virtualisierung benötigen (VT-x/AMD-V), wird diese VM-Einstellung automatisch aktiviert.</translation>
    </message>
    <message>
        <source>you have selected a 64-bit guest OS type for this VM. VirtualBox does not currently support more than one virtual CPU for 64-bit guests executed on 32-bit hosts.</source>
        <translation>haben Sie einen 64-Bit Gastbetriebssystem für diese VM ausgewählt. VirtualBox unterstützt momentan nur eine virtuelle CPU für 64-Bit-Gäste auf 32-Bit Hosts.</translation>
    </message>
    <message>
        <source>you have 2D Video Acceleration enabled. As 2D Video Acceleration is supported for Windows guests only, this feature will be disabled.</source>
        <translation>haben Sie 2D-Video-Beschleunigung aktiviert. Diese Funktion wird momentan nur für Windows-Gäste unterstützt und wird daher deaktiviert.</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsGeneral</name>
    <message>
        <source>Displays the path where snapshots of this virtual machine will be stored. Be aware that snapshots can take quite a lot of disk space.</source>
        <translation>Zeigt den Pfad an, wo Sicherungspunkte für diese virtuelle Maschine gespeichert werden. Beachten Sie, dass Sicherungspunkte viel Platz beanspruchen können.</translation>
    </message>
    <message>
        <source>&amp;Basic</source>
        <translation>&amp;Basis</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>&amp;Name:</translation>
    </message>
    <message>
        <source>Displays the name of the virtual machine.</source>
        <translation>Zeigt den Namen der virtuellen Maschine.</translation>
    </message>
    <message>
        <source>&amp;Advanced</source>
        <translation>&amp;Erweitert</translation>
    </message>
    <message>
        <source>&amp;Shared Clipboard:</source>
        <translation>&amp;Gemeinsame Zwischenablage:</translation>
    </message>
    <message>
        <source>Selects which clipboard data will be copied between the guest and the host OS. This feature requires Guest Additions to be installed in the guest OS.</source>
        <translation>Legt den Modus der gemeinsamen Zwischenablage zwischen Host und Gast fest. Dieses Feature benötigt installierte Gasterweiterungen im Gast-Betriebssystem.</translation>
    </message>
    <message>
        <source>S&amp;napshot Folder:</source>
        <translation>Ordner &amp;Sicherungspunkte:</translation>
    </message>
    <message>
        <source>&amp;Description</source>
        <translation>&amp;Beschreibung</translation>
    </message>
    <message>
        <source>Displays the description of the virtual machine. The description field is useful for commenting on configuration details of the installed guest OS.</source>
        <translation>Zeigt die Beschreibung für diese virtuelle Maschine. Das Beschreibungsfeld kann für Kommentare über die Konfiguration des installierten Gastsystems verwendet werden.</translation>
    </message>
    <message>
        <source>If checked, any change to mounted CD/DVD or Floppy media performed during machine execution will be saved in the settings file in order to preserve the configuration of mounted media between runs.</source>
        <translation>Falls ausgewählt, werden zur Laufzeit zugewiesene CD/DVD- oder Diskettenmedien in den Einstellungen gespeichert um die Zuweisung über die Laufzeit der VM hinaus beizubehalten.</translation>
    </message>
    <message>
        <source>Removable Media:</source>
        <translation>Entfernbare Medien:</translation>
    </message>
    <message>
        <source>&amp;Remember Runtime Changes</source>
        <translation>&amp;Gebundene Medien merken</translation>
    </message>
    <message>
        <source>Mini ToolBar:</source>
        <translation>Mini-Toolbar:</translation>
    </message>
    <message>
        <source>If checked, show the Mini ToolBar in Fullscreen and Seamless modes.</source>
        <translation>Falls aktiviert wird die Mini-Toolbar im Vollbildmodus und im nahtlosen Modus gezeigt.</translation>
    </message>
    <message>
        <source>Show At &amp;Top Of Screen</source>
        <translation>Zeige am oberen Bildschirmrand</translation>
    </message>
    <message>
        <source>Show In &amp;Fullscreen/Seamless</source>
        <translation>Zeige im &amp;Vollbild-/Seamless-Modus</translation>
    </message>
    <message>
        <source>If checked, show the Mini ToolBar at the top of the screen, rather than in its default position at the bottom of the screen.</source>
        <translation>Zeigt die Mini-Toolbar am oberen Rand des Bildschirms wenn ausgewählt, sonst am unteren Rand.</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsHD</name>
    <message>
        <source>If checked, shows the differencing hard disks that are attached to slots rather than their base hard disks (shown for indirect attachments) and allows explicit attaching of differencing hard disks. Check this only if you need a complex hard disk setup.</source>
        <translation>Ist diese Einstellung aktiviert, werden Differenzabbilder, die diesem Slot zugewiesen sind anstelle der normalen Abbilder gezeigt. Wählen Sie diese Einstellung nur für komplexe Setups.</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Bus:&amp;nbsp;&amp;nbsp;%2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Type:&amp;nbsp;&amp;nbsp;%3&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Bus:&amp;nbsp;&amp;nbsp;%2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Typ:&amp;nbsp;&amp;nbsp;%3&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Expand/Collapse&amp;nbsp;Item&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;Eintrag&amp;nbsp;auf-/zuklappen&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Add&amp;nbsp;Hard&amp;nbsp;Disk&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;Festplatte hinzufügen&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Add&amp;nbsp;CD/DVD&amp;nbsp;Device&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;CD/DVD-Laufwerk hinzufügen&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Add&amp;nbsp;Floppy&amp;nbsp;Device&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;Diskettenlaufwerk hinzufügen&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>No hard disk is selected for &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation>Keine Festplatte für &lt;i&gt;%1&lt;/i&gt; ausgewählt.</translation>
    </message>
    <message>
        <source>&lt;i&gt;%1&lt;/i&gt; uses a medium that is already attached to &lt;i&gt;%2&lt;/i&gt;.</source>
        <translation>&lt;i&gt;%1&lt;/i&gt; benutzt ein Medium, das bereits an &lt;i&gt;%2&lt;/i&gt; angeschlossen ist.</translation>
    </message>
    <message>
        <source>Add Controller</source>
        <translation>Controller hinzufügen</translation>
    </message>
    <message>
        <source>Add IDE Controller</source>
        <translation>IDE-Controller hinzufügen</translation>
    </message>
    <message>
        <source>Add SATA Controller</source>
        <translation>SATA-Controller hinzufügen</translation>
    </message>
    <message>
        <source>Add SCSI Controller</source>
        <translation>SCSI-Controller hinzufügen</translation>
    </message>
    <message>
        <source>Add Floppy Controller</source>
        <translation>Disketten-Controller hinzufügen</translation>
    </message>
    <message>
        <source>Remove Controller</source>
        <translation>Controller entfernen</translation>
    </message>
    <message>
        <source>Add Attachment</source>
        <translation>Anschluss hinzufügen</translation>
    </message>
    <message>
        <source>Add Hard Disk</source>
        <translation>Festplatte hinzufügen</translation>
    </message>
    <message>
        <source>Add CD/DVD Device</source>
        <translation>CD/DVD-Laufwerk hinzufügen</translation>
    </message>
    <message>
        <source>Add Floppy Device</source>
        <translation>Diskettenlaufwerk hinzufügen</translation>
    </message>
    <message>
        <source>Remove Attachment</source>
        <translation>Anschluss entfernen</translation>
    </message>
    <message>
        <source>Adds a new controller to the end of the Storage Tree.</source>
        <translation>Fügt einen neuen Controller hinzu.</translation>
    </message>
    <message>
        <source>Removes the controller highlighted in the Storage Tree.</source>
        <translation>Entfernt den ausgewählten Controller.</translation>
    </message>
    <message>
        <source>Adds a new attachment to the Storage Tree using currently selected controller as parent.</source>
        <translation>Fügt einen neuen Anschluss für den ausgewählten Controller hinzu.</translation>
    </message>
    <message>
        <source>Removes the attachment highlighted in the Storage Tree.</source>
        <translation>Entfernt den ausgewählten Anschluss.</translation>
    </message>
    <message>
        <source>IDE Controller</source>
        <translation>IDE-Controller</translation>
    </message>
    <message>
        <source>SATA Controller</source>
        <translation>SATA-Controller</translation>
    </message>
    <message>
        <source>SCSI Controller</source>
        <translation>SCSI-Controller</translation>
    </message>
    <message>
        <source>Floppy Controller</source>
        <translation>Disketten-Controller</translation>
    </message>
    <message>
        <source>Hard &amp;Disk:</source>
        <translation>&amp;Festplatte:</translation>
    </message>
    <message>
        <source>&amp;CD/DVD Device:</source>
        <translation>&amp;CD/DVD-Laufwerk:</translation>
    </message>
    <message>
        <source>&amp;Floppy Device:</source>
        <translation>&amp;Diskettenlaufwerk:</translation>
    </message>
    <message>
        <source>&amp;Storage Tree</source>
        <translation>&amp;Massenspeicher</translation>
    </message>
    <message>
        <source>Contains all storage controllers for this machine and the virtual images and host drives attached to them.</source>
        <translation>Zeigt alle Controller für Massenspeicher dieser Maschine sowie die angeschlossenen Abbilder bzw. Host-Laufwerke.</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>The Storage Tree can contain several controllers of different types. This machine currently has no controllers.</source>
        <translation>Sie können mehrere Controller verschiedenen Typs anschließen. Momentan besitzt die virtuelle Maschine keinen Controller.</translation>
    </message>
    <message>
        <source>Attributes</source>
        <translation>Attribute</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>&amp;Name:</translation>
    </message>
    <message>
        <source>Changes the name of the storage controller currently selected in the Storage Tree.</source>
        <translation>Ändert den Namen des aktuell ausgewählten Controllers.</translation>
    </message>
    <message>
        <source>&amp;Type:</source>
        <translation>&amp;Typ:</translation>
    </message>
    <message>
        <source>Selects the sub-type of the storage controller currently selected in the Storage Tree.</source>
        <translation>Wählt den Subtyp des momentan ausgewählten Controllers aus.</translation>
    </message>
    <message>
        <source>S&amp;lot:</source>
        <translation>S&amp;lot:</translation>
    </message>
    <message>
        <source>Selects the slot on the storage controller used by this attachment. The available slots depend on the type of the controller and other attachments on it.</source>
        <translation>Wählt den Slot des Controllers, der für diesen Anschluss benutzt wird. Anzahl und Art der verfügbaren Slots sind abhängig vom Typ des Controllers.</translation>
    </message>
    <message>
        <source>Selects the virtual disk image or the host drive used by this attachment.</source>
        <translation>Wählt das virtuelle Abbild oder das Host-Laufwerk aus, das für diesen Anschluss benutzt wird.</translation>
    </message>
    <message>
        <source>Opens the Virtual Media Manager to select a virtual image for this attachment.</source>
        <translation>Öffnet den Manager für virtuelle Medien, um ein Abbild für diesen Anschluss auszuwählen.</translation>
    </message>
    <message>
        <source>Open Virtual Media Manager</source>
        <translation>Manager für virtuelle Medien öffnen</translation>
    </message>
    <message>
        <source>D&amp;ifferencing Disks</source>
        <translation>&amp;Differenz-Abbilder</translation>
    </message>
    <message>
        <source>When checked, allows the guest to send ATAPI commands directly to the host-drive which makes it possible to use CD/DVD writers connected to the host inside the VM. Note that writing audio CD inside the VM is not yet supported.</source>
        <translation>Ist diese Box aktiviert, dann darf der Gast ATAPI-Kommandos direkt an das Hostlaufwerk senden. Dies ermöglicht die Nutzung von CD/DVD-Schreibgeräten des Hosts innerhalb der VM. Bitte beachten Sie, dass das Schreiben von Audio-CDs innerhalb der VM noch nicht unterstützt wird.</translation>
    </message>
    <message>
        <source>&amp;Passthrough</source>
        <translation>&amp;Passthrough</translation>
    </message>
    <message>
        <source>Virtual Size:</source>
        <translation>Virtuelle Größe:</translation>
    </message>
    <message>
        <source>Actual Size:</source>
        <translation>Wirkliche Größe:</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation>Größe:</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation>Ort:</translation>
    </message>
    <message>
        <source>Type (Format):</source>
        <translation>Typ (Format):</translation>
    </message>
    <message>
        <source>Attached To:</source>
        <translation>Angeschlossen an:</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsNetwork</name>
    <message>
        <source>no bridged network adapter is selected</source>
        <translation>haben Sie kein Interface für die Netzwerkbrücke ausgewählt</translation>
    </message>
    <message>
        <source>no internal network name is specified</source>
        <translation>haben Sie keinen Namen für das interne Netzwerk ausgewählt</translation>
    </message>
    <message>
        <source>no host-only network adapter is selected</source>
        <translation></translation>
    </message>
    <message>
        <source>Not selected</source>
        <comment>network adapter name</comment>
        <translation>nicht ausgewählt</translation>
    </message>
    <message>
        <source>When checked, plugs this virtual network adapter into the virtual machine.</source>
        <translation>Aktiviert den virtuellen Netzwerkadapter für die virtuelle Maschine.</translation>
    </message>
    <message>
        <source>&amp;Enable Network Adapter</source>
        <translation>Netzwerkadapter &amp;aktivieren</translation>
    </message>
    <message>
        <source>Selects the type of the virtual network adapter. Depending on this value, VirtualBox will provide different network hardware to the virtual machine.</source>
        <translation>Wählt den Typ des virtuellen Netzwerkadapters. Ausgehend von dieser Einstellung emuliert VirtualBox verschiedene Netzwerkkarten.</translation>
    </message>
    <message>
        <source>&amp;Attached to:</source>
        <translation>&amp;Angeschlossen an:</translation>
    </message>
    <message>
        <source>Controls how this virtual adapter is attached to the real network of the Host OS.</source>
        <translation>Legt fest, wie dieser virtuelle Netzwerkadapter an das physische Netzwerk des Host-Betriebssystems angeschlossen ist.</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>&amp;Name:</translation>
    </message>
    <message>
        <source>Selects the name of the network adapter for &lt;b&gt;Bridged Adapter&lt;/b&gt; or &lt;b&gt;Host-only Adapter&lt;/b&gt; attachments and the name of the network &lt;b&gt;Internal Network&lt;/b&gt; attachments.</source>
        <translation>Wählt den Namen des Netzwerkadapters für eine &lt;b&gt;Netzwerkbrücke&lt;/b&gt; oder für einen &lt;b&gt;Host-Only-Adapter&lt;/b&gt; bzw. den Namen eines internen Netzwerkes, falls die Einstellung &lt;b&gt;Internes Netzwerk&lt;/b&gt; gewählt wurde.</translation>
    </message>
    <message>
        <source>Adapter &amp;Type:</source>
        <translation>Adapter&amp;typ:</translation>
    </message>
    <message>
        <source>A&amp;dvanced</source>
        <translation>&amp;Erweitert</translation>
    </message>
    <message>
        <source>Shows or hides additional network adapter options.</source>
        <translation>Zeigt oder verdeckt erweiterte Einstellungen für den Netzwerkadapter.</translation>
    </message>
    <message>
        <source>&amp;Mac Address:</source>
        <translation>&amp;Mac-Adresse:</translation>
    </message>
    <message>
        <source>Displays the MAC address of this adapter. It contains exactly 12 characters chosen from {0-9,A-F}. Note that the second character must be an even digit.</source>
        <translation>Zeigt die MAC-Adresse dieses Adapters. Sie besteht aus genau 12 Zeichen aus dem Zeichenvorrat {0-9,A-F}. Das zweite Zeichen muss eine gerate Zahl sein.</translation>
    </message>
    <message>
        <source>Generates a new random MAC address.</source>
        <translation>Erzeugt eine neue zufällige MAC-Adresse.</translation>
    </message>
    <message>
        <source>Indicates whether the virtual network cable is plugged in on machine startup or not.</source>
        <translation>Zeigt, ob das virtuelle Netzwerkkabel an die virtuelle Maschine angeschlossen ist oder nicht.</translation>
    </message>
    <message>
        <source>&amp;Cable connected</source>
        <translation>&amp;Kabel verbunden</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsParallel</name>
    <message>
        <source>Port %1</source>
        <comment>parallel ports</comment>
        <translation>Port %1</translation>
    </message>
    <message>
        <source>When checked, enables the given parallel port of the virtual machine.</source>
        <translation>Aktiviert die ausgewählte parallele Schnittstelle für diese virtuelle Maschine.</translation>
    </message>
    <message>
        <source>&amp;Enable Parallel Port</source>
        <translation>&amp;Parallele Schnittstelle aktivieren</translation>
    </message>
    <message>
        <source>Port &amp;Number:</source>
        <translation>Port&amp;nummer:</translation>
    </message>
    <message>
        <source>Displays the parallel port number. You can choose one of the standard parallel ports or select &lt;b&gt;User-defined&lt;/b&gt; and specify port parameters manually.</source>
        <translation>Zeigt die Portnummer der parallelen Schnittstelle. Sie können einen der Standardports auswählen, oder Sie wählen &lt;b&gt;benutzerdefiniert&lt;/b&gt; und können die Portparameter frei einstellen.</translation>
    </message>
    <message>
        <source>&amp;IRQ:</source>
        <translation>&amp;IRQ:</translation>
    </message>
    <message>
        <source>I/O Po&amp;rt:</source>
        <translation>I/O-Po&amp;rt:</translation>
    </message>
    <message>
        <source>Port &amp;Path:</source>
        <translation>Port&amp;pfad:</translation>
    </message>
    <message>
        <source>Displays the host parallel device name.</source>
        <translation>Zeigt den Namen der parallelen Schnittstelle des Hostes an.</translation>
    </message>
    <message>
        <source>Displays the IRQ number of this parallel port. This should be a whole number between &lt;tt&gt;0&lt;/tt&gt; and &lt;tt&gt;255&lt;/tt&gt;. Values greater than &lt;tt&gt;15&lt;/tt&gt; may only be used if the &lt;b&gt;IO APIC&lt;/b&gt; setting is enabled for this virtual machine.</source>
        <translation>Zeigt die IRQ-Nummer dieser parallelen Schnittstelle. Gültige Werte sind ganze Zahlen im Bereich von &lt;tt&gt;0&lt;/tt&gt; bis &lt;tt&gt;255&lt;/tt&gt;. Zahlen größer als &lt;tt&gt;15&lt;/tt&gt; können nur verwendet werden, wenn der &lt;b&gt;I/O-APIC&lt;/b&gt; für diese virtuelle Maschine aktiviert ist.</translation>
    </message>
    <message>
        <source>Displays the base I/O port address of this parallel port. Valid values are integer numbers in range from &lt;tt&gt;0&lt;/tt&gt; to &lt;tt&gt;0xFFFF&lt;/tt&gt;.</source>
        <translation>Zeigt die Basis-Portadresse dieser parallelen Schnittstelle. Zulässige Werte sind ganze Zahlen im Bereich von &lt;tt&gt;0&lt;/tt&gt; bis &lt;tt&gt;0xFFFF&lt;/tt&gt;.</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsParallelPage</name>
    <message>
        <source>Duplicate port number selected </source>
        <translation>Doppelte Portnummer ist ausgewählt</translation>
    </message>
    <message>
        <source>Port path not specified </source>
        <translation>Der Pfad für den Port fehlt</translation>
    </message>
    <message>
        <source>Duplicate port path entered </source>
        <translation>Doppelter Portpfad ist ausgewählt</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSF</name>
    <message>
        <source>Adds a new shared folder definition.</source>
        <translation>Fügt einen neuen gemeinsamen Ordner hinzu.</translation>
    </message>
    <message>
        <source>Edits the selected shared folder definition.</source>
        <translation>Ändert den ausgewählten gemeinsamen Ordner.</translation>
    </message>
    <message>
        <source>Removes the selected shared folder definition.</source>
        <translation>Entfernt den ausgewählten gemeinsamen Ordner.</translation>
    </message>
    <message>
        <source> Machine Folders</source>
        <translation> Ordner der virtuellen Maschine</translation>
    </message>
    <message>
        <source> Transient Folders</source>
        <translation> Transiente Ordner</translation>
    </message>
    <message>
        <source>Full</source>
        <translation>Voll</translation>
    </message>
    <message>
        <source>Read-only</source>
        <translation>Nur lesbar</translation>
    </message>
    <message>
        <source>Lists all shared folders accessible to this machine. Use &apos;net use x: \\vboxsvr\share&apos; to access a shared folder named &lt;i&gt;share&lt;/i&gt; from a DOS-like OS, or &apos;mount -t vboxsf share mount_point&apos; to access it from a Linux OS. This feature requires Guest Additions.</source>
        <translation>Zeigt alle von dieser virtuellen Maschine zugreifbaren gemeinsamen Ordner mit dem Host. Benutzen Sie &apos;net use x: \\vboxsvr\share&apos; um auf einen gemeinsamen Ordner namens &lt;i&gt;share&lt;/i&gt; von einem DOS-artigen BS bzw. &apos;mount -t vboxsf share mount_point&apos; um von Linux darauf zuzugreifen. Dieses Merkmal benötigt Gasterweiterungen.</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <source>Access</source>
        <translation>Zugriff</translation>
    </message>
    <message>
        <source> Global Folders</source>
        <translation> Globale Ordner</translation>
    </message>
    <message>
        <source>&amp;Add Shared Folder</source>
        <translation>Gemeinsamen Ordner &amp;hinzufügen</translation>
    </message>
    <message>
        <source>&amp;Edit Shared Folder</source>
        <translation>Gemeinsamen Ordner &amp;ändern</translation>
    </message>
    <message>
        <source>&amp;Remove Shared Folder</source>
        <translation>Gemeinsamen Ordner &amp;löschen</translation>
    </message>
    <message>
        <source>&amp;Folders List</source>
        <translation>Ordner&amp;liste</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSFDetails</name>
    <message>
        <source>Add Share</source>
        <translation>Ordner hinzufügen</translation>
    </message>
    <message>
        <source>Edit Share</source>
        <translation>Ordner ändern</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <source>Folder Path:</source>
        <translation>Ordner-Pfad:</translation>
    </message>
    <message>
        <source>Folder Name:</source>
        <translation>Ordner-Name:</translation>
    </message>
    <message>
        <source>Displays the name of the shared folder (as it will be seen by the guest OS).</source>
        <translation>Zeigt den Namen des gemeinsamen Ordners (wie er vom Gastsystem gesehen wird).</translation>
    </message>
    <message>
        <source>When checked, the guest OS will not be able to write to the specified shared folder.</source>
        <translation>Der Gast erhält nur lesenden Zugriff auf das Verzeichnis.</translation>
    </message>
    <message>
        <source>&amp;Read-only</source>
        <translation>Nur &amp;lesbar</translation>
    </message>
    <message>
        <source>&amp;Make Permanent</source>
        <translation>&amp;Permanent erzeugen</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSerial</name>
    <message>
        <source>Port %1</source>
        <comment>serial ports</comment>
        <translation>Port %1</translation>
    </message>
    <message>
        <source>When checked, enables the given serial port of the virtual machine.</source>
        <translation>Aktiviert die ausgewählte serielle Schnittstelle für diese virtuelle Maschine.</translation>
    </message>
    <message>
        <source>&amp;Enable Serial Port</source>
        <translation>&amp;Serielle Schnittstelle aktivieren</translation>
    </message>
    <message>
        <source>Port &amp;Number:</source>
        <translation>Port&amp;nummer:</translation>
    </message>
    <message>
        <source>Displays the serial port number. You can choose one of the standard serial ports or select &lt;b&gt;User-defined&lt;/b&gt; and specify port parameters manually.</source>
        <translation>Zeigt die Portnummer der seriellen Schnittstelle. Sie können einen der Standardports auswählen, oder Sie wählen &lt;b&gt;benutzerdefiniert&lt;/b&gt; und können die Portparameter frei einstellen.</translation>
    </message>
    <message>
        <source>&amp;IRQ:</source>
        <translation>&amp;IRQ:</translation>
    </message>
    <message>
        <source>I/O Po&amp;rt:</source>
        <translation>I/O-Po&amp;rt:</translation>
    </message>
    <message>
        <source>Port &amp;Mode:</source>
        <translation>Port&amp;modus:</translation>
    </message>
    <message>
        <source>Controls the working mode of this serial port. If you select &lt;b&gt;Disconnected&lt;/b&gt;, the guest OS will detect the serial port but will not be able to operate it.</source>
        <translation>Stellt den Modus für diese serielle Schnittstelle ein. Falls &lt;b&gt;nicht verbunden&lt;/b&gt; gewählt wurde, wird ein serieller Port virtualisiert, der jedoch nicht mit dem Host verbunden ist.</translation>
    </message>
    <message>
        <source>If checked, the pipe specified in the &lt;b&gt;Port Path&lt;/b&gt; field will be created by the virtual machine when it starts. Otherwise, the virtual machine will assume that the pipe exists and try to use it.</source>
        <translation>Wenn diese Box ausgewählt wurde, dann wird die Pipe im &lt;b&gt;Portpfad&lt;/b&gt; von der virtuellen Maschine erzeugt, wenn diese startet. Anderenfalls versucht die virtuelle Maschine, sich an eine vorhandene Pipe zu anzuschließen.</translation>
    </message>
    <message>
        <source>&amp;Create Pipe</source>
        <translation>Erzeuge &amp;Pipe</translation>
    </message>
    <message>
        <source>Displays the path to the serial port&apos;s pipe on the host when the port is working in &lt;b&gt;Host Pipe&lt;/b&gt; mode, or the host serial device name when the port is working in &lt;b&gt;Host Device&lt;/b&gt; mode.</source>
        <translation>Zeigt den Pfad zur seriellen Port-Pipe des Hostes wenn der Port im Modus &lt;b&gt;Host-Pipe&lt;/b&gt; arbeitet oder den Namen der seriellen Schnittstelle des Hostes im Modus &lt;b&gt;Host-Schnittstelle&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>Port/File &amp;Path:</source>
        <translation>Port/Datei-&amp;Pfad:</translation>
    </message>
    <message>
        <source>Displays the IRQ number of this serial port. This should be a whole number between &lt;tt&gt;0&lt;/tt&gt; and &lt;tt&gt;255&lt;/tt&gt;. Values greater than &lt;tt&gt;15&lt;/tt&gt; may only be used if the &lt;b&gt;IO APIC&lt;/b&gt; setting is enabled for this virtual machine.</source>
        <translation>Zeigt die IRQ-Nummer dieser seriellen Schnittstelle. Gültige Werte sind ganze Zahlen im Bereich von &lt;tt&gt;0&lt;/tt&gt; bis &lt;tt&gt;255&lt;/tt&gt;. Zahlen größer als &lt;tt&gt;15&lt;/tt&gt; können nur verwendet werden, wenn der &lt;b&gt;I/O-APIC&lt;/b&gt; für diese virtuelle Maschine aktiviert ist.</translation>
    </message>
    <message>
        <source>Displays the base I/O port address of this serial port. Valid values are integer numbers in range from &lt;tt&gt;0&lt;/tt&gt; to &lt;tt&gt;0xFFFF&lt;/tt&gt;.</source>
        <translation>Zeigt die Basis-Portadresse dieser seriellen Schnittstelle. Zulässige Werte sind ganze Zahlen im Bereich von &lt;tt&gt;0&lt;/tt&gt; bis &lt;tt&gt;0xFFFF&lt;/tt&gt;.</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSerialPage</name>
    <message>
        <source>Duplicate port number selected </source>
        <translation>Doppelte Portnummer ist ausgewählt</translation>
    </message>
    <message>
        <source>Port path not specified </source>
        <translation>Der Pfad für den Port fehlt</translation>
    </message>
    <message>
        <source>Duplicate port path entered </source>
        <translation>Doppelter Portpfad ist ausgewählt</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSystem</name>
    <message>
        <source>you have assigned more than &lt;b&gt;%1%&lt;/b&gt; of your computer&apos;s memory (&lt;b&gt;%2&lt;/b&gt;) to the virtual machine. Not enough memory is left for your host operating system. Please select a smaller amount.</source>
        <translation>haben Sie mehr als &lt;b&gt;%1%&lt;/b&gt; des Hauptspeichers (&lt;b&gt;%2&lt;/b&gt;) für die virtuelle Maschine reserviert. Es bleibt nicht genügend Speicher für das Host-Betriebssystem übrig. Bitte wählen Sie einen kleineren Wert.</translation>
    </message>
    <message>
        <source>you have assigned more than &lt;b&gt;%1%&lt;/b&gt; of your computer&apos;s memory (&lt;b&gt;%2&lt;/b&gt;) to the virtual machine. There might not be enough memory left for your host operating system. Continue at your own risk.</source>
        <translation>haben Sie mehr als &lt;b&gt;%1%&lt;/b&gt; des Hauptspeichers (&lt;b&gt;%2&lt;/b&gt;) für die virtuelle Maschine reserviert. Dadurch bleibt eventuell zu wenig Speicher für den Host übrig. Fahren Sie auf eigenes Risiko fort.</translation>
    </message>
    <message>
        <source>for performance reasons, the number of virtual CPUs attached to the virtual machine may not be more than twice the number of physical CPUs on the host (&lt;b&gt;%1&lt;/b&gt;). Please reduce the number of virtual CPUs.</source>
        <translation>sollte die Anzahl der virtuellen CPUs nicht mehr als doppelt so groß sein, wie die Anzahl der physischen CPU-Cores auf dem Host (&lt;b&gt;%1&lt;/b&gt;). Bitte wählen Sie einen kleineren Wert.</translation>
    </message>
    <message>
        <source>you have assigned more virtual CPUs to the virtual machine than the number of physical CPUs on your host system (&lt;b&gt;%1&lt;/b&gt;). This is likely to degrade the performance of your virtual machine. Please consider reducing the number of virtual CPUs.</source>
        <translation>haben Sie mehr virtuellle CPUs eingestellt als physische CPU-Cores auf Ihrem Host vorhanden sind (&lt;b&gt;%1&lt;/b&gt;). Für beste Performance sollten Sie die Anzahl der virtuellen CPUs verringern.</translation>
    </message>
    <message>
        <source>you have assigned more than one virtual CPU to this VM. This will not work unless the IO-APIC feature is also enabled. This will be done automatically when you accept the VM Settings by pressing the OK button.</source>
        <translation>haben Sie mehr als eine virtuelle CPU eingestellt. Unterstützung für mehrere CPUs im Gast erfordert die Aktivierung des IO-APIC. Dieses Feature wird automatisch aktiviert, sobald Sie die Einstellungen bestätigen.</translation>
    </message>
    <message>
        <source>you have assigned more than one virtual CPU to this VM. This will not work unless hardware virtualization (VT-x/AMD-V) is also enabled. This will be done automatically when you accept the VM Settings by pressing the OK button.</source>
        <translation>haben Sie mehr als eine virtuelle CPU eingestellt. Unterstützung für mehrere CPUs im Gast erfordert die Aktivierung von VT-x/AMD-V. Dieses Feature wird automatisch aktiviert, sobald Sie die Einstellungen bestätigen.</translation>
    </message>
    <message>
        <source>&lt;qt&gt;%1&amp;nbsp;MB&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;%1&amp;nbsp;MB&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&lt;qt&gt;%1&amp;nbsp;CPU&lt;/qt&gt;</source>
        <comment>%1 is 1 for now</comment>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Motherboard</source>
        <translation>&amp;Hauptplatine</translation>
    </message>
    <message>
        <source>Base &amp;Memory:</source>
        <translation>&amp;Hauptspeicher:</translation>
    </message>
    <message>
        <source>Controls the amount of memory provided to the virtual machine. If you assign too much, the machine might not start.</source>
        <translation>Legt die Größe des Hauptspeichers für die virtuelle Maschine fest. Wenn dieser Wert zu groß ist, kann die virtuelle Maschine nicht starten.</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>&amp;Boot Order:</source>
        <translation>Boo&amp;t-Reihenfolge:</translation>
    </message>
    <message>
        <source>Defines the boot device order. Use the checkboxes on the left to enable or disable individual boot devices. Move items up and down to change the device order.</source>
        <translation>Legt die Bootreihenfolge fest. Mittels der Checkboxen auf der linken Seite können Geräte aktiviert bzw. deaktiviert werden. Durch Auf- bzw. Abwärtsbewegen der Einträge wird die Bootreihenfolge geändert.</translation>
    </message>
    <message>
        <source>Move Down (Ctrl-Down)</source>
        <translation>Abwärts (Strg+Ab)</translation>
    </message>
    <message>
        <source>Moves the selected boot device down.</source>
        <translation>Bewegt das ausgewählte Gerät weiter nach unten und verringert damit dessen Priorität in der Bootreihenfolge.</translation>
    </message>
    <message>
        <source>Move Up (Ctrl-Up)</source>
        <translation>Aufwärts (Strg+Auf)</translation>
    </message>
    <message>
        <source>Moves the selected boot device up.</source>
        <translation>Bewegt das ausgewählte Gerät weiter nach oben und erhöht damit dessen Priorität in der Bootreihenfolge.</translation>
    </message>
    <message>
        <source>Extended Features:</source>
        <translation>Erweiterte Einstellungen:</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will support the Input Output APIC (IO APIC), which may slightly decrease performance. &lt;b&gt;Note:&lt;/b&gt; don&apos;t disable this feature after having installed a Windows guest operating system!</source>
        <translation>Aktiviert die Unterstützung für den Input/Output APIC (IO-APIC). Dies kann negative Auswirkungen auf die VM-Performance haben. &lt;b&gt;Beachten Sie:&lt;/b&gt; Deaktivieren Sie die Unterstützung nicht, nachdem Sie Windows installiert haben!</translation>
    </message>
    <message>
        <source>Enable &amp;IO APIC</source>
        <translation>&amp;IO-APIC aktivieren</translation>
    </message>
    <message>
        <source>&amp;Processor</source>
        <translation>&amp;Prozessor</translation>
    </message>
    <message>
        <source>&amp;Processor(s):</source>
        <translation>&amp;Prozessoren:</translation>
    </message>
    <message>
        <source>Controls the number of virtual CPUs in the virtual machine. You need hardware virtualization support on your host system to use more than one virtual CPU.</source>
        <translation>Legt die Anzahld der virtuellen CPUs dieser VM fest. Der Host muss Hardware-Virtualisierung unterstützen, falls mehr als eine virtuelle CPU emuliert werden soll.</translation>
    </message>
    <message>
        <source>When checked, the Physical Address Extension (PAE) feature of the host CPU will be exposed to the virtual machine.</source>
        <translation>Aktiviert die Unterstützung für Physical Address Extension (PAE) für Gäste. Nur möglich, wenn die Host-CPU diesen Modus ebenfalls unterstützt.</translation>
    </message>
    <message>
        <source>Enable PA&amp;E/NX</source>
        <translation>PA&amp;E/NX aktivieren</translation>
    </message>
    <message>
        <source>Acce&amp;leration</source>
        <translation>&amp;Beschleunigung</translation>
    </message>
    <message>
        <source>Hardware Virtualization:</source>
        <translation>Hardware-Virtualisierung:</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will try to make use of the host CPU&apos;s hardware virtualization extensions such as Intel VT-x and AMD-V.</source>
        <translation>Durch Aktivieren dieser Option wird die virtuelle Maschine einen speziellen Modus der CPU für die Virtualisierung (Intel VT-x bzw. AMD-V) verwenden, falls dieser Modus verfügbar ist.</translation>
    </message>
    <message>
        <source>Enable &amp;VT-x/AMD-V</source>
        <translation>&amp;VT-x/AMD-V aktivieren</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will try to make use of the nested paging extension of Intel VT-x and AMD-V.</source>
        <translation>Durch Auswählen dieser Option wird die virtuelle Maschine den Modus &quot;Nested Paging&quot; verwenden, falls die CPU dies unterstützt (nur falls VT-x/AMD-V aktiviert ist).</translation>
    </message>
    <message>
        <source>Enable Nested Pa&amp;ging</source>
        <translation>Nested P&amp;aging aktivieren</translation>
    </message>
    <message>
        <source>&lt;qt&gt;%1&amp;nbsp;CPUs&lt;/qt&gt;</source>
        <comment>%1 is host cpu count * 2 for now</comment>
        <translation>&lt;qt&gt;%1&amp;nbsp;CPUs&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>When checked, the guest will support the Extended Firmware Interface (EFI), which is required to boot certain guest OSes. Non-EFI aware OSes will not be able to boot if this option is activated.</source>
        <translation>Aktiviert das Extended Firmware Interface (EFI), das zum Booten von manchen speziellen Gästen benötigt wird. Nicht-EFI-fährige Gäste können nicht booten, wenn diese Option aktiviert ist.</translation>
    </message>
    <message>
        <source>Enable &amp;EFI (special OSes only)</source>
        <translation>&amp;EFI aktivieren (nur spezielle Gäste)</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsUSB</name>
    <message>
        <source>&amp;Add Empty Filter</source>
        <translation>&amp;Leeren Filter hinzufügen</translation>
    </message>
    <message>
        <source>A&amp;dd Filter From Device</source>
        <translation>Filter von einem Gerät &amp;hinzufügen</translation>
    </message>
    <message>
        <source>&amp;Edit Filter</source>
        <translation>Filter &amp;ändern</translation>
    </message>
    <message>
        <source>&amp;Remove Filter</source>
        <translation>Filter &amp;entfernen</translation>
    </message>
    <message>
        <source>&amp;Move Filter Up</source>
        <translation>Bewege Filter nach &amp;oben</translation>
    </message>
    <message>
        <source>M&amp;ove Filter Down</source>
        <translation>Bewege Filter nach &amp;unten</translation>
    </message>
    <message>
        <source>Adds a new USB filter with all fields initially set to empty strings. Note that such a filter will match any attached USB device.</source>
        <translation>Fügt einen neuen USB-Filter hinzu, bei dem alle Felder leer sind. Beachten Sie, dass solch ein Filter auf angeschlossene USB-Geräte passt.</translation>
    </message>
    <message>
        <source>Adds a new USB filter with all fields set to the values of the selected USB device attached to the host PC.</source>
        <translation>Fügt einen neuen USB-Filter hinzu und initialisiert alle Felder mit den Werten des ausgewählten Gerätes, das an den PC angeschlossen ist.</translation>
    </message>
    <message>
        <source>Edits the selected USB filter.</source>
        <translation>Ändert den ausgewählten USB-Filter.</translation>
    </message>
    <message>
        <source>Removes the selected USB filter.</source>
        <translation>Entfernt den ausgewählten USB-Filter.</translation>
    </message>
    <message>
        <source>Moves the selected USB filter up.</source>
        <translation>Bewegt den ausgewählten USB-Filter nach oben.</translation>
    </message>
    <message>
        <source>Moves the selected USB filter down.</source>
        <translation>Bewegt den ausgewählten USB-Filter nach unten.</translation>
    </message>
    <message>
        <source>New Filter %1</source>
        <comment>usb</comment>
        <translation>Neuer Filter %1</translation>
    </message>
    <message>
        <source>When checked, enables the virtual USB controller of this machine.</source>
        <translation>Aktiviert den virtuellen USB-Controller für diese Maschine.</translation>
    </message>
    <message>
        <source>Enable &amp;USB Controller</source>
        <translation>&amp;USB-Controller aktivieren</translation>
    </message>
    <message>
        <source>When checked, enables the virtual USB EHCI controller of this machine. The USB EHCI controller provides USB 2.0 support.</source>
        <translation>Aktiviert den virtuellen USB-EHCI-Controller für diese Maschine und damit USB-2.0-Unterstützung.</translation>
    </message>
    <message>
        <source>Enable USB 2.0 (E&amp;HCI) Controller</source>
        <translation>USB-&amp;2.0-Controller aktivieren</translation>
    </message>
    <message>
        <source>USB Device &amp;Filters</source>
        <translation>&amp;Filter für USB-Geräte</translation>
    </message>
    <message>
        <source>Lists all USB filters of this machine. The checkbox to the left defines whether the particular filter is enabled or not. Use the context menu or buttons to the right to add or remove USB filters.</source>
        <translation>Zeigt alle USB-Filter dieser VM. Die Checkbox auf der linken Seite legt fest, ob ein Filter aktiviert ist oder nicht. Benutzen Sie das Kontextmenü oder die Buttons auf der rechten Seite, um Filter hinzuzufügen oder zu entfernen.</translation>
    </message>
    <message>
        <source>[filter]</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsUSBFilterDetails</name>
    <message>
        <source>Any</source>
        <comment>remote</comment>
        <translation>Beides</translation>
    </message>
    <message>
        <source>Yes</source>
        <comment>remote</comment>
        <translation>Ja</translation>
    </message>
    <message>
        <source>No</source>
        <comment>remote</comment>
        <translation>Nein</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>&amp;Name:</translation>
    </message>
    <message>
        <source>Displays the filter name.</source>
        <translation>Zeigt den Namen des Filters.</translation>
    </message>
    <message>
        <source>&amp;Vendor ID:</source>
        <translation>&amp;Hersteller-ID:</translation>
    </message>
    <message>
        <source>Defines the vendor ID filter. The &lt;i&gt;exact match&lt;/i&gt; string format is &lt;tt&gt;XXXX&lt;/tt&gt; where &lt;tt&gt;X&lt;/tt&gt; is a hexadecimal digit. An empty string will match any value.</source>
        <translation>Legt die Hersteller-ID für den Filter fest. Das Format für den &lt;i&gt;genauen&lt;/i&gt; Vergleich ist &lt;tt&gt;XXXX&lt;/tt&gt;, wobei &lt;tt&gt;X&lt;/tt&gt; eine hexadezimale Zahl darstellt. Eine leere Zeichenfolge passt auf beliebige IDs.</translation>
    </message>
    <message>
        <source>&amp;Product ID:</source>
        <translation>&amp;Produkt-ID:</translation>
    </message>
    <message>
        <source>Defines the product ID filter. The &lt;i&gt;exact match&lt;/i&gt; string format is &lt;tt&gt;XXXX&lt;/tt&gt; where &lt;tt&gt;X&lt;/tt&gt; is a hexadecimal digit. An empty string will match any value.</source>
        <translation>Legt die Produkt-ID für den Filter fest. Das Format für den &lt;i&gt;genauen&lt;/i&gt; Vergleich ist &lt;tt&gt;XXXX&lt;/tt&gt;, wobei &lt;tt&gt;X&lt;/tt&gt; eine hexadezimale Zahl darstellt. Eine leere Zeichenfolge passt auf beliebige IDs.</translation>
    </message>
    <message>
        <source>&amp;Revision:</source>
        <translation>&amp;Revisions-Nr:</translation>
    </message>
    <message>
        <source>Defines the revision number filter. The &lt;i&gt;exact match&lt;/i&gt; string format is &lt;tt&gt;IIFF&lt;/tt&gt; where &lt;tt&gt;I&lt;/tt&gt; is a decimal digit of the integer part and &lt;tt&gt;F&lt;/tt&gt; is a decimal digit of the fractional part. An empty string will match any value.</source>
        <translation>Legt die Revisions-Nr für den Filter fest. Das Format für den &lt;i&gt;genauen&lt;/i&gt; Vergleich ist &lt;tt&gt;XXXX&lt;/tt&gt;, wobei &lt;tt&gt;X&lt;/tt&gt; eine hexadezimale Zahl darstellt. Eine leere Zeichenfolge passt auf beliebige IDs.</translation>
    </message>
    <message>
        <source>&amp;Manufacturer:</source>
        <translation>&amp;Hersteller:</translation>
    </message>
    <message>
        <source>Defines the manufacturer filter as an &lt;i&gt;exact match&lt;/i&gt; string. An empty string will match any value.</source>
        <translation>Verwendet den Hersteller als &lt;i&gt;exakte&lt;/i&gt; Zeichenfolge. Eine leere Zeichenfolge passt auf beliebige Hersteller.</translation>
    </message>
    <message>
        <source>Pro&amp;duct:</source>
        <translation>&amp;Produkt:</translation>
    </message>
    <message>
        <source>Defines the product name filter as an &lt;i&gt;exact match&lt;/i&gt; string. An empty string will match any value.</source>
        <translation>Verwendet den Produktnamen als &lt;i&gt;exakte&lt;/i&gt; Zeichenfolge. Eine leere Zeichenfolge passt auf beliebige Produktnamen.</translation>
    </message>
    <message>
        <source>&amp;Serial No.:</source>
        <translation>&amp;Seriennr.:</translation>
    </message>
    <message>
        <source>Defines the serial number filter as an &lt;i&gt;exact match&lt;/i&gt; string. An empty string will match any value.</source>
        <translation>Verwendet die Seriennummer als &lt;i&gt;exakte&lt;/i&gt; Zeichenfolge. Eine leere Zeichenfolge passt auf alle Seriennummern.</translation>
    </message>
    <message>
        <source>Por&amp;t:</source>
        <translation>&amp;Port:</translation>
    </message>
    <message>
        <source>Defines the host USB port filter as an &lt;i&gt;exact match&lt;/i&gt; string. An empty string will match any value.</source>
        <translation>Legt den USB-Port für den Filter fest mit &lt;i&gt;genauer&lt;/i&gt; Übereinstimmung fest. Eine leere Zeichenfolge passt auf beliebig Ports.</translation>
    </message>
    <message>
        <source>R&amp;emote:</source>
        <translation>&amp;Fernzugriff:</translation>
    </message>
    <message>
        <source>Defines whether this filter applies to USB devices attached locally to the host computer (&lt;i&gt;No&lt;/i&gt;), to a VRDP client&apos;s computer (&lt;i&gt;Yes&lt;/i&gt;), or both (&lt;i&gt;Any&lt;/i&gt;).</source>
        <translation>Legt fest, ob dieser Filter auf USB-Geräte angewendet wird, die nur lokal an den Hostcomputer angeschlossen sind (&lt;i&gt;Nein&lt;/i&gt;), nur an einen entfernten Computer über VRDP (&lt;i&gt;Ja&lt;/i&gt;) oder beides (&lt;i&gt;Beides&lt;/i&gt;).</translation>
    </message>
    <message>
        <source>&amp;Action:</source>
        <translation>&amp;Aktion:</translation>
    </message>
    <message>
        <source>Defines an action performed by the host computer when a matching device is attached: give it up to the host OS (&lt;i&gt;Ignore&lt;/i&gt;) or grab it for later usage by virtual machines (&lt;i&gt;Hold&lt;/i&gt;).</source>
        <translation>Legt fest, was der Host machen soll, wenn ein passendes Gerät angeschlossen wurde: Zur Verwendung durch das Host-BS freigeben (&lt;i&gt;Ignorieren&lt;/i&gt;) oder für spätere Verwendung durch das Gast-BS ergreifen (&lt;i&gt;Halten&lt;/i&gt;).</translation>
    </message>
    <message>
        <source>USB Filter Details</source>
        <translation>Filter für USB-Geräte</translation>
    </message>
</context>
</TS>
