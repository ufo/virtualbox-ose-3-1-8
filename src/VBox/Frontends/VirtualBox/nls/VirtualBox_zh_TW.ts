<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_TW">
<context>
    <name>@@@</name>
    <message>
        <source>English</source>
        <comment>Native language name</comment>
        <translation>正體中文</translation>
    </message>
    <message>
        <source>--</source>
        <comment>Native language country name (empty if this language is for all countries)</comment>
        <translation>臺灣</translation>
    </message>
    <message>
        <source>English</source>
        <comment>Language name, in English</comment>
        <translation>Traditional Chinese</translation>
    </message>
    <message>
        <source>--</source>
        <comment>Language country name, in English (empty if native country name is empty)</comment>
        <translation>Taiwan</translation>
    </message>
    <message>
        <source>Sun Microsystems, Inc.</source>
        <comment>Comma-separated list of translators</comment>
        <translation type="obsolete">alan@stable</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Executable &lt;b&gt;%1&lt;/b&gt; requires Qt %2.x, found Qt %3.</source>
        <translation>執行檔 &lt;b&gt;%1&lt;/b&gt; 需要 Qt %2.x，但找到 Qt %3。</translation>
    </message>
    <message>
        <source>Incompatible Qt Library Error</source>
        <translation>不相容的 Qt 程式庫錯誤</translation>
    </message>
    <message>
        <source>VirtualBox - Error In %1</source>
        <translation>VirtualBox - 錯誤於 %1</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;b&gt;%1 (rc=%2)&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;%1 (rc=%2)&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;</translation>
    </message>
    <message>
        <source>Please try reinstalling VirtualBox.</source>
        <translation>請嘗試重新安裝 VirtualBox。</translation>
    </message>
    <message>
        <source>The VirtualBox Linux kernel driver (vboxdrv) is either not loaded or there is a permission problem with /dev/vboxdrv. Please reinstall the kernel module by executing&lt;br/&gt;&lt;br/&gt;  &lt;font color=blue&gt;&apos;/etc/init.d/vboxdrv setup&apos;&lt;/font&gt;&lt;br/&gt;&lt;br/&gt;as root. Users of Ubuntu, Fedora or Mandriva should install the DKMS package first. This package keeps track of Linux kernel changes and recompiles the vboxdrv kernel module if necessary.</source>
        <translation>未載入 VirtualBox Linux 核心驅動程式 (vboxdrv) 或是 /dev/vboxdrv 的權限問題。 請以 root 執行 &lt;br/&gt;&lt;br/&gt;  &lt;font color=blue&gt;&apos;/etc/init.d/vboxdrv setup&apos; 來重新安裝核心模組。 Ubuntu, Fedora 或 Mandriva 的使用者應先安裝 DKMS 封裝。 這個封裝保留 Linux 核心變更的追蹤並在需要時重新編譯 vboxdrv 核心模組。</translation>
    </message>
    <message>
        <source>Make sure the kernel module has been loaded successfully.</source>
        <translation>請確認核心模組已載入成功。</translation>
    </message>
    <message>
        <source>VirtualBox - Runtime Error</source>
        <translation>VirtualBox - 執行階段錯誤</translation>
    </message>
    <message>
        <source>&lt;b&gt;Cannot access the kernel driver!&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;</source>
        <translation>&lt;b&gt;無法存取核心驅動程式!&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;</translation>
    </message>
    <message>
        <source>Unknown error %2 during initialization of the Runtime</source>
        <translation>執行階段的初始化期間未知錯誤 %2</translation>
    </message>
    <message>
        <source>Kernel driver not accessible</source>
        <translation>核心驅動程式不可存取</translation>
    </message>
    <message>
        <source>The VirtualBox kernel modules do not match this version of VirtualBox. The installation of VirtualBox was apparently not successful. Please try completely uninstalling and reinstalling VirtualBox.</source>
        <translation>VirtualBox 核心模組不符合這個 VirtualBox 的版本。 顯然 VirtualBox 的安裝不成功。 請嘗試完整解除安裝並重新安裝 VirtualBox 。</translation>
    </message>
    <message>
        <source>The VirtualBox kernel modules do not match this version of VirtualBox. The installation of VirtualBox was apparently not successful. Executing&lt;br/&gt;&lt;br/&gt;  &lt;font color=blue&gt;&apos;/etc/init.d/vboxdrv setup&apos;&lt;/font&gt;&lt;br/&gt;&lt;br/&gt;may correct this. Make sure that you do not mix the OSE version and the PUEL version of VirtualBox.</source>
        <translation>VirtualBox 核心模組不符合這個 VirtualBox 的版本。 顯然 VirtualBox 的安裝不成功。 執行&lt;br/&gt;&lt;br/&gt;  &lt;font color=blue&gt;&apos;/etc/init.d/vboxdrv setup&apos;&lt;/font&gt;&lt;br/&gt;&lt;br/&gt;應能修正問題。 請確認您未混合 VirtualBox 的 OSE 版本與 PUEL 版本。</translation>
    </message>
    <message>
        <source>This error means that the kernel driver was either not able to allocate enough memory or that some mapping operation failed.</source>
        <translation>這個錯誤代表核心驅動程式無法配置足夠的記憶體或某些對應操作失敗。</translation>
    </message>
</context>
<context>
    <name>QIArrowSplitter</name>
    <message>
        <source>&amp;Back</source>
        <translation>上一個(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation>下一個(&amp;N)</translation>
    </message>
</context>
<context>
    <name>QIFileDialog</name>
    <message>
        <source>Select a directory</source>
        <translation>選取目錄</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation>選取檔案</translation>
    </message>
</context>
<context>
    <name>QIHotKeyEdit</name>
    <message>
        <source>Left </source>
        <translation>←</translation>
    </message>
    <message>
        <source>Right </source>
        <translation>→</translation>
    </message>
    <message>
        <source>Left Shift</source>
        <translation>左側 Shift</translation>
    </message>
    <message>
        <source>Right Shift</source>
        <translation>右側 Shift</translation>
    </message>
    <message>
        <source>Left Ctrl</source>
        <translation>左側 Ctrl</translation>
    </message>
    <message>
        <source>Right Ctrl</source>
        <translation>右側 Ctrl</translation>
    </message>
    <message>
        <source>Left Alt</source>
        <translation>左側 Alt</translation>
    </message>
    <message>
        <source>Right Alt</source>
        <translation>右側 Alt</translation>
    </message>
    <message>
        <source>Left WinKey</source>
        <translation>左側 WinKey</translation>
    </message>
    <message>
        <source>Right WinKey</source>
        <translation>右側 WinKey</translation>
    </message>
    <message>
        <source>Menu key</source>
        <translation>功能表鍵</translation>
    </message>
    <message>
        <source>Alt Gr</source>
        <translation>Alt Gr</translation>
    </message>
    <message>
        <source>Caps Lock</source>
        <translation>Caps Lock</translation>
    </message>
    <message>
        <source>Scroll Lock</source>
        <translation>Scroll Lock</translation>
    </message>
    <message>
        <source>&lt;key_%1&gt;</source>
        <translation>&lt;key_%1&gt;</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation>Print Screen</translation>
    </message>
    <message>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <source>F7</source>
        <translation>F7</translation>
    </message>
    <message>
        <source>F8</source>
        <translation>F8</translation>
    </message>
    <message>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <source>F12</source>
        <translation>F12</translation>
    </message>
    <message>
        <source>F13</source>
        <translation>F13</translation>
    </message>
    <message>
        <source>F14</source>
        <translation>F14</translation>
    </message>
    <message>
        <source>F15</source>
        <translation>F15</translation>
    </message>
    <message>
        <source>F16</source>
        <translation>F16</translation>
    </message>
    <message>
        <source>F17</source>
        <translation>F17</translation>
    </message>
    <message>
        <source>F18</source>
        <translation>F18</translation>
    </message>
    <message>
        <source>F19</source>
        <translation>F19</translation>
    </message>
    <message>
        <source>F20</source>
        <translation>F20</translation>
    </message>
    <message>
        <source>F21</source>
        <translation>F21</translation>
    </message>
    <message>
        <source>F22</source>
        <translation>F22</translation>
    </message>
    <message>
        <source>F23</source>
        <translation>F23</translation>
    </message>
    <message>
        <source>F24</source>
        <translation>F24</translation>
    </message>
    <message>
        <source>Num Lock</source>
        <translation>Num Lock</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>下一步</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
</context>
<context>
    <name>QIHttp</name>
    <message>
        <source>Connection timed out</source>
        <translation>連線逾時</translation>
    </message>
    <message>
        <source>Could not locate the file on the server (response: %1)</source>
        <translation>找不到伺服器上的檔案 (回應: %1)</translation>
    </message>
</context>
<context>
    <name>QILabel</name>
    <message>
        <source>&amp;Copy</source>
        <translation>複製(&amp;C)</translation>
    </message>
</context>
<context>
    <name>QIMessageBox</name>
    <message>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <source>&amp;Details</source>
        <translation>詳細資料(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Details (%1 of %2)</source>
        <translation>詳細資料 (%1 之 %2)(&amp;D)</translation>
    </message>
</context>
<context>
    <name>QIWidgetValidator</name>
    <message>
        <source>not complete</source>
        <comment>value state</comment>
        <translation>不完整</translation>
    </message>
    <message>
        <source>invalid</source>
        <comment>value state</comment>
        <translation>無效</translation>
    </message>
    <message>
        <source>&lt;qt&gt;The value of the &lt;b&gt;%1&lt;/b&gt; field on the &lt;b&gt;%2&lt;/b&gt; page is %3.&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;在 &lt;b&gt;%2&lt;/b&gt; 頁面欄位 &lt;b&gt;%1&lt;/b&gt; 的值為 %3。&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&lt;qt&gt;One of the values on the &lt;b&gt;%1&lt;/b&gt; page is %2.&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;在 &lt;b&gt;%1&lt;/b&gt; 頁中的值之一是 %2。&lt;/qt&gt;</translation>
    </message>
</context>
<context>
    <name>VBoxAboutDlg</name>
    <message>
        <source>VirtualBox - About</source>
        <translation>VirtualBox - 關於</translation>
    </message>
    <message>
        <source>VirtualBox Graphical User Interface</source>
        <translation>VirtualBox 圖形化使用者介面</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation>版本 %1</translation>
    </message>
</context>
<context>
    <name>VBoxAdditionsDownloader</name>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Downloading the VirtualBox Guest Additions CD image from &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;...&lt;/nobr&gt;</source>
        <translation>正在從 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt; 下載 VirtualBox Guest Additions CD 映像...&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Cancel the VirtualBox Guest Additions CD image download</source>
        <translation>取消下載 VirtualBox Guest Additions CD 映像</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to save the downloaded file as &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;.&lt;/nobr&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;儲存下載檔案為 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt; 失敗。&lt;/nobr&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Select folder to save Guest Additions image to</source>
        <translation>選取要儲存 Guest Additions 映像的資料夾</translation>
    </message>
</context>
<context>
    <name>VBoxApplianceEditorWgt</name>
    <message>
        <source>Virtual System %1</source>
        <translation>虛擬系統 %1</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <source>Product</source>
        <translation>產品</translation>
    </message>
    <message>
        <source>Product-URL</source>
        <translation>產品網址</translation>
    </message>
    <message>
        <source>Vendor</source>
        <translation>供應商</translation>
    </message>
    <message>
        <source>Vendor-URL</source>
        <translation>供應商-URL</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <source>License</source>
        <translation>授權</translation>
    </message>
    <message>
        <source>Guest OS Type</source>
        <translation>客體作業系統類型</translation>
    </message>
    <message>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <source>RAM</source>
        <translation>RAM</translation>
    </message>
    <message>
        <source>Hard Disk Controller (IDE)</source>
        <translation>硬碟控制器 (IDE)</translation>
    </message>
    <message>
        <source>Hard Disk Controller (SATA)</source>
        <translation>硬碟控制器 (SATA)</translation>
    </message>
    <message>
        <source>Hard Disk Controller (SCSI)</source>
        <translation>硬碟控制器 (SCSI)</translation>
    </message>
    <message>
        <source>DVD</source>
        <translation>DVD</translation>
    </message>
    <message>
        <source>Floppy</source>
        <translation>軟碟</translation>
    </message>
    <message>
        <source>Network Adapter</source>
        <translation>網路卡</translation>
    </message>
    <message>
        <source>USB Controller</source>
        <translation>USB 控制器</translation>
    </message>
    <message>
        <source>Sound Card</source>
        <translation>音效卡</translation>
    </message>
    <message>
        <source>Virtual Disk Image</source>
        <translation>虛擬磁碟映像</translation>
    </message>
    <message>
        <source>Unknown Hardware Item</source>
        <translation>未知硬體項目</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>&lt;b&gt;Original Value:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;原始值:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>組態</translation>
    </message>
    <message>
        <source>Warnings:</source>
        <translation>警告:</translation>
    </message>
</context>
<context>
    <name>VBoxCloseVMDlg</name>
    <message>
        <source>Close Virtual Machine</source>
        <translation>關閉虛擬機器</translation>
    </message>
    <message>
        <source>You want to:</source>
        <translation>您想要:</translation>
    </message>
    <message>
        <source>&amp;Save the machine state</source>
        <translation>儲存機器狀態(&amp;S)</translation>
    </message>
    <message>
        <source>&amp;Power off the machine</source>
        <translation>關閉機器(&amp;P)</translation>
    </message>
    <message>
        <source>S&amp;end the shutdown signal</source>
        <translation>傳送關機訊號(&amp;E)</translation>
    </message>
    <message>
        <source>&lt;p&gt;When checked, the machine will be returned to the state stored in the current snapshot after it is turned off. This is useful if you are sure that you want to discard the results of your last sessions and start again at that snapshot.&lt;/p&gt;</source>
        <translation>&lt;p&gt;勾選時，機器將返回目前快照中儲存的狀態在它關閉之後。 這是有用的，如果您確認要放棄您最後的工作階段結果並再次啟動該快照。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Saves the current execution state of the virtual machine to the physical hard disk of the host PC.&lt;/p&gt;&lt;p&gt;Next time this machine is started, it will be restored from the saved state and continue execution from the same place you saved it at, which will let you continue your work immediately.&lt;/p&gt;&lt;p&gt;Note that saving the machine state may take a long time, depending on the guest operating system type and the amount of memory you assigned to the virtual machine.&lt;/p&gt;</source>
        <translation>&lt;p&gt;儲存虛擬機器的目前執行狀態到主機 PC 的實體硬碟。&lt;/p&gt;&lt;p&gt;下次啟動這個機器，它將從儲存狀態還原並從您儲存的相同位置繼續執行，讓您立即繼續您的工作。&lt;/p&gt;&lt;p&gt;請注意儲存機器狀態可能需要很長時間取決於客體作業系統類型與分配至虛擬機器的記憶體量。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Sends the ACPI Power Button press event to the virtual machine.&lt;/p&gt;&lt;p&gt;Normally, the guest operating system running inside the virtual machine will detect this event and perform a clean shutdown procedure. This is a recommended way to turn off the virtual machine because all applications running inside it will get a chance to save their data and state.&lt;/p&gt;&lt;p&gt;If the machine doesn&apos;t respond to this action then the guest operating system may be misconfigured or doesn&apos;t understand ACPI Power Button events at all. In this case you should select the &lt;b&gt;Power off the machine&lt;/b&gt; action to stop virtual machine execution.&lt;/p&gt;</source>
        <translation>&lt;p&gt;傳送 ACPI 電源按鈕按下事件到虛擬機器。&lt;/p&gt;&lt;p&gt;通常，執行在虛擬機器內的客體作業系統將偵測這個事件，並執行乾淨關閉程序。 這是推薦關閉虛擬機器的方法，因為所有內部執行的應用程式將有機會儲存其資料和狀態。&lt;/p&gt;&lt;p&gt;如果機器不回應這個動作，客體作業系統可能組態不正確，或根本不明白 ACPI 電源按鈕事件。 在這種情況下您應該選擇&lt;b&gt;關閉機器電源&lt;/b&gt;動作停止虛擬機器執行。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Turns off the virtual machine.&lt;/p&gt;&lt;p&gt;Note that this action will stop machine execution immediately so that the guest operating system running inside it will not be able to perform a clean shutdown procedure which may result in &lt;i&gt;data loss&lt;/i&gt; inside the virtual machine. Selecting this action is recommended only if the virtual machine does not respond to the &lt;b&gt;Send the shutdown signal&lt;/b&gt; action.&lt;/p&gt;</source>
        <translation>&lt;p&gt;關閉虛擬機器。&lt;/p&gt;&lt;p&gt;請注意這個動作將立即停止機器執行，以致執行在客體內部的作業系統無法執行乾淨關機程序，這可能會造成虛擬機器中的&lt;i&gt;資料遺失&lt;/i&gt;。 建議只有在虛擬機器對&lt;b&gt;傳送關機訊號&lt;/b&gt;動作沒有回應時才選取這個動作。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Restore the machine state stored in the current snapshot</source>
        <translation>還原儲存在目前快照的機器狀態</translation>
    </message>
    <message>
        <source>&amp;Restore current snapshot &apos;%1&apos;</source>
        <translation>還原目前快照「%1」(&amp;R)</translation>
    </message>
</context>
<context>
    <name>VBoxConsoleWnd</name>
    <message>
        <source>VirtualBox OSE</source>
        <translation>VirtualBox 開放原始碼版本 (OSE)</translation>
    </message>
    <message>
        <source>&amp;Fullscreen Mode</source>
        <translation>全螢幕模式(&amp;F)</translation>
    </message>
    <message>
        <source>Switch to fullscreen mode</source>
        <translation>切換到全螢幕模式</translation>
    </message>
    <message>
        <source>Mouse Integration</source>
        <comment>enable/disable...</comment>
        <translation>滑鼠整合</translation>
    </message>
    <message>
        <source>Auto-resize Guest Display</source>
        <comment>enable/disable...</comment>
        <translation>自動調整客體顯示大小</translation>
    </message>
    <message>
        <source>Auto-resize &amp;Guest Display</source>
        <translation>自動調整客體顯示大小(&amp;G)</translation>
    </message>
    <message>
        <source>Automatically resize the guest display when the window is resized (requires Guest Additions)</source>
        <translation>當變更視窗大小時自動調整客體顯示的大小 (需要 Guest Additions)</translation>
    </message>
    <message>
        <source>&amp;Adjust Window Size</source>
        <translation>調整視窗大小(&amp;A)</translation>
    </message>
    <message>
        <source>Adjust window size and position to best fit the guest display</source>
        <translation>調整視窗大小與位置以最符合客體顯示</translation>
    </message>
    <message>
        <source>&amp;Insert Ctrl-Alt-Del</source>
        <translation>插入 Ctrl-Alt-Del (&amp;I)</translation>
    </message>
    <message>
        <source>Send the Ctrl-Alt-Del sequence to the virtual machine</source>
        <translation>傳送 Ctrl-Alt-Del 序列到虛擬機器</translation>
    </message>
    <message>
        <source>&amp;Insert Ctrl-Alt-Backspace</source>
        <translation>插入 Ctrl-Alt-Backspace (&amp;I)</translation>
    </message>
    <message>
        <source>Send the Ctrl-Alt-Backspace sequence to the virtual machine</source>
        <translation>傳送 Ctrl-Alt-Backspace 序列到虛擬機器</translation>
    </message>
    <message>
        <source>&amp;Reset</source>
        <translation>重設(&amp;R)</translation>
    </message>
    <message>
        <source>Reset the virtual machine</source>
        <translation>重設虛擬機器</translation>
    </message>
    <message>
        <source>ACPI S&amp;hutdown</source>
        <translation>ACPI 關機(&amp;H)</translation>
    </message>
    <message>
        <source>Send the ACPI Power Button press event to the virtual machine</source>
        <translation>傳送按下 ACPI 電源開關按鈕的事件到虛擬機器</translation>
    </message>
    <message>
        <source>&amp;Close...</source>
        <translation>關閉(&amp;C)...</translation>
    </message>
    <message>
        <source>Close the virtual machine</source>
        <translation>關閉虛擬機器</translation>
    </message>
    <message>
        <source>Take &amp;Snapshot...</source>
        <translation>取得快照(&amp;S)...</translation>
    </message>
    <message>
        <source>Take a snapshot of the virtual machine</source>
        <translation>取得虛擬機器的快照</translation>
    </message>
    <message>
        <source>Enable or disable remote desktop (RDP) connections to this machine</source>
        <translation>啟用或停用遠端桌面 (RDP) 連線到這個機器</translation>
    </message>
    <message>
        <source>&amp;Shared Folders...</source>
        <translation>共用資料夾(&amp;S)...</translation>
    </message>
    <message>
        <source>Create or modify shared folders</source>
        <translation>建立或修改共用資料夾</translation>
    </message>
    <message>
        <source>&amp;Install Guest Additions...</source>
        <translation>安裝 Guest Additions (&amp;I)...</translation>
    </message>
    <message>
        <source>Mount the Guest Additions installation image</source>
        <translation>掛載 Guest Additions 安裝映像</translation>
    </message>
    <message>
        <source>&amp;USB Devices</source>
        <translation>USB 裝置(&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Devices</source>
        <translation>裝置(&amp;D)</translation>
    </message>
    <message>
        <source>De&amp;bug</source>
        <translation>除錯(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>說明(&amp;H)</translation>
    </message>
    <message>
        <source>&lt;hr&gt;The VRDP Server is listening on port %1</source>
        <translation>&lt;hr&gt;VRDP 伺服器正在偵聽連接埠 %1</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>暫停(&amp;P)</translation>
    </message>
    <message>
        <source>Suspend the execution of the virtual machine</source>
        <translation>暫停虛擬機器的執行</translation>
    </message>
    <message>
        <source>R&amp;esume</source>
        <translation>繼續執行(&amp;E)</translation>
    </message>
    <message>
        <source>Resume the execution of the virtual machine</source>
        <translation>繼續執行虛擬機器</translation>
    </message>
    <message>
        <source>Disable &amp;Mouse Integration</source>
        <translation>停用滑鼠整合(&amp;M)</translation>
    </message>
    <message>
        <source>Temporarily disable host mouse pointer integration</source>
        <translation>暫時停用主機滑鼠指標整合</translation>
    </message>
    <message>
        <source>Enable &amp;Mouse Integration</source>
        <translation>啟用滑鼠整合(&amp;M)</translation>
    </message>
    <message>
        <source>Enable temporarily disabled host mouse pointer integration</source>
        <translation>啟用暫時停用的主機滑鼠指標整合</translation>
    </message>
    <message>
        <source>Snapshot %1</source>
        <translation>快照 %1</translation>
    </message>
    <message>
        <source>&amp;Machine</source>
        <translation>機器(&amp;M)</translation>
    </message>
    <message>
        <source>Seam&amp;less Mode</source>
        <translation>無縫模式(&amp;L)</translation>
    </message>
    <message>
        <source>Switch to seamless desktop integration mode</source>
        <translation>切換到無縫桌面整合模式</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No hard disks attached&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>HDD tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;未附加硬碟&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;Adapter %1 (%2)&lt;/b&gt;: cable %3&lt;/nobr&gt;</source>
        <comment>Network adapters tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;介面卡 %1 (%2)&lt;/b&gt;: 線路 %3&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>connected</source>
        <comment>Network adapters tooltip</comment>
        <translation>已連接</translation>
    </message>
    <message>
        <source>disconnected</source>
        <comment>Network adapters tooltip</comment>
        <translation>已中斷連接</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;All network adapters are disabled&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>Network adapters tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;所有網路卡已停用&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No USB devices attached&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;未附加 USB 裝置&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;USB Controller is disabled&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;USB 控制器已停用&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No shared folders&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>Shared folders tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;無共用資料夾&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Session I&amp;nformation Dialog</source>
        <translation>工作階段資訊對話方塊(&amp;N)</translation>
    </message>
    <message>
        <source>Show Session Information Dialog</source>
        <translation>顯示工作階段資訊對話方塊</translation>
    </message>
    <message>
        <source>&amp;Statistics...</source>
        <comment>debug action</comment>
        <translation>統計(&amp;S)...</translation>
    </message>
    <message>
        <source>&amp;Command Line...</source>
        <comment>debug action</comment>
        <translation>命令列(&amp;C)...</translation>
    </message>
    <message>
        <source>Indicates whether the guest display auto-resize function is On (&lt;img src=:/auto_resize_on_16px.png/&gt;) or Off (&lt;img src=:/auto_resize_off_16px.png/&gt;). Note that this function requires Guest Additions to be installed in the guest OS.</source>
        <translation>指示自動調整客體顯示的大小功能為開啟 (&lt;img src=:/auto_resize_on_16px.png/&gt;) 或關閉 (&lt;img src=:/auto_resize_off_16px.png/&gt;). 請注意: 這個功能需要在客體作業系統中安裝 Guest Additions 。</translation>
    </message>
    <message>
        <source>Indicates whether the host mouse pointer is captured by the guest OS:&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_disabled_16px.png/&gt;&amp;nbsp;&amp;nbsp;pointer is not captured&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_16px.png/&gt;&amp;nbsp;&amp;nbsp;pointer is captured&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_seamless_16px.png/&gt;&amp;nbsp;&amp;nbsp;mouse integration (MI) is On&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_can_seamless_16px.png/&gt;&amp;nbsp;&amp;nbsp;MI is Off, pointer is captured&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_can_seamless_uncaptured_16px.png/&gt;&amp;nbsp;&amp;nbsp;MI is Off, pointer is not captured&lt;/nobr&gt;&lt;br&gt;Note that the mouse integration feature requires Guest Additions to be installed in the guest OS.</source>
        <translation>指示客體作業系統是否擷取主機滑鼠指標:&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_disabled_16px.png/&gt;&amp;nbsp;&amp;nbsp;未擷取指標&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_16px.png/&gt;&amp;nbsp;&amp;nbsp;擷取指標&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_seamless_16px.png/&gt;&amp;nbsp;&amp;nbsp;滑鼠整合 (MI) 開啟&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_can_seamless_16px.png/&gt;&amp;nbsp;&amp;nbsp; MI 關閉，擷取指標&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;img src=:/mouse_can_seamless_uncaptured_16px.png/&gt;&amp;nbsp;&amp;nbsp; MI 關閉，未擷取指標&lt;/nobr&gt;&lt;br&gt;請注意: 滑鼠整合功能需要在客體作業系統中安裝 Guest Additions 。</translation>
    </message>
    <message>
        <source>Indicates whether the keyboard is captured by the guest OS (&lt;img src=:/hostkey_captured_16px.png/&gt;) or not (&lt;img src=:/hostkey_16px.png/&gt;).</source>
        <translation>指示客體作業系統擷取 (&lt;img src=:/hostkey_captured_16px.png/&gt;) 或未擷取 (&lt;img src=:/hostkey_16px.png/&gt;) 鍵盤。</translation>
    </message>
    <message>
        <source>Indicates whether the Remote Display (VRDP Server) is enabled (&lt;img src=:/vrdp_16px.png/&gt;) or not (&lt;img src=:/vrdp_disabled_16px.png/&gt;).</source>
        <translation>指示啟用 (&lt;img src=:/vrdp_16px.png/&gt;) 或未啟用 (&lt;img src=:/vrdp_disabled_16px.png/&gt;) 遠端顯示 (VRDP 伺服器) 。</translation>
    </message>
    <message>
        <source>&amp;Logging...</source>
        <comment>debug action</comment>
        <translation>紀錄(&amp;L)...</translation>
    </message>
    <message>
        <source>Shows the currently assigned Host key.&lt;br&gt;This key, when pressed alone, toggles the keyboard and mouse capture state. It can also be used in combination with other keys to quickly perform actions from the main menu.</source>
        <translation>顯示目前指派的 Host 鍵。&lt;br&gt;當單獨按下這個按鍵，切換鍵盤和滑鼠的擷取狀態。 它也可以與其它按鍵結合來快速從主功能表執行動作。</translation>
    </message>
    <message>
        <source>Sun VirtualBox</source>
        <translation>Sun VirtualBox</translation>
    </message>
    <message>
        <source>Indicates the status of the hardware virtualization features used by this virtual machine:&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%1:&lt;/b&gt;&amp;nbsp;%2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%3:&lt;/b&gt;&amp;nbsp;%4&lt;/nobr&gt;</source>
        <comment>Virtualization Stuff LED</comment>
        <translation>指示這個虛擬機器使用的硬體虛擬化功能之狀態:&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%1:&lt;/b&gt;&amp;nbsp;%2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%3:&lt;/b&gt;&amp;nbsp;%4&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%1:&lt;/b&gt;&amp;nbsp;%2&lt;/nobr&gt;</source>
        <comment>Virtualization Stuff LED</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;%1:&lt;/b&gt;&amp;nbsp;%2&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source> EXPERIMENTAL build %1r%2 - %3</source>
        <translation>EXPERIMENTAL 組建 %1r%2 - %3</translation>
    </message>
    <message>
        <source>&amp;CD/DVD Devices</source>
        <translation>CD/DVD 裝置(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Floppy Devices</source>
        <translation>軟碟裝置(&amp;F)</translation>
    </message>
    <message>
        <source>&amp;Network Adapters...</source>
        <translation>網路卡(&amp;N)...</translation>
    </message>
    <message>
        <source>Change the settings of network adapters</source>
        <translation>變更網路卡的設定值</translation>
    </message>
    <message>
        <source>&amp;Remote Display</source>
        <translation>遠端顯示(&amp;R)</translation>
    </message>
    <message>
        <source>Remote Desktop (RDP) Server</source>
        <comment>enable/disable...</comment>
        <translation>遠端桌面 (RDP) 伺服器</translation>
    </message>
    <message>
        <source>More CD/DVD Images...</source>
        <translation>更多 CD/DVD 映像...</translation>
    </message>
    <message>
        <source>Unmount CD/DVD Device</source>
        <translation>卸載 CD/DVD 裝置</translation>
    </message>
    <message>
        <source>More Floppy Images...</source>
        <translation>更多軟碟映像...</translation>
    </message>
    <message>
        <source>Unmount Floppy Device</source>
        <translation>卸載軟碟裝置</translation>
    </message>
    <message>
        <source>No CD/DVD Devices Attached</source>
        <translation>未附加 CD/DVD 裝置</translation>
    </message>
    <message>
        <source>No Floppy Devices Attached</source>
        <translation>未附加軟碟裝置</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the virtual hard disks:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>HDD tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;指示虛擬硬碟之活動:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the CD/DVD devices:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>CD/DVD tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;指示附加的 CD/DVD 裝置之活動:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No CD/DVD devices attached&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>CD/DVD tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;未附加 CD/DVD 裝置&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the floppy devices:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>FD tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;指示軟碟裝置之活動:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;No floppy devices attached&lt;/b&gt;&lt;/nobr&gt;</source>
        <comment>FD tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;&lt;b&gt;未附加軟體裝置&lt;/b&gt;&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the network interfaces:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>Network adapters tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;指示網路卡之活動:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the attached USB devices:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;指示附加的 USB 裝置之活動:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;Indicates the activity of the machine&apos;s shared folders:&lt;/nobr&gt;%1&lt;/p&gt;</source>
        <comment>Shared folders tooltip</comment>
        <translation>&lt;p style=&apos;white-space:pre&apos;&gt;&lt;nobr&gt;指示電腦的共用資料夾之活動:&lt;/nobr&gt;%1&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>VBoxDownloaderWgt</name>
    <message>
        <source>The download process has been cancelled by the user.</source>
        <translation>使用者已取消下載程序。</translation>
    </message>
</context>
<context>
    <name>VBoxEmptyFileSelector</name>
    <message>
        <source>&amp;Choose...</source>
        <translation>選擇(&amp;C)...</translation>
    </message>
</context>
<context>
    <name>VBoxExportApplianceWzd</name>
    <message>
        <source>Select a file to export into</source>
        <translation>選取匯出的檔案</translation>
    </message>
    <message>
        <source>Open Virtualization Format (%1)</source>
        <translation>開放虛擬化格式 (%1)</translation>
    </message>
    <message>
        <source>Appliance</source>
        <translation>應用裝置</translation>
    </message>
    <message>
        <source>Exporting Appliance ...</source>
        <translation>匯出應用裝置 ...</translation>
    </message>
    <message>
        <source>Appliance Export Wizard</source>
        <translation>應用裝置匯出精靈</translation>
    </message>
    <message>
        <source>Welcome to the Appliance Export Wizard!</source>
        <translation>歡迎使用應用裝置匯出精靈!</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This wizard will guide you through the process of exporting an appliance. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Use the &lt;span style=&quot; font-weight:600;&quot;&gt;Next&lt;/span&gt; button to go the next page of the wizard and the &lt;span style=&quot; font-weight:600;&quot;&gt;Back&lt;/span&gt; button to return to the previous page.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Please select the virtual machines that you wish to the appliance. You can select more than one. Please note that these machines have to be turned off before they can be exported.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;這個精靈將導引您匯出應用裝置。 &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;使用 [&lt;span style=&quot; font-weight:600;&quot;&gt;下一步&lt;/span&gt;] 按鈕前往精靈的下一頁與 [&lt;span style=&quot; font-weight:600;&quot;&gt;上一步&lt;/span&gt;] 按鈕返回上一頁。&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;請選取您要匯出到應用裝置的虛擬機器。 您可以選取一個以上。 請注意，在可以匯出這些機器前必須將它們關機。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; 上一步(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>下一步(&amp;N) &gt;</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Appliance Export Settings</source>
        <translation>應用裝置匯出設定值</translation>
    </message>
    <message>
        <source>Here you can change additional configuration values of the selected virtual machines. You can modify most of the properties shown by double-clicking on the items.</source>
        <translation>在此您可以變更選取虛擬機器的額外組態值。 您可以在項目連按兩下修改大多數顯示的內容。</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>還原預設值</translation>
    </message>
    <message>
        <source>&amp;Export &gt;</source>
        <translation>匯出(&amp;E) &gt;</translation>
    </message>
    <message>
        <source>Write in legacy OVF 0.9 format for compatibility with other virtualization products.</source>
        <translation>以傳統 OVF 0.9 格式寫入與其它虛擬化產品相容。</translation>
    </message>
    <message>
        <source>&amp;Write legacy OVF 0.9</source>
        <translation>寫入舊版 OVF 0.9 (&amp;W)</translation>
    </message>
    <message>
        <source>Please choose a filename to export the OVF to.</source>
        <translation>請選擇要匯出 OVF 的檔案名稱。</translation>
    </message>
    <message>
        <source>Please complete the additional fields like the username, password and the bucket, and provide a filename for the OVF target.</source>
        <translation>請完成額外欄位比如使用者名稱、密碼與貯體，並提供 OVF 目標的檔案名稱。</translation>
    </message>
    <message>
        <source>Please complete the additional fields like the username, password, hostname and the bucket, and provide a filename for the OVF target.</source>
        <translation>請完成額外欄位比如使用者名稱、密碼、主機名稱與貯體，並提供 OVF 目標的檔案名稱。</translation>
    </message>
    <message>
        <source>Checking files ...</source>
        <translation>正在檢查檔案 ...</translation>
    </message>
    <message>
        <source>Removing files ...</source>
        <translation>正在移除檔案 ...</translation>
    </message>
    <message>
        <source>Please specify the target for the OVF export. You can choose between a local file system export, uploading the OVF to the Sun Cloud service or an S3 storage server.</source>
        <translation>請指定 OVF 匯出的目標。 您可以選擇本機檔案系統匯出，或上傳 OVF 至 Sun Cloud 伺服器或 S3 存放伺服器。</translation>
    </message>
    <message>
        <source>&amp;Local Filesystem </source>
        <translation>本機檔案系統(&amp;L)</translation>
    </message>
    <message>
        <source>Sun &amp;Cloud</source>
        <translation>Sun &amp;Cloud</translation>
    </message>
    <message>
        <source>&amp;Simple Storage System (S3)</source>
        <translation>簡單存放系統 (S3)(&amp;S)</translation>
    </message>
    <message>
        <source>&amp;Username:</source>
        <translation>使用者名稱(&amp;U):</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation>密碼(&amp;P):</translation>
    </message>
    <message>
        <source>&amp;File:</source>
        <translation>檔案(&amp;F):</translation>
    </message>
    <message>
        <source>&amp;Bucket:</source>
        <translation>貯體(&amp;B):</translation>
    </message>
    <message>
        <source>&amp;Hostname:</source>
        <translation>主機名稱(&amp;H):</translation>
    </message>
</context>
<context>
    <name>VBoxFilePathSelectorWidget</name>
    <message>
        <source>&lt;reset to default&gt;</source>
        <translation>&lt;重設為預設值&gt;</translation>
    </message>
    <message>
        <source>The actual default path value will be displayed after accepting the changes and opening this dialog again.</source>
        <translation>接受所做的變更並重新開啟這個對話方塊後，將顯示實際的預設路徑值。</translation>
    </message>
    <message>
        <source>&lt;not selected&gt;</source>
        <translation>&lt;未選取&gt;</translation>
    </message>
    <message>
        <source>Please use the &lt;b&gt;Other...&lt;/b&gt; item from the drop-down list to select a path.</source>
        <translation>請從下拉清單使用 &lt;b&gt;其它...&lt;/b&gt; 項目選取路徑。</translation>
    </message>
    <message>
        <source>Other...</source>
        <translation>其它...</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>重設</translation>
    </message>
    <message>
        <source>Opens a dialog to select a different folder.</source>
        <translation>開啟選取不同資料夾的對話方塊。</translation>
    </message>
    <message>
        <source>Resets the folder path to the default value.</source>
        <translation>重設資料夾路徑為預設值。</translation>
    </message>
    <message>
        <source>Opens a dialog to select a different file.</source>
        <translation>開啟選取不同檔案的對話方塊。</translation>
    </message>
    <message>
        <source>Resets the file path to the default value.</source>
        <translation>重設檔案路徑為預設值。</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>複製(&amp;C)</translation>
    </message>
    <message>
        <source>Please type the folder path here.</source>
        <translation>請在此輸入資料夾路徑。</translation>
    </message>
    <message>
        <source>Please type the file path here.</source>
        <translation>請在此輸入檔案路徑。</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsDlg</name>
    <message>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <source>Input</source>
        <translation>輸入</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>語言</translation>
    </message>
    <message>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <source>VirtualBox - %1</source>
        <translation>VirtualBox - %1</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>網路</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsGeneral</name>
    <message>
        <source>Displays the path to the default virtual machine folder. This folder is used, if not explicitly specified otherwise, when creating new virtual machines.</source>
        <translation>顯示預設虛擬機器資料夾的路徑。 當加入現有或新建虛擬機器時，如果沒有特別指定，就會使用這個資料夾。</translation>
    </message>
    <message>
        <source>Displays the path to the library that provides authentication for Remote Display (VRDP) clients.</source>
        <translation>顯示提供遠端顯示 (VRDP) 用戶端驗證的程式庫路徑。</translation>
    </message>
    <message>
        <source>Default &amp;Hard Disk Folder:</source>
        <translation>預設硬碟資料夾(&amp;H):</translation>
    </message>
    <message>
        <source>Default &amp;Machine Folder:</source>
        <translation>預設機器資料夾(&amp;M):</translation>
    </message>
    <message>
        <source>V&amp;RDP Authentication Library:</source>
        <translation>VRDP 驗證程式庫(&amp;R):</translation>
    </message>
    <message>
        <source>Displays the path to the default hard disk folder. This folder is used, if not explicitly specified otherwise, when adding existing or creating new virtual hard disks.</source>
        <translation>顯示預設硬碟資料夾的路徑。 當加入現有或新建虛擬硬碟時，如果沒有特別指定，就會使用這個資料夾。</translation>
    </message>
    <message>
        <source>When checked, the application will provide an icon with the context menu in the system tray.</source>
        <translation>勾選時，應用程式將在系統通知區提供含內容功能表的圖示。</translation>
    </message>
    <message>
        <source>&amp;Show System Tray Icon</source>
        <translation>顯示系統通知區圖示(&amp;S)</translation>
    </message>
    <message>
        <source>When checked, the Dock Icon will reflect the VM window content in realtime.</source>
        <translation>勾選時，停駐圖示將即時顯示 VM 視窗內容。</translation>
    </message>
    <message>
        <source>&amp;Dock Icon Realtime Preview</source>
        <translation>停駐圖示即時預覽(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Auto show Dock and Menubar in fullscreen</source>
        <translation>在全螢幕自動顯示停駐 &amp;&amp; 功能表列(&amp;A)</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsInput</name>
    <message>
        <source>Host &amp;Key:</source>
        <translation>Host 鍵(&amp;K):</translation>
    </message>
    <message>
        <source>Displays the key used as a Host Key in the VM window. Activate the entry field and press a new Host Key. Note that alphanumeric, cursor movement and editing keys cannot be used.</source>
        <translation>顯示於虛擬機器視窗中使用的 Host 鍵。 啟動輸入欄位並按下新的 Host 鍵。 請注意，無法使用字母數字、游標移動與編輯鍵。</translation>
    </message>
    <message>
        <source>When checked, the keyboard is automatically captured every time the VM window is activated. When the keyboard is captured, all keystrokes (including system ones like Alt-Tab) are directed to the VM.</source>
        <translation>勾選時，每次虛擬機器視窗啟動時，鍵盤會自動擷取。 當擷取鍵盤時，所有的按鍵動作 (包括系統的 Alt-Tab) 都會針對虛擬機器。</translation>
    </message>
    <message>
        <source>&amp;Auto Capture Keyboard</source>
        <translation>自動擷取鍵盤(&amp;A)</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsLanguage</name>
    <message>
        <source> (built-in)</source>
        <comment>Language</comment>
        <translation>(內建)</translation>
    </message>
    <message>
        <source>&lt;unavailable&gt;</source>
        <comment>Language</comment>
        <translation>&lt;不可用&gt;</translation>
    </message>
    <message>
        <source>&lt;unknown&gt;</source>
        <comment>Author(s)</comment>
        <translation>&lt;未知&gt;</translation>
    </message>
    <message>
        <source>Default</source>
        <comment>Language</comment>
        <translation>預設值</translation>
    </message>
    <message>
        <source>Language:</source>
        <translation>語言:</translation>
    </message>
    <message>
        <source>&amp;Interface Language:</source>
        <translation>介面語言(&amp;I):</translation>
    </message>
    <message>
        <source>Lists all available user interface languages. The effective language is written in &lt;b&gt;bold&lt;/b&gt;. Select &lt;i&gt;Default&lt;/i&gt; to reset to the system default language.</source>
        <translation>列出所有可用的使用者介面語言。 生效的語言以&lt;b&gt;粗體&lt;/b&gt;顯示。 選取&lt;i&gt;預設值&lt;/i&gt;來重設為系統預設語言。</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>語言</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <source>Author(s):</source>
        <translation>作者:</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsNetwork</name>
    <message>
        <source>%1 network</source>
        <comment>&lt;adapter name&gt; network</comment>
        <translation>%1 網路</translation>
    </message>
    <message>
        <source>host IPv4 address of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;的主機 IPv4 位址錯誤</translation>
    </message>
    <message>
        <source>host IPv4 network mask of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;的主機 IPv4 網路遮罩錯誤</translation>
    </message>
    <message>
        <source>host IPv6 address of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;的主機 IPv6 位址錯誤</translation>
    </message>
    <message>
        <source>DHCP server address of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;的 DHCP 伺服器位址錯誤</translation>
    </message>
    <message>
        <source>DHCP server network mask of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;的 DHCP 伺服器網路遮罩錯誤</translation>
    </message>
    <message>
        <source>DHCP lower address bound of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;的 DHCP 位址下邊界錯誤</translation>
    </message>
    <message>
        <source>DHCP upper address bound of &lt;b&gt;%1&lt;/b&gt; is wrong</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;的 DHCP 位址上邊界錯誤</translation>
    </message>
    <message>
        <source>Adapter</source>
        <translation>介面卡</translation>
    </message>
    <message>
        <source>Automatically configured</source>
        <comment>interface</comment>
        <translation>自動組態</translation>
    </message>
    <message>
        <source>Manually configured</source>
        <comment>interface</comment>
        <translation>手動組態</translation>
    </message>
    <message>
        <source>IPv4 Address</source>
        <translation>IPv4 位址</translation>
    </message>
    <message>
        <source>Not set</source>
        <comment>address</comment>
        <translation>未設定</translation>
    </message>
    <message>
        <source>IPv4 Network Mask</source>
        <translation>IPv4 網路遮罩</translation>
    </message>
    <message>
        <source>Not set</source>
        <comment>mask</comment>
        <translation>未設定</translation>
    </message>
    <message>
        <source>IPv6 Address</source>
        <translation>IPv6 位址</translation>
    </message>
    <message>
        <source>IPv6 Network Mask Length</source>
        <translation>IPv6 網路遮罩長度</translation>
    </message>
    <message>
        <source>Not set</source>
        <comment>length</comment>
        <translation>未設定</translation>
    </message>
    <message>
        <source>DHCP Server</source>
        <translation>DHCP 伺服器</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>server</comment>
        <translation>啟用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>server</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>位址</translation>
    </message>
    <message>
        <source>Network Mask</source>
        <translation>網路遮罩</translation>
    </message>
    <message>
        <source>Lower Bound</source>
        <translation>下邊界</translation>
    </message>
    <message>
        <source>Not set</source>
        <comment>bound</comment>
        <translation>未設定</translation>
    </message>
    <message>
        <source>Upper Bound</source>
        <translation>上邊界</translation>
    </message>
    <message>
        <source>&amp;Add host-only network</source>
        <translation>加入「僅限主機」網路(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;Remove host-only network</source>
        <translation>移除「僅限主機」網路(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Edit host-only network</source>
        <translation>編輯「僅限主機」網路(&amp;E)</translation>
    </message>
    <message>
        <source>Performing</source>
        <comment>creating/removing host-only network</comment>
        <translation>正在執行</translation>
    </message>
    <message>
        <source>&amp;Host-only Networks:</source>
        <translation>「僅限主機」網路(&amp;H):</translation>
    </message>
    <message>
        <source>Lists all available host-only networks.</source>
        <translation>列出所有可用「僅限主機」網路。</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsNetworkDetails</name>
    <message>
        <source>Host-only Network Details</source>
        <translation>「僅限主機」網路詳細資料</translation>
    </message>
    <message>
        <source>&amp;Adapter</source>
        <translation>介面卡(&amp;A)</translation>
    </message>
    <message>
        <source>Manual &amp;Configuration</source>
        <translation>手動組態(&amp;C)</translation>
    </message>
    <message>
        <source>Use manual configuration for this host-only network adapter.</source>
        <translation>使用手動組態這個「僅限主機」網路卡。</translation>
    </message>
    <message>
        <source>&amp;IPv4 Address:</source>
        <translation>IPv4 位址(&amp;I):</translation>
    </message>
    <message>
        <source>Displays the host IPv4 address for this adapter.</source>
        <translation>顯示這個介面卡的主機 IPv4 位址。</translation>
    </message>
    <message>
        <source>IPv4 Network &amp;Mask:</source>
        <translation>IPv4 網路遮罩(&amp;M):</translation>
    </message>
    <message>
        <source>Displays the host IPv4 network mask for this adapter.</source>
        <translation>顯示這個介面卡的主機 IPv4 網路遮罩。</translation>
    </message>
    <message>
        <source>I&amp;Pv6 Address:</source>
        <translation>IPv6 位址(&amp;P):</translation>
    </message>
    <message>
        <source>Displays the host IPv6 address for this adapter if IPv6 is supported.</source>
        <translation>顯示這個介面卡的主機 IPv6 位址若支援 IPv6 。</translation>
    </message>
    <message>
        <source>IPv6 Network Mask &amp;Length:</source>
        <translation>IPv6 網路遮罩長度(&amp;L):</translation>
    </message>
    <message>
        <source>Displays the host IPv6 network mask prefix length for this adapter if IPv6 is supported.</source>
        <translation>顯示這個介面卡的主機 IPv6 網路遮罩前置長度若支援 IPv6 。</translation>
    </message>
    <message>
        <source>&amp;DHCP Server</source>
        <translation>DHCP 伺服器(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Enable Server</source>
        <translation>啟用伺服器(&amp;E)</translation>
    </message>
    <message>
        <source>Indicates whether the DHCP Server is enabled on machine startup or not.</source>
        <translation>指示機器啟動時是否啟用 DHCP 伺服器。</translation>
    </message>
    <message>
        <source>Server Add&amp;ress:</source>
        <translation>伺服器位址(&amp;R):</translation>
    </message>
    <message>
        <source>Displays the address of the DHCP server servicing the network associated with this host-only adapter.</source>
        <translation>顯示與這個「僅限主機」介面卡關聯的網路之 DHCP 伺服器服務位址。</translation>
    </message>
    <message>
        <source>Server &amp;Mask:</source>
        <translation>伺服器遮罩(&amp;M):</translation>
    </message>
    <message>
        <source>Displays the network mask of the DHCP server servicing the network associated with this host-only adapter.</source>
        <translation>顯示與這個「僅限主機」介面卡關聯的網路之 DHCP 伺服器服務網路遮罩。</translation>
    </message>
    <message>
        <source>&amp;Lower Address Bound:</source>
        <translation>位址下邊界(&amp;L):</translation>
    </message>
    <message>
        <source>Displays the lower address bound offered by the DHCP server servicing the network associated with this host-only adapter.</source>
        <translation>顯示由 DHCP 伺服器服務提供與這個「僅限主機」介面卡關聯的網路之位址下邊界。</translation>
    </message>
    <message>
        <source>&amp;Upper Address Bound:</source>
        <translation>位址上邊界(&amp;U):</translation>
    </message>
    <message>
        <source>Displays the upper address bound offered by the DHCP server servicing the network associated with this host-only adapter.</source>
        <translation>顯示由 DHCP 伺服器服務提供與這個「僅限主機」介面卡關聯的網路之位址上邊界。</translation>
    </message>
</context>
<context>
    <name>VBoxGLSettingsUpdate</name>
    <message>
        <source>When checked, the application will periodically connect to the VirtualBox website and check whether a new VirtualBox version is available.</source>
        <translation>勾選時，應用程式將定時連線到 VirtualBox 網站並檢查新的 VirtualBox 版本是否可用。</translation>
    </message>
    <message>
        <source>&amp;Check for updates</source>
        <translation>檢查更新(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Once per:</source>
        <translation>每隔(&amp;O):</translation>
    </message>
    <message>
        <source>Specifies how often the new version check should be performed. Note that if you want to completely disable this check, just clear the above check box.</source>
        <translation>指定應執行檢查新版本的頻率。 請注意，如果您要完全停用這個檢查，只需清除上方核取方塊。</translation>
    </message>
    <message>
        <source>Next Check:</source>
        <translation>下次檢查:</translation>
    </message>
    <message>
        <source>Check for:</source>
        <translation>檢查:</translation>
    </message>
    <message>
        <source>&lt;p&gt;Choose this if you only wish to be notified about stable updates to VirtualBox.&lt;/p&gt;</source>
        <translation>&lt;p&gt;選擇這個，如果您只想要有關 VirtualBox 的穩定版更新通知。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Stable release versions</source>
        <translation>穩定發佈版本(&amp;S)</translation>
    </message>
    <message>
        <source>&lt;p&gt;Choose this if you wish to be notified about all new VirtualBox releases.&lt;/p&gt;</source>
        <translation>&lt;p&gt;選擇這個，如果您想要有關所有新的 VirtualBox 發佈通知。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;All new releases</source>
        <translation>所有新的發佈(&amp;A)</translation>
    </message>
    <message>
        <source>&lt;p&gt;Choose this to be notified about all new VirtualBox releases and pre-release versions of VirtualBox.&lt;/p&gt;</source>
        <translation>&lt;p&gt;選擇這個，如果您想要有關所有新的 VirtualBox 發佈與 VirtualBox 的預先發佈版本通知。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>All new releases and &amp;pre-releases</source>
        <translation>所有新的發佈與預先發佈(&amp;P)</translation>
    </message>
</context>
<context>
    <name>VBoxGlobal</name>
    <message>
        <source>Unknown device %1:%2</source>
        <comment>USB device details</comment>
        <translation>未知的裝置 %1:%2</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Vendor ID: %1&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Product ID: %2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Revision: %3&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;nobr&gt;供應商 ID: %1&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;產品 ID: %2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;修訂: %3&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;Serial No. %1&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;序號 %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;&lt;nobr&gt;State: %1&lt;/nobr&gt;</source>
        <comment>USB device tooltip</comment>
        <translation>&lt;br&gt;&lt;nobr&gt;狀態: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Name</source>
        <comment>details report</comment>
        <translation>名稱</translation>
    </message>
    <message>
        <source>OS Type</source>
        <comment>details report</comment>
        <translation>作業系統類型</translation>
    </message>
    <message>
        <source>Base Memory</source>
        <comment>details report</comment>
        <translation>基本記憶體</translation>
    </message>
    <message>
        <source>General</source>
        <comment>details report</comment>
        <translation>一般</translation>
    </message>
    <message>
        <source>Video Memory</source>
        <comment>details report</comment>
        <translation>視訊記憶體</translation>
    </message>
    <message>
        <source>Boot Order</source>
        <comment>details report</comment>
        <translation>開機順序</translation>
    </message>
    <message>
        <source>ACPI</source>
        <comment>details report</comment>
        <translation>ACPI</translation>
    </message>
    <message>
        <source>IO APIC</source>
        <comment>details report</comment>
        <translation>IO APIC</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (ACPI)</comment>
        <translation>啟用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (ACPI)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (IO APIC)</comment>
        <translation>啟用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (IO APIC)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (audio)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Audio</source>
        <comment>details report</comment>
        <translation>音效</translation>
    </message>
    <message>
        <source>Adapter %1</source>
        <comment>details report (network)</comment>
        <translation>介面卡 %1</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (network)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Network</source>
        <comment>details report</comment>
        <translation>網路</translation>
    </message>
    <message>
        <source>Device Filters</source>
        <comment>details report (USB)</comment>
        <translation>裝置篩選器</translation>
    </message>
    <message>
        <source>%1 (%2 active)</source>
        <comment>details report (USB)</comment>
        <translation>%1 (%2 個啟用)</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (USB)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Powered Off</source>
        <comment>MachineState</comment>
        <translation>電源關閉</translation>
    </message>
    <message>
        <source>Saved</source>
        <comment>MachineState</comment>
        <translation>已儲存</translation>
    </message>
    <message>
        <source>Aborted</source>
        <comment>MachineState</comment>
        <translation>已中止</translation>
    </message>
    <message>
        <source>Running</source>
        <comment>MachineState</comment>
        <translation>執行中</translation>
    </message>
    <message>
        <source>Paused</source>
        <comment>MachineState</comment>
        <translation>已暫停</translation>
    </message>
    <message>
        <source>Starting</source>
        <comment>MachineState</comment>
        <translation>正在開始</translation>
    </message>
    <message>
        <source>Stopping</source>
        <comment>MachineState</comment>
        <translation>正在停止</translation>
    </message>
    <message>
        <source>Saving</source>
        <comment>MachineState</comment>
        <translation>正在儲存</translation>
    </message>
    <message>
        <source>Restoring</source>
        <comment>MachineState</comment>
        <translation>正在還原</translation>
    </message>
    <message>
        <source>Closed</source>
        <comment>SessionState</comment>
        <translation>已關閉</translation>
    </message>
    <message>
        <source>Open</source>
        <comment>SessionState</comment>
        <translation>開啟</translation>
    </message>
    <message>
        <source>Spawning</source>
        <comment>SessionState</comment>
        <translation>正在衍生</translation>
    </message>
    <message>
        <source>Closing</source>
        <comment>SessionState</comment>
        <translation>正在關閉</translation>
    </message>
    <message>
        <source>None</source>
        <comment>DeviceType</comment>
        <translation>無</translation>
    </message>
    <message>
        <source>Floppy</source>
        <comment>DeviceType</comment>
        <translation>軟碟</translation>
    </message>
    <message>
        <source>CD/DVD-ROM</source>
        <comment>DeviceType</comment>
        <translation>CD/DVD-ROM</translation>
    </message>
    <message>
        <source>Hard Disk</source>
        <comment>DeviceType</comment>
        <translation>硬碟</translation>
    </message>
    <message>
        <source>Network</source>
        <comment>DeviceType</comment>
        <translation>網路</translation>
    </message>
    <message>
        <source>Normal</source>
        <comment>DiskType</comment>
        <translation>標準</translation>
    </message>
    <message>
        <source>Immutable</source>
        <comment>DiskType</comment>
        <translation>不可變</translation>
    </message>
    <message>
        <source>Writethrough</source>
        <comment>DiskType</comment>
        <translation>直接寫入</translation>
    </message>
    <message>
        <source>Null</source>
        <comment>VRDPAuthType</comment>
        <translation>空</translation>
    </message>
    <message>
        <source>External</source>
        <comment>VRDPAuthType</comment>
        <translation>外部</translation>
    </message>
    <message>
        <source>Guest</source>
        <comment>VRDPAuthType</comment>
        <translation>客體</translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>USBFilterActionType</comment>
        <translation>忽略</translation>
    </message>
    <message>
        <source>Hold</source>
        <comment>USBFilterActionType</comment>
        <translation>保留</translation>
    </message>
    <message>
        <source>Null Audio Driver</source>
        <comment>AudioDriverType</comment>
        <translation>空的音效驅動程式</translation>
    </message>
    <message>
        <source>Windows Multimedia</source>
        <comment>AudioDriverType</comment>
        <translation>Windows 多媒體</translation>
    </message>
    <message>
        <source>OSS Audio Driver</source>
        <comment>AudioDriverType</comment>
        <translation>OSS 音效驅動程式</translation>
    </message>
    <message>
        <source>ALSA Audio Driver</source>
        <comment>AudioDriverType</comment>
        <translation>ALSA 音效驅動程式</translation>
    </message>
    <message>
        <source>Windows DirectSound</source>
        <comment>AudioDriverType</comment>
        <translation>Windows DirectSound</translation>
    </message>
    <message>
        <source>CoreAudio</source>
        <comment>AudioDriverType</comment>
        <translation>CoreAudio</translation>
    </message>
    <message>
        <source>Not attached</source>
        <comment>NetworkAttachmentType</comment>
        <translation>未附加</translation>
    </message>
    <message>
        <source>NAT</source>
        <comment>NetworkAttachmentType</comment>
        <translation>NAT</translation>
    </message>
    <message>
        <source>Internal Network</source>
        <comment>NetworkAttachmentType</comment>
        <translation>內部網路</translation>
    </message>
    <message>
        <source>Not supported</source>
        <comment>USBDeviceState</comment>
        <translation>未支援</translation>
    </message>
    <message>
        <source>Unavailable</source>
        <comment>USBDeviceState</comment>
        <translation>不可用</translation>
    </message>
    <message>
        <source>Busy</source>
        <comment>USBDeviceState</comment>
        <translation>忙碌</translation>
    </message>
    <message>
        <source>Available</source>
        <comment>USBDeviceState</comment>
        <translation>可用</translation>
    </message>
    <message>
        <source>Held</source>
        <comment>USBDeviceState</comment>
        <translation>已保留</translation>
    </message>
    <message>
        <source>Captured</source>
        <comment>USBDeviceState</comment>
        <translation>已擷取</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>ClipboardType</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Host To Guest</source>
        <comment>ClipboardType</comment>
        <translation>主機到客體</translation>
    </message>
    <message>
        <source>Guest To Host</source>
        <comment>ClipboardType</comment>
        <translation>客體到主機</translation>
    </message>
    <message>
        <source>Bidirectional</source>
        <comment>ClipboardType</comment>
        <translation>雙向</translation>
    </message>
    <message>
        <source>Port %1</source>
        <comment>details report (serial ports)</comment>
        <translation>連接埠 %1</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (serial ports)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Serial Ports</source>
        <comment>details report</comment>
        <translation>序列埠</translation>
    </message>
    <message>
        <source>USB</source>
        <comment>details report</comment>
        <translation>USB</translation>
    </message>
    <message>
        <source>Shared Folders</source>
        <comment>details report (shared folders)</comment>
        <translation>共用資料夾</translation>
    </message>
    <message>
        <source>None</source>
        <comment>details report (shared folders)</comment>
        <translation>無</translation>
    </message>
    <message>
        <source>Shared Folders</source>
        <comment>details report</comment>
        <translation>共用資料夾</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <comment>PortMode</comment>
        <translation>已中斷連接</translation>
    </message>
    <message>
        <source>Host Pipe</source>
        <comment>PortMode</comment>
        <translation>主機管線</translation>
    </message>
    <message>
        <source>Host Device</source>
        <comment>PortMode</comment>
        <translation>主機裝置</translation>
    </message>
    <message>
        <source>User-defined</source>
        <comment>serial port</comment>
        <translation>使用者定義</translation>
    </message>
    <message>
        <source>VT-x/AMD-V</source>
        <comment>details report</comment>
        <translation>VT-x/AMD-V</translation>
    </message>
    <message>
        <source>PAE/NX</source>
        <comment>details report</comment>
        <translation>PAE/NX</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (VT-x/AMD-V)</comment>
        <translation>啟用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (VT-x/AMD-V)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (PAE/NX)</comment>
        <translation>啟用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (PAE/NX)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Host Driver</source>
        <comment>details report (audio)</comment>
        <translation>主機驅動程式</translation>
    </message>
    <message>
        <source>Controller</source>
        <comment>details report (audio)</comment>
        <translation>控制器</translation>
    </message>
    <message>
        <source>Port %1</source>
        <comment>details report (parallel ports)</comment>
        <translation>連接埠 %1</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (parallel ports)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Parallel Ports</source>
        <comment>details report</comment>
        <translation>串列埠</translation>
    </message>
    <message>
        <source>USB</source>
        <comment>DeviceType</comment>
        <translation>USB</translation>
    </message>
    <message>
        <source>Shared Folder</source>
        <comment>DeviceType</comment>
        <translation>共用資料夾</translation>
    </message>
    <message>
        <source>IDE</source>
        <comment>StorageBus</comment>
        <translation>IDE</translation>
    </message>
    <message>
        <source>SATA</source>
        <comment>StorageBus</comment>
        <translation>SATA</translation>
    </message>
    <message>
        <source>Primary</source>
        <comment>StorageBusChannel</comment>
        <translation>第一個</translation>
    </message>
    <message>
        <source>Secondary</source>
        <comment>StorageBusChannel</comment>
        <translation>第二個</translation>
    </message>
    <message>
        <source>Master</source>
        <comment>StorageBusDevice</comment>
        <translation>主</translation>
    </message>
    <message>
        <source>Slave</source>
        <comment>StorageBusDevice</comment>
        <translation>副</translation>
    </message>
    <message>
        <source>Port %1</source>
        <comment>StorageBusChannel</comment>
        <translation>連接埠 %1</translation>
    </message>
    <message>
        <source>Solaris Audio</source>
        <comment>AudioDriverType</comment>
        <translation>Solaris 音效</translation>
    </message>
    <message>
        <source>PulseAudio</source>
        <comment>AudioDriverType</comment>
        <translation>PulseAudio</translation>
    </message>
    <message>
        <source>ICH AC97</source>
        <comment>AudioControllerType</comment>
        <translation>ICH AC97</translation>
    </message>
    <message>
        <source>SoundBlaster 16</source>
        <comment>AudioControllerType</comment>
        <translation>SoundBlaster 16</translation>
    </message>
    <message>
        <source>PCnet-PCI II (Am79C970A)</source>
        <comment>NetworkAdapterType</comment>
        <translation>PCnet-PCI II (Am79C970A)</translation>
    </message>
    <message>
        <source>PCnet-FAST III (Am79C973)</source>
        <comment>NetworkAdapterType</comment>
        <translation>PCnet-FAST III (Am79C973)</translation>
    </message>
    <message>
        <source>Intel PRO/1000 MT Desktop (82540EM)</source>
        <comment>NetworkAdapterType</comment>
        <translation>Intel PRO/1000 MT Desktop (82540EM)</translation>
    </message>
    <message>
        <source>Intel PRO/1000 T Server (82543GC)</source>
        <comment>NetworkAdapterType</comment>
        <translation>Intel PRO/1000 T Server (82543GC)</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Vendor ID: %1&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;供應商 ID: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Product ID: %2&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;產品 ID: %2&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Revision: %3&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;修訂: %3&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Product: %4&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;產品: %4&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Manufacturer: %5&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;製造商: %5&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Serial No.: %1&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;序號: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Port: %1&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;連接埠: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;State: %1&lt;/nobr&gt;</source>
        <comment>USB filter tooltip</comment>
        <translation>&lt;nobr&gt;狀態: %1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Adapter %1</source>
        <comment>network</comment>
        <translation>介面卡 %1</translation>
    </message>
    <message>
        <source>Checking...</source>
        <comment>medium</comment>
        <translation>正在檢查...</translation>
    </message>
    <message>
        <source>Inaccessible</source>
        <comment>medium</comment>
        <translation>無法存取</translation>
    </message>
    <message>
        <source>3D Acceleration</source>
        <comment>details report</comment>
        <translation>3D 加速</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (3D Acceleration)</comment>
        <translation>啟用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (3D Acceleration)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Setting Up</source>
        <comment>MachineState</comment>
        <translation>設定</translation>
    </message>
    <message>
        <source>Differencing</source>
        <comment>DiskType</comment>
        <translation>差異</translation>
    </message>
    <message>
        <source>Nested Paging</source>
        <comment>details report</comment>
        <translation>巢狀分頁</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (Nested Paging)</comment>
        <translation>啟用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (Nested Paging)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Internal network, &apos;%1&apos;</source>
        <comment>details report (network)</comment>
        <translation>內部網路，「%1」</translation>
    </message>
    <message>
        <source>SCSI</source>
        <comment>StorageBus</comment>
        <translation>SCSI</translation>
    </message>
    <message>
        <source>PIIX3</source>
        <comment>StorageControllerType</comment>
        <translation>PIIX3</translation>
    </message>
    <message>
        <source>PIIX4</source>
        <comment>StorageControllerType</comment>
        <translation>PIIX4</translation>
    </message>
    <message>
        <source>ICH6</source>
        <comment>StorageControllerType</comment>
        <translation>ICH6</translation>
    </message>
    <message>
        <source>AHCI</source>
        <comment>StorageControllerType</comment>
        <translation>AHCI</translation>
    </message>
    <message>
        <source>Lsilogic</source>
        <comment>StorageControllerType</comment>
        <translation>Lsilogic</translation>
    </message>
    <message>
        <source>BusLogic</source>
        <comment>StorageControllerType</comment>
        <translation>BusLogic</translation>
    </message>
    <message>
        <source>Bridged adapter, %1</source>
        <comment>details report (network)</comment>
        <translation>橋接介面卡, %1</translation>
    </message>
    <message>
        <source>Host-only adapter, &apos;%1&apos;</source>
        <comment>details report (network)</comment>
        <translation>「僅限主機」介面卡，「%1」</translation>
    </message>
    <message>
        <source>Intel PRO/1000 MT Server (82545EM)</source>
        <comment>NetworkAdapterType</comment>
        <translation>Intel PRO/1000 MT Server (82545EM)</translation>
    </message>
    <message>
        <source>Bridged Adapter</source>
        <comment>NetworkAttachmentType</comment>
        <translation>橋接介面卡</translation>
    </message>
    <message>
        <source>Host-only Adapter</source>
        <comment>NetworkAttachmentType</comment>
        <translation>「僅限主機」介面卡</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;%1 MB&lt;/nobr&gt;</source>
        <comment>details report</comment>
        <translation>&lt;nobr&gt;%1 MB&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Processor(s)</source>
        <comment>details report</comment>
        <translation>處理器</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;%1&lt;/nobr&gt;</source>
        <comment>details report</comment>
        <translation>&lt;nobr&gt;%1&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>System</source>
        <comment>details report</comment>
        <translation>系統</translation>
    </message>
    <message>
        <source>Remote Display Server Port</source>
        <comment>details report (VRDP Server)</comment>
        <translation>遠端顯示伺服器連接埠</translation>
    </message>
    <message>
        <source>Remote Display Server</source>
        <comment>details report (VRDP Server)</comment>
        <translation>遠端顯示伺服器</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (VRDP Server)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Display</source>
        <comment>details report</comment>
        <translation>顯示</translation>
    </message>
    <message>
        <source>Raw File</source>
        <comment>PortMode</comment>
        <translation>Raw 檔案</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>details report (2D Video Acceleration)</comment>
        <translation>啟用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>details report (2D Video Acceleration)</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>2D Video Acceleration</source>
        <comment>details report</comment>
        <translation>2D 視訊加速</translation>
    </message>
    <message>
        <source>Not Attached</source>
        <comment>details report (Storage)</comment>
        <translation>未附加</translation>
    </message>
    <message>
        <source>Storage</source>
        <comment>details report</comment>
        <translation>存放裝置</translation>
    </message>
    <message>
        <source>Teleported</source>
        <comment>MachineState</comment>
        <translation>已瞬間移動</translation>
    </message>
    <message>
        <source>Guru Meditation</source>
        <comment>MachineState</comment>
        <translation>Guru Meditation</translation>
    </message>
    <message>
        <source>Teleporting</source>
        <comment>MachineState</comment>
        <translation>瞬間移動</translation>
    </message>
    <message>
        <source>Taking Live Snapshot</source>
        <comment>MachineState</comment>
        <translation>取得即時快照</translation>
    </message>
    <message>
        <source>Teleporting Paused VM</source>
        <comment>MachineState</comment>
        <translation>瞬間移動已暫停虛擬機器</translation>
    </message>
    <message>
        <source>Restoring Snapshot</source>
        <comment>MachineState</comment>
        <translation>還原快照</translation>
    </message>
    <message>
        <source>Deleting Snapshot</source>
        <comment>MachineState</comment>
        <translation>刪除快照</translation>
    </message>
    <message>
        <source>Floppy</source>
        <comment>StorageBus</comment>
        <translation>軟碟</translation>
    </message>
    <message>
        <source>Device %1</source>
        <comment>StorageBusDevice</comment>
        <translation>裝置 %1</translation>
    </message>
    <message>
        <source>IDE Primary Master</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>IDE 第一個主</translation>
    </message>
    <message>
        <source>IDE Primary Slave</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>IDE 第一個副</translation>
    </message>
    <message>
        <source>IDE Secondary Master</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>IDE 第二個主</translation>
    </message>
    <message>
        <source>IDE Secondary Slave</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>IDE 第二個副</translation>
    </message>
    <message>
        <source>SATA Port %1</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>SATA 連接埠 %1</translation>
    </message>
    <message>
        <source>SCSI Port %1</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>SCSI 連接埠 %1</translation>
    </message>
    <message>
        <source>Floppy Device %1</source>
        <comment>New Storage UI : Slot Name</comment>
        <translation>軟碟裝置 %1</translation>
    </message>
    <message>
        <source>Paravirtualized Network (virtio-net)</source>
        <comment>NetworkAdapterType</comment>
        <translation>虛擬化網路 (virtio-net)</translation>
    </message>
    <message>
        <source>I82078</source>
        <comment>StorageControllerType</comment>
        <translation>I82078</translation>
    </message>
    <message>
        <source>Empty</source>
        <comment>medium</comment>
        <translation>空的</translation>
    </message>
    <message>
        <source>Host Drive &apos;%1&apos;</source>
        <comment>medium</comment>
        <translation>主機磁碟機「%1」</translation>
    </message>
    <message>
        <source>Host Drive %1 (%2)</source>
        <comment>medium</comment>
        <translation>主機磁碟機 %1 (%2)</translation>
    </message>
    <message>
        <source>&lt;p style=white-space:pre&gt;Type (Format):  %1 (%2)&lt;/p&gt;</source>
        <comment>medium</comment>
        <translation>&lt;p style=white-space:pre&gt;類型 (格式):  %1 (%2)&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Attached to:  %1&lt;/p&gt;</source>
        <comment>image</comment>
        <translation>&lt;p&gt;附加到:  %1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;i&gt;Not Attached&lt;/i&gt;</source>
        <comment>image</comment>
        <translation>&lt;i&gt;未附加&lt;/i&gt;</translation>
    </message>
    <message>
        <source>&lt;i&gt;Checking accessibility...&lt;/i&gt;</source>
        <comment>medium</comment>
        <translation>&lt;i&gt;正在檢查可存取性...&lt;/i&gt;</translation>
    </message>
    <message>
        <source>Failed to check media accessibility.</source>
        <comment>medium</comment>
        <translation>檢查媒體的可存取性失敗。</translation>
    </message>
    <message>
        <source>&lt;b&gt;No medium selected&lt;/b&gt;</source>
        <comment>medium</comment>
        <translation>&lt;b&gt;沒有選取媒體&lt;/b&gt;</translation>
    </message>
    <message>
        <source>You can also change this while the machine is running.</source>
        <translation>您也可以在機器正在執行時變更。</translation>
    </message>
    <message>
        <source>&lt;b&gt;No media available&lt;/b&gt;</source>
        <comment>medium</comment>
        <translation>&lt;b&gt;沒有可用媒體&lt;/b&gt;</translation>
    </message>
    <message>
        <source>You can create media images using the virtual media manager.</source>
        <translation>您可以使用虛擬媒體管理員建立媒體映像。</translation>
    </message>
    <message>
        <source>Attaching this hard disk will be performed indirectly using a newly created differencing hard disk.</source>
        <comment>medium</comment>
        <translation>附加這個硬碟將使用新建立的差異硬碟間接地執行。</translation>
    </message>
    <message>
        <source>Some of the media in this hard disk chain are inaccessible. Please use the Virtual Media Manager in &lt;b&gt;Show Differencing Hard Disks&lt;/b&gt; mode to inspect these media.</source>
        <comment>medium</comment>
        <translation>硬碟鏈結中某些媒體無法存取。 請使用虛擬媒體管理員在&lt;b&gt;顯示差異硬碟&lt;/b&gt;模式來檢查這些媒體。</translation>
    </message>
    <message>
        <source>This base hard disk is indirectly attached using the following differencing hard disk:</source>
        <comment>medium</comment>
        <translation>這是基礎硬碟使用以下差異硬碟間接附加:</translation>
    </message>
    <message numerus="yes">
        <source>%n year(s)</source>
        <translation type="unfinished">
            <numerusform>%n 年</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n month(s)</source>
        <translation type="unfinished">
            <numerusform>%n 月</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n day(s)</source>
        <translation type="unfinished">
            <numerusform>%n 日</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n hour(s)</source>
        <translation type="unfinished">
            <numerusform>%n 小時</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n minute(s)</source>
        <translation type="unfinished">
            <numerusform>%n 分鐘</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation type="unfinished">
            <numerusform>%n 秒</numerusform>
        </translation>
    </message>
    <message>
        <source>(CD/DVD)</source>
        <translation>(CD/DVD)</translation>
    </message>
</context>
<context>
    <name>VBoxGlobalSettings</name>
    <message>
        <source>&apos;%1 (0x%2)&apos; is an invalid host key code.</source>
        <translation>「%1 (0x%2)」是無效的主機鍵碼。</translation>
    </message>
    <message>
        <source>The value &apos;%1&apos; of the key &apos;%2&apos; doesn&apos;t match the regexp constraint &apos;%3&apos;.</source>
        <translation>按鍵「%2」的值「%1」不符合正規運算式限制「%3」。</translation>
    </message>
    <message>
        <source>Cannot delete the key &apos;%1&apos;.</source>
        <translation>無法刪除按鍵「%1」。</translation>
    </message>
</context>
<context>
    <name>VBoxHelpButton</name>
    <message>
        <source>&amp;Help</source>
        <translation>說明(&amp;H)</translation>
    </message>
</context>
<context>
    <name>VBoxImportApplianceWgt</name>
    <message>
        <source>Importing Appliance ...</source>
        <translation>正在匯入應用裝置 ...</translation>
    </message>
    <message>
        <source>Reading Appliance ...</source>
        <translation>正在讀取應用裝置 ...</translation>
    </message>
</context>
<context>
    <name>VBoxImportApplianceWzd</name>
    <message>
        <source>Select an appliance to import</source>
        <translation>選取匯入的應用裝置</translation>
    </message>
    <message>
        <source>Open Virtualization Format (%1)</source>
        <translation>開放虛擬化格式 (%1)</translation>
    </message>
    <message>
        <source>Appliance Import Wizard</source>
        <translation>應用裝置匯入精靈</translation>
    </message>
    <message>
        <source>Welcome to the Appliance Import Wizard!</source>
        <translation>歡迎使用應用裝置匯入精靈!</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This wizard will guide you through importing an appliance. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Use the &lt;span style=&quot; font-weight:600;&quot;&gt;Next&lt;/span&gt; button to go the next page of the wizard and the &lt;span style=&quot; font-weight:600;&quot;&gt;Back&lt;/span&gt; button to return to the previous page.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;VirtualBox currently supports importing appliances saved in the Open Virtualization Format (OVF). To continue, select the file to import below:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;這個精靈將導引您匯入應用裝置。 &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;使用 [&lt;span style=&quot; font-weight:600;&quot;&gt;下一步&lt;/span&gt;] 按鈕前往精靈的下一頁與 [&lt;span style=&quot; font-weight:600;&quot;&gt;上一步&lt;/span&gt;] 按鈕返回上一頁。&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; VirtualBox 目前支援匯入以 開放虛擬化格式 (OVF) 儲存的應用裝置。 要繼續，請選取以下要匯入的檔案:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; 上一步(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>下一步(&amp;N) &gt;</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Appliance Import Settings</source>
        <translation>應用裝置匯入設定值</translation>
    </message>
    <message>
        <source>These are the virtual machines contained in the appliance and the suggested settings of the imported VirtualBox machines. You can change many of the properties shown by double-clicking on the items and disable others using the check boxes below.</source>
        <translation>這些是應用裝置中包含的虛擬機器與匯入 VirtualBox 機器的建議設定值。 您可以連按兩下項目變更許多顯示的內容，並使用以下核取方塊停用其它。</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>還原預設值</translation>
    </message>
    <message>
        <source>&amp;Import &gt;</source>
        <translation>匯入(&amp;I) &gt;</translation>
    </message>
</context>
<context>
    <name>VBoxImportLicenseViewer</name>
    <message>
        <source>&lt;b&gt;The virtual system &quot;%1&quot; requires that you agree to the terms and conditions of the software license agreement shown below.&lt;/b&gt;&lt;br /&gt;&lt;br /&gt;Click &lt;b&gt;Agree&lt;/b&gt; to continue or click &lt;b&gt;Disagree&lt;/b&gt; to cancel the import.</source>
        <translation>&lt;b&gt;虛擬系統「%1」需要您同意以下顯示的軟體授權合約之條款與條件。&lt;/b&gt;&lt;br /&gt;&lt;br /&gt;按一下 [&lt;b&gt;同意&lt;/b&gt;] 繼續或按一下 [&lt;b&gt;不同意&lt;/b&gt;] 取消匯入。</translation>
    </message>
    <message>
        <source>Software License Agreement</source>
        <translation>軟體授權合約</translation>
    </message>
    <message>
        <source>&amp;Disagree</source>
        <translation>不同意(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Agree</source>
        <translation>同意(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation>列印(&amp;P)...</translation>
    </message>
    <message>
        <source>&amp;Save...</source>
        <translation>儲存(&amp;S)...</translation>
    </message>
    <message>
        <source>Text (*.txt)</source>
        <translation>文字 (*.txt)</translation>
    </message>
    <message>
        <source>Save license to file...</source>
        <translation>儲存授權為檔案...</translation>
    </message>
</context>
<context>
    <name>VBoxLicenseViewer</name>
    <message>
        <source>VirtualBox License</source>
        <translation>VirtualBox 授權</translation>
    </message>
    <message>
        <source>I &amp;Agree</source>
        <translation>我同意(&amp;A)</translation>
    </message>
    <message>
        <source>I &amp;Disagree</source>
        <translation>我不同意(&amp;D)</translation>
    </message>
</context>
<context>
    <name>VBoxLineTextEdit</name>
    <message>
        <source>&amp;Edit</source>
        <translation>編輯(&amp;E)</translation>
    </message>
</context>
<context>
    <name>VBoxLogSearchPanel</name>
    <message>
        <source>Close the search panel</source>
        <translation>關閉搜尋面板</translation>
    </message>
    <message>
        <source>Find </source>
        <translation>尋找</translation>
    </message>
    <message>
        <source>Enter a search string here</source>
        <translation>在此輸入搜尋字串</translation>
    </message>
    <message>
        <source>&amp;Previous</source>
        <translation>上一個(&amp;P)</translation>
    </message>
    <message>
        <source>Search for the previous occurrence of the string</source>
        <translation>搜尋上一個出現的字串</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation>下一個(&amp;N)</translation>
    </message>
    <message>
        <source>Search for the next occurrence of the string</source>
        <translation>搜尋下一個出現的字串</translation>
    </message>
    <message>
        <source>C&amp;ase Sensitive</source>
        <translation>區分大小寫(&amp;A)</translation>
    </message>
    <message>
        <source>Perform case sensitive search (when checked)</source>
        <translation>執行區分大小寫的搜尋 (勾選時)</translation>
    </message>
    <message>
        <source>String not found</source>
        <translation>找不到字串</translation>
    </message>
</context>
<context>
    <name>VBoxMediaManagerDlg</name>
    <message>
        <source>&amp;Actions</source>
        <translation>動作(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation>新增(&amp;N)...</translation>
    </message>
    <message>
        <source>&amp;Add...</source>
        <translation>加入(&amp;A)...</translation>
    </message>
    <message>
        <source>R&amp;emove</source>
        <translation>移除(&amp;E)</translation>
    </message>
    <message>
        <source>Re&amp;lease</source>
        <translation>釋放(&amp;L)</translation>
    </message>
    <message>
        <source>Re&amp;fresh</source>
        <translation>重新整理(&amp;F)</translation>
    </message>
    <message>
        <source>Create a new virtual hard disk</source>
        <translation>新建虛擬硬碟</translation>
    </message>
    <message>
        <source>Add an existing medium</source>
        <translation>加入現有的媒體</translation>
    </message>
    <message>
        <source>Remove the selected medium</source>
        <translation>移除選取的媒體</translation>
    </message>
    <message>
        <source>Release the selected medium by detaching it from the machines</source>
        <translation>從機器分離它來釋放選取的媒體</translation>
    </message>
    <message>
        <source>Refresh the media list</source>
        <translation>重新整理媒體清單</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <source>Type (Format)</source>
        <translation>類型 (格式)</translation>
    </message>
    <message>
        <source>Attached to</source>
        <translation>附加到</translation>
    </message>
    <message>
        <source>Checking accessibility</source>
        <translation>檢查可存取性</translation>
    </message>
    <message>
        <source>&amp;Select</source>
        <translation>選取(&amp;S)</translation>
    </message>
    <message>
        <source>All hard disk images (%1)</source>
        <translation>所有硬碟映像 (%1)</translation>
    </message>
    <message>
        <source>All files (*)</source>
        <translation>所有檔案 (*)</translation>
    </message>
    <message>
        <source>Select a hard disk image file</source>
        <translation>選取一個硬碟映像檔</translation>
    </message>
    <message>
        <source>CD/DVD-ROM images (*.iso);;All files (*)</source>
        <translation>CD/DVD-ROM 映像 (*.iso);;所有檔案 (*)</translation>
    </message>
    <message>
        <source>Select a CD/DVD-ROM disk image file</source>
        <translation>選取一個 CD/DVD-ROM 光碟映像檔</translation>
    </message>
    <message>
        <source>Floppy images (*.img);;All files (*)</source>
        <translation>軟碟映像 (*.img);;所有檔案 (*)</translation>
    </message>
    <message>
        <source>Select a floppy disk image file</source>
        <translation>選取一個軟碟映像檔</translation>
    </message>
    <message>
        <source>&lt;i&gt;Not&amp;nbsp;Attached&lt;/i&gt;</source>
        <translation>&lt;i&gt;未附加&lt;/i&gt;</translation>
    </message>
    <message>
        <source>--</source>
        <comment>no info</comment>
        <translation>--</translation>
    </message>
    <message>
        <source>Virtual Media Manager</source>
        <translation>虛擬媒體管理員</translation>
    </message>
    <message>
        <source>Hard &amp;Disks</source>
        <translation>硬碟(&amp;D)</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <source>Virtual Size</source>
        <translation>虛擬大小</translation>
    </message>
    <message>
        <source>Actual Size</source>
        <translation>實際大小</translation>
    </message>
    <message>
        <source>&amp;CD/DVD Images</source>
        <translation>CD/DVD 映像(&amp;C)</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <source>&amp;Floppy Images</source>
        <translation>軟碟映像(&amp;F)</translation>
    </message>
    <message>
        <source>Attached to</source>
        <comment>VMM: Virtual Disk</comment>
        <translation>附加到</translation>
    </message>
    <message>
        <source>Attached to</source>
        <comment>VMM: CD/DVD Image</comment>
        <translation>附加到</translation>
    </message>
    <message>
        <source>Attached to</source>
        <comment>VMM: Floppy Image</comment>
        <translation>附加到</translation>
    </message>
</context>
<context>
    <name>VBoxMiniToolBar</name>
    <message>
        <source>Always show the toolbar</source>
        <translation>始終顯示工具列</translation>
    </message>
    <message>
        <source>Exit Full Screen or Seamless Mode</source>
        <translation>結束全螢幕或無縫模式</translation>
    </message>
    <message>
        <source>Close VM</source>
        <translation>關閉 VM</translation>
    </message>
</context>
<context>
    <name>VBoxNetworkDialog</name>
    <message>
        <source>Network Adapters</source>
        <translation>網路卡</translation>
    </message>
</context>
<context>
    <name>VBoxNewHDWzd</name>
    <message>
        <source>Create New Virtual Disk</source>
        <translation>新建虛擬磁碟</translation>
    </message>
    <message>
        <source>Welcome to the Create New Virtual Disk Wizard!</source>
        <translation>歡迎使用「新建虛擬磁碟精靈」!</translation>
    </message>
    <message>
        <source>Virtual Disk Location and Size</source>
        <translation>虛擬磁碟位置與大小</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>摘要</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;%1 Bytes&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;%1 位元組&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Hard disk images (*.vdi)</source>
        <translation>硬碟映像 (*.vdi)</translation>
    </message>
    <message>
        <source>Select a file for the new hard disk image file</source>
        <translation>選擇一個做為新硬碟映像檔的檔案</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; 上一步(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>下一步(&amp;N) &gt;</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation>完成(&amp;F)</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>summary</comment>
        <translation>類型</translation>
    </message>
    <message>
        <source>Location</source>
        <comment>summary</comment>
        <translation>位置</translation>
    </message>
    <message>
        <source>Size</source>
        <comment>summary</comment>
        <translation>大小</translation>
    </message>
    <message>
        <source>Bytes</source>
        <comment>summary</comment>
        <translation>Bytes</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>&lt;p&gt;This wizard will help you to create a new virtual hard disk for your virtual machine.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Next&lt;/b&gt; button to go to the next page of the wizard and the &lt;b&gt;Back&lt;/b&gt; button to return to the previous page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;這個精靈將協助您新建虛擬機器的虛擬硬碟。&lt;/p&gt;&lt;p&gt;使用 [&lt;b&gt;下一步&lt;/b&gt;] 按鈕前往精靈的下一頁和 [&lt;b&gt;上一步&lt;/b&gt;] 按鈕返回上一頁。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Hard Disk Storage Type</source>
        <translation>硬碟存放類型</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the type of virtual hard disk you want to create.&lt;/p&gt;&lt;p&gt;A &lt;b&gt;dynamically expanding storage&lt;/b&gt; initially occupies a very small amount of space on your physical hard disk. It will grow dynamically (up to the size specified) as the Guest OS claims disk space.&lt;/p&gt;&lt;p&gt;A &lt;b&gt;fixed-size storage&lt;/b&gt; does not grow. It is stored in a file of approximately the same size as the size of the virtual hard disk. The creation of a fixed-size storage may take a long time depending on the storage size and the write performance of your harddisk.&lt;/p&gt;</source>
        <translation>&lt;p&gt;選擇您要建立的虛擬硬碟類型。 &lt;/p&gt;&lt;p&gt;&lt;b&gt;動態延伸存放&lt;/b&gt;最初在您的實體硬碟佔用非常少的空間。 它將會動態成長 (直到指定的大小) 作為客體作業系統索取的磁碟空間。 &lt;/p&gt;&lt;p&gt;&lt;b&gt;固定大小存放&lt;/b&gt;不會成長。 它儲存在與虛擬硬碟大小大約相同的檔案大小。 固定大小存放可能需要長時間的建立，取決於存放大小與您硬碟的寫入性能。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Storage Type</source>
        <translation>存放裝置類型</translation>
    </message>
    <message>
        <source>&amp;Dynamically expanding storage</source>
        <translation>動態延伸存放(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Fixed-size storage</source>
        <translation>固定大小存放(&amp;F)</translation>
    </message>
    <message>
        <source>&lt;p&gt;Press the &lt;b&gt;Select&lt;/b&gt; button to select the location of a file to store the hard disk data or type a file name in the entry field.&lt;/p&gt;</source>
        <translation>&lt;p&gt;按下 [&lt;b&gt;選取&lt;/b&gt;] 按鈕選取儲存硬碟資料的檔案位置或在欄位中輸入檔案名稱。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Location</source>
        <translation>位置(&amp;L)</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the size of the virtual hard disk in megabytes. This size will be reported to the Guest OS as the maximum size of this hard disk.&lt;/p&gt;</source>
        <translation>&lt;p&gt;以 MB 為單位選取虛擬硬碟的大小。這個大小將回報至客體作業系統作為硬碟的最大容量。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation>大小(&amp;S)</translation>
    </message>
    <message>
        <source>You are going to create a new virtual hard disk with the following parameters:</source>
        <translation>您即將使用以下參數新建虛擬硬碟:</translation>
    </message>
    <message>
        <source>If the above settings are correct, press the &lt;b&gt;Finish&lt;/b&gt; button. Once you press it, a new hard disk will be created.</source>
        <translation>如果上方設定值正確，按下 [&lt;b&gt;完成&lt;/b&gt;] 按鈕。 一旦您按下後，將建立新硬碟。</translation>
    </message>
</context>
<context>
    <name>VBoxNewVMWzd</name>
    <message>
        <source>Create New Virtual Machine</source>
        <translation>新建虛擬機器</translation>
    </message>
    <message>
        <source>Welcome to the New Virtual Machine Wizard!</source>
        <translation>歡迎使用「新增虛擬機器精靈」!</translation>
    </message>
    <message>
        <source>N&amp;ame</source>
        <translation>名稱(&amp;A)</translation>
    </message>
    <message>
        <source>OS &amp;Type</source>
        <translation>作業系統類型(&amp;T)</translation>
    </message>
    <message>
        <source>VM Name and OS Type</source>
        <translation>虛擬機器名稱和作業系統類型</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the amount of base memory (RAM) in megabytes to be allocated to the virtual machine.&lt;/p&gt;</source>
        <translation>&lt;p&gt;以 MB 為單位選取配置於虛擬機器的基本記憶體 (RAM) 數量&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Base &amp;Memory Size</source>
        <translation>基本記憶體大小(&amp;M)</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation>記憶體</translation>
    </message>
    <message>
        <source>Virtual Hard Disk</source>
        <translation>虛擬硬碟</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>摘要</translation>
    </message>
    <message>
        <source>The recommended base memory size is &lt;b&gt;%1&lt;/b&gt; MB.</source>
        <translation>建議的基本記憶體大小為 &lt;b&gt;%1&lt;/b&gt; MB。</translation>
    </message>
    <message>
        <source>The recommended size of the boot hard disk is &lt;b&gt;%1&lt;/b&gt; MB.</source>
        <translation>建議的開機硬碟大小為 &lt;b&gt;%1&lt;/b&gt; MB。</translation>
    </message>
    <message>
        <source>&lt;p&gt;This wizard will guide you through the steps that are necessary to create a new virtual machine for VirtualBox.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Next&lt;/b&gt; button to go the next page of the wizard and the &lt;b&gt;Back&lt;/b&gt; button to return to the previous page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;這個精靈將指導您逐步完成建立一個新的 VirtualBox 虛擬機器所需的步驟。&lt;/p&gt;&lt;p&gt;使用 [&lt;b&gt;下一步&lt;/b&gt;] 按鈕前往精靈的下一頁和 [&lt;b&gt;上一步&lt;/b&gt;] 按鈕返回上一頁。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; 上一步(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>下一步(&amp;N) &gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Enter a name for the new virtual machine and select the type of the guest operating system you plan to install onto the virtual machine.&lt;/p&gt;&lt;p&gt;The name of the virtual machine usually indicates its software and hardware configuration. It will be used by all VirtualBox components to identify your virtual machine.&lt;/p&gt;</source>
        <translation>&lt;p&gt;輸入新的虛擬機器名稱並選取您計畫安裝到虛擬機器的客體作業系統類型。 &lt;/p&gt;&lt;p&gt;虛擬機器的名稱通常指示它的軟體與硬體組態。 所有的 VirtualBox 元件將使用它來識別您的虛擬機器。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;You are going to create a new virtual machine with the following parameters:&lt;/p&gt;</source>
        <translation>&lt;p&gt;您即將使用以下參數新建虛擬機器:&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;If the above is correct press the &lt;b&gt;Finish&lt;/b&gt; button. Once you press it, a new virtual machine will be created. &lt;/p&gt;&lt;p&gt;Note that you can alter these and all other setting of the created virtual machine at any time using the &lt;b&gt;Settings&lt;/b&gt; dialog accessible through the menu of the main window.&lt;/p&gt;</source>
        <translation>&lt;p&gt;如果以上正確按下 [&lt;b&gt;完成&lt;/b&gt;] 按鈕。 一旦您按下後，將新建虛擬機器。 &lt;/p&gt;&lt;p&gt;請注意您可以通過主視窗的功能表存取 &lt;b&gt;設定值&lt;/b&gt; 對話方塊，隨時變更建立虛擬機器的這些和所有其它設定值。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation>完成(&amp;F)</translation>
    </message>
    <message>
        <source>MB</source>
        <comment>megabytes</comment>
        <translation>MB</translation>
    </message>
    <message>
        <source>Name</source>
        <comment>summary</comment>
        <translation>名稱</translation>
    </message>
    <message>
        <source>OS Type</source>
        <comment>summary</comment>
        <translation>作業系統類型</translation>
    </message>
    <message>
        <source>Base Memory</source>
        <comment>summary</comment>
        <translation>基本記憶體</translation>
    </message>
    <message>
        <source>Boot Hard Disk</source>
        <comment>summary</comment>
        <translation>開機硬碟</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select a hard disk image to be used as the boot hard disk of the virtual machine. You can either create a new hard disk using the &lt;b&gt;New&lt;/b&gt; button or select an existing hard disk image from the drop-down list or by pressing the &lt;b&gt;Existing&lt;/b&gt; button (to invoke the Virtual Media Manager dialog).&lt;/p&gt;&lt;p&gt;If you need a more complicated hard disk setup, you can also skip this step and attach hard disks later using the VM Settings dialog.&lt;/p&gt;</source>
        <translation>&lt;p&gt;選取要作為虛擬機器開機硬碟的硬碟映像。 您可以使用 [&lt;b&gt;新增&lt;/b&gt;] 按鈕新建硬碟，或者從下拉清單選取現有的硬碟映像或者按下 [&lt;b&gt;現有&lt;/b&gt;] 按鈕 (使用虛擬媒體管理員對話方塊) 。&lt;/p&gt;&lt;p&gt;如果您需要更複雜的硬碟設定，您也可以略過這個步驟並使用 VM 設定值對話方塊稍後附加硬碟。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Boot Hard &amp;Disk (Primary Master)</source>
        <translation>開機硬碟 (第一個主)(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Create new hard disk</source>
        <translation>建立新硬碟(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Use existing hard disk</source>
        <translation>使用現有硬碟(&amp;U)</translation>
    </message>
</context>
<context>
    <name>VBoxOSTypeSelectorWidget</name>
    <message>
        <source>Operating &amp;System:</source>
        <translation>作業系統(&amp;S):</translation>
    </message>
    <message>
        <source>Displays the operating system family that you plan to install into this virtual machine.</source>
        <translation>顯示您計畫要安裝於這個虛擬機器的作業系統家族。</translation>
    </message>
    <message>
        <source>Displays the operating system type that you plan to install into this virtual machine (called a guest operating system).</source>
        <translation>顯示您計畫要安裝於這個虛擬機器的作業系統類型 (稱之為客體作業系統)。</translation>
    </message>
    <message>
        <source>&amp;Version:</source>
        <translation>版本(&amp;V):</translation>
    </message>
</context>
<context>
    <name>VBoxProblemReporter</name>
    <message>
        <source>VirtualBox - Information</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - 資訊</translation>
    </message>
    <message>
        <source>VirtualBox - Question</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - 問題</translation>
    </message>
    <message>
        <source>VirtualBox - Warning</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - 警告</translation>
    </message>
    <message>
        <source>VirtualBox - Error</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - 錯誤</translation>
    </message>
    <message>
        <source>VirtualBox - Critical Error</source>
        <comment>msg box title</comment>
        <translation>VirtualBox - 嚴重錯誤</translation>
    </message>
    <message>
        <source>Do not show this message again</source>
        <comment>msg box flag</comment>
        <translation>不再顯示這個訊息</translation>
    </message>
    <message>
        <source>Failed to open &lt;tt&gt;%1&lt;/tt&gt;. Make sure your desktop environment can properly handle URLs of this type.</source>
        <translation>開啟 &lt;tt&gt;%1&lt;/tt&gt; 時失敗。請確定您的桌面環境可以適當地處理此類型的 URL。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to initialize COM or to find the VirtualBox COM server. Most likely, the VirtualBox server is not running or failed to start.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;初始化 COM 或尋找 VirtualBox COM 伺服器時失敗。很有可能，VirtualBox 伺服器未執作或是啟動失敗。&lt;/p&gt;&lt;p&gt;應用程式現在將終止。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to create the VirtualBox COM object.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;建立 VirtualBox COM 物件時失敗。&lt;/p&gt;&lt;p&gt;應用程式將會結束。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to set global VirtualBox properties.</source>
        <translation>設定全域 VirtualBox 內容時失敗。</translation>
    </message>
    <message>
        <source>Failed to access the USB subsystem.</source>
        <translation>存取 USB 子系統時失敗。</translation>
    </message>
    <message>
        <source>Failed to create a new virtual machine.</source>
        <translation>新建虛擬機器時失敗。</translation>
    </message>
    <message>
        <source>Failed to create a new virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>新建虛擬機器 &lt;b&gt;%1&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to apply the settings to the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>套用虛擬機器 &lt;b&gt;%1&lt;/b&gt; 設定值時失敗。</translation>
    </message>
    <message>
        <source>Failed to start the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>啟動虛擬機器 &lt;b&gt;%1&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to pause the execution of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>暫停虛擬機器 &lt;b&gt;%1&lt;/b&gt; 的執行時失敗。</translation>
    </message>
    <message>
        <source>Failed to resume the execution of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>繼續執行虛擬機器 &lt;b&gt;%1&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to save the state of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>儲存虛擬機器 &lt;b&gt;%1&lt;/b&gt; 的狀態時失敗。</translation>
    </message>
    <message>
        <source>Failed to create a snapshot of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>建立虛擬機器 &lt;b&gt;%1&lt;/b&gt; 的快照時失敗。</translation>
    </message>
    <message>
        <source>Failed to stop the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>停止虛擬機器 &lt;b&gt;%1&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to remove the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>移除虛擬機器 &lt;b&gt;%1&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to discard the saved state of the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>捨棄已儲存的虛擬機器 &lt;b&gt;%1&lt;/b&gt; 狀態時失敗。</translation>
    </message>
    <message>
        <source>There is no virtual machine named &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>沒有名為 &lt;b&gt;%1&lt;/b&gt; 的虛擬機器。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to permanently delete the virtual machine &lt;b&gt;%1&lt;/b&gt;?&lt;/p&gt;&lt;p&gt;This operation cannot be undone.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您確定要永久刪除虛擬機器 &lt;b&gt;%1&lt;/b&gt;嗎? &lt;/p&gt;&lt;p&gt;這個操作無法復原。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to unregister the inaccessible virtual machine &lt;b&gt;%1&lt;/b&gt;?&lt;/p&gt;&lt;p&gt;You will not be able to register it again from GUI.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您確定要取消登錄無法存取的虛擬機器 &lt;b&gt;%1&lt;/b&gt; 嗎?&lt;/p&gt;&lt;p&gt;您將無法從 GUI 再次登錄它。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to discard the saved state of the virtual machine &lt;b&gt;%1&lt;/b&gt;?&lt;/p&gt;&lt;p&gt;This operation is equivalent to resetting or powering off the machine without doing a proper shutdown of the guest OS.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您確定要放棄虛擬機器 &lt;b&gt;%1&lt;/b&gt; 的已儲存狀態嗎?&lt;/p&gt;&lt;p&gt;這個操作相當於重設或關閉機器而不使用客體作業系統的正常關機。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to create a new session.</source>
        <translation>新建工作階段時失敗。</translation>
    </message>
    <message>
        <source>Failed to open a session for the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>開啟虛擬機器 &lt;b&gt;%1&lt;/b&gt; 的執行階段時失敗。</translation>
    </message>
    <message>
        <source>Failed to remove the host network interface &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>移除主機網路介面 &lt;b&gt;%1&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to attach the USB device &lt;b&gt;%1&lt;/b&gt; to the virtual machine &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>附加 USB 裝置 &lt;b&gt;%1&lt;/b&gt; 到虛擬機器 &lt;b&gt;%2&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to detach the USB device &lt;b&gt;%1&lt;/b&gt; from the virtual machine &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>從虛擬機器 &lt;b&gt;%2&lt;/b&gt; 分離 USB 裝置 &lt;b&gt;%1&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to create the shared folder &lt;b&gt;%1&lt;/b&gt; (pointing to &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) for the virtual machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>建立虛擬機器 &lt;b&gt;%3&lt;/b&gt; 的共用資料夾 &lt;b&gt;%1&lt;/b&gt; (指向 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) 失敗。</translation>
    </message>
    <message>
        <source>Failed to remove the shared folder &lt;b&gt;%1&lt;/b&gt; (pointing to &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) from the virtual machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>從虛擬機器 &lt;b&gt;%3&lt;/b&gt; 移除共用資料夾 &lt;b&gt;%1&lt;/b&gt; (指向 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) 時失敗。</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Virtual Machine reports that the guest OS does not support &lt;b&gt;mouse pointer integration&lt;/b&gt; in the current video mode. You need to capture the mouse (by clicking over the VM display or pressing the host key) in order to use the mouse inside the guest OS.&lt;/p&gt;</source>
        <translation>&lt;p&gt;虛擬機器回報在目前的視訊模式中，客體作業系統不支援&lt;b&gt;滑鼠指標整合&lt;/b&gt;。 為了在客體作業系統之內使用滑鼠，您需要擷取滑鼠 (在虛擬機器顯示上方按一下或按下 Host 鍵) 。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Virtual Machine is currently in the &lt;b&gt;Paused&lt;/b&gt; state and not able to see any keyboard or mouse input. If you want to continue to work inside the VM, you need to resume it by selecting the corresponding action from the menu bar.&lt;/p&gt;</source>
        <translation>&lt;p&gt;虛擬機器目前在&lt;b&gt;暫停&lt;/b&gt;狀態且無法看到任何鍵盤或滑鼠輸入。 如果您要繼續在 VM 中工作，您需要從功能表列選取相應動作來繼續。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Cannot run VirtualBox in &lt;i&gt;VM Selector&lt;/i&gt; mode due to local restrictions.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;由於本機限制，無法在 &lt;i&gt;VM Selector&lt;/i&gt; 模式中執行 VirtualBox。&lt;/p&gt;&lt;p&gt;應用程式現在將終止。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Fatal Error&lt;/nobr&gt;</source>
        <comment>runtime error info</comment>
        <translation>&lt;nobr&gt;嚴重錯誤&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Non-Fatal Error&lt;/nobr&gt;</source>
        <comment>runtime error info</comment>
        <translation>&lt;nobr&gt;非嚴重錯誤&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Warning&lt;/nobr&gt;</source>
        <comment>runtime error info</comment>
        <translation>&lt;nobr&gt;警告&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Error ID: &lt;/nobr&gt;</source>
        <comment>runtime error info</comment>
        <translation>&lt;nobr&gt;錯誤 ID: &lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Severity: </source>
        <comment>runtime error info</comment>
        <translation>嚴重性:</translation>
    </message>
    <message>
        <source>&lt;p&gt;A fatal error has occurred during virtual machine execution! The virtual machine will be powered off. Please copy the following error message using the clipboard to help diagnose the problem:&lt;/p&gt;</source>
        <translation>&lt;p&gt;虛擬機器執行期間發生嚴重錯誤! 虛擬機器將關機。 請使用剪貼簿複製以下錯誤訊息以協助診斷問題:&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;An error has occurred during virtual machine execution! The error details are shown below. You may try to correct the error and resume the virtual machine execution.&lt;/p&gt;</source>
        <translation>&lt;p&gt;虛擬機器執行期間發生錯誤! 錯誤詳細資料如下方所示。 您可以嘗試修正錯誤並繼續執行虛擬機器。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The virtual machine execution may run into an error condition as described below. We suggest that you take an appropriate action to avert the error.&lt;/p&gt;</source>
        <translation>&lt;p&gt;虛擬機器的執行可能會遇到如下所述的錯誤情況。 我們建議您採取適當的動作，以避免錯誤。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Result&amp;nbsp;Code: </source>
        <comment>error info</comment>
        <translation>結果碼:</translation>
    </message>
    <message>
        <source>Component: </source>
        <comment>error info</comment>
        <translation>元件:</translation>
    </message>
    <message>
        <source>Interface: </source>
        <comment>error info</comment>
        <translation>介面:</translation>
    </message>
    <message>
        <source>Callee: </source>
        <comment>error info</comment>
        <translation>被呼叫端:</translation>
    </message>
    <message>
        <source>Callee&amp;nbsp;RC: </source>
        <comment>error info</comment>
        <translation>被呼叫端&amp;nbsp;RC:</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not find a language file for the language &lt;b&gt;%1&lt;/b&gt; in the directory &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;The language will be temporarily reset to the system default language. Please go to the &lt;b&gt;Preferences&lt;/b&gt; dialog which you can open from the &lt;b&gt;File&lt;/b&gt; menu of the main VirtualBox window, and select one of the existing languages on the &lt;b&gt;Language&lt;/b&gt; page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;目錄 &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; 中找不到語言 &lt;b&gt;%1&lt;/b&gt; 的語言檔案。&lt;/p&gt;&lt;p&gt;語言將會暫時重設為系統的預設語言。 請前往 VirtualBox 主視窗 &lt;b&gt;檔案&lt;/b&gt; 功能表開啟 &lt;b&gt;喜好設定&lt;/b&gt; 對話方塊，並且在 &lt;b&gt;語言&lt;/b&gt; 頁面選取現有的語言之一。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not load the language file &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt;. &lt;p&gt;The language will be temporarily reset to English (built-in). Please go to the &lt;b&gt;Preferences&lt;/b&gt; dialog which you can open from the &lt;b&gt;File&lt;/b&gt; menu of the main VirtualBox window, and select one of the existing languages on the &lt;b&gt;Language&lt;/b&gt; page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;無法載入語言檔 &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt;。 &lt;p&gt;語言將會暫時重設為英文 (內建)。 請前往 VirtualBox 主視窗 &lt;b&gt;檔案&lt;/b&gt; 功能表開啟 &lt;b&gt;喜好設定&lt;/b&gt; 對話方塊，並且在 &lt;b&gt;語言&lt;/b&gt; 頁面選取現有的語言之一。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The VirtualBox Guest Additions installed in the Guest OS are too old: the installed version is %1, the expected version is %2. Some features that require Guest Additions (mouse integration, guest display auto-resize) will most likely stop working properly.&lt;/p&gt;&lt;p&gt;Please update the Guest Additions to the current version by choosing &lt;b&gt;Install Guest Additions&lt;/b&gt; from the &lt;b&gt;Devices&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;安裝在客體作業系統的 VirtualBox Guest Additions 太舊: 安裝版本為 %1，預期版本為 %2。 某些功能需要 Guest Additions (滑鼠整合、自動調整客體顯示大小) 很可能會停止正常工作。&lt;/p&gt;&lt;p&gt;請從&lt;b&gt;裝置&lt;/b&gt;功能表選擇&lt;b&gt;安裝 Guest Additions &lt;/b&gt;來更新 Guest Additions 為目前版本。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The VirtualBox Guest Additions installed in the Guest OS are outdated: the installed version is %1, the expected version is %2. Some features that require Guest Additions (mouse integration, guest display auto-resize) may not work as expected.&lt;/p&gt;&lt;p&gt;It is recommended to update the Guest Additions to the current version  by choosing &lt;b&gt;Install Guest Additions&lt;/b&gt; from the &lt;b&gt;Devices&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;安裝在客體作業系統的 VirtualBox Guest Additions 已過期: 安裝版本為 %1，預期版本為 %2。 某些功能需要 Guest Additions (滑鼠整合、自動調整客體顯示大小) 可能無法如預期的工作。&lt;/p&gt;&lt;p&gt;建議從&lt;b&gt;裝置&lt;/b&gt;功能表選擇&lt;b&gt;安裝 Guest Additions &lt;/b&gt;來更新 Guest Additions 為目前版本。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The VirtualBox Guest Additions installed in the Guest OS are too recent for this version of VirtualBox: the installed version is %1, the expected version is %2.&lt;/p&gt;&lt;p&gt;Using a newer version of Additions with an older version of VirtualBox is not supported. Please install the current version of the Guest Additions by choosing &lt;b&gt;Install Guest Additions&lt;/b&gt; from the &lt;b&gt;Devices&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;安裝在客體作業系統的 VirtualBox Guest Additions 是 VirtualBox 的最近版本: 安裝版本為 %1，預期版本為 %2。 不支援使用 Guest Additions 的較新版本連同 VirtualBox 的較舊版本。 請從&lt;b&gt;裝置&lt;/b&gt;功能表選擇&lt;b&gt;安裝 Guest Additions &lt;/b&gt;來安裝 Guest Additions 的目前版本。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to change the snapshot folder path of the virtual machine &lt;b&gt;%1&lt;b&gt; to &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>將虛擬機器的快照資料夾路徑從 &lt;b&gt;%1&lt;b&gt; 變更到 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; 時失敗。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to remove the shared folder &lt;b&gt;%1&lt;/b&gt; (pointing to &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) from the virtual machine &lt;b&gt;%3&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Please close all programs in the guest OS that may be using this shared folder and try again.&lt;/p&gt;</source>
        <translation>&lt;p&gt;從虛擬機器 &lt;b&gt;%3&lt;/b&gt; 移除共用資料夾 &lt;b&gt;%1&lt;/b&gt; (指到 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;) 時失敗。&lt;/p&gt;&lt;p&gt;請關閉客體作業系統中所有可能會用到這個共用資料夾的程式，並且再試一次。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not find the VirtualBox Guest Additions CD image file &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; or &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;.&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;Do you wish to download this CD image from the Internet?&lt;/p&gt;</source>
        <translation>&lt;p&gt;找不到 VirtualBox Guest Additions CD 映像檔案 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; 或 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;。&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;您要從網際網路下載這個 CD 映像嗎?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to download the VirtualBox Guest Additions CD image from &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;.&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;%3&lt;/p&gt;</source>
        <translation>&lt;p&gt;從 &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; 下載 VirtualBox Guest Additions 光碟映像失敗。&lt;/p&gt;&lt;p&gt;%3&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to download the VirtualBox Guest Additions CD image from &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; (size %3 bytes)?&lt;/p&gt;</source>
        <translation>&lt;p&gt;您確定要從 &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; 下載 VirtualBox Guest Additions 光碟映像 (大小 %3 位元組) 嗎? &lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The VirtualBox Guest Additions CD image has been successfully downloaded from &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; and saved locally as &lt;nobr&gt;&lt;b&gt;%3&lt;/b&gt;.&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;Do you wish to register this CD image and mount it on the virtual CD/DVD drive?&lt;/p&gt;</source>
        <translation>&lt;p&gt;VirtualBox Guest Additions CD 映像已成功從 &lt;nobr&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/nobr&gt; 下載並本機儲存為 &lt;nobr&gt;&lt;b&gt;%3&lt;/b&gt;。&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;您要登錄這個 CD 映像並將它掛載在虛擬 CD/DVD 光碟機嗎?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The virtual machine window is optimized to work in &lt;b&gt;%1&amp;nbsp;bit&lt;/b&gt; color mode but the virtual display is currently set to &lt;b&gt;%2&amp;nbsp;bit&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Please open the display properties dialog of the guest OS and select a &lt;b&gt;%3&amp;nbsp;bit&lt;/b&gt; color mode, if it is available, for best possible performance of the virtual video subsystem.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Note&lt;/b&gt;. Some operating systems, like OS/2, may actually work in 32&amp;nbsp;bit mode but report it as 24&amp;nbsp;bit (16 million colors). You may try to select a different color mode to see if this message disappears or you can simply disable the message now if you are sure the required color mode (%4&amp;nbsp;bit) is not available in the guest OS.&lt;/p&gt;</source>
        <translation>&lt;p&gt;虛擬機器視窗已最佳化於 &lt;b&gt;%1&amp;nbsp;位元&lt;/b&gt; 的色彩模式中工作，但是目前虛擬顯示的色彩品質設定為 &lt;b&gt;%2&amp;nbsp;位元&lt;/b&gt;。&lt;/p&gt;&lt;p&gt;如果可用，請開啟客體作業系統的顯示內容對話方塊，並且選取 &lt;b&gt;%3&amp;nbsp;位元&lt;/b&gt; 的色彩模式，以獲得虛擬視訊子系統的最佳效能。&lt;/p&gt;&lt;p&gt;&lt;b&gt;請注意&lt;/b&gt;，某些像是 OS/2 的作業系統，可能實際上作用於 32&amp;nbsp;位元模式，卻回報為 24&amp;nbsp;位元 (1600 萬色) 。您可以試著選取不同的色彩品質，來查看是否出現訊息。 如果您確定所要求的色彩品質 (%4&amp;nbsp;位元)，在給予的客體作業系統中並未提供，您也可以現在就停用這個訊息。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;You didn&apos;t attach a hard disk to the new virtual machine. The machine will not be able to boot unless you attach a hard disk with a guest operating system or some other bootable media to it later using the machine settings dialog or the First Run Wizard.&lt;/p&gt;&lt;p&gt;Do you wish to continue?&lt;/p&gt;</source>
        <translation>&lt;p&gt;您未附加硬碟至新虛擬機器。 除非您稍後使用機器設定對話方塊或首次執行精靈，來附加一顆含有客體作業系統的硬碟，或是某種其它可以開機的媒體，否則機器將無法開機。&lt;/p&gt;&lt;p&gt;您要繼續嗎? &lt;p&gt;</translation>
    </message>
    <message>
        <source>Failed to find license files in &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>在 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; 中尋找使用授權檔案時失敗。</translation>
    </message>
    <message>
        <source>Failed to open the license file &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;. Check file permissions.</source>
        <translation>開啟使用授權書檔案 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; 時失敗。請檢查檔案的權限。</translation>
    </message>
    <message>
        <source>Failed to send the ACPI Power Button press event to the virtual machine &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>送出 ACPI 關關鈕的按下事件給虛擬機器 &lt;b&gt;%1&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Congratulations! You have been successfully registered as a user of VirtualBox.&lt;/p&gt;&lt;p&gt;Thank you for finding time to fill out the registration form!&lt;/p&gt;</source>
        <translation>&lt;p&gt;恭喜! 您已經成功以 VirtualBox 使用者身分註冊。&lt;/p&gt;&lt;p&gt;謝謝您花費時間來填寫註冊表單! &lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to save the global VirtualBox settings to &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;儲存全域 VirtualBox 設定值到 &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; 失敗。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to load the global GUI configuration from &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;從 &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; 載入全域 GUI 組態失敗。&lt;/p&gt;&lt;p&gt;應用程式現在將終止。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to save the global GUI configuration to &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;The application will now terminate.&lt;/p&gt;</source>
        <translation>&lt;p&gt;儲存全域 GUI 組態到 &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; 失敗。&lt;/p&gt;&lt;p&gt;應用程式現在將終止。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to save the settings of the virtual machine &lt;b&gt;%1&lt;/b&gt; to &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt;.</source>
        <translation>儲存虛擬機器 &lt;b&gt;%1&lt;/b&gt; 的設定值到 &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to load the settings of the virtual machine &lt;b&gt;%1&lt;/b&gt; from &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt;.</source>
        <translation>從 &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; 載入虛擬機器 &lt;b&gt;%1&lt;/b&gt; 的設定值時失敗。</translation>
    </message>
    <message>
        <source>Delete</source>
        <comment>machine</comment>
        <translation>刪除</translation>
    </message>
    <message>
        <source>Unregister</source>
        <comment>machine</comment>
        <translation>解除登錄</translation>
    </message>
    <message>
        <source>Discard</source>
        <comment>saved state</comment>
        <translation>捨棄</translation>
    </message>
    <message>
        <source>Disable</source>
        <comment>hard disk</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Download</source>
        <comment>additions</comment>
        <translation>下載</translation>
    </message>
    <message>
        <source>Mount</source>
        <comment>additions</comment>
        <translation>掛載</translation>
    </message>
    <message>
        <source>&lt;p&gt;The host key is currently defined as &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <comment>additional message box paragraph</comment>
        <translation>&lt;p&gt;Host 鍵目前定義為 &lt;b&gt;%1&lt;/b&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Capture</source>
        <comment>do input capture</comment>
        <translation>擷取</translation>
    </message>
    <message>
        <source>Check</source>
        <comment>inaccessible media message box</comment>
        <translation>檢查</translation>
    </message>
    <message>
        <source>Switch</source>
        <comment>fullscreen</comment>
        <translation>切換</translation>
    </message>
    <message>
        <source>Switch</source>
        <comment>seamless</comment>
        <translation>切換</translation>
    </message>
    <message>
        <source>&lt;p&gt;Do you really want to reset the virtual machine?&lt;/p&gt;&lt;p&gt;This will cause any unsaved data in applications running inside it to be lost.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您確定要重設虛擬機器嗎?&lt;/p&gt;&lt;p&gt;這將造成在虛擬機器執行中應用程式的任何未儲存資料遺失。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Reset</source>
        <comment>machine</comment>
        <translation>重設</translation>
    </message>
    <message>
        <source>Continue</source>
        <comment>no hard disk attached</comment>
        <translation>繼續</translation>
    </message>
    <message>
        <source>Go Back</source>
        <comment>no hard disk attached</comment>
        <translation>返回</translation>
    </message>
    <message>
        <source>Failed to copy file &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; to &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; (%3).</source>
        <translation>複製檔案 &lt;b&gt;&lt;nobr&gt;%1&lt;/nobr&gt;&lt;/b&gt; 到 &lt;b&gt;&lt;nobr&gt;%2&lt;/nobr&gt;&lt;/b&gt; (%3) 時失敗。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not enter seamless mode due to insufficient guest video memory.&lt;/p&gt;&lt;p&gt;You should configure the virtual machine to have at least &lt;b&gt;%1&lt;/b&gt; of video memory.&lt;/p&gt;</source>
        <translation>&lt;p&gt;由於客體視訊記憶體不足無法進入無縫模式。&lt;/p&gt;&lt;p&gt;您應組態虛擬機器至少 &lt;b&gt;%1&lt;/b&gt; 的視訊記憶體。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not switch the guest display to fullscreen mode due to insufficient guest video memory.&lt;/p&gt;&lt;p&gt;You should configure the virtual machine to have at least &lt;b&gt;%1&lt;/b&gt; of video memory.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;Ignore&lt;/b&gt; to switch to fullscreen mode anyway or press &lt;b&gt;Cancel&lt;/b&gt; to cancel the operation.&lt;/p&gt;</source>
        <translation>&lt;p&gt;由於客體視訊記憶體不足無法切換客體顯示為全螢幕模式。&lt;/p&gt;&lt;p&gt;您應組態虛擬機器至少 &lt;b&gt;%1&lt;/b&gt; 的視訊記憶體。&lt;/p&gt;&lt;p&gt;按下 [&lt;b&gt;忽略&lt;/b&gt;] 無論如何切換為全螢幕或按下 [&lt;b&gt;取消&lt;/b&gt;] 來取消操作。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>You are already running the most recent version of VirtualBox.</source>
        <translation>您已經使用 VirtualBox 的最新版本。</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have &lt;b&gt;clicked the mouse&lt;/b&gt; inside the Virtual Machine display or pressed the &lt;b&gt;host key&lt;/b&gt;. This will cause the Virtual Machine to &lt;b&gt;capture&lt;/b&gt; the host mouse pointer (only if the mouse pointer integration is not currently supported by the guest OS) and the keyboard, which will make them unavailable to other applications running on your host machine.&lt;/p&gt;&lt;p&gt;You can press the &lt;b&gt;host key&lt;/b&gt; at any time to &lt;b&gt;uncapture&lt;/b&gt; the keyboard and mouse (if it is captured) and return them to normal operation. The currently assigned host key is shown on the status bar at the bottom of the Virtual Machine window, next to the&amp;nbsp;&lt;img src=:/hostkey_16px.png/&gt;&amp;nbsp;icon. This icon, together with the mouse icon placed nearby, indicate the current keyboard and mouse capture state.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您已在虛擬機器顯示中 &lt;b&gt;按下滑鼠&lt;/b&gt; 或按下 [&lt;b&gt;Host 鍵&lt;/b&gt;]。 這將造成虛擬機器 &lt;b&gt;擷取&lt;/b&gt; 主機滑鼠指標 (除非客體 OS 目前不支援滑鼠指標整合時) 與鍵盤，並使得其它執行在您主機機器的應用程式無法使用它們。&lt;/p&gt;&lt;p&gt;您可以隨時按下 [&lt;b&gt;Host 鍵&lt;/b&gt;] 來 &lt;b&gt;取消擷取&lt;/b&gt; 鍵盤與滑鼠 (如果它擷取時) 並使它們回到正常操作。 目前指派的Host 鍵顯示在虛擬機器視窗下方的狀態列，&amp;nbsp;&lt;img src=:/hostkey_16px.png/&gt;&amp;nbsp;旁的圖示。 這個圖示與滑鼠的圖示一起放置在附近。 指示目前鍵盤和滑鼠的擷取狀態。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have the &lt;b&gt;Auto capture keyboard&lt;/b&gt; option turned on. This will cause the Virtual Machine to automatically &lt;b&gt;capture&lt;/b&gt; the keyboard every time the VM window is activated and make it unavailable to other applications running on your host machine: when the keyboard is captured, all keystrokes (including system ones like Alt-Tab) will be directed to the VM.&lt;/p&gt;&lt;p&gt;You can press the &lt;b&gt;host key&lt;/b&gt; at any time to &lt;b&gt;uncapture&lt;/b&gt; the keyboard and mouse (if it is captured) and return them to normal operation. The currently assigned host key is shown on the status bar at the bottom of the Virtual Machine window, next to the&amp;nbsp;&lt;img src=:/hostkey_16px.png/&gt;&amp;nbsp;icon. This icon, together with the mouse icon placed nearby, indicate the current keyboard and mouse capture state.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您已開啟 &lt;b&gt;自動擷取鍵盤&lt;/b&gt; 選項。 這將造成每次啟動 VM 視窗時虛擬機器自動 &lt;b&gt; 擷取 &lt;/b&gt;&lt;/p&gt; 鍵盤並使其它執行在您主機機器的應用程式不可使用它: 擷取鍵盤時，會將所有按鍵 (包括系統的如 Alt Tab) 都引導至 VM。&lt;/p&gt;&lt;p&gt;您可以隨時按下 [&lt;b&gt;Host 鍵&lt;/b&gt;] 來&lt;b&gt;取消擷取&lt;/b&gt;鍵盤和滑鼠 (如果它擷取時)，並返回它們來正常操作。 目前指派的Host 鍵顯示在虛擬機器視窗下方的狀態列，&amp;nbsp;&lt; img src=:/hostkey_16px.png/ &gt;&amp;nbsp;旁的圖示。 這個圖示與滑鼠的圖示一起放置在附近。 指示目前鍵盤和滑鼠的擷取狀態。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Virtual Machine reports that the guest OS supports &lt;b&gt;mouse pointer integration&lt;/b&gt;. This means that you do not need to &lt;i&gt;capture&lt;/i&gt; the mouse pointer to be able to use it in your guest OS -- all mouse actions you perform when the mouse pointer is over the Virtual Machine&apos;s display are directly sent to the guest OS. If the mouse is currently captured, it will be automatically uncaptured.&lt;/p&gt;&lt;p&gt;The mouse icon on the status bar will look like&amp;nbsp;&lt;img src=:/mouse_seamless_16px.png/&gt;&amp;nbsp;to inform you that mouse pointer integration is supported by the guest OS and is currently turned on.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Note&lt;/b&gt;: Some applications may behave incorrectly in mouse pointer integration mode. You can always disable it for the current session (and enable it again) by selecting the corresponding action from the menu bar.&lt;/p&gt;</source>
        <translation>&lt;p&gt;虛擬機器回報客體作業系統支援&lt;b&gt;滑鼠指標整合&lt;/b&gt;。 這意味著您不需要在客體作業系統中使用&lt;i&gt;擷取&lt;/i&gt;滑鼠指標來使用它 -- 當滑鼠指標位於虛擬機器的顯示上方時，所有您執行的滑鼠動作將直接傳送到客體作業系統。 如果目前已擷取滑鼠，將會自動取消擷取。&lt;/p&gt;&lt;p&gt;狀態列的滑鼠圖示將看起來像&amp;nbsp;&lt; img src=:/mouse_seamless_16px.png/ &gt;&amp;nbsp;以通知您客體作業系統支援滑鼠指標整合並且目前已開啟。 &lt;/p&gt;&lt;p&gt;&lt;b&gt;注意&lt;/b&gt;: 某些應用程式可能在滑鼠指標整合模式下的行為不正確。 您可以從功能表列中選取相對應的動作於目前工作階段一律停用它 (與再次啟用) 。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The virtual machine window will be now switched to &lt;b&gt;fullscreen&lt;/b&gt; mode. You can go back to windowed mode at any time by pressing &lt;b&gt;%1&lt;/b&gt;. Note that the &lt;i&gt;Host&lt;/i&gt; key is currently defined as &lt;b&gt;%2&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Note that the main menu bar is hidden in fullscreen mode. You can access it by pressing &lt;b&gt;Host+Home&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;將切換虛擬機器視窗到&lt;b&gt;全螢幕&lt;/b&gt;模式。您可以隨時按下 [&lt;b&gt;%1&lt;/b&gt;] 以回到視窗模式。請注意，目前的 &lt;i&gt;Host&lt;/i&gt; 鍵已定義為 &lt;b&gt;%2&lt;/b&gt;。&lt;/p&gt;&lt;p&gt;請注意，主功能表列在全螢幕模式中是隱藏的。您可以按下 [&lt;b&gt;Host+Home&lt;/b&gt;] 來存取它。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The virtual machine window will be now switched to &lt;b&gt;Seamless&lt;/b&gt; mode. You can go back to windowed mode at any time by pressing &lt;b&gt;%1&lt;/b&gt;. Note that the &lt;i&gt;Host&lt;/i&gt; key is currently defined as &lt;b&gt;%2&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Note that the main menu bar is hidden in seamless mode. You can access it by pressing &lt;b&gt;Host+Home&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;將切換虛擬機器視窗到&lt;b&gt;無縫&lt;/b&gt;模式。 您可以隨時按下 [&lt;b&gt;%1&lt;/b&gt;] 以回到視窗模式。請注意，目前的 &lt;i&gt;Host&lt;/i&gt; 鍵已定義為 &lt;b&gt;%2&lt;/b&gt;。&lt;/p&gt;&lt;p&gt;請注意，主功能表列在無縫模式中是隱藏的。您可以按下 [&lt;b&gt;Host+Home&lt;/b&gt;] 來存取它。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Contents...</source>
        <translation>內容(&amp;C)...</translation>
    </message>
    <message>
        <source>Show the online help contents</source>
        <translation>顯示線上說明內容</translation>
    </message>
    <message>
        <source>&amp;VirtualBox Web Site...</source>
        <translation>VirtualBox 網站(&amp;V)...</translation>
    </message>
    <message>
        <source>Open the browser and go to the VirtualBox product web site</source>
        <translation>開啟瀏覽器並前往 VirtualBox 產品網站</translation>
    </message>
    <message>
        <source>&amp;Reset All Warnings</source>
        <translation>重設所有警告(&amp;R)</translation>
    </message>
    <message>
        <source>Go back to showing all suppressed warnings and messages</source>
        <translation>返回以顯示所有抑制的警告與訊息</translation>
    </message>
    <message>
        <source>R&amp;egister VirtualBox...</source>
        <translation>註冊 VirtualBox (&amp;E)...</translation>
    </message>
    <message>
        <source>Open VirtualBox registration form</source>
        <translation>開啟 VirtualBox 註冊表單</translation>
    </message>
    <message>
        <source>C&amp;heck for Updates...</source>
        <translation>檢查更新(&amp;H)...</translation>
    </message>
    <message>
        <source>Check for a new VirtualBox version</source>
        <translation>檢查 VirtualBox 的新版本</translation>
    </message>
    <message>
        <source>&amp;About VirtualBox...</source>
        <translation>關於 VirtualBox (&amp;A)...</translation>
    </message>
    <message>
        <source>Show a dialog with product information</source>
        <translation>顯示產品資訊對話方塊</translation>
    </message>
    <message>
        <source>&lt;p&gt;A new version of VirtualBox has been released! Version &lt;b&gt;%1&lt;/b&gt; is available at &lt;a href=&quot;http://www.virtualbox.org/&quot;&gt;virtualbox.org&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;You can download this version using the link:&lt;/p&gt;&lt;p&gt;&lt;a href=%2&gt;%3&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;VirtualBox 的新版本已發佈! 版本 &lt;b&gt;%1&lt;/b&gt; 在 &lt;a href=&quot;http://www.virtualbox.org/&quot;&gt;virtualbox.org&lt;/a&gt;可用。&lt;/p&gt;&lt;p&gt;您可以使用連結:&lt;/p&gt;&lt;p&gt;&lt;a href=%2&gt;%3&lt;/a&gt;&lt;/p&gt;下載這個版本</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to release the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;?&lt;/p&gt;&lt;p&gt;This will detach it from the following virtual machine(s): &lt;b&gt;%3&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您確定要釋放 %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;嗎?&lt;/p&gt;&lt;p&gt;這將從以下的虛擬機器分離: &lt;b&gt;%3&lt;/b&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Release</source>
        <comment>detach medium</comment>
        <translation>釋放</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to remove the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; from the list of known media?&lt;/p&gt;</source>
        <translation>&lt;p&gt;您確定要從未知媒體的清單移除 %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; 嗎?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Note that as this hard disk is inaccessible its storage unit cannot be deleted right now.</source>
        <translation>請注意，由於這個硬碟不可存取它的存放單元現在不可刪除。</translation>
    </message>
    <message>
        <source>The next dialog will let you choose whether you also want to delete the storage unit of this hard disk or keep it for later usage.</source>
        <translation>下一個對話方塊會讓您選擇是否也要刪除這個硬碟存放單元或保留它供以後使用。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Note that the storage unit of this medium will not be deleted and that it will be possible to add it to the list later again.&lt;/p&gt;</source>
        <translation>&lt;p&gt;請注意，這個媒體的存放單元將不會刪除且這將有可能稍後再次將其加入到清單中。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Remove</source>
        <comment>medium</comment>
        <translation>移除</translation>
    </message>
    <message>
        <source>&lt;p&gt;The hard disk storage unit at location &lt;b&gt;%1&lt;/b&gt; already exists. You cannot create a new virtual hard disk that uses this location because it can be already used by another virtual hard disk.&lt;/p&gt;&lt;p&gt;Please specify a different location.&lt;/p&gt;</source>
        <translation>&lt;p&gt;硬碟存放單元於位置 &lt;b&gt;%1&lt;/b&gt; 已經存在。 您不能使用這個位置新建虛擬硬碟因為另一個虛擬硬碟已經使用中。&lt;/p&gt;&lt;p&gt;請指定不同的位置。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Do you want to delete the storage unit of the hard disk &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;?&lt;/p&gt;&lt;p&gt;If you select &lt;b&gt;Delete&lt;/b&gt; then the specified storage unit will be permanently deleted. This operation &lt;b&gt;cannot be undone&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;If you select &lt;b&gt;Keep&lt;/b&gt; then the hard disk will be only removed from the list of known hard disks, but the storage unit will be left untouched which makes it possible to add this hard disk to the list later again.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您要刪除硬碟的存放單元 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;嗎?&lt;/p&gt;&lt;p&gt;如果您選取 [&lt;b&gt;刪除&lt;/b&gt;] 將永久刪除指定的存放單元。 這個操作&lt;b&gt;無法復原&lt;/b&gt;。 &lt;/p&gt;&lt;p&gt;如果您選取 [&lt;b&gt;保留&lt;/b&gt;] 硬碟將只從未知硬碟清單中移除，但存放單元將保持未接觸，使得稍後可以再次加入這個硬碟到清單。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Delete</source>
        <comment>hard disk storage</comment>
        <translation>刪除</translation>
    </message>
    <message>
        <source>Keep</source>
        <comment>hard disk storage</comment>
        <translation>保留</translation>
    </message>
    <message>
        <source>Failed to delete the storage unit of the hard disk &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>刪除硬碟 &lt;b&gt;%1&lt;/b&gt; 的存放單元時失敗。</translation>
    </message>
    <message>
        <source>Failed to create the hard disk storage &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;.&lt;/nobr&gt;</source>
        <translation>建立硬碟存放 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt; 失敗。&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>Failed to open the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>開啟 %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to close the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>關閉 %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; 時失敗。</translation>
    </message>
    <message>
        <source>Failed to determine the accessibility state of the medium &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;.</source>
        <translation>判斷媒體 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; 的可存取性狀態失敗。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to connect to the VirtualBox online registration service due to the following error:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;連線到 VirtualBox 線上註冊服務失敗，由於以下錯誤:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Unable to obtain the new version information due to the following error:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;無法獲取新版本資訊由於以下錯誤:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;One or more virtual hard disks, CD/DVD or floppy media are not currently accessible. As a result, you will not be able to operate virtual machines that use these media until they become accessible later.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;Check&lt;/b&gt; to open the Virtual Media Manager window and see what media are inaccessible, or press &lt;b&gt;Ignore&lt;/b&gt; to ignore this message.&lt;/p&gt;</source>
        <translation>&lt;p&gt;一或多個虛擬硬碟、CD/DVD 或軟碟媒體目前不可存取。 因此您將無法操作使用這些媒體的虛擬機器，直至它們稍後成為可存取。&lt;/p&gt;&lt;p&gt;按下 [&lt;b&gt;檢查&lt;/b&gt;] 來開啟虛擬媒體管理員視窗並查看哪些媒體不可存取或按下 [&lt;b&gt;忽略&lt;/b&gt;] 來忽略這個訊息。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;A critical error has occurred while running the virtual machine and the machine execution has been stopped.&lt;/p&gt;&lt;p&gt;For help, please see the Community section on &lt;a href=http://www.virtualbox.org&gt;http://www.virtualbox.org&lt;/a&gt; or your support contract. Please provide the contents of the log file &lt;tt&gt;VBox.log&lt;/tt&gt; and the image file &lt;tt&gt;VBox.png&lt;/tt&gt;, which you can find in the &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; directory, as well as a description of what you were doing when this error happened. Note that you can also access the above files by selecting &lt;b&gt;Show Log&lt;/b&gt; from the &lt;b&gt;Machine&lt;/b&gt; menu of the main VirtualBox window.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;OK&lt;/b&gt; if you want to power off the machine or press &lt;b&gt;Ignore&lt;/b&gt; if you want to leave it as is for debugging. Please note that debugging requires special knowledge and tools, so it is recommended to press &lt;b&gt;OK&lt;/b&gt; now.&lt;/p&gt;</source>
        <translation>&lt;p&gt;執行虛擬機器時發生關鍵錯誤並已停止機器執行。&lt;/p&gt;&lt;p&gt;有關協助，請查閱 Community 單元於 &lt;a href=http://www.virtualbox.org&gt;http://www.virtualbox.org&lt;/a&gt; 或您的技術支援。 請提供日誌檔 &lt;tt&gt;VBox.log&lt;/tt&gt; 的內容和圖像檔 &lt;tt&gt;VBox.png&lt;/tt&gt;，您可以在 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt; 目錄找到，以及說明您正在做什麼時發生此錯誤。 請注意您也可以存取以上檔案從 VirtualBox 主視窗的 &lt;b&gt;機器&lt;/b&gt; 功能表選取 &lt;b&gt;顯示紀錄&lt;/b&gt; 。&lt;/p&gt;&lt;p&gt;按下 [&lt;b&gt;確定&lt;/b&gt;] 如果您要關閉機器或按下 [&lt;b&gt;忽略&lt;/b&gt;] 如果您要保留它作為除錯。 請注意除錯需要特殊的知識和工具，因此建議您立即按下 [&lt;b&gt;確定&lt;/b&gt;] 。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The following files already exist:&lt;br /&gt;&lt;br /&gt;%1&lt;br /&gt;&lt;br /&gt;Are you sure you want to replace them? Replacing them will overwrite their contents.</source>
        <translation>以下檔案已經存在:&lt;br /&gt;&lt;br /&gt;%1&lt;br /&gt;&lt;br /&gt;您確定要取代它們嗎?&lt;br /&gt;&lt;br /&gt; 取代它們將覆寫它的內容。</translation>
    </message>
    <message>
        <source>Failed to remove the file &lt;b&gt;%1&lt;/b&gt;.&lt;br /&gt;&lt;br /&gt;Please try to remove the file yourself and try again.</source>
        <translation>移除檔案 &lt;b&gt;%1&lt;/b&gt; 失敗。&lt;br /&gt;&lt;br /&gt;請試著自行移除檔案並重試。</translation>
    </message>
    <message>
        <source>You are running a prerelease version of VirtualBox. This version is not suitable for production use.</source>
        <translation>您正在執行預先發佈的 VirtualBox 版本。 這個版本不適合在生產使用。</translation>
    </message>
    <message>
        <source>Could not access USB on the host system, because neither the USB file system (usbfs) nor the DBus and hal services are currently available. If you wish to use host USB devices inside guest systems, you must correct this and restart VirtualBox.</source>
        <translation>無法存取主機系統上的 USB ，因為 USB 檔案系統 (usbfs) 或 DBus 和 hal 服務目前不可用。 如果您要在客體系統內使用主機 USB 裝置，您必須修正此並重新啟動 VirtualBox。</translation>
    </message>
    <message>
        <source>You are trying to shut down the guest with the ACPI power button. This is currently not possible because the guest does not support software shutdown.</source>
        <translation>您嘗試使用 ACPI 電源按鈕關閉客體。 目前不可能因為客體不支援軟體關機。</translation>
    </message>
    <message>
        <source>&lt;p&gt;VT-x/AMD-V hardware acceleration has been enabled, but is not operational. Your 64-bit guest will fail to detect a 64-bit CPU and will not be able to boot.&lt;/p&gt;&lt;p&gt;Please ensure that you have enabled VT-x/AMD-V properly in the BIOS of your host computer.&lt;/p&gt;</source>
        <translation>&lt;p&gt;VT-x/AMD-V 硬體加速已啟用，但是不可操作。 您的 64-bit 客體將偵測 64-bit CPU 失敗且將無法開機。&lt;/p&gt;&lt;p&gt;請確認在您主機電腦的 BIOS 中已正確啟用 VT-x/AMD-V 。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Close VM</source>
        <translation>關閉 VM</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>繼續</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>&lt;p&gt;There are hard disks attached to ports of the additional controller. If you disable the additional controller, all these hard disks will be automatically detached.&lt;/p&gt;&lt;p&gt;Are you sure you want to disable the additional controller?&lt;/p&gt;</source>
        <translation>&lt;p&gt;硬碟附加到額外控制器的連接埠。 如果您停用額外控制器，這些所有硬碟將自動解除附加。&lt;/p&gt;&lt;p&gt;您確定要停用額外控制器嗎?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;There are hard disks attached to ports of the additional controller. If you change the additional controller, all these hard disks will be automatically detached.&lt;/p&gt;&lt;p&gt;Are you sure you want to change the additional controller?&lt;/p&gt;</source>
        <translation>&lt;p&gt;硬碟附加到額外控制器的連接埠。 如果您變更額外控制器，這些所有硬碟將自動解除附加。&lt;/p&gt;&lt;p&gt;您確定要變更額外控制器嗎?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Change</source>
        <comment>hard disk</comment>
        <translation>變更</translation>
    </message>
    <message>
        <source>Failed to create the host-only network interface.</source>
        <translation>建立「僅限主機」網路介面失敗。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Your existing VirtualBox settings files will be automatically converted from the old format to a new format required by the new version of VirtualBox.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;OK&lt;/b&gt; to start VirtualBox now or press &lt;b&gt;Exit&lt;/b&gt; if you want to terminate the VirtualBox application without any further actions.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您現有的 VirtualBox 設定檔將自動從舊格式轉換為 VirtualBox 新版本所需的新格式。&lt;/p&gt;&lt;p&gt;按下 [&lt;b&gt;確定&lt;/b&gt;] 或按下 [&lt;b&gt;結束&lt;/b&gt;] 如果您要終止 VirtualBox 應用程式而不作任何進一步動作。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to open appliance.</source>
        <translation>開啟應用裝置失敗。</translation>
    </message>
    <message>
        <source>Failed to open/interpret appliance &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>開啟/解釋應用裝置 &lt;b&gt;%1&lt;/b&gt; 失敗。</translation>
    </message>
    <message>
        <source>Failed to import appliance &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>匯入應用裝置 &lt;b&gt;%1&lt;/b&gt; 失敗。</translation>
    </message>
    <message>
        <source>Failed to create appliance.</source>
        <translation>建立應用裝置失敗。</translation>
    </message>
    <message>
        <source>Failed to prepare the export of the appliance &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>準備匯出應用裝置 &lt;b&gt;%1&lt;/b&gt; 失敗。</translation>
    </message>
    <message>
        <source>Failed to create an appliance.</source>
        <translation>建立應用裝置失敗。</translation>
    </message>
    <message>
        <source>Failed to export appliance &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>匯出應用裝置 &lt;b&gt;%1&lt;/b&gt; 失敗。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Deleting this host-only network will remove the host-only interface this network is based on. Do you want to remove the (host-only network) interface &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;?&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Note:&lt;/b&gt; this interface may be in use by one or more virtual network adapters belonging to one of your VMs. After it is removed, these adapters will no longer be usable until you correct their settings by either choosing a different interface name or a different adapter attachment type.&lt;/p&gt;</source>
        <translation>&lt;p&gt;刪除這個「僅限主機」網路將移除基於這個網路的「僅限主機」介面。 您要要移除 (只有主機網路) 介面 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt; 嗎?&lt;/nobr&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;注意:&lt;/b&gt;這個介面可能由一或多個虛擬網路卡使用中屬於您的虛擬機器之一。 移除後，這些介面卡將不再可用，直到您選擇不同介面名稱或不同介面卡附件類型來修正它們的設定。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>A file named &lt;b&gt;%1&lt;/b&gt; already exists. Are you sure you want to replace it?&lt;br /&gt;&lt;br /&gt;Replacing it will overwrite its contents.</source>
        <translation>檔案名稱 &lt;b&gt;%1&lt;/b&gt; 已經存在。 您確定要取代它嗎?&lt;br /&gt;&lt;br /&gt;取代它將覆寫它的內容。</translation>
    </message>
    <message>
        <source>&lt;p&gt;VT-x/AMD-V hardware acceleration has been enabled, but is not operational. Certain guests (e.g. OS/2 and QNX) require this feature.&lt;/p&gt;&lt;p&gt;Please ensure that you have enabled VT-x/AMD-V properly in the BIOS of your host computer.&lt;/p&gt;</source>
        <translation>&lt;p&gt;VT-x/AMD-V 硬體加速已啟用，但是不可操作。 某些客體 (例如 OS/2 與 QNX) 需要這個功能。&lt;/p&gt;&lt;p&gt;請確認在您主機電腦的 BIOS 中已正確啟用 VT-x/AMD-V 。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Invalid e-mail address or password specified.&lt;/p&gt;</source>
        <translation>&lt;p&gt;指定無效的電子郵件或密碼。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Failed to register the VirtualBox product.&lt;/p&gt;&lt;p&gt;%1&lt;/p&gt;</source>
        <translation>&lt;p&gt;註冊 VirtualBox 產品失敗。&lt;/p&gt;&lt;p&gt;%1&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to check files.</source>
        <translation>檢查檔案失敗。</translation>
    </message>
    <message>
        <source>Failed to remove file.</source>
        <translation>移除檔案失敗。</translation>
    </message>
    <message>
        <source>You seem to have the USBFS filesystem mounted at /sys/bus/usb/drivers. We strongly recommend that you change this, as it is a severe mis-configuration of your system which could cause USB devices to fail in unexpected ways.</source>
        <translation>您似乎在 /sys/bus/usb/drivers 掛載 USBFS 檔案系統。 我們強烈建議您變更此，由於它是一個嚴重的錯誤組態您的系統，這可能導致 USB 裝置以意想不到的方式失敗。</translation>
    </message>
    <message>
        <source>You are running an EXPERIMENTAL build of VirtualBox. This version is not suitable for production use.</source>
        <translation>您正在執行 VirtualBox 的 EXPERIMENTAL 組建。 這個版本不適合在生產上使用。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to restore snapshot &lt;b&gt;%1&lt;/b&gt;? This will cause you to lose your current machine state, which cannot be recovered.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您確定要還原快照 &lt;b&gt;%1&lt;/b&gt; 嗎? 這將造成您目前的機器快照，其無法復原。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>還原</translation>
    </message>
    <message>
        <source>&lt;p&gt;Deleting the snapshot will cause the state information saved in it to be lost, and disk data spread over several image files that VirtualBox has created together with the snapshot will be merged into one file. This can be a lengthy process, and the information in the snapshot cannot be recovered.&lt;/p&gt;&lt;/p&gt;Are you sure you want to delete the selected snapshot &lt;b&gt;%1&lt;/b&gt;?&lt;/p&gt;</source>
        <translation>&lt;p&gt;刪除快照將造成它儲存的狀態資訊遺失，且 VirtualBox 與快照一起建立分布在數個磁碟資料的映像檔案將合併為一個檔案。 這可能是一個漫長的過程，且快照中資訊無法恢復。&lt;/p&gt;&lt;/p&gt;您確定要刪除選取的快照 &lt;b&gt;%1&lt;/b&gt; 嗎?&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <source>Failed to restore the snapshot &lt;b&gt;%1&lt;/b&gt; of the virtual machine &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>還原虛擬機器 &lt;b&gt;%2&lt;/b&gt; 的快照 &lt;b&gt;%1&lt;/b&gt; 失敗。</translation>
    </message>
    <message>
        <source>Failed to delete the snapshot &lt;b&gt;%1&lt;/b&gt; of the virtual machine &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>刪除虛擬機器 &lt;b&gt;%2&lt;/b&gt; 的快照 &lt;b&gt;%1&lt;/b&gt; 失敗。</translation>
    </message>
    <message>
        <source>&lt;p&gt;There are no unused media available for the newly created attachment.&lt;/p&gt;&lt;p&gt;Press the &lt;b&gt;Create&lt;/b&gt; button to start the &lt;i&gt;New Virtual Disk&lt;/i&gt; wizard and create a new medium, or press the &lt;b&gt;Select&lt;/b&gt; if you wish to open the &lt;i&gt;Virtual Media Manager&lt;/i&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;沒有未使用的媒體可用作為最近建立的附件。&lt;/p&gt;&lt;p&gt;按下 [&lt;b&gt;建立&lt;/b&gt;] 按鈕開始 &lt;i&gt;新增虛擬磁碟&lt;/i&gt; 精靈並建立新媒體，或按下 [&lt;b&gt;選取&lt;/b&gt;] 如果您想開啟 &lt;i&gt;虛擬媒體管理員&lt;/i&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Create</source>
        <comment>medium</comment>
        <translation>建立(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Select</source>
        <comment>medium</comment>
        <translation>選取(&amp;S)</translation>
    </message>
    <message>
        <source>&lt;p&gt;There are no unused media available for the newly created attachment.&lt;/p&gt;&lt;p&gt;Press the &lt;b&gt;Select&lt;/b&gt; if you wish to open the &lt;i&gt;Virtual Media Manager&lt;/i&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;沒有未使用的媒體可用作為最近建立的附件。&lt;/p&gt;&lt;p&gt;按下 [&lt;b&gt;選取&lt;/b&gt;] 如果您想開啟 &lt;i&gt;虛擬媒體管理員&lt;/i&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Failed to attach the %1 to slot &lt;i&gt;%2&lt;/i&gt; of the machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>附加 %1 至機器 &lt;b&gt;%3&lt;/b&gt; 的插槽 &lt;i&gt;%2&lt;/i&gt; 失敗。</translation>
    </message>
    <message>
        <source>Failed to detach the %1 from slot &lt;i&gt;%2&lt;/i&gt; of the machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>從機器 &lt;b&gt;%3&lt;/b&gt; 的插槽 &lt;i&gt;%2&lt;/i&gt; 分離 %1 失敗。</translation>
    </message>
    <message>
        <source>Unable to mount the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; on the machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>無法在機器 &lt;b&gt;%3&lt;/b&gt; 掛載 %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;。</translation>
    </message>
    <message>
        <source> Would you like to force mounting of this medium?</source>
        <translation>您要強制掛載這個媒體嗎?</translation>
    </message>
    <message>
        <source>Unable to unmount the %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt; from the machine &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>無法從機器 &lt;b&gt;%3&lt;/b&gt; 卸載 %1 &lt;nobr&gt;&lt;b&gt;%2&lt;/b&gt;&lt;/nobr&gt;。</translation>
    </message>
    <message>
        <source> Would you like to force unmounting of this medium?</source>
        <translation>您要強制卸載這個媒體嗎?</translation>
    </message>
    <message>
        <source>Force Unmount</source>
        <translation>強制卸載</translation>
    </message>
    <message>
        <source>Failed to eject the disk from the virtual drive. The drive may be locked by the guest operating system. Please check this and try again.</source>
        <translation>從虛擬磁碟機退出磁碟失敗。 磁碟機可能由客體作業系統鎖定。 請檢查此並重試。</translation>
    </message>
    <message>
        <source>&lt;p&gt;Could not insert the VirtualBox Guest Additions installer CD image into the virtual machine &lt;b&gt;%1&lt;/b&gt;, as the machine has no CD/DVD-ROM drives. Please add a drive using the storage page of the virtual machine settings dialog.&lt;/p&gt;</source>
        <translation>&lt;p&gt;無法插入 VirtualBox Guest Additions 安裝程式 CD 映像至虛擬機器 &lt;b&gt;%1&lt;/b&gt;，由於機器沒有 CD/DVD-ROM 光碟機。 請使用虛擬機器設定對話方塊的存放裝置頁面加入一個光碟機。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <comment>warnAboutSettingsAutoConversion message box</comment>
        <translation>結束(&amp;X)</translation>
    </message>
    <message>
        <source>&lt;p&gt;The following VirtualBox settings files will be automatically converted from the old format to a new format required by the new version of VirtualBox.&lt;/p&gt;&lt;p&gt;Press &lt;b&gt;OK&lt;/b&gt; to start VirtualBox now or press &lt;b&gt;Exit&lt;/b&gt; if you want to terminate the VirtualBox application without any further actions.&lt;/p&gt;</source>
        <translation>&lt;p&gt;以下 VirtualBox 設定檔將自動從舊格式轉換為 VirtualBox 新版本所需的新格式。&lt;/p&gt;&lt;p&gt;按下 [&lt;b&gt;確定&lt;/b&gt;] 立即啟動 VirtualBox 或按下 [&lt;b&gt;結束&lt;/b&gt;] 若您要終止 VirtualBox 應用程式而不要任何進一步動作。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>hard disk</source>
        <comment>failed to mount ...</comment>
        <translation>硬碟</translation>
    </message>
    <message>
        <source>CD/DVD</source>
        <comment>failed to mount ... host-drive</comment>
        <translation>CD/DVD</translation>
    </message>
    <message>
        <source>CD/DVD image</source>
        <comment>failed to mount ...</comment>
        <translation>CD/DVD 映像</translation>
    </message>
    <message>
        <source>floppy</source>
        <comment>failed to mount ... host-drive</comment>
        <translation>軟碟</translation>
    </message>
    <message>
        <source>floppy image</source>
        <comment>failed to mount ...</comment>
        <translation>軟碟映像</translation>
    </message>
    <message>
        <source>hard disk</source>
        <comment>failed to attach ...</comment>
        <translation>硬碟</translation>
    </message>
    <message>
        <source>CD/DVD device</source>
        <comment>failed to attach ...</comment>
        <translation>CD/DVD 裝置</translation>
    </message>
    <message>
        <source>floppy device</source>
        <comment>failed to close ...</comment>
        <translation>軟碟裝置</translation>
    </message>
    <message>
        <source>&lt;p&gt;Are you sure you want to delete the CD/DVD-ROM device?&lt;/p&gt;&lt;p&gt;You will not be able to mount any CDs or ISO images or install the Guest Additions without it!&lt;/p&gt;</source>
        <translation>&lt;p&gt;您確定要刪除 CD/DVD-ROM 裝置嗎?&lt;/p&gt;&lt;p&gt;沒有它您將無法掛載任何 CD 或 ISO 映像或安裝 Guest Additions !&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <comment>medium</comment>
        <translation>移除(&amp;R)</translation>
    </message>
</context>
<context>
    <name>VBoxProgressDialog</name>
    <message>
        <source>A few seconds remaining</source>
        <translation>剩下幾秒鐘</translation>
    </message>
    <message>
        <source>Canceling...</source>
        <translation>正在取消...</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>取消(&amp;C)</translation>
    </message>
    <message>
        <source>Cancel the current operation</source>
        <translation>取消目前操作</translation>
    </message>
    <message>
        <source>%1, %2 remaining</source>
        <comment>You may wish to translate this more like &quot;Time remaining: %1, %2&quot;</comment>
        <translation>剩下 %1, %2</translation>
    </message>
    <message>
        <source>%1 remaining</source>
        <comment>You may wish to translate this more like &quot;Time remaining: %1&quot;</comment>
        <translation>剩下 %1</translation>
    </message>
</context>
<context>
    <name>VBoxRegistrationDlg</name>
    <message>
        <source>VirtualBox Registration Dialog</source>
        <translation type="obsolete">VirtualBox 註冊對話方塊</translation>
    </message>
    <message>
        <source>Enter your full name using Latin characters.</source>
        <translation type="obsolete">使用拉丁字母輸入您的全名。</translation>
    </message>
    <message>
        <source>Enter your e-mail address. Please use a valid address here.</source>
        <translation type="obsolete">輸入您的電子郵件位址。 請在此使用有效的位址。</translation>
    </message>
    <message>
        <source>Welcome to the VirtualBox Registration Form!</source>
        <translation type="obsolete">歡迎使用 VirtualBox 註冊表單!</translation>
    </message>
    <message>
        <source>Could not perform connection handshake.</source>
        <translation type="obsolete">無法執行連線交握。</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Select Country/Territory</source>
        <translation type="obsolete">選取國家/地區</translation>
    </message>
    <message>
        <source>&lt;p&gt;Please fill out this registration form to let us know that you use VirtualBox and, optionally, to keep you informed about VirtualBox news and updates.&lt;/p&gt;&lt;p&gt;Please use Latin characters only to fill in  the fields below. Sun Microsystems will use this information only to gather product usage statistics and to send you VirtualBox newsletters. In particular, Sun Microsystems will never pass your data to third parties. Detailed information about how we use your personal data can be found in the &lt;b&gt;Privacy Policy&lt;/b&gt; section of the VirtualBox Manual or on the &lt;a href=http://www.virtualbox.org/wiki/PrivacyPolicy&gt;Privacy Policy&lt;/a&gt; page of the VirtualBox web-site.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;請填寫這個註冊表單可以讓我們知道您使用 VirtualBox ，並選擇性的保持通知您有關 VirtualBox 新聞與更新。&lt;/p&gt;&lt;p&gt;請只使用拉丁字元填寫以下欄位。 Sun Microsystems 只使用這個資訊，收集產品使用情況統計資訊並向您傳送 VirtualBox 電子報。 特別是， Sun Microsystems 將永不傳遞您的資料給協力廠商。 VirtualBox 手冊的 &lt;b&gt; 隱私權原則&lt;/b&gt; 單元或 VirtualBox 的網站 &lt;a href=http://www.virtualbox.org/wiki/PrivacyPolicy&gt; 隱私權原則 &lt;/a&gt; 中可以找到有關我們如何使用您個人資料的詳細資訊。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>I &amp;already have a Sun Online account:</source>
        <translation type="obsolete">我已經有 Sun Online 帳戶(&amp;A):</translation>
    </message>
    <message>
        <source>&amp;E-mail:</source>
        <translation type="obsolete">電子郵件(&amp;E):</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation type="obsolete">密碼(&amp;P):</translation>
    </message>
    <message>
        <source>I &amp;would like to create a new Sun Online account:</source>
        <translation type="obsolete">我希望新建 Sun Online 帳戶(&amp;W):</translation>
    </message>
    <message>
        <source>&amp;First Name:</source>
        <translation type="obsolete">名字(&amp;F):</translation>
    </message>
    <message>
        <source>&amp;Last Name:</source>
        <translation type="obsolete">姓氏(&amp;L):</translation>
    </message>
    <message>
        <source>&amp;Company:</source>
        <translation type="obsolete">公司(&amp;C):</translation>
    </message>
    <message>
        <source>Co&amp;untry:</source>
        <translation type="obsolete">國家(&amp;U):</translation>
    </message>
    <message>
        <source>E-&amp;mail:</source>
        <translation type="obsolete">電子郵件(&amp;M):</translation>
    </message>
    <message>
        <source>P&amp;assword:</source>
        <translation type="obsolete">密碼(&amp;A):</translation>
    </message>
    <message>
        <source>Co&amp;nfirm Password:</source>
        <translation type="obsolete">確認密碼(&amp;N):</translation>
    </message>
    <message>
        <source>&amp;Register</source>
        <translation type="obsolete">註冊(&amp;R)</translation>
    </message>
</context>
<context>
    <name>VBoxSFDialog</name>
    <message>
        <source>Shared Folders</source>
        <translation>共用資料夾</translation>
    </message>
</context>
<context>
    <name>VBoxScreenshotViewer</name>
    <message>
        <source>Screenshot of %1 (%2)</source>
        <translation>%1 (%2) 的快照</translation>
    </message>
    <message>
        <source>Click to view non-scaled screenshot.</source>
        <translation>按一下檢視非縮放快照。</translation>
    </message>
    <message>
        <source>Click to view scaled screenshot.</source>
        <translation>按一下檢視縮放快照。</translation>
    </message>
</context>
<context>
    <name>VBoxSelectorWnd</name>
    <message>
        <source>VirtualBox OSE</source>
        <translation>VirtualBox 開放原始碼版本 (OSE)</translation>
    </message>
    <message>
        <source>&amp;Details</source>
        <translation>詳細資料(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Preferences...</source>
        <comment>global settings</comment>
        <translation>喜好設定(&amp;P)...</translation>
    </message>
    <message>
        <source>Display the global settings dialog</source>
        <translation>顯示全域設定值對話方塊</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>結束(&amp;X)</translation>
    </message>
    <message>
        <source>Close application</source>
        <translation>關閉應用程式</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation>新增(&amp;N)...</translation>
    </message>
    <message>
        <source>Create a new virtual machine</source>
        <translation>新建虛擬機器</translation>
    </message>
    <message>
        <source>&amp;Settings...</source>
        <translation>設定值(&amp;S)...</translation>
    </message>
    <message>
        <source>Configure the selected virtual machine</source>
        <translation>組態選取的虛擬機器</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>刪除(&amp;D)</translation>
    </message>
    <message>
        <source>Delete the selected virtual machine</source>
        <translation>刪除選取的虛擬機器</translation>
    </message>
    <message>
        <source>D&amp;iscard</source>
        <translation>捨棄(&amp;I)</translation>
    </message>
    <message>
        <source>Discard the saved state of the selected virtual machine</source>
        <translation>捨棄選取虛擬機器的已儲存狀態</translation>
    </message>
    <message>
        <source>Refresh the accessibility state of the selected virtual machine</source>
        <translation>重新整理選取虛擬機器的可存取能力狀態</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>檔案(&amp;F)</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>說明(&amp;H)</translation>
    </message>
    <message>
        <source>&amp;Snapshots</source>
        <translation>快照(&amp;S)</translation>
    </message>
    <message>
        <source>D&amp;escription</source>
        <translation>描述(&amp;E)</translation>
    </message>
    <message>
        <source>D&amp;escription *</source>
        <translation>描述(&amp;E) *</translation>
    </message>
    <message>
        <source>S&amp;how</source>
        <translation>顯示(&amp;H)</translation>
    </message>
    <message>
        <source>Switch to the window of the selected virtual machine</source>
        <translation>切換到選取虛擬機器的視窗</translation>
    </message>
    <message>
        <source>S&amp;tart</source>
        <translation>啟動(&amp;T)</translation>
    </message>
    <message>
        <source>Start the selected virtual machine</source>
        <translation>啟動選取的虛擬機器</translation>
    </message>
    <message>
        <source>&amp;Machine</source>
        <translation>機器(&amp;M)</translation>
    </message>
    <message>
        <source>Show &amp;Log...</source>
        <translation>顯示記錄(&amp;L)...</translation>
    </message>
    <message>
        <source>Show the log files of the selected virtual machine</source>
        <translation>顯示選取虛擬機器的記錄檔案</translation>
    </message>
    <message>
        <source>R&amp;esume</source>
        <translation>繼續執行(&amp;E)</translation>
    </message>
    <message>
        <source>Resume the execution of the virtual machine</source>
        <translation>繼續執行虛擬機器</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>暫停(&amp;P)</translation>
    </message>
    <message>
        <source>Suspend the execution of the virtual machine</source>
        <translation>暫停虛擬機器的執行</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Welcome to VirtualBox!&lt;/h3&gt;&lt;p&gt;The left part of this window is  a list of all virtual machines on your computer. The list is empty now because you haven&apos;t created any virtual machines yet.&lt;img src=:/welcome.png align=right/&gt;&lt;/p&gt;&lt;p&gt;In order to create a new virtual machine, press the &lt;b&gt;New&lt;/b&gt; button in the main tool bar located at the top of the window.&lt;/p&gt;&lt;p&gt;You can press the &lt;b&gt;%1&lt;/b&gt; key to get instant help, or visit &lt;a href=http://www.virtualbox.org&gt;www.virtualbox.org&lt;/a&gt; for the latest information and news.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;歡迎使用 VirtualBox !&lt;/h3&gt;&lt;p&gt;這個視窗的左側部分是在您電腦中的所有虛擬機器清單。 清單現在是空的因為您尚未建立任何虛擬機器。&lt;img src=:/welcome.png align=right/&gt;&lt;/p&gt;&lt;p&gt;為了新建虛擬機器，在視窗上方位置的主工具列按下 [&lt;b&gt;新增&lt;/b&gt;] 按鈕。&lt;/p&gt;&lt;p&gt;您可以按下 [&lt;b&gt;%1&lt;/b&gt;] 按鍵取得即時說明，或訪問 &lt;a href=http://www.virtualbox.org&gt;www.virtualbox.org&lt;/a&gt; 取得最新資訊與新聞。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Virtual Media Manager...</source>
        <translation>虛擬媒體管理員(&amp;V)...</translation>
    </message>
    <message>
        <source>Display the Virtual Media Manager dialog</source>
        <translation>顯示虛擬媒體管理員對話方塊</translation>
    </message>
    <message>
        <source>Log</source>
        <comment>icon text</comment>
        <translation>紀錄</translation>
    </message>
    <message>
        <source>Sun VirtualBox</source>
        <translation>Sun VirtualBox</translation>
    </message>
    <message>
        <source>&amp;Import Appliance...</source>
        <translation>匯入應用裝置(&amp;I)...</translation>
    </message>
    <message>
        <source>Import an appliance into VirtualBox</source>
        <translation>匯入應用裝置到 VirtualBox</translation>
    </message>
    <message>
        <source>&amp;Export Appliance...</source>
        <translation>匯出應用裝置(&amp;E)...</translation>
    </message>
    <message>
        <source>Export one or more VirtualBox virtual machines as an appliance</source>
        <translation>匯出一或多個 VirtualBox 虛擬機器為應用裝置</translation>
    </message>
    <message>
        <source>Re&amp;fresh</source>
        <translation>重新整理(&amp;F)</translation>
    </message>
</context>
<context>
    <name>VBoxSettingsDialog</name>
    <message>
        <source>&lt;i&gt;Select a settings category from the list on the left-hand side and move the mouse over a settings item to get more information&lt;/i&gt;.</source>
        <translation>&lt;i&gt;請從左手邊的清單選取設定分類並將滑鼠移至設定項目上方以取得更多資訊&lt;/i&gt;。</translation>
    </message>
    <message>
        <source>Invalid settings detected</source>
        <translation>偵測到無效的設定值</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>設定值</translation>
    </message>
    <message>
        <source>Non-optimal settings detected</source>
        <translation>偵測到非最佳設定值</translation>
    </message>
    <message>
        <source>On the &lt;b&gt;%1&lt;/b&gt; page, %2</source>
        <translation>於 &lt;b&gt;%1&lt;/b&gt; 頁, %2</translation>
    </message>
</context>
<context>
    <name>VBoxSnapshotDetailsDlg</name>
    <message>
        <source>Details of %1 (%2)</source>
        <translation>%1 (%2) 的詳細資料</translation>
    </message>
    <message>
        <source>Click to enlarge the screenshot.</source>
        <translation>按一下放大快照。</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>名稱(&amp;N):</translation>
    </message>
    <message>
        <source>Taken:</source>
        <translation>取得:</translation>
    </message>
    <message>
        <source>&amp;Description:</source>
        <translation>描述(&amp;D):</translation>
    </message>
    <message>
        <source>D&amp;etails:</source>
        <translation>詳細資料(&amp;E):</translation>
    </message>
</context>
<context>
    <name>VBoxSnapshotsWgt</name>
    <message>
        <source>VBoxSnapshotsWgt</source>
        <translation>VBoxSnapshotsWgt</translation>
    </message>
    <message>
        <source>Current State (changed)</source>
        <comment>Current State (Modified)</comment>
        <translation>目前狀態 (已變更)</translation>
    </message>
    <message>
        <source>Current State</source>
        <comment>Current State (Unmodified)</comment>
        <translation>目前狀態</translation>
    </message>
    <message>
        <source>The current state differs from the state stored in the current snapshot</source>
        <translation>目前狀態與目前快照中儲存的狀態不同</translation>
    </message>
    <message>
        <source>The current state is identical to the state stored in the current snapshot</source>
        <translation>目前狀態與目前快照中儲存的狀態完全相同</translation>
    </message>
    <message>
        <source> (current, </source>
        <comment>Snapshot details</comment>
        <translation>(目前，</translation>
    </message>
    <message>
        <source>online)</source>
        <comment>Snapshot details</comment>
        <translation>線上)</translation>
    </message>
    <message>
        <source>offline)</source>
        <comment>Snapshot details</comment>
        <translation>離線)</translation>
    </message>
    <message>
        <source>Taken at %1</source>
        <comment>Snapshot (time)</comment>
        <translation>在 %1 取得</translation>
    </message>
    <message>
        <source>Taken on %1</source>
        <comment>Snapshot (date + time)</comment>
        <translation>在 %1 取得</translation>
    </message>
    <message>
        <source>%1 since %2</source>
        <comment>Current State (time or date + time)</comment>
        <translation>%1 自從 %2</translation>
    </message>
    <message>
        <source>Snapshot %1</source>
        <translation>快照 %1</translation>
    </message>
    <message>
        <source>Take &amp;Snapshot</source>
        <translation>取得快照(&amp;S)</translation>
    </message>
    <message>
        <source>S&amp;how Details</source>
        <translation>顯示詳細資料(&amp;H)</translation>
    </message>
    <message>
        <source>Take a snapshot of the current virtual machine state</source>
        <translation>取得目前虛擬機器狀態的快照</translation>
    </message>
    <message>
        <source>Show the details of the selected snapshot</source>
        <translation>顯示選取快照的詳細資料</translation>
    </message>
    <message>
        <source> (%1)</source>
        <translation>(%1)</translation>
    </message>
    <message>
        <source>&amp;Restore Snapshot</source>
        <translation>還原快照(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Delete Snapshot</source>
        <translation>刪除快照(&amp;D)</translation>
    </message>
    <message>
        <source>Restore the selected snapshot of the virtual machine</source>
        <translation>還原選取的虛擬機器快照</translation>
    </message>
    <message>
        <source>Delete the selected snapshot of the virtual machine</source>
        <translation>刪除選取的虛擬機器快照</translation>
    </message>
    <message>
        <source> (%1 ago)</source>
        <translation>(%1 之前)</translation>
    </message>
</context>
<context>
    <name>VBoxSwitchMenu</name>
    <message>
        <source>Disable</source>
        <translation>停用</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>啟用</translation>
    </message>
</context>
<context>
    <name>VBoxTakeSnapshotDlg</name>
    <message>
        <source>Take Snapshot of Virtual Machine</source>
        <translation>取得虛擬機器的快照</translation>
    </message>
    <message>
        <source>Snapshot &amp;Name</source>
        <translation>快照名稱(&amp;N)</translation>
    </message>
    <message>
        <source>Snapshot &amp;Description</source>
        <translation>快照描述(&amp;D)</translation>
    </message>
    <message numerus="yes">
        <source>Warning: You are taking a snapshot of a running machine which has %n immutable image(s) attached to it. As long as you are working from this snapshot the immutable image(s) will not be reset to avoid loss of data.</source>
        <translation>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>VBoxTextEditor</name>
    <message>
        <source>Edit text</source>
        <translation>編輯文字</translation>
    </message>
    <message>
        <source>&amp;Replace...</source>
        <translation>取代(&amp;R)...</translation>
    </message>
    <message>
        <source>Replaces the current text with the content of a file.</source>
        <translation>以檔案的內容取代目前文字。</translation>
    </message>
    <message>
        <source>Text (*.txt);;All (*.*)</source>
        <translation>文字 (*.txt);;所有 (*.*)</translation>
    </message>
    <message>
        <source>Select a file to open...</source>
        <translation>選取開啟的檔案...</translation>
    </message>
</context>
<context>
    <name>VBoxTrayIcon</name>
    <message>
        <source>Show Selector Window</source>
        <translation>顯示選取器視窗</translation>
    </message>
    <message>
        <source>Show the selector window assigned to this menu</source>
        <translation>顯示指派到這個功能表的選取器視窗</translation>
    </message>
    <message>
        <source>Hide Tray Icon</source>
        <translation>隱藏通知區圖示</translation>
    </message>
    <message>
        <source>Remove this icon from the system tray</source>
        <translation>從系統通知區移除這個圖示</translation>
    </message>
    <message>
        <source>&amp;Other Machines...</source>
        <comment>tray menu</comment>
        <translation>其它機器(&amp;O)...</translation>
    </message>
</context>
<context>
    <name>VBoxUSBMenu</name>
    <message>
        <source>&lt;no devices available&gt;</source>
        <comment>USB devices</comment>
        <translation>&lt;沒有可用裝置&gt;</translation>
    </message>
    <message>
        <source>No supported devices connected to the host PC</source>
        <comment>USB device tooltip</comment>
        <translation>主機 PC 未連接支援的裝置</translation>
    </message>
</context>
<context>
    <name>VBoxUpdateDlg</name>
    <message>
        <source>1 day</source>
        <translation>1 天</translation>
    </message>
    <message>
        <source>2 days</source>
        <translation>2 天</translation>
    </message>
    <message>
        <source>3 days</source>
        <translation>3 天</translation>
    </message>
    <message>
        <source>4 days</source>
        <translation>4 天</translation>
    </message>
    <message>
        <source>5 days</source>
        <translation>5 天</translation>
    </message>
    <message>
        <source>6 days</source>
        <translation>6 天</translation>
    </message>
    <message>
        <source>1 week</source>
        <translation>1 星期</translation>
    </message>
    <message>
        <source>2 weeks</source>
        <translation>2 星期</translation>
    </message>
    <message>
        <source>3 weeks</source>
        <translation>3 星期</translation>
    </message>
    <message>
        <source>1 month</source>
        <translation>1 個月</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>永不</translation>
    </message>
    <message>
        <source>Chec&amp;k</source>
        <translation>檢查(&amp;K)</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>關閉(&amp;C)</translation>
    </message>
    <message>
        <source>VirtualBox Update Wizard</source>
        <translation>VirtualBox 更新精靈</translation>
    </message>
    <message>
        <source>Check for Updates</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>摘要</translation>
    </message>
    <message>
        <source>&lt;p&gt;A new version of VirtualBox has been released! Version &lt;b&gt;%1&lt;/b&gt; is available at &lt;a href=&quot;http://www.virtualbox.org/&quot;&gt;virtualbox.org&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;You can download this version using the link:&lt;/p&gt;&lt;p&gt;&lt;a href=%2&gt;%3&lt;/a&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;VirtualBox 的新版本已發佈! 版本 &lt;b&gt;%1&lt;/b&gt; 在 &lt;a href=&quot;http://www.virtualbox.org/&quot;&gt;virtualbox.org&lt;/a&gt;可用。&lt;/p&gt;&lt;p&gt;您可以使用連結:&lt;/p&gt;&lt;p&gt;&lt;a href=%2&gt;%3&lt;/a&gt;&lt;/p&gt;下載這個版本</translation>
    </message>
    <message>
        <source>&lt;p&gt;Unable to obtain the new version information due to the following network error:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;無法獲取新版本資訊由於以下網路錯誤:&lt;/p&gt;&lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>You are already running the most recent version of VirtualBox.</source>
        <translation>您已經使用 VirtualBox 的最新版本。</translation>
    </message>
    <message>
        <source>&lt;p&gt;This wizard will connect to the VirtualBox web-site and check if a newer version of VirtualBox is available.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Check&lt;/b&gt; button to check for a new version now or the &lt;b&gt;Cancel&lt;/b&gt; button if you do not want to perform this check.&lt;/p&gt;&lt;p&gt;You can run this wizard at any time by choosing &lt;b&gt;Check for Updates...&lt;/b&gt; from the &lt;b&gt;Help&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;這個精靈將連接到 VirtualBox 網站並檢查是否有可用的 VirtualBox 更新版本。&lt;/p&gt;&lt;p&gt;使用 [&lt;b&gt;檢查&lt;/b&gt;] 按鈕立即檢查新版本或 [&lt;b&gt;取消&lt;/b&gt;] 按鈕如果您不想執行這個檢查。&lt;/p&gt;&lt;p&gt;您可以在任何時候從 [&lt;b&gt;說明&lt;/b&gt;] 功能表選擇 [&lt;b&gt;檢查更新...&lt;/b&gt;] 執行這個精靈。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>VBoxVMDescriptionPage</name>
    <message>
        <source>No description. Press the Edit button below to add it.</source>
        <translation>沒有任何描述。按下方的 [編輯] 按鈕來加入它。</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <source>Edit (Ctrl+E)</source>
        <translation>編輯 (Ctrl+E)</translation>
    </message>
</context>
<context>
    <name>VBoxVMDetailsView</name>
    <message>
        <source>The selected virtual machine is &lt;i&gt;inaccessible&lt;/i&gt;. Please inspect the error message shown below and press the &lt;b&gt;Refresh&lt;/b&gt; button if you want to repeat the accessibility check:</source>
        <translation>選取的虛擬機器是 &lt;i&gt;不可存取&lt;/i&gt;。 請核閱顯示於下方的錯誤訊息，如果您想要重複檢查可存取性，請按下 [&lt;b&gt;重新整理&lt;/b&gt;] 按鈕:</translation>
    </message>
</context>
<context>
    <name>VBoxVMFirstRunWzd</name>
    <message>
        <source>First Run Wizard</source>
        <translation>首次執行精靈</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have started a newly created virtual machine for the first time. This wizard will help you to perform the steps necessary for installing an operating system of your choice onto this virtual machine.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Next&lt;/b&gt; button to go to the next page of the wizard and the &lt;b&gt;Back&lt;/b&gt; button to return to the previous page. You can also press &lt;b&gt;Cancel&lt;/b&gt; if you want to cancel the execution of this wizard.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您已首次啟動新建立的虛擬機器。 這個精靈將協助您進行必要步驟，以在虛擬機器中安裝您所選的作業系統。&lt;/p&gt;&lt;p&gt;使用 [&lt;b&gt;下一步&lt;/b&gt;] 按鈕前往精靈的下一頁，或是 [&lt;b&gt;上一步&lt;/b&gt;] 按鈕回到上一頁。 如果您想要取消精靈的執行，您也可以按下 [&lt;b&gt;取消&lt;/b&gt;]。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Welcome to the First Run Wizard!</source>
        <translation>歡迎使用「首次執行精靈」!</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the type of media you would like to use for installation.&lt;/p&gt;</source>
        <translation>&lt;p&gt;選取您希望用來安裝的媒體類型。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Media Type</source>
        <translation>媒體類型</translation>
    </message>
    <message>
        <source>&amp;CD/DVD-ROM Device</source>
        <translation>CD/DVD-ROM 裝置(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Floppy Device</source>
        <translation>軟碟裝置(&amp;F)</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the media which contains the setup program of the operating system you want to install. This media must be bootable, otherwise the setup program will not be able to start.&lt;/p&gt;</source>
        <translation>&lt;p&gt;選取您想要安裝包含作業系統安裝程式的媒體。 這個媒體必須是可開機，否則作業系統將無法啟動。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Media Source</source>
        <translation>媒體來源</translation>
    </message>
    <message>
        <source>Select Installation Media</source>
        <translation>選取安裝媒體</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have selected the following media to boot from:&lt;/p&gt;</source>
        <translation>&lt;p&gt;您已選取以下媒體來開機: &lt;/p&gt;</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>摘要</translation>
    </message>
    <message>
        <source>CD/DVD-ROM Device</source>
        <translation>CD/DVD-ROM 裝置</translation>
    </message>
    <message>
        <source>Floppy Device</source>
        <translation>軟碟裝置</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have started a newly created virtual machine for the first time. This wizard will help you to perform the steps necessary for booting an operating system of your choice on the virtual machine.&lt;/p&gt;&lt;p&gt;Note that you will not be able to install an operating system into this virtual machine right now because you did not attach any hard disk to it. If this is not what you want, you can cancel the execution of this wizard, select &lt;b&gt;Settings&lt;/b&gt; from the &lt;b&gt;Machine&lt;/b&gt; menu of the main VirtualBox window to access the settings dialog of this machine and change the hard disk configuration.&lt;/p&gt;&lt;p&gt;Use the &lt;b&gt;Next&lt;/b&gt; button to go to the next page of the wizard and the &lt;b&gt;Back&lt;/b&gt; button to return to the previous page. You can also press &lt;b&gt;Cancel&lt;/b&gt; if you want to cancel the execution of this wizard.&lt;/p&gt;</source>
        <translation>&lt;p&gt;您已首次啟動新建立的虛擬機器。 這個精靈將協助您執行必要步驟，以在虛擬機器中啟動您選擇的作業系統。&lt;/p&gt;&lt;p&gt;請注意，您現在無法將作業系統安裝到這個虛擬機器，因為您未附加任何硬碟。 如果這不是您所要的，您可取消這個精靈的執行，藉由從 VirtualBox 主視窗的 &lt;b&gt;機器&lt;/b&gt; 選單中選擇 &lt;b&gt;設定值&lt;/b&gt;，以存取這個機器的設定值對話方塊並變更硬碟組態。&lt;/p&gt;&lt;p&gt;使用 [&lt;b&gt;下一步&lt;/b&gt;] 按鈕前往精靈的下一頁，或是 [&lt;b&gt;上一步&lt;/b&gt;] 按鈕回到上一頁。如果您想要取消精靈的執行，您也可以按下 [&lt;b&gt;取消&lt;/b&gt;]。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the type of media you would like to use for booting an operating system.&lt;/p&gt;</source>
        <translation>&lt;p&gt;選取您希望作業系統用來開機的媒體類型。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Select the media that contains the operating system you want to work with. This media must be bootable, otherwise the operating system will not be able to start.&lt;/p&gt;</source>
        <translation>&lt;p&gt;選取您想要工作包含作業系統的媒體。 這個媒體必須是可開機，否則作業系統將無法啟動。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;You have selected the following media to boot an operating system from:&lt;/p&gt;</source>
        <translation>&lt;p&gt;您已選取以下媒體來從作業系統開機: &lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;If the above is correct, press the &lt;b&gt;Finish&lt;/b&gt; button. Once you press it, the selected media will be mounted on the virtual machine and the machine will start execution.&lt;/p&gt;</source>
        <translation>&lt;p&gt;如果上述正確，請按下 [&lt;b&gt;結束&lt;/b&gt;] 鈕。一旦您按下它，選取的媒體將掛載到虛擬機器，而機器將會開始執行。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; 上一步(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>下一步(&amp;N) &gt;</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation>完成(&amp;F)</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>summary</comment>
        <translation>類型</translation>
    </message>
    <message>
        <source>Source</source>
        <comment>summary</comment>
        <translation>來源</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>&lt;p&gt;If the above is correct, press the &lt;b&gt;Finish&lt;/b&gt; button. Once you press it, the selected media will be temporarily mounted on the virtual machine and the machine will start execution.&lt;/p&gt;&lt;p&gt;Please note that when you close the virtual machine, the specified media will be automatically unmounted and the boot device will be set back to the first hard disk.&lt;/p&gt;&lt;p&gt;Depending on the type of the setup program, you may need to manually unmount (eject) the media after the setup program reboots the virtual machine, to prevent the installation process from starting again. You can do this by selecting the corresponding &lt;b&gt;Unmount...&lt;/b&gt; action in the &lt;b&gt;Devices&lt;/b&gt; menu.&lt;/p&gt;</source>
        <translation>&lt;p&gt;如果以上正確，請按下 [&lt;b&gt;結束&lt;/b&gt;] 鈕。一旦您按下它，選取的媒體將暫時掛載到虛擬機器且機器將會開始執行。&lt;/p&gt;&lt;p&gt;請注意，當您關閉虛擬機器，指定的媒體將自動卸載且開機裝只將回到第一個硬碟。&lt;/p&gt;&lt;p&gt;根據安裝程式類型，您可能需要手動掛載 (退出) 媒體在安裝程式重新啟動虛擬機器之後，以防止安裝程序再次啟動。 您可以在 &lt;b&gt;裝置&lt;/b&gt; 功能表選取相對的 &lt;b&gt;卸載...&lt;/b&gt; 來進行。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>VBoxVMInformationDlg</name>
    <message>
        <source>%1 - Session Information</source>
        <translation>%1 - 工作階段資訊</translation>
    </message>
    <message>
        <source>&amp;Details</source>
        <translation>詳細資料(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Runtime</source>
        <translation>執行階段(&amp;R)</translation>
    </message>
    <message>
        <source>DMA Transfers</source>
        <translation>DMA 傳輸</translation>
    </message>
    <message>
        <source>PIO Transfers</source>
        <translation>PIO 傳輸</translation>
    </message>
    <message>
        <source>Data Read</source>
        <translation>資料讀取</translation>
    </message>
    <message>
        <source>Data Written</source>
        <translation>資料寫入</translation>
    </message>
    <message>
        <source>Data Transmitted</source>
        <translation>資料已傳送</translation>
    </message>
    <message>
        <source>Data Received</source>
        <translation>資料已接收</translation>
    </message>
    <message>
        <source>Runtime Attributes</source>
        <translation>執行階段屬性</translation>
    </message>
    <message>
        <source>Screen Resolution</source>
        <translation>螢幕解析度</translation>
    </message>
    <message>
        <source>Version %1.%2</source>
        <comment>guest additions</comment>
        <translation>版本 %1.%2</translation>
    </message>
    <message>
        <source>Not Detected</source>
        <comment>guest additions</comment>
        <translation>未偵測到</translation>
    </message>
    <message>
        <source>Not Detected</source>
        <comment>guest os type</comment>
        <translation>未偵測到</translation>
    </message>
    <message>
        <source>Guest Additions</source>
        <translation>Guest Additions</translation>
    </message>
    <message>
        <source>Guest OS Type</source>
        <translation>客體作業系統類型</translation>
    </message>
    <message>
        <source>No Network Adapters</source>
        <translation>無網路卡</translation>
    </message>
    <message>
        <source>Enabled</source>
        <comment>nested paging</comment>
        <translation>啟用</translation>
    </message>
    <message>
        <source>Disabled</source>
        <comment>nested paging</comment>
        <translation>停用</translation>
    </message>
    <message>
        <source>Nested Paging</source>
        <translation>巢狀分頁</translation>
    </message>
    <message>
        <source>VBoxVMInformationDlg</source>
        <translation>VBoxVMInformationDlg</translation>
    </message>
    <message>
        <source>Not Available</source>
        <comment>details report (VRDP server port)</comment>
        <translation>不可用</translation>
    </message>
    <message>
        <source>Storage Statistics</source>
        <translation>存放裝置統計</translation>
    </message>
    <message>
        <source>No Storage Devices</source>
        <translation>無存放裝置</translation>
    </message>
    <message>
        <source>Network Statistics</source>
        <translation>網路狀態</translation>
    </message>
</context>
<context>
    <name>VBoxVMListView</name>
    <message>
        <source>Inaccessible</source>
        <translation>無法存取</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;%1&lt;br&gt;&lt;/nobr&gt;&lt;nobr&gt;%2 since %3&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Session %4&lt;/nobr&gt;</source>
        <comment>VM tooltip (name, last state change, session state)</comment>
        <translation>&lt;nobr&gt;%1&lt;br&gt;&lt;/nobr&gt;&lt;nobr&gt;%2 自從 %3&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;工作階段 %4&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;/nobr&gt;&lt;nobr&gt;Inaccessible since %2&lt;/nobr&gt;</source>
        <comment>Inaccessible VM tooltip (name, last state change)</comment>
        <translation>&lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br&gt;&lt;/nobr&gt;&lt;nobr&gt;無法存取自從 %2&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>S&amp;how</source>
        <translation>顯示(&amp;H)</translation>
    </message>
    <message>
        <source>Switch to the window of the selected virtual machine</source>
        <translation>切換到選取虛擬機器的視窗</translation>
    </message>
    <message>
        <source>S&amp;tart</source>
        <translation>啟動(&amp;T)</translation>
    </message>
    <message>
        <source>Start the selected virtual machine</source>
        <translation>啟動選取的虛擬機器</translation>
    </message>
    <message>
        <source>R&amp;esume</source>
        <translation>繼續執行(&amp;E)</translation>
    </message>
    <message>
        <source>Resume the execution of the virtual machine</source>
        <translation>繼續執行虛擬機器</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>暫停(&amp;P)</translation>
    </message>
    <message>
        <source>Suspend the execution of the virtual machine</source>
        <translation>暫停虛擬機器的執行</translation>
    </message>
</context>
<context>
    <name>VBoxVMLogViewer</name>
    <message>
        <source>Log Viewer</source>
        <translation>記錄檢視器</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>儲存(&amp;S)</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation>重新整理(&amp;R)</translation>
    </message>
    <message>
        <source>%1 - VirtualBox Log Viewer</source>
        <translation>%1 - VirtualBox 記錄檢視器</translation>
    </message>
    <message>
        <source>&lt;p&gt;No log files found. Press the &lt;b&gt;Refresh&lt;/b&gt; button to rescan the log folder &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;找不到記錄檔案。按下 [&lt;b&gt;重新整理&lt;/b&gt;] 按鈕以重新掃瞄記錄資料夾 &lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Save VirtualBox Log As</source>
        <translation>儲存 VirtualBox 記錄為</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>尋找(&amp;F)</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsAudio</name>
    <message>
        <source>When checked, a virtual PCI audio card will be plugged into the virtual machine and will communicate with the host audio system using the specified driver.</source>
        <translation>勾選時，虛擬 PCI 音效卡將插入到虛擬機器並使用指定的驅動程式與主機音訊系統通訊。</translation>
    </message>
    <message>
        <source>Enable &amp;Audio</source>
        <translation>啟用音效(&amp;A)</translation>
    </message>
    <message>
        <source>Host Audio &amp;Driver:</source>
        <translation>主機音效驅動程式(&amp;D):</translation>
    </message>
    <message>
        <source>Controls the audio output driver. The &lt;b&gt;Null Audio Driver&lt;/b&gt; makes the guest see an audio card, however every access to it will be ignored.</source>
        <translation>控制音效輸出驅動程式。 &lt;b&gt;空的音效驅動程式&lt;/b&gt; 使客體擁有音效卡，然而會忽略它的聲音輸出。</translation>
    </message>
    <message>
        <source>Audio &amp;Controller:</source>
        <translation>音效控制器(&amp;C):</translation>
    </message>
    <message>
        <source>Selects the type of the virtual sound card. Depending on this value, VirtualBox will provide different audio hardware to the virtual machine.</source>
        <translation>選擇虛擬音效卡的類型。 VirtualBox 將在虛擬機器提供不同的音效硬體，取決於這個值。</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsDisplay</name>
    <message>
        <source>you have assigned less than &lt;b&gt;%1&lt;/b&gt; of video memory which is the minimum amount required to switch the virtual machine to fullscreen or seamless mode.</source>
        <translation>您已指派少於 &lt;b&gt;%1&lt;/b&gt; 的視訊記憶體，這是切換虛擬機器為全螢幕或無縫模式所需的最小量。</translation>
    </message>
    <message>
        <source>&lt;qt&gt;%1&amp;nbsp;MB&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;%1&amp;nbsp;MB&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&amp;Video</source>
        <translation>視訊(&amp;V)</translation>
    </message>
    <message>
        <source>Video &amp;Memory:</source>
        <translation>視訊記憶體(&amp;M):</translation>
    </message>
    <message>
        <source>Controls the amount of video memory provided to the virtual machine.</source>
        <translation>控制要提供給虛擬機器的視訊記憶體數量。</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>Extended Features:</source>
        <translation>延伸功能:</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will be given access to the 3D graphics capabilities available on the host.</source>
        <translation>勾選時，虛擬機器將給予存取在主機可用的 3D 圖形能力。</translation>
    </message>
    <message>
        <source>Enable &amp;3D Acceleration</source>
        <translation>啟用 3D 加速(&amp;3)</translation>
    </message>
    <message>
        <source>&amp;Remote Display</source>
        <translation>遠端顯示(&amp;R)</translation>
    </message>
    <message>
        <source>When checked, the VM will act as a Remote Desktop Protocol (RDP) server, allowing remote clients to connect and operate the VM (when it is running) using a standard RDP client.</source>
        <translation>勾選時，虛擬機器將作為遠端桌面協定 (RDP) 伺服器，允許遠端用戶端使用標準的 RDP 用戶端連線與操作虛擬機器 (當它正在執行時)。</translation>
    </message>
    <message>
        <source>&amp;Enable Server</source>
        <translation>啟用伺服器(&amp;E)</translation>
    </message>
    <message>
        <source>Server &amp;Port:</source>
        <translation>伺服器埠(&amp;P):</translation>
    </message>
    <message>
        <source>Authentication &amp;Method:</source>
        <translation>驗證方法(&amp;M):</translation>
    </message>
    <message>
        <source>Defines the VRDP authentication method.</source>
        <translation>定義 VRDP 驗證方法。</translation>
    </message>
    <message>
        <source>Authentication &amp;Timeout:</source>
        <translation>驗證逾時(&amp;T):</translation>
    </message>
    <message>
        <source>Specifies the timeout for guest authentication, in milliseconds.</source>
        <translation>指定客體驗證的逾時，以毫秒為單位。</translation>
    </message>
    <message>
        <source>you have assigned less than &lt;b&gt;%1&lt;/b&gt; of video memory which is the minimum amount required for HD Video to be played efficiently.</source>
        <translation>您已指派少於 &lt;b&gt;%1&lt;/b&gt; 的視訊記憶體，這是有效播放 HD 視訊所需的最小量。</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will be given access to the Video Acceleration capabilities available on the host.</source>
        <translation>勾選時，虛擬機器將給予存取在主機可用的視訊加速能力。</translation>
    </message>
    <message>
        <source>Enable &amp;2D Video Acceleration</source>
        <translation>啟用 2D 視訊加速(&amp;2)</translation>
    </message>
    <message>
        <source>The VRDP Server port number. You may specify &lt;tt&gt;0&lt;/tt&gt; (zero), to select port 3389, the standard port for RDP.</source>
        <translation>VRDP 伺服器連接埠號。 您可以指定 &lt;tt&gt;0&lt;/tt&gt; (零)，來選擇連接埠 3389， RDP 的標準連接埠。</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsDlg</name>
    <message>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <source>Storage</source>
        <translation>存放裝置</translation>
    </message>
    <message>
        <source>Hard Disks</source>
        <translation>硬碟</translation>
    </message>
    <message>
        <source>CD/DVD-ROM</source>
        <translation>CD/DVD-ROM</translation>
    </message>
    <message>
        <source>Floppy</source>
        <translation>軟碟</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>音效</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>網路</translation>
    </message>
    <message>
        <source>Ports</source>
        <translation>連接埠</translation>
    </message>
    <message>
        <source>Serial Ports</source>
        <translation>序列埠</translation>
    </message>
    <message>
        <source>Parallel Ports</source>
        <translation>串列埠</translation>
    </message>
    <message>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <source>Shared Folders</source>
        <translation>共用資料夾</translation>
    </message>
    <message>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <source>System</source>
        <translation>系統</translation>
    </message>
    <message>
        <source>Display</source>
        <translation>顯示</translation>
    </message>
    <message>
        <source>you have selected a 64-bit guest OS type for this VM. As such guests require hardware virtualization (VT-x/AMD-V), this feature will be enabled automatically.</source>
        <translation>您已針對這個 VM 選取 64 位元的客體作業系統類型。 如此的客體需要硬體虛擬化 (VT-x/AMD-V)，這個功能將會自動啟用。</translation>
    </message>
    <message>
        <source>you have selected a 64-bit guest OS type for this VM. VirtualBox does not currently support more than one virtual CPU for 64-bit guests executed on 32-bit hosts.</source>
        <translation>您已針對這個 VM 選取 64 位元的客體作業系統類型。 VirtualBox 目前不支援在 32 位元主機執行多個虛擬 CPU 的 64 位元客體。</translation>
    </message>
    <message>
        <source>you have 2D Video Acceleration enabled. As 2D Video Acceleration is supported for Windows guests only, this feature will be disabled.</source>
        <translation>您已啟用 2D 視訊加速。 由於 2D 視訊加速只支援 Windows 客體，這個功能將停用。</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsGeneral</name>
    <message>
        <source>Displays the path where snapshots of this virtual machine will be stored. Be aware that snapshots can take quite a lot of disk space.</source>
        <translation>顯示將儲存這個虛擬機器快照的路徑。 注意，快照可能需要相當多的磁碟空間。</translation>
    </message>
    <message>
        <source>&amp;Basic</source>
        <translation>基本(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>名稱(&amp;N):</translation>
    </message>
    <message>
        <source>Displays the name of the virtual machine.</source>
        <translation>顯示虛擬機器的名稱。</translation>
    </message>
    <message>
        <source>&amp;Advanced</source>
        <translation>進階(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;Shared Clipboard:</source>
        <translation>共用剪貼簿(&amp;S):</translation>
    </message>
    <message>
        <source>Selects which clipboard data will be copied between the guest and the host OS. This feature requires Guest Additions to be installed in the guest OS.</source>
        <translation>選取在客體與主機作業系統之間將複製哪個剪貼簿資料。 這個功能需要在客體作業系統安裝 Guest Additions 。</translation>
    </message>
    <message>
        <source>S&amp;napshot Folder:</source>
        <translation>快照資料夾(&amp;N):</translation>
    </message>
    <message>
        <source>&amp;Description</source>
        <translation>描述(&amp;D)</translation>
    </message>
    <message>
        <source>Displays the description of the virtual machine. The description field is useful for commenting on configuration details of the installed guest OS.</source>
        <translation>顯示虛擬機器的描述。 描述欄位對於註解已安裝的客體作業系統之組態詳細資料是有用的。</translation>
    </message>
    <message>
        <source>If checked, any change to mounted CD/DVD or Floppy media performed during machine execution will be saved in the settings file in order to preserve the configuration of mounted media between runs.</source>
        <translation>如果勾選，任何在機器執行期間對於掛載的光碟與軟碟所進行的變更，將會儲存在設定檔之中，以便於每次執行之間保存掛載媒體的組態。</translation>
    </message>
    <message>
        <source>Removable Media:</source>
        <translation>卸除式媒體:</translation>
    </message>
    <message>
        <source>&amp;Remember Runtime Changes</source>
        <translation>記住執行階段變更(&amp;R)</translation>
    </message>
    <message>
        <source>Mini ToolBar:</source>
        <translation>迷你工具列:</translation>
    </message>
    <message>
        <source>If checked, show the Mini ToolBar in Fullscreen and Seamless modes.</source>
        <translation>如果勾選，在全螢幕及無縫模式顯示迷你工具列。</translation>
    </message>
    <message>
        <source>Show In &amp;Fullscreen/Seamless</source>
        <translation>在全螢幕/無縫模式顯示(&amp;F)</translation>
    </message>
    <message>
        <source>If checked, show the Mini ToolBar at the top of the screen, rather than in its default position at the bottom of the screen.</source>
        <translation>如果勾選，在螢幕上方顯示迷你工具列，而不是它的預設位置在螢幕的下方。</translation>
    </message>
    <message>
        <source>Show At &amp;Top Of Screen</source>
        <translation>顯示在螢幕上方(&amp;T)</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsHD</name>
    <message>
        <source>If checked, shows the differencing hard disks that are attached to slots rather than their base hard disks (shown for indirect attachments) and allows explicit attaching of differencing hard disks. Check this only if you need a complex hard disk setup.</source>
        <translation>如果勾選，顯示附加到插槽的差異硬碟而不是它們的基礎硬碟 (顯示間接附件) 並允許附加明確的差異硬碟。 勾選此，如果您需要複雜的硬碟設定。</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Bus:&amp;nbsp;&amp;nbsp;%2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;Type:&amp;nbsp;&amp;nbsp;%3&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;匯流排:&amp;nbsp;&amp;nbsp;%2&lt;/nobr&gt;&lt;br&gt;&lt;nobr&gt;類型:&amp;nbsp;&amp;nbsp;%3&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Expand/Collapse&amp;nbsp;Item&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;展開/摺疊項目&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Add&amp;nbsp;Hard&amp;nbsp;Disk&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;加入硬碟&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Add&amp;nbsp;CD/DVD&amp;nbsp;Device&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;加入&amp;nbsp;CD/DVD&amp;nbsp;裝置&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>&lt;nobr&gt;Add&amp;nbsp;Floppy&amp;nbsp;Device&lt;/nobr&gt;</source>
        <translation>&lt;nobr&gt;加入軟碟裝置&lt;/nobr&gt;</translation>
    </message>
    <message>
        <source>No hard disk is selected for &lt;i&gt;%1&lt;/i&gt;.</source>
        <translation>未選取 &lt;i&gt;%1&lt;/i&gt; 的硬碟。</translation>
    </message>
    <message>
        <source>&lt;i&gt;%1&lt;/i&gt; uses a medium that is already attached to &lt;i&gt;%2&lt;/i&gt;.</source>
        <translation>&lt;i&gt;%1&lt;/i&gt; 使用已經附加到 &lt;i&gt;%2&lt;/i&gt; 的媒體。</translation>
    </message>
    <message>
        <source>Add Controller</source>
        <translation>加入控制器</translation>
    </message>
    <message>
        <source>Add IDE Controller</source>
        <translation>加入 IDE 控制器</translation>
    </message>
    <message>
        <source>Add SATA Controller</source>
        <translation>加入 SATA 控制器</translation>
    </message>
    <message>
        <source>Add SCSI Controller</source>
        <translation>加入 SCSI 控制器</translation>
    </message>
    <message>
        <source>Add Floppy Controller</source>
        <translation>加入軟碟控制器</translation>
    </message>
    <message>
        <source>Remove Controller</source>
        <translation>移除控制器</translation>
    </message>
    <message>
        <source>Add Attachment</source>
        <translation>加入附件</translation>
    </message>
    <message>
        <source>Add Hard Disk</source>
        <translation>加入硬碟</translation>
    </message>
    <message>
        <source>Add CD/DVD Device</source>
        <translation>加入 CD/DVD 裝置</translation>
    </message>
    <message>
        <source>Add Floppy Device</source>
        <translation>加入軟碟裝置</translation>
    </message>
    <message>
        <source>Remove Attachment</source>
        <translation>移除附件</translation>
    </message>
    <message>
        <source>Adds a new controller to the end of the Storage Tree.</source>
        <translation>新增控制器至存放裝置樹的結尾。</translation>
    </message>
    <message>
        <source>Removes the controller highlighted in the Storage Tree.</source>
        <translation>移除存放裝置樹中反白的控制器。</translation>
    </message>
    <message>
        <source>Adds a new attachment to the Storage Tree using currently selected controller as parent.</source>
        <translation>使用目前選取的控制器新增附件至存放裝置樹為上層。</translation>
    </message>
    <message>
        <source>Removes the attachment highlighted in the Storage Tree.</source>
        <translation>移除存放裝置樹中反白的附件。</translation>
    </message>
    <message>
        <source>IDE Controller</source>
        <translation>IDE 控制器</translation>
    </message>
    <message>
        <source>SATA Controller</source>
        <translation>SATA 控制器</translation>
    </message>
    <message>
        <source>SCSI Controller</source>
        <translation>SCSI 控制器</translation>
    </message>
    <message>
        <source>Floppy Controller</source>
        <translation>軟碟控制器</translation>
    </message>
    <message>
        <source>Hard &amp;Disk:</source>
        <translation>硬碟(&amp;D):</translation>
    </message>
    <message>
        <source>&amp;CD/DVD Device:</source>
        <translation>CD/DVD 裝置(&amp;C):</translation>
    </message>
    <message>
        <source>&amp;Floppy Device:</source>
        <translation>軟碟裝置(&amp;F):</translation>
    </message>
    <message>
        <source>&amp;Storage Tree</source>
        <translation>存放裝置樹(&amp;S)</translation>
    </message>
    <message>
        <source>Contains all storage controllers for this machine and the virtual images and host drives attached to them.</source>
        <translation>包含這個機器的所有存放控制器與附加到它們的虛擬機器及主機裝置。</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>資訊</translation>
    </message>
    <message>
        <source>The Storage Tree can contain several controllers of different types. This machine currently has no controllers.</source>
        <translation>存放裝置樹能包含數個不同類型的控制器。 這個機器目前沒有控制器。</translation>
    </message>
    <message>
        <source>Attributes</source>
        <translation>屬性</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>名稱(&amp;N):</translation>
    </message>
    <message>
        <source>Changes the name of the storage controller currently selected in the Storage Tree.</source>
        <translation>變更目前在存放裝置樹選取的儲存控制器名稱。</translation>
    </message>
    <message>
        <source>&amp;Type:</source>
        <translation>類型(&amp;T):</translation>
    </message>
    <message>
        <source>Selects the sub-type of the storage controller currently selected in the Storage Tree.</source>
        <translation>選取目前在存放裝置樹選取的存放控制器的子類型。</translation>
    </message>
    <message>
        <source>S&amp;lot:</source>
        <translation>插槽(&amp;L):</translation>
    </message>
    <message>
        <source>Selects the slot on the storage controller used by this attachment. The available slots depend on the type of the controller and other attachments on it.</source>
        <translation>選取這個附件所使用的存放控制器的插槽。 可用插槽根據控制器的類型與其它附加的附件而定。</translation>
    </message>
    <message>
        <source>Selects the virtual disk image or the host drive used by this attachment.</source>
        <translation>選取這個附件使用的虛擬磁碟映像或主機磁碟機。</translation>
    </message>
    <message>
        <source>Opens the Virtual Media Manager to select a virtual image for this attachment.</source>
        <translation>開啟虛擬媒體管理員來選取這個附件的虛擬映像。</translation>
    </message>
    <message>
        <source>Open Virtual Media Manager</source>
        <translation>開啟虛擬媒體管理員</translation>
    </message>
    <message>
        <source>D&amp;ifferencing Disks</source>
        <translation>差異磁碟(&amp;I)</translation>
    </message>
    <message>
        <source>When checked, allows the guest to send ATAPI commands directly to the host-drive which makes it possible to use CD/DVD writers connected to the host inside the VM. Note that writing audio CD inside the VM is not yet supported.</source>
        <translation>勾選時，允許客體直接傳送 ATAPI 命令至主機光碟機，如此才能在虛擬機器中使用連接到主機的 CD/DVD 燒錄機。 請注意，目前尚未支援在虛擬機器中燒錄音樂 CD。</translation>
    </message>
    <message>
        <source>&amp;Passthrough</source>
        <translation>透通(&amp;P)</translation>
    </message>
    <message>
        <source>Virtual Size:</source>
        <translation>虛擬大小:</translation>
    </message>
    <message>
        <source>Actual Size:</source>
        <translation>實際大小:</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation>大小:</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation>位置:</translation>
    </message>
    <message>
        <source>Type (Format):</source>
        <translation>類型 (格式):</translation>
    </message>
    <message>
        <source>Attached To:</source>
        <translation>附加到:</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsNetwork</name>
    <message>
        <source>When checked, plugs this virtual network adapter into the virtual machine.</source>
        <translation>勾選時，會將這個虛擬網路卡插入虛擬機器內。</translation>
    </message>
    <message>
        <source>&amp;Enable Network Adapter</source>
        <translation>啟用網路卡(&amp;E)</translation>
    </message>
    <message>
        <source>Selects the type of the virtual network adapter. Depending on this value, VirtualBox will provide different network hardware to the virtual machine.</source>
        <translation>選擇虛擬網路卡的類型。 VirtualBox 將取決於這個值提供不同的網路硬體到虛擬機器。</translation>
    </message>
    <message>
        <source>&amp;Attached to:</source>
        <translation>附加到(&amp;A):</translation>
    </message>
    <message>
        <source>Controls how this virtual adapter is attached to the real network of the Host OS.</source>
        <translation>控制這個虛擬介面卡如何附加到主機作業系統的真實網路。</translation>
    </message>
    <message>
        <source>Adapter &amp;Type:</source>
        <translation>介面卡類型(&amp;T):</translation>
    </message>
    <message>
        <source>no bridged network adapter is selected</source>
        <translation>未選取橋接網路卡</translation>
    </message>
    <message>
        <source>no internal network name is specified</source>
        <translation>未指定內部網路名稱</translation>
    </message>
    <message>
        <source>no host-only network adapter is selected</source>
        <translation>未選取「僅限主機」網路卡</translation>
    </message>
    <message>
        <source>Not selected</source>
        <comment>network adapter name</comment>
        <translation>未選取</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>名稱(&amp;N):</translation>
    </message>
    <message>
        <source>Selects the name of the network adapter for &lt;b&gt;Bridged Adapter&lt;/b&gt; or &lt;b&gt;Host-only Adapter&lt;/b&gt; attachments and the name of the network &lt;b&gt;Internal Network&lt;/b&gt; attachments.</source>
        <translation>選取&lt;b&gt;橋接介面卡&lt;/b&gt;或&lt;b&gt;「僅限主機」介面卡&lt;/b&gt;附件的網路卡名稱與&lt;b&gt;內部網路&lt;/b&gt;附件的網路名稱。</translation>
    </message>
    <message>
        <source>A&amp;dvanced</source>
        <translation>進階(&amp;D)</translation>
    </message>
    <message>
        <source>Shows or hides additional network adapter options.</source>
        <translation>顯示或隱藏額外網路卡選項。</translation>
    </message>
    <message>
        <source>&amp;Mac Address:</source>
        <translation>Mac 位址(&amp;M):</translation>
    </message>
    <message>
        <source>Displays the MAC address of this adapter. It contains exactly 12 characters chosen from {0-9,A-F}. Note that the second character must be an even digit.</source>
        <translation>顯示這個介面卡的 MAC 位址。 它包含確切地 12 個字元從 {0-9,A-F} 選擇。 請注意第二個字元必須是一個偶數數位。</translation>
    </message>
    <message>
        <source>Generates a new random MAC address.</source>
        <translation>產生新的隨機 MAC 位址。</translation>
    </message>
    <message>
        <source>Indicates whether the virtual network cable is plugged in on machine startup or not.</source>
        <translation>指示機器啟動時是否已插入虛擬網路線。</translation>
    </message>
    <message>
        <source>&amp;Cable connected</source>
        <translation>線路已連接(&amp;C)</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsParallel</name>
    <message>
        <source>Port %1</source>
        <comment>parallel ports</comment>
        <translation>連接埠 %1</translation>
    </message>
    <message>
        <source>When checked, enables the given parallel port of the virtual machine.</source>
        <translation>勾選時，表示啟用給予的虛擬機器串列埠。</translation>
    </message>
    <message>
        <source>&amp;Enable Parallel Port</source>
        <translation>啟用串列埠(&amp;E)</translation>
    </message>
    <message>
        <source>Port &amp;Number:</source>
        <translation>連接埠號(&amp;N):</translation>
    </message>
    <message>
        <source>Displays the parallel port number. You can choose one of the standard parallel ports or select &lt;b&gt;User-defined&lt;/b&gt; and specify port parameters manually.</source>
        <translation>顯示串列埠號。您可以選擇標準串列埠之一，或是選擇&lt;b&gt;使用者定義&lt;/b&gt;並手動指定連接埠參數。</translation>
    </message>
    <message>
        <source>&amp;IRQ:</source>
        <translation>&amp;IRQ:</translation>
    </message>
    <message>
        <source>I/O Po&amp;rt:</source>
        <translation>I/O 連接埠(&amp;R):</translation>
    </message>
    <message>
        <source>Port &amp;Path:</source>
        <translation>連接埠路徑(&amp;P):</translation>
    </message>
    <message>
        <source>Displays the host parallel device name.</source>
        <translation>顯示主機串列埠裝置名稱。</translation>
    </message>
    <message>
        <source>Displays the IRQ number of this parallel port. This should be a whole number between &lt;tt&gt;0&lt;/tt&gt; and &lt;tt&gt;255&lt;/tt&gt;. Values greater than &lt;tt&gt;15&lt;/tt&gt; may only be used if the &lt;b&gt;IO APIC&lt;/b&gt; setting is enabled for this virtual machine.</source>
        <translation>顯示這個串列埠的 IRQ 號碼。這應該是介於 &lt;tt&gt;0&lt;/tt&gt; 與 &lt;tt&gt;255&lt;/tt&gt; 之間的整數。 只有當這個虛擬機器啟用 &lt;b&gt;IO APIC&lt;/b&gt; 時，才能使用大於 &lt;tt&gt;15&lt;/tt&gt; 的值。</translation>
    </message>
    <message>
        <source>Displays the base I/O port address of this parallel port. Valid values are integer numbers in range from &lt;tt&gt;0&lt;/tt&gt; to &lt;tt&gt;0xFFFF&lt;/tt&gt;.</source>
        <translation>顯示這個串列埠的基礎 I/O 連接埠位址。有效值為從 &lt;tt&gt;0&lt;/tt&gt; 到 &lt;tt&gt;0xFFFF&lt;/tt&gt; 的整數。</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsParallelPage</name>
    <message>
        <source>Duplicate port number selected </source>
        <translation>選取重複的連接埠號</translation>
    </message>
    <message>
        <source>Port path not specified </source>
        <translation>未指定連接埠路徑</translation>
    </message>
    <message>
        <source>Duplicate port path entered </source>
        <translation>輸入重複的連接埠路徑</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSF</name>
    <message>
        <source>Adds a new shared folder definition.</source>
        <translation>加入新的共用資料夾定義。</translation>
    </message>
    <message>
        <source>Edits the selected shared folder definition.</source>
        <translation>編輯選取的共用資料夾定義。</translation>
    </message>
    <message>
        <source>Removes the selected shared folder definition.</source>
        <translation>移除選取的共用資料夾定義。</translation>
    </message>
    <message>
        <source> Machine Folders</source>
        <translation>機器資料夾</translation>
    </message>
    <message>
        <source> Transient Folders</source>
        <translation>瞬變資料夾</translation>
    </message>
    <message>
        <source>Full</source>
        <translation>完整</translation>
    </message>
    <message>
        <source>Read-only</source>
        <translation>唯讀</translation>
    </message>
    <message>
        <source>Lists all shared folders accessible to this machine. Use &apos;net use x: \\vboxsvr\share&apos; to access a shared folder named &lt;i&gt;share&lt;/i&gt; from a DOS-like OS, or &apos;mount -t vboxsf share mount_point&apos; to access it from a Linux OS. This feature requires Guest Additions.</source>
        <translation>列出所有可存取這個機器的共用資料夾。 從類似 DOS 的作業系統使用 &apos;net use x: \\vboxsvr\share&apos; 存取名為 &lt;i&gt; share &lt;/i&gt; 的共用資料夾，或從 Linux 作業系統 &apos;mount -t vboxsf share mount_point&apos; 來存取它。 這個功能需要 Guest Additions 。</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>路徑</translation>
    </message>
    <message>
        <source>Access</source>
        <translation>存取</translation>
    </message>
    <message>
        <source> Global Folders</source>
        <translation>全域資料夾</translation>
    </message>
    <message>
        <source>&amp;Add Shared Folder</source>
        <translation>加入共用資料夾(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;Edit Shared Folder</source>
        <translation>編輯共用資料夾(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Remove Shared Folder</source>
        <translation>移除共用資料夾(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Folders List</source>
        <translation>資料夾清單(&amp;F)</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSFDetails</name>
    <message>
        <source>Add Share</source>
        <translation>加入共用</translation>
    </message>
    <message>
        <source>Edit Share</source>
        <translation>編輯共用</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <source>Folder Path:</source>
        <translation>資料夾路徑:</translation>
    </message>
    <message>
        <source>Folder Name:</source>
        <translation>資料夾名稱:</translation>
    </message>
    <message>
        <source>Displays the name of the shared folder (as it will be seen by the guest OS).</source>
        <translation>顯示共用資料夾的名稱 (客體作業系統將看到的)。</translation>
    </message>
    <message>
        <source>When checked, the guest OS will not be able to write to the specified shared folder.</source>
        <translation>勾選時，客體作業系統將不能寫入指定的共用資料夾。</translation>
    </message>
    <message>
        <source>&amp;Read-only</source>
        <translation>唯讀(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Make Permanent</source>
        <translation>成為永久性(&amp;M)</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSerial</name>
    <message>
        <source>Port %1</source>
        <comment>serial ports</comment>
        <translation>連接埠 %1</translation>
    </message>
    <message>
        <source>When checked, enables the given serial port of the virtual machine.</source>
        <translation>勾選時，表示啟用給予的虛擬機器序列埠。</translation>
    </message>
    <message>
        <source>&amp;Enable Serial Port</source>
        <translation>啟用序列埠(&amp;E)</translation>
    </message>
    <message>
        <source>Port &amp;Number:</source>
        <translation>連接埠號(&amp;N):</translation>
    </message>
    <message>
        <source>Displays the serial port number. You can choose one of the standard serial ports or select &lt;b&gt;User-defined&lt;/b&gt; and specify port parameters manually.</source>
        <translation>顯示序列埠號。您可以選擇標準序列埠之一，或是選擇&lt;b&gt;使用者定義&lt;/b&gt;並手動指定連接埠參數。</translation>
    </message>
    <message>
        <source>&amp;IRQ:</source>
        <translation>&amp;IRQ:</translation>
    </message>
    <message>
        <source>I/O Po&amp;rt:</source>
        <translation>I/O 連接埠(&amp;R):</translation>
    </message>
    <message>
        <source>Port &amp;Mode:</source>
        <translation>連接埠模式(&amp;M):</translation>
    </message>
    <message>
        <source>Controls the working mode of this serial port. If you select &lt;b&gt;Disconnected&lt;/b&gt;, the guest OS will detect the serial port but will not be able to operate it.</source>
        <translation>控制這個序列埠的工作模式。如果您選取&lt;b&gt;已中斷連接&lt;/b&gt;，客體作業系統將會偵測到序列埠但無法操作它。</translation>
    </message>
    <message>
        <source>If checked, the pipe specified in the &lt;b&gt;Port Path&lt;/b&gt; field will be created by the virtual machine when it starts. Otherwise, the virtual machine will assume that the pipe exists and try to use it.</source>
        <translation>如果勾選，在&lt;b&gt;連接埠路徑&lt;/b&gt;欄位中指定的管線將由虛擬機器於啟動時建立。 否則，虛擬機器將假設管線存在並嘗試使用它。</translation>
    </message>
    <message>
        <source>&amp;Create Pipe</source>
        <translation>建立管線(&amp;C)</translation>
    </message>
    <message>
        <source>Displays the path to the serial port&apos;s pipe on the host when the port is working in &lt;b&gt;Host Pipe&lt;/b&gt; mode, or the host serial device name when the port is working in &lt;b&gt;Host Device&lt;/b&gt; mode.</source>
        <translation>當連接埠工作在&lt;b&gt;主機管線&lt;/b&gt;模式時顯示主機中序列埠管線的路徑，或者當連接埠工作在&lt;b&gt;主機裝置&lt;/b&gt;模式時顯示主機序列裝置名稱。</translation>
    </message>
    <message>
        <source>Port/File &amp;Path:</source>
        <translation>連接埠/檔案路徑(&amp;P):</translation>
    </message>
    <message>
        <source>Displays the IRQ number of this serial port. This should be a whole number between &lt;tt&gt;0&lt;/tt&gt; and &lt;tt&gt;255&lt;/tt&gt;. Values greater than &lt;tt&gt;15&lt;/tt&gt; may only be used if the &lt;b&gt;IO APIC&lt;/b&gt; setting is enabled for this virtual machine.</source>
        <translation>顯示這個序列埠的 IRQ 號碼。這應該是介於 &lt;tt&gt;0&lt;/tt&gt; 與 &lt;tt&gt;255&lt;/tt&gt; 之間的整數。 只有當這個虛擬機器啟用 &lt;b&gt;IO APIC&lt;/b&gt; 時，才能使用大於 &lt;tt&gt;15&lt;/tt&gt; 的值。</translation>
    </message>
    <message>
        <source>Displays the base I/O port address of this serial port. Valid values are integer numbers in range from &lt;tt&gt;0&lt;/tt&gt; to &lt;tt&gt;0xFFFF&lt;/tt&gt;.</source>
        <translation>顯示這個序列埠的基礎 I/O 連接埠位址。有效值為從 &lt;tt&gt;0&lt;/tt&gt; 到 &lt;tt&gt;0xFFFF&lt;/tt&gt; 的整數。</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSerialPage</name>
    <message>
        <source>Duplicate port number selected </source>
        <translation>選取重複的連接埠號</translation>
    </message>
    <message>
        <source>Port path not specified </source>
        <translation>未指定連接埠路徑</translation>
    </message>
    <message>
        <source>Duplicate port path entered </source>
        <translation>輸入重複的連接埠路徑</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsSystem</name>
    <message>
        <source>you have assigned more than &lt;b&gt;%1%&lt;/b&gt; of your computer&apos;s memory (&lt;b&gt;%2&lt;/b&gt;) to the virtual machine. Not enough memory is left for your host operating system. Please select a smaller amount.</source>
        <translation>您已指派超過您電腦記憶體 (&lt;b&gt;%2&lt;/b&gt;) 的 &lt;b&gt;%1%&lt;/b&gt; 到虛擬機器。 您主機的作業系統沒有足夠的剩餘記憶體。 請選取較小的量。</translation>
    </message>
    <message>
        <source>you have assigned more than &lt;b&gt;%1%&lt;/b&gt; of your computer&apos;s memory (&lt;b&gt;%2&lt;/b&gt;) to the virtual machine. There might not be enough memory left for your host operating system. Continue at your own risk.</source>
        <translation>您已指派超過您電腦記憶體 (&lt;b&gt;%2&lt;/b&gt;) 的 &lt;b&gt;%1%&lt;/b&gt; 到虛擬機器。 您主機的作業系統可能沒有足夠的剩餘記憶體。 繼續則風險自負。</translation>
    </message>
    <message>
        <source>for performance reasons, the number of virtual CPUs attached to the virtual machine may not be more than twice the number of physical CPUs on the host (&lt;b&gt;%1&lt;/b&gt;). Please reduce the number of virtual CPUs.</source>
        <translation>由於效能原因，附加到虛擬機器的虛擬 CPU 數不可多於主機中實體 CPU 數 (&lt;b&gt;%1&lt;/b&gt; 個) 的兩倍。 請降低虛擬 CPU 數。</translation>
    </message>
    <message>
        <source>you have assigned more virtual CPUs to the virtual machine than the number of physical CPUs on your host system (&lt;b&gt;%1&lt;/b&gt;). This is likely to degrade the performance of your virtual machine. Please consider reducing the number of virtual CPUs.</source>
        <translation>您已指派超過您主機系統實體 CPU  (&lt;b&gt;%1&lt;/b&gt; 個) 的虛擬 CPU 到此虛擬機器。 這很可能會降低您虛擬機器的效能。 請考慮降低除虛擬 CPU 數。</translation>
    </message>
    <message>
        <source>you have assigned more than one virtual CPU to this VM. This will not work unless the IO-APIC feature is also enabled. This will be done automatically when you accept the VM Settings by pressing the OK button.</source>
        <translation>您已指派一個以上虛擬 CPU 到此 VM。 除非也啟用 IO-APIC 功能這將不會動作。 當您按下 [確定] 按鈕接受 VM 設定值這將自動完成。</translation>
    </message>
    <message>
        <source>you have assigned more than one virtual CPU to this VM. This will not work unless hardware virtualization (VT-x/AMD-V) is also enabled. This will be done automatically when you accept the VM Settings by pressing the OK button.</source>
        <translation>您已指派一個以上虛擬 CPU 到此 VM。 除非也啟用硬體虛擬化 (VT-x/AMD-V) 否則這將不會動作。 當您按下 [確定] 按鈕接受 VM 設定值這將自動完成。</translation>
    </message>
    <message>
        <source>&lt;qt&gt;%1&amp;nbsp;MB&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;%1&amp;nbsp;MB&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&lt;qt&gt;%1&amp;nbsp;CPU&lt;/qt&gt;</source>
        <comment>%1 is 1 for now</comment>
        <translation>&lt;qt&gt;%1&amp;nbsp;CPU&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&amp;Motherboard</source>
        <translation>主機板(&amp;M)</translation>
    </message>
    <message>
        <source>Base &amp;Memory:</source>
        <translation>基本記憶體(&amp;M):</translation>
    </message>
    <message>
        <source>Controls the amount of memory provided to the virtual machine. If you assign too much, the machine might not start.</source>
        <translation>控制要提供給虛擬機器的記憶體數量。如果您指派太多，機器可能無法啟動。</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>&amp;Boot Order:</source>
        <translation>開機順序(&amp;B):</translation>
    </message>
    <message>
        <source>Defines the boot device order. Use the checkboxes on the left to enable or disable individual boot devices. Move items up and down to change the device order.</source>
        <translation>定義開機裝置順序。 使用左方的核取方塊來啟用或停用個別開機裝置。 上移與下移項目來變更順序。</translation>
    </message>
    <message>
        <source>Move Down (Ctrl-Down)</source>
        <translation>下移 (Ctrl-Down)</translation>
    </message>
    <message>
        <source>Moves the selected boot device down.</source>
        <translation>上移選取的開機裝置。</translation>
    </message>
    <message>
        <source>Move Up (Ctrl-Up)</source>
        <translation>上移 (Ctrl-Up)</translation>
    </message>
    <message>
        <source>Moves the selected boot device up.</source>
        <translation>下移選取的開機裝置。</translation>
    </message>
    <message>
        <source>Extended Features:</source>
        <translation>延伸功能:</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will support the Input Output APIC (IO APIC), which may slightly decrease performance. &lt;b&gt;Note:&lt;/b&gt; don&apos;t disable this feature after having installed a Windows guest operating system!</source>
        <translation>勾選時，虛擬機器將支援 Input Output APIC (IO APIC)，這可能略微降低性能。 &lt;b&gt;注意:&lt;/b&gt; 安裝 Windows 客體作業系統後不要停用這個功能!</translation>
    </message>
    <message>
        <source>Enable &amp;IO APIC</source>
        <translation>啟用 &amp;IO APIC</translation>
    </message>
    <message>
        <source>&amp;Processor</source>
        <translation>處理器(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Processor(s):</source>
        <translation>處理器(&amp;P):</translation>
    </message>
    <message>
        <source>Controls the number of virtual CPUs in the virtual machine. You need hardware virtualization support on your host system to use more than one virtual CPU.</source>
        <translation>控制虛擬機器中的虛擬 CPU 數。</translation>
    </message>
    <message>
        <source>When checked, the Physical Address Extension (PAE) feature of the host CPU will be exposed to the virtual machine.</source>
        <translation>勾選時，主機 CPU 的 Physical Address Extension (PAE) 功能將暴露於虛擬機器。</translation>
    </message>
    <message>
        <source>Enable PA&amp;E/NX</source>
        <translation>啟用 PAE/NX (&amp;E)</translation>
    </message>
    <message>
        <source>Acce&amp;leration</source>
        <translation>加速(&amp;L)</translation>
    </message>
    <message>
        <source>Hardware Virtualization:</source>
        <translation>硬體虛擬化:</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will try to make use of the host CPU&apos;s hardware virtualization extensions such as Intel VT-x and AMD-V.</source>
        <translation>勾選時，虛擬機器將嘗試使用主機 CPU 的硬體虛擬化擴充比如 Intel VT-x 和 AMD-V 。</translation>
    </message>
    <message>
        <source>Enable &amp;VT-x/AMD-V</source>
        <translation>啟用 VT-x/AMD-V (&amp;V)</translation>
    </message>
    <message>
        <source>When checked, the virtual machine will try to make use of the nested paging extension of Intel VT-x and AMD-V.</source>
        <translation>勾選時，虛擬機器將嘗試使用 Intel VT-x 和 AMD-V 的巢狀分頁擴充。</translation>
    </message>
    <message>
        <source>Enable Nested Pa&amp;ging</source>
        <translation>啟用巢狀分頁(&amp;G)</translation>
    </message>
    <message>
        <source>&lt;qt&gt;%1&amp;nbsp;CPUs&lt;/qt&gt;</source>
        <comment>%1 is host cpu count * 2 for now</comment>
        <translation>&lt;qt&gt;%1&amp;nbsp;CPUs&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>When checked, the guest will support the Extended Firmware Interface (EFI), which is required to boot certain guest OSes. Non-EFI aware OSes will not be able to boot if this option is activated.</source>
        <translation>勾選時，客體將支援Extended Firmware Interface (EFI)，某種客體作業系統開機是需要的。 非 EFI 感知的作業系統將無法開機，如果啟用這個選項。</translation>
    </message>
    <message>
        <source>Enable &amp;EFI (special OSes only)</source>
        <translation>啟用 EFI (僅特殊作業系統)(&amp;E)</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsUSB</name>
    <message>
        <source>&amp;Add Empty Filter</source>
        <translation>加入空的篩選器(&amp;A)</translation>
    </message>
    <message>
        <source>A&amp;dd Filter From Device</source>
        <translation>從裝置加入篩選器(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Edit Filter</source>
        <translation>編輯篩選器(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Remove Filter</source>
        <translation>移除篩選器(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Move Filter Up</source>
        <translation>上移篩選器(&amp;M)</translation>
    </message>
    <message>
        <source>M&amp;ove Filter Down</source>
        <translation>下移篩選器(&amp;O)</translation>
    </message>
    <message>
        <source>Adds a new USB filter with all fields initially set to empty strings. Note that such a filter will match any attached USB device.</source>
        <translation>加入一個新的 USB 篩選器與最初設定為空字串的所有欄位。 請注意，這些篩選器會符合任何附加的 USB 裝置。</translation>
    </message>
    <message>
        <source>Adds a new USB filter with all fields set to the values of the selected USB device attached to the host PC.</source>
        <translation>加入一個新的 USB 篩選器，其所有欄位設定為選取 USB 裝置附加到主機 PC 的值。</translation>
    </message>
    <message>
        <source>Edits the selected USB filter.</source>
        <translation>編輯選取的 USB 篩選器。</translation>
    </message>
    <message>
        <source>Removes the selected USB filter.</source>
        <translation>移除選取的 USB 篩選器。</translation>
    </message>
    <message>
        <source>Moves the selected USB filter up.</source>
        <translation>上移選取的 USB 篩選器。</translation>
    </message>
    <message>
        <source>Moves the selected USB filter down.</source>
        <translation>下移選取的 USB 篩選器。</translation>
    </message>
    <message>
        <source>New Filter %1</source>
        <comment>usb</comment>
        <translation>新增篩選器 %1</translation>
    </message>
    <message>
        <source>When checked, enables the virtual USB controller of this machine.</source>
        <translation>勾選時，啟用這個機器的虛擬 USB 控制器。</translation>
    </message>
    <message>
        <source>Enable &amp;USB Controller</source>
        <translation>啟用 USB 控制器(&amp;U)</translation>
    </message>
    <message>
        <source>When checked, enables the virtual USB EHCI controller of this machine. The USB EHCI controller provides USB 2.0 support.</source>
        <translation>勾選時，啟用這個機器的虛擬 USB EHCI 控制器。 USB EHCI 控制器提供 USB 2.0 支援。</translation>
    </message>
    <message>
        <source>Enable USB 2.0 (E&amp;HCI) Controller</source>
        <translation>啟用 USB 2.0 (EHCI) 控制器(&amp;H)</translation>
    </message>
    <message>
        <source>USB Device &amp;Filters</source>
        <translation>USB 裝置篩選器(&amp;F)</translation>
    </message>
    <message>
        <source>Lists all USB filters of this machine. The checkbox to the left defines whether the particular filter is enabled or not. Use the context menu or buttons to the right to add or remove USB filters.</source>
        <translation>列出這個機器的所有 USB 篩選器。 左側的核取方塊定義是否啟用特定的篩選器。 使用內容功能表或右側的按鈕加入或移除 USB 篩選器。</translation>
    </message>
    <message>
        <source>[filter]</source>
        <translation>[篩選器]</translation>
    </message>
</context>
<context>
    <name>VBoxVMSettingsUSBFilterDetails</name>
    <message>
        <source>Any</source>
        <comment>remote</comment>
        <translation>任何</translation>
    </message>
    <message>
        <source>Yes</source>
        <comment>remote</comment>
        <translation>是</translation>
    </message>
    <message>
        <source>No</source>
        <comment>remote</comment>
        <translation>否</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation>名稱(&amp;N):</translation>
    </message>
    <message>
        <source>Displays the filter name.</source>
        <translation>顯示篩選器名稱。</translation>
    </message>
    <message>
        <source>&amp;Vendor ID:</source>
        <translation>供應商 ID (&amp;V):</translation>
    </message>
    <message>
        <source>Defines the vendor ID filter. The &lt;i&gt;exact match&lt;/i&gt; string format is &lt;tt&gt;XXXX&lt;/tt&gt; where &lt;tt&gt;X&lt;/tt&gt; is a hexadecimal digit. An empty string will match any value.</source>
        <translation>定義供應商 ID 篩選器。 &lt;i&gt;完全相符&lt;/i&gt; 字串格式為 &lt;tt&gt;XXXX&lt;/tt&gt; 其中 &lt;tt&gt;X&lt;/tt&gt; 是十六進位數字。 空字串將符合任何值。</translation>
    </message>
    <message>
        <source>&amp;Product ID:</source>
        <translation>產品 ID (&amp;P):</translation>
    </message>
    <message>
        <source>Defines the product ID filter. The &lt;i&gt;exact match&lt;/i&gt; string format is &lt;tt&gt;XXXX&lt;/tt&gt; where &lt;tt&gt;X&lt;/tt&gt; is a hexadecimal digit. An empty string will match any value.</source>
        <translation>定義產品 ID 篩選器。 &lt;i&gt;完全相符&lt;/i&gt; 字串格式為 &lt;tt&gt;XXXX&lt;/tt&gt; 其中 &lt;tt&gt;X&lt;/tt&gt; 是十六進位數字。 空字串將符合任何值。</translation>
    </message>
    <message>
        <source>&amp;Revision:</source>
        <translation>修訂(&amp;R):</translation>
    </message>
    <message>
        <source>Defines the revision number filter. The &lt;i&gt;exact match&lt;/i&gt; string format is &lt;tt&gt;IIFF&lt;/tt&gt; where &lt;tt&gt;I&lt;/tt&gt; is a decimal digit of the integer part and &lt;tt&gt;F&lt;/tt&gt; is a decimal digit of the fractional part. An empty string will match any value.</source>
        <translation>定義修訂號篩選器。&lt;i&gt;完全相符&lt;/i&gt; 字串格式為 &lt;tt&gt;IIFF&lt;/tt&gt; 其中 &lt;tt&gt;I&lt;/tt&gt; 是十進位數字的整數部分 &lt;tt&gt;F&lt;/tt&gt; 是十進位數字的小數部分。 空字串將符合任何值。</translation>
    </message>
    <message>
        <source>&amp;Manufacturer:</source>
        <translation>製造商(&amp;M):</translation>
    </message>
    <message>
        <source>Defines the manufacturer filter as an &lt;i&gt;exact match&lt;/i&gt; string. An empty string will match any value.</source>
        <translation>定義製造商篩選器為 &lt;i&gt;完全相符&lt;/i&gt; 字串。 空字串將符合任何值。</translation>
    </message>
    <message>
        <source>Pro&amp;duct:</source>
        <translation>產品(&amp;D):</translation>
    </message>
    <message>
        <source>Defines the product name filter as an &lt;i&gt;exact match&lt;/i&gt; string. An empty string will match any value.</source>
        <translation>定義產品名稱篩選器為 &lt;i&gt;完全相符&lt;/i&gt; 字串。 空字串將符合任何值。</translation>
    </message>
    <message>
        <source>&amp;Serial No.:</source>
        <translation>序號(&amp;S):</translation>
    </message>
    <message>
        <source>Defines the serial number filter as an &lt;i&gt;exact match&lt;/i&gt; string. An empty string will match any value.</source>
        <translation>定義產序號篩選器為 &lt;i&gt;完全相符&lt;/i&gt; 字串。 空字串將符合任何值。</translation>
    </message>
    <message>
        <source>Por&amp;t:</source>
        <translation>連接埠(&amp;T):</translation>
    </message>
    <message>
        <source>Defines the host USB port filter as an &lt;i&gt;exact match&lt;/i&gt; string. An empty string will match any value.</source>
        <translation>定義主機 USB 連接埠篩選器為 &lt;i&gt;完全相符&lt;/i&gt; 字串。 空字串將符合任何值。</translation>
    </message>
    <message>
        <source>R&amp;emote:</source>
        <translation>遠端(&amp;E):</translation>
    </message>
    <message>
        <source>Defines whether this filter applies to USB devices attached locally to the host computer (&lt;i&gt;No&lt;/i&gt;), to a VRDP client&apos;s computer (&lt;i&gt;Yes&lt;/i&gt;), or both (&lt;i&gt;Any&lt;/i&gt;).</source>
        <translation>定義這個篩選器是否套用到本機附加的主機電腦之 USB 裝置 (&lt;i&gt;否&lt;/i&gt;) ，到 VRDP 用戶端的電腦 (&lt;i&gt;是&lt;/i&gt;)，或同時 (&lt;i&gt;任何&lt;/i&gt;)。</translation>
    </message>
    <message>
        <source>&amp;Action:</source>
        <translation>動作(&amp;A):</translation>
    </message>
    <message>
        <source>Defines an action performed by the host computer when a matching device is attached: give it up to the host OS (&lt;i&gt;Ignore&lt;/i&gt;) or grab it for later usage by virtual machines (&lt;i&gt;Hold&lt;/i&gt;).</source>
        <translation>定義當附加相符的裝置時主機電腦執行的動作: 在主機作業系統放棄 (&lt;i&gt;忽略&lt;/i&gt;) 或由虛擬機器擷取它作為之後使用 (&lt;i&gt;保留&lt;/i&gt;)。</translation>
    </message>
    <message>
        <source>USB Filter Details</source>
        <translation>USB 篩選器詳細資料</translation>
    </message>
</context>
</TS>
