# $Id: Makefile.kmk $
## @file
# Sub-Makefile for the VBox API testcases.
#

#
# Copyright (C) 2006-2007 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

SUB_DEPTH = ../../../..
include $(KBUILD_PATH)/subheader.kmk

#
# Target and globals (small mess)
#
ifndef VBOX_ONLY_SDK
 if defined(VBOX_WITH_TESTCASES) || "$(USERNAME)" == "dmik"
  PROGRAMS       += \
	tstAPI \
	$(if $(VBOX_WITH_XPCOM),tstVBoxAPILinux,tstVBoxAPIWin) \
	$(if $(VBOX_WITH_RESOURCE_USAGE_API),tstCollector,)
  PROGRAMS.linux += \
	$(if $(VBOX_WITH_USB),tstHostHardwareLinux,)
 endif # !VBOX_WITH_TESTCASES
endif # !VBOX_ONLY_SDK
if defined(VBOX_ONLY_SDK) || !defined(VBOX_WITH_XPCOM)
 INSTALLS += samplesMSCOM
endif
if defined(VBOX_ONLY_SDK) || defined(VBOX_WITH_XPCOM)
 INSTALLS += samplesXPCOM
endif


#
# The samples
#
samplesMSCOM_MODE = a+r,u+w
samplesMSCOM_INST = $(INST_SDK)bindings/mscom/samples/
samplesMSCOM_SOURCES = tstVBoxAPIWin.cpp

samplesXPCOM_MODE = a+r,u+w
samplesXPCOM_INST = $(INST_SDK)bindings/xpcom/samples/
samplesXPCOM_SOURCES = tstVBoxAPILinux.cpp makefile.tstVBoxAPILinux=>Makefile


#
# tstAPI
#
tstAPI_TEMPLATE = VBOXMAINCLIENTEXE
#tstAPI_INST = $(INST_SDK)bindings/gluecom/samples/
tstAPI_SOURCES  = tstAPI.cpp
ifeq ($(KBUILD_TARGET),win) ## @todo just add this to the template.
tstAPI_DEPS = $(VBOX_PATH_SDK)/bindings/mscom/include/VirtualBox.h
else
tstAPI_DEPS = $(VBOX_PATH_SDK)/bindings/xpcom/include/VirtualBox_XPCOM.h
endif
ifdef VBOX_WITH_RESOURCE_USAGE_API
tstAPI_DEFS += VBOX_WITH_RESOURCE_USAGE_API
endif


#
# tstVBoxAPILinux
#
# We only build the testcase here to make sure it builds.
# It comes with a custom makefile which should be tested as well!
#
tstVBoxAPILinux_TEMPLATE = VBOXR3EXE
tstVBoxAPILinux_SOURCES  = tstVBoxAPILinux.cpp
tstVBoxAPILinux_CXXFLAGS = -Wno-non-virtual-dtor -fshort-wchar
tstVBoxAPILinux_LDFLAGS.solaris += '$(VBOX_GCC_RPATH_OPT)$$(VBOX_ORIGIN)/../../..'
ifdef VBOX_WITH_XPCOM_NAMESPACE_CLEANUP
 tstVBoxAPILinux_DEFS += VBOX_WITH_XPCOM_NAMESPACE_CLEANUP
endif
tstVBoxAPILinux_INCS     = \
	$(VBOX_XPCOM_INCS) \
	$(VBOX_PATH_SDK)/bindings/xpcom/include
tstVBoxAPILinux_LIBPATH  = $(LIBPATH_XPCOM)
tstVBoxAPILinux_LIBS     = $(LIB_XPCOM) $(LIB_RUNTIME)
tstVBoxAPILinux_DEPS     = \
	$(VBOX_PATH_SDK)/bindings/xpcom/include/VirtualBox_XPCOM.h


#
# tstVBoxAPIWin
#
tstVBoxAPIWin_TEMPLATE = VBOXMAINCLIENTEXE
tstVBoxAPIWin_SOURCES  = \
	tstVBoxAPIWin.cpp \
	$(VBOX_PATH_SDK)/bindings/mscom/lib/VirtualBox_i.c
tstVBoxAPIWin_DEPS     = \
	$(VBOX_PATH_SDK)/bindings/mscom/include/VirtualBox.h


#
# tstCollector
#
tstCollector_TEMPLATE = VBOXMAINCLIENTEXE
tstCollector_SOURCES  = \
	tstCollector.cpp \
	../Performance.cpp
tstCollector_INCS     = ../include
tstCollector_LDFLAGS.darwin  += -lproc
tstCollector_LDFLAGS.solaris += -lkstat
tstCollector_LDFLAGS.win     += psapi.lib powrprof.lib



#
# tstHostHardwareLinux
#
tstHostHardwareLinux_TEMPLATE  = VBOXR3TSTEXE
tstHostHardwareLinux_SOURCES   = \
	tstHostHardwareLinux.cpp \
	../linux/HostHardwareLinux.cpp
tstHostHardwareLinux_INCS      = . ../include
tstHostHardwareLinux_DEFS      = \
	VBOX_TEST_USB_LINUX \
	TESTCASE \
	$(if $(VBOX_WITH_LINUX_COMPILER_H),VBOX_WITH_LINUX_COMPILER_H,) \
	$(if $(VBOX_WITH_DBUS),$(if $(VBOX_USB_WITH_DBUS),VBOX_USB_WITH_DBUS,),) \
	$(if $(VBOX_USB_WITH_SYSFS),VBOX_USB_WITH_SYSFS,) \
	$(if $(VBOX_USB_WITH_INOTIFY),VBOX_USB_WITH_INOTIFY,)
tstHostHardwareLinux_LIBS     += \
	$(PATH_OUT)/lib/USBLib.a


# generate rules.
include $(KBUILD_PATH)/subfooter.kmk

